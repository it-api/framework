<?php

namespace Barebone;

/**

* @author Frank
* @copyright Frank - YaYaBLa (tm)
* @version 1.0.0.0
* @name FileTransferAdapter
*
* 	@sample
* 	$adapter = new FileTransferAdapter();

$adapter->set_Destination('C:\temp');

if (!$adapter->receive()) {
$messages = $adapter->getMessages();
echo implode("\n", $messages);
}

Attention

This example is suitable only for demonstrating the basic API of FileTransfer.
You should never use this code listing in a production environment, because severe security issues may be introduced.
You should always use validators to increase security.
*/
class FileTransferAdapter {

    public $destination = 'NULL';
    public $messages = [];
    public $files;
    public $allowedMimeTypes = [];
    /**
    * Class creator
    * @return unknown_type
    */
    function __construct($destination) {
        if (isset($_FILES)) {
            $this->files = $_FILES;
        }
        $this->set_Destination($destination);
    }

    /**
    * this will save the files posted in any form
    * @return unknown_type
    */
    function receive() {

        //$POST_MAX_SIZE = ini_get('post_max_size');
        //$mul = substr($POST_MAX_SIZE, -1);
        //$mul = ($mul == 'M' ? 1048576 : ($mul == 'K' ? 1024 : ($mul == 'G' ? 1073741824 : 1)));
        ///if ($_SERVER['CONTENT_LENGTH'] > $mul*(int)$POST_MAX_SIZE && $POST_MAX_SIZE) $error = true;
        //if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) && $_SERVER['CONTENT_LENGTH'] > 0) {
        //	$this->messages[] = '1 van de bestanden is te groot om te uploaden. De upload is geannuleerd. De maximale grootte dat een bestand mag zijn is: '.$POST_MAX_SIZE;
        //	return false;
        //throw new Exception(sprintf('The server was unable to handle that much POST data (%s bytes) due to its current configuration', $_SERVER['CONTENT_LENGTH']));
        //}
        //check of de upload directory bestaat
        if (!file_exists($this->destination)) {
            $this->messages[] = 'Doel directory "' . $this->destination . '" bestaat niet.';
            return false;
        }
        //check of de directory schrijfbaar is
        if (!is_writable($this->destination)) {
            $this->messages[] = 'Doel directory "' . $this->destination . '" is niet beschrijfbaar.';
            return false;
        }

        foreach ($this->files as $file) {
            if (file_exists($file['tmp_name'])) {
                if (!move_uploaded_file((string) $file['tmp_name'], (string) $this->destination . $file['name'])) {
                    //$this->messages[] = 'Kan het bestand "' . $file['name'] . '"niet uploaden.';
                    //$this->messages[] = $file['error'];
                }
            } else {
                //$this->messages[] = sprintf('Temp file %s does not exist.', $file['tmp_name']). ' Heb je wel files geselecteerd?';
            }
        }
        //print_r($this->messages);
        if (count($this->messages) > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
    *
    * @return array of messages
    */
    function getMessages() {
        return $this->messages;
    }

    /**
    *
    * @param string $inputid
    * @return if inputid is given it will return that file
    * if no inputid is given it will return an array with all files
    */
    function getFileInfo($inputid = '') {
        if ($inputid == '') {
            $i = 0;
            foreach ($this->files as $file) {
                $files[$i] = array(
                    'full_path' => $this->get_Destination() . $file['name'],
                    'name' => $file['name'],
                    'type' => $file['type'],
                    'tmp_name' => $file['tmp_name'],
                    'error' => $file['error'],
                    'size' => $this->display_filesize($file['size'])
                );
                $i++;
            }

            return $files;

        } else {

            return $this->files[$inputid];

        }
    }

    /**
    * checks if a file is uploaded
    * @param $inputid
    * @return boolean
    */
    function isUploaded($inputid) {
        if (array_key_exists($inputid, $this->files)) {
            return $this->files[$inputid]['tmp_name'] !== '';
        } else {
            return false;
        }
    }

    /**
    * checks if the file is a valid file
    * @param unknown_type $inputid
    * @return boolean
    */
    function isValid($inputid) {
        return $this->files[$inputid]['error'] == 0;
    }

    /**
    *
    * @param $destination  //the destination directory for the upload
    * @return
    */
    function set_Destination($destination) {
        $this->destination = $destination;
    }

    /**
    *
    * @return the destination directory for the upload
    */
    function get_Destination() {
        return $this->destination;
    }

    /**
    * @return the mimetype for the name='foo' form element
    */
    function get_MimeType($inputid) {
        return $this->files[$inputid]['type'];
    }
    
    function allowMimeTypeImages(){
        $this->allowedMimeTypes[] = 'image/gif';
        $this->allowedMimeTypes[] = 'image/jpeg';
        $this->allowedMimeTypes[] = 'image/tiff';
        $this->allowedMimeTypes[] = 'image/bmp';
        $this->allowedMimeTypes[] = 'image/png';
        $this->allowedMimeTypes[] = 'text/plain';
    }
    
    function allowMimeTypeTextAndPDF(){
        $this->allowedMimeTypes[] = 'application/pdf';
        $this->allowedMimeTypes[] = 'text/plain';
    }
    
    function allowMimeTypeOffice(){
        $this->allowedMimeTypes[] = 'application/msword';
        $this->allowedMimeTypes[] = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
    }
    
    function isMimeTypeAllowed($MimeType){
        return in_array($MimeType, $this->allowedMimeTypes);
    }

    function display_filesize($filesize) {

        if (is_numeric($filesize)) {
            $decr = 1024;
            $step = 0;
            $prefix = array('Byte', 'KB', 'MB', 'GB', 'TB', 'PB');

            while (($filesize / $decr) > 0.9) {
                $filesize = $filesize / $decr;
                $step++;
            }

            return round($filesize, 2) . ' ' . $prefix[$step];

        } else {

            return 'NaN';

        }
    }

    /**
    * set the new upload size TODO: FIX THIS!!
    * @param $newsize
    */
    function set_UploadSize($newsize) {
        return ini_set('post_max_size', $newsize);
    }

}