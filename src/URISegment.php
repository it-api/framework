<?php

namespace Barebone;

class URISegment{
	
	private $name;
	private $value;
	
	/**
	 * Segment can be [a-z/0-9] OR [name:value]
	 * @param unknown $segment
	 */
	function __construct($segment){
		
		if(stristr($segment, ':')){
			
			$s = explode(':', $segment);
			$this->setName($s[0]);
			$this->setValue($s[1]);
			
		}else{
			
			$this->setValue($segment);
			
		}
	}
	
	function __toString(){
		return $this->getValue();
	}

    /**
     * name
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * name
     * @param string $name
     * @return URISegment{
     */
    public function setName($name){
        $this->name = $name;
        return $this;
    }

    /**
     * value
     * @return string
     */
    public function getValue(){
        return $this->value;
    }

    /**
     * value
     * @param string $value
     * @return URISegment{
     */
    public function setValue($value){
        $this->value = $value;
        return $this;
    }

}
