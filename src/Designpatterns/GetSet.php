<?php

namespace Barebone\Designpatterns;
use Barebone\Exception;
/**
 * This design is for a class with no properties and getter and setters
 * @author Frank
 * @example 
 * class your_class extends GetSet{}
 * $your_class = new your_class();
 * $your_class->Foo = 'bar';
 * $foo = $your_class->getFoo(); // bar
 * this normally would end with an warning but this way not
 */

class GetSet{
	
	public function __call($name, $arguments = []) {
	    
        if(substr($name, 0, 3) == 'get'){
            
	        // function call getId() OR getFoo()
            $propertie = substr($name, 3, strlen($name));
            
            if (!isset($this->$propertie)) {
                throw new Exception(sprintf('The propertie "%s" does not exist in the class "%s"', $propertie, get_class($this)));
            }
        
            return $this->$propertie;
            
	    }elseif(substr($name,0,3) == 'set'){
	        
	        // function call setId(1) OR setFoo('bar')
            $propertie = substr($name, 3, strlen($name));
            
            if (!isset($this->$propertie)) {
                throw new Exception(sprintf('The propertie "%s" does not exist in the class "%s"', $propertie, get_class($this)));
            }
            
            return $this->$propertie = $arguments[0];
	    }
   
    }
	
	public function __get($name){
		return $this->$name;
	}
	
	public function __set($name, $value){
		$this->$name = $value;
		return $this;
	}
	
}
