<?php

namespace Barebone;

class Encryption {

    static $cypher = 'aes-128-ctr';
    static $mode   = 'cfb';
    static $key    = '9m7b8r9sgfrtyTTEdfEStt0eg6t7h7';

    public static function encrypt($plaintext) {
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length(self::$cypher));
        // file deepcode ignore HardcodedNonCryptoSecret: <please specify a reason of ignoring this>
        $crypttext = openssl_encrypt($plaintext, self::$cypher, self::$key, OPENSSL_RAW_DATA, $iv);
        return $iv . $crypttext;
    }

    public static function decrypt($crypttext) {
        $ivsize = openssl_cipher_iv_length(self::$cypher);
        $iv = substr($crypttext, 0, $ivsize);
        $crypttext = substr($crypttext, $ivsize);
        $plaintext = openssl_decrypt($crypttext, self::$cypher, self::$key, OPENSSL_RAW_DATA, $iv);
        return $plaintext;
    }
}

// Encrypt text
//$encrypted_text = Encryption::encrypt('this text is unencrypted');

// Decrypt text
//$decrypted_text = Encryption::decrypt($encrypted_text);