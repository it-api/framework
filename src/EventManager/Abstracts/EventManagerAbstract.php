<?php

/*
  EventManagerAbstract.php
  UTF-8
  30-apr-2018 22:06:26
  barebone-php7
  Creator Frank Teklenburg
 */

namespace Barebone\EventManager\Abstracts;
use Barebone\EventManager\Interfaces\EventManagerInterface;
use Barebone\EventManager\EventListener;
use Barebone\EventManager\EventDispatcher;
/**
 * Description of EventManagerAbstract
 *
 * @author frank
 */
abstract class EventManagerAbstract implements EventManagerInterface {

	/**
	 * @var array $registered_events The registered events
	 */
	private $registered_events = array();

	/**
	 * @var array $handled_events The handled events during execution
	 */
	private $handled_events = array();

	/**
	 * @var array $unregistered_events The unregistered events during execution
	 */
	private $unregistered_events = array();

	/**
	 *
	 * @var string The last executed event
	 */
	private $last_event;

	/**
	 * @var array $observers The registered observers
	 */
	private $observers = array();

	function __construct() {
		
		$router = \Barebone\Router::getInstance();
		$this->listen($router->getRouteDotted(), array(new EventDispatcher(), 'dispatch'));
	}

	function __destruct() {
		//$router = \Barebone\Router::getInstance();
		//$eventListener = new EventListener('callv');
		
		//$authuser = new AuthUser;
		//$this->dispatch($router->getRouteDotted(), array($this));
		//debug($this);
	}

	/**
     * {@inheritDoc}
     */
	public function listen(string $event, $callback) {
		// create command object
		$command = new \Barebone\EventManager\EventCommand( $callback );
		// add the listener to the observers array
		$this->observers[$event][] = new EventListener($event, $command);
		// add the event to the registred events array
		$this->registered_events[] = $event;
		
		return $this;
	}

	/**
     * {@inheritDoc}
     */
	public function detach(string $event) {

		if (isset($this->observers[$event])) {
			unset($this->observers[$event]);
		}

		$this->unregistered_events[] = $event;

		return $this;
	}

	/**
     * {@inheritDoc}
     */
	public function dispatch(string $event, $param = null) {
		
		if (!isset($this->observers[$event])) {
			return false;
		}

		$this->handled_events[] = $event;
		$this->last_event = $event;
		
		foreach ($this->observers[$event] as $listener) {

			$eventdispatcher = new \Barebone\EventManager\EventDispatcher();
			$eventdispatcher->dispatch($listener, $param);

		}
	}
	/**
     * {@inheritDoc}
     */
	function load_from_disk(string $filename) {

		if (!file_exists($filename)) {
			throw new \Barebone\Exception(sprintf('File: "%s" not found!', $filename));
		}

		$json = file_get_contents($filename);
		$object = json_decode($json);
		foreach ($object->listeners as $listener) {
			$this->listen($listener->event, $listener->callback);
		}
	}

}
