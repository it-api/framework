<?php

/*
  EventListenerAbstract.php
  UTF-8
  30-apr-2018 22:19:06
  barebone-php7
  Creator Frank Teklenburg
 */

namespace Barebone\EventManager\Abstracts;
use Barebone\EventManager\Interfaces\EventListenerInterface;
/**
 * Description of EventListenerAbstract
 *
 * @author frank
 */
abstract class EventListenerAbstract implements EventListenerInterface {
	
	private $event;
	/**
	 *
	 * @var \Barebone\EventManager\EventCommand
	 */
    private $command;
	/**
	 * 
	 * @param string $event
	 * @param mixed $command
	 */
    public function __construct(string $event, $command) {
        $this->event = $event;
        $this->command = $command;
    }
	/**
     * {@inheritDoc}
     */
    function getEvent() : string{
        return $this->event;
    }
	/**
     * {@inheritDoc}
     */
    function getCommand(){
        return $this->command;
    }
}
