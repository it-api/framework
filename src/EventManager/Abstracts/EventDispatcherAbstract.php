<?php

/*
  EventDispatcherAbstract.php
  UTF-8
  30-apr-2018 22:27:39
  barebone-php7
  Creator Frank Teklenburg
 */

namespace Barebone\EventManager\Abstracts;
use Barebone\EventManager\Interfaces\EventDispatcherInterface;
/**
 * Description of EventDispatcherAbstract
 *
 * @author frank
 */
abstract class EventDispatcherAbstract implements EventDispatcherInterface{
	
	function __construct() {
		
	}
	
	function dispatch(\Barebone\EventManager\EventListener $listener, $param){
		$listener->getCommand()->execute( $param );
	}
}
