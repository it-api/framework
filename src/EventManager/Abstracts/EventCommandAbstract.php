<?php

/*
  EventCommandAbstract.php
  UTF-8
  30-apr-2018 22:43:11
  barebone-php7
  Creator Frank Teklenburg
 */

namespace Barebone\EventManager\Abstracts;
use Barebone\EventManager\Interfaces\EventCommandInterface;
/**
 * Description of EventCommandAbstract
 *
 * @author frank
 */
abstract class EventCommandAbstract implements EventCommandInterface {
	
	public function execute($params = null) {
		
		$class = $this->getClass();
		$method = $this->getMethod();
		
		if(!is_null($class)){
			
			if (method_exists($class, $method)) {
				
				call_user_func([$class, $method], $params);
				
			}else{
				
				throw new \Barebone\Exception(sprintf('Method %s does not exist.', $method));
			}	
			
		}else{
			
			if(function_exists($method)){
				call_user_func($method, $params);
			}else{
				throw new \Barebone\Exception(sprintf('Function %s does not exist.', $method));
			}
			
		}
		
		
	}

}
