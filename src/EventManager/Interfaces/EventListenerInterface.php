<?php

/*
  EventListenerInterface.php
  UTF-8
  30-apr-2018 22:18:53
  barebone-php7
  Creator Frank Teklenburg
 */

namespace Barebone\EventManager\Interfaces;

/**
 *
 * @author frank
 */
interface EventListenerInterface {
	/**
	 * Get the name of the event
	 */
	function getEvent() : string;
	/**
	 * Get the command of the event
	 * @return \Barebone\EventManager\Abstracts\EventCommandAbstract 
	 */
	function getCommand();
	
}
