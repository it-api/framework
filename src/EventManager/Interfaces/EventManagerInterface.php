<?php

/*
  EventManagerInterface.php
  UTF-8
  30-apr-2018 22:11:53
  barebone-php7
  Creator Frank Teklenburg
 */

namespace Barebone\EventManager\Interfaces;

/**
 *
 * @author frank
 * 
* create event listener
* @usage use a listener and dispatch to a method in a class
* $controller->events_manager->listen('switch_service_switch', array(new Switch_service(), 'SwitchClient'));
* create a workflow
* $controller->events_manager->listen('register_user', 'UserSignUp');
* $controller->events_manager->listen('register_user', 'UserCreateAccount');
* $controller->events_manager->listen('register_user', 'UserSendWelcomeMail');
* $controller->events_manager->listen('register_user', 'IncreaseStatsUsers');
* 
 */
interface EventManagerInterface {
	
	/**
	 * Listen for an event
	 * @param string $event
	 * @param mixed array|string $callback what happens when we have the event
	 */
	function listen(string $event, $callback);
	/**
	 * Detach a listener
	 * @param string $event
	 */
	function detach(string $event);
	/**
	 * Dispatch the event
	 * @param string $event
	 * @param mixed $param
	 */
	function dispatch(string $event, $param);
	/**
	 * Load the listeners from a json file from disk
	 * @param string $filename
	 * @throws \Barebone\Exception
	 */
	function load_from_disk(string $filename);
	
}
