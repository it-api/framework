<?php

/*
  EventCommandInterface.php
  UTF-8
  30-apr-2018 22:42:46
  barebone-php7
  Creator Frank Teklenburg
 */

namespace Barebone\EventManager\Interfaces;

/**
 *
 * @author frank
 */
interface EventCommandInterface {
	
	function execute();
	function getClass();
	function getMethod();
	function setClass($class);
	function setMethod($method);
	
}
