<?php

/*
  EventDispatcherInterface.php
  UTF-8
  30-apr-2018 22:28:01
  barebone-php7
  Creator Frank Teklenburg
 */

namespace Barebone\EventManager\Interfaces;

/**
 *
 * @author frank
 */
interface EventDispatcherInterface {
	function dispatch(\Barebone\EventManager\EventListener $listener, $param);
}
