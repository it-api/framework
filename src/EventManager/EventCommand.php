<?php

/*
  EventCommand.php
  UTF-8
  30-apr-2018 22:44:12
  barebone-php7
  Creator Frank Teklenburg
 */

namespace Barebone\EventManager;
use Barebone\EventManager\Abstracts\EventCommandAbstract;
/**
 * Description of EventCommand
 *
 * @author frank
 */
class EventCommand extends EventCommandAbstract{
	
	private $method = null;
	private $class = null;
	
	public function __construct($callback) {
		
		if(is_array($callback)){
			if(is_object($callback[0])){
				$this->setClass($callback[0]);
				$this->setMethod($callback[1]);
			}
		}elseif(is_string($callback)){
			
			$this->setMethod($callback);
		}
		
	}
	
	public function getMethod() {
		return $this->method;
	}

	public function getClass() {
		return $this->class;
	}

	public function setMethod($method) {
		$this->method = $method;
		return $this;
	}

	public function setClass($class) {
		$this->class = $class;
		return $this;
	}

}
