<?php

namespace Barebone\ActionTypes\Interfaces;

interface IAction {
    
    function __construct(string $method);
    function __toString();
    function getMethod(): string;
    function setMethod(string $method): void;
    function canrun(string $classname);
    function run(string $classname, array $params = []);
    
}
