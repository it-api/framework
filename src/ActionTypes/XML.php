<?php

namespace Barebone\ActionTypes;

use Barebone\ActionTypes\Abstracts\AbstractAction;
use Barebone\ActionTypes\Interfaces\IAction;
use Barebone\Classbuilder\ClassLoader;
/**
 * This class will run the action type XML in the class
 */
class XML extends AbstractAction implements IAction {
    
    function __construct(string $method) {
        $this->setMethod( $method . 'XML' );
    }

    function __toString() {
        return $this->getMethod();
    }

    function run(string $classname, array $params = []) {
        //set header for xml
        header('Content-Type: application/xml');
        
        $classLoader = new ClassLoader();
        // create a class by its classname
        $class = $classLoader->loadClass($classname);
        // disable auto rendering
        $class->auto_render = false;
        // and execute the action in the class
        $classLoader->loadClassMethod($class, $this->method, $params);

        // get data from view properties
        $data = $class->view->properties;
        // creating object of SimpleXMLElement
        $xml_data = new \SimpleXMLElement('<?xml version="1.0"?><data></data>');
        // function call to convert array to xml
        \Barebone\Functions::array_to_xml($data, $xml_data);
        //saving generated xml file; 
        echo $xml_data->asXML();
    }
}
