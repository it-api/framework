<?php

namespace Barebone\ActionTypes;

use Barebone\ActionTypes\Abstracts\AbstractAction;
use Barebone\ActionTypes\Interfaces\IAction;
use Barebone\Classbuilder\ClassLoader;
/**
 * This class will run the action type JSON in the class
 */
class JSON extends AbstractAction implements IAction {
    
    function __construct(string $method) {
        $this->setMethod( $method . 'JSON' );
    }

    function __toString() {
        return $this->getMethod();
    }

    function run(string $classname, array $params = []) {
        header('Content-Type: application/json');
        
        $classLoader = new ClassLoader();
        // create a class by its classname
        $class = $classLoader->loadClass($classname);
        // disable auto rendering
        $class->auto_render = false;
        // and execute the action in the class
        $classLoader->loadClassMethod($class, $this->method, $params);

        // render the view as json if there are any properties else silent
        if (count($class->view->properties) > 0) {
            echo json_encode($class->view->properties);
        }
    }
}
