<?php

namespace Barebone\ActionTypes;

use Barebone\ActionTypes\Abstracts\AbstractAction;
use Barebone\ActionTypes\Interfaces\IAction;
use Barebone\Classbuilder\ClassLoader;
/**
 * This class will run the action type AJAX in the class
 */
class AJAX extends AbstractAction implements IAction {
    
    function __construct(string $method) {
        $this->setMethod( $method . 'AJAX' );
    }

    function __toString() {
        return $this->getMethod();
    }

    function run(string $classname, array $params = []) {

        try {
            
            $classLoader = new ClassLoader();
            // create a class by its classname
            $class = $classLoader->loadClass($classname);
            // disable auto rendering
            $class->auto_render = false;
            // and execute the action in the class
            $classLoader->loadClassMethod($class, $this->method, $params);
            
        } catch (\Exception $e) {
            
            throw new \Barebone\Exception($e->getMessage());
        }
    }
}
