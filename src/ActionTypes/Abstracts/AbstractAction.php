<?php

namespace Barebone\ActionTypes\Abstracts;

abstract class AbstractAction {
    
    protected string $method;
    
    public function setMethod(string $method): void{
        $this->method = $method;
    }
    
    public function getMethod(): string{
        return $this->method;
    }
    
    function canrun(string $classname) {
        return method_exists((string) $classname, (string) $this->method);
    }
    
}
