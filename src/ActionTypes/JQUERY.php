<?php

namespace Barebone\ActionTypes;

use Barebone\ActionTypes\Abstracts\AbstractAction;
use Barebone\ActionTypes\Interfaces\IAction;
use Barebone\Classbuilder\ClassLoader;
/**
 * This class will run the action type JQUERY in the class
 */
class JQUERY extends AbstractAction implements IAction {
    
    function __construct(string $method) {
        $this->setMethod( $method . 'JQUERY' );
    }

    function __toString() {
        return $this->getMethod();
    }

    function run($classname, $params = []) {
        /* special ajax here */
        try {
            $classLoader = new ClassLoader();
            // create a class by its classname
            $class = $classLoader->loadClass($classname);
            // disable auto rendering
            $class->auto_render = false;
            // initialize jquery response object
            \Barebone\jQuery::init();
            // and execute the action in the class
            $classLoader->loadClassMethod($class, $this->method, $params);
            // now show the response
            \Barebone\jQuery::getResponse();

        } catch (\Exception $e) {

            throw new \Barebone\Exception($e->getMessage());
        }
    }
}
