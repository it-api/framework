<?php

namespace Barebone;

    /**
    
    class cls_doeiets{
        function doeiets(){
            echo '<p>Done executing class method</p>';
        }    
    }
    function doeiets(){
        echo '<p>Done</p>';
    }
    
    $a = 1;
    $b = 2;
    $Conditie = new Conditie();
    $Conditie->Als($a === $b)->Doe( 'doeiets' ); // Als is FALSE
    $Conditie->Als($a !== $b)->Doe( 'doeiets' ); // Done
    $Conditie->Als($a === $b)->Tenzij($a !== $b)->Doe( 'doeiets' ); // Done
    $Conditie->Als($a !== $b)->Tenzij($b !== $a)->Doe( 'doeiets' ); // Done
    $Conditie->Als($a === $b)->Tenzij(10 === 1)->Tenzij($a !== $b)->Doe( new cls_doeiets(), 'doeiets' );
*/

class Conditie {
    
    private $als = false;
    private $tenzij = false;
    /**
     * @var $args a comparison $a === $b
     * @return Barebone\Conditie
     */
    function Als(bool $args) : Conditie {
        //var_dump($args);
        if($this->als === false){
            $this->als = $args;
        }
        return $this;
    }
    function En(bool $args){
        //var_dump($args);
        //if($this->als === false){
            $this->als = $args;
        //}
        return $this;
    }
    function Of(bool $args){
        //var_dump($args);
        //if($this->als === false){
            $this->als = $args;
        //}
        return $this;
    }
    /**
     * @var $args a comparison $a === $b
     * @return Barebone\Conditie
     */
    function Tenzij(bool $args) : Conditie {
        //var_dump($args);
        if($this->tenzij === false){
            $this->tenzij = $args;
        }
        return $this;
    }
    
    function Anders(){
        $this->Execute( func_get_args() );
    }
    /**
     * Execute a method on a class or function
     */
    function Doe(){
        if($this->als === true && $this->tenzij === true){
            //echo '<p>Tenzij is TRUE, Als is TRUE';
            $this->Execute( func_get_args() );
        }elseif($this->als === false && $this->tenzij === true){
            //echo '<p>Tenzij is TRUE, Als is FALSE';
            $this->Execute( func_get_args() );
        }elseif($this->als === true){
            //echo '<p>Als is TRUE';
            $this->Execute( func_get_args() );
        }elseif($this->als === false){
            //echo '<p>Als is FALSE';
        }
        return $this;
    }
    /**
     * Execute call_user_func
     * @var $args mixed array|object|anomious function
     */
    private function Execute($args = []){
        // anomious function is passed in
        if(is_callable($args[0])){
            $args[0]();
        // an object is passed in with a method to execute
        }elseif(is_object($args[0])){
            call_user_func([$args[0], $args[1]]);
        // just call the function
        }else{
            $args[0]();
        }
    }
}
