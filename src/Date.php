<?php

namespace Barebone;

/**
 * Description of Date
 * VLBBackend2017 
 * UTF-8
 *
 * @author Frank
 * @filesource Date.php
 * @since 6-mrt-2017 16:05:56
 * 
 */
class Date {
    
    public static function isTodayWeekend() {
        $currentDate = new \DateTime("now", new \DateTimeZone("Europe/Amsterdam"));
        return $currentDate->format('N') >= 6;
    }
    
    public static function isDateInWeekend(?string $date) {
        $inputDate = \DateTime::createFromFormat("d-m-Y", $date, new \DateTimeZone("Europe/Amsterdam"));
        return $inputDate->format('N') >= 6;
    }
    
    /**
     * This function reformats the date comming from the database
     * @param string $date A date in the format 2017-12-31
     * If the date is empty it returns 0000-00-00
     * @return string A date in format 31-21-2017
     */
    public static function date_from_mysql(?string $date): string{
        
        if(is_null($date) || $date === '' || $date === '0000-00-00'){
			return '00-00-0000';
		}
	
		$datum = date('d-m-Y', strtotime($date));
		return $datum;
    }
    
    /**
     * This function reformats the date going into the database
     * @param string $date A date in the format 31-21-2017
     * If the date is empty it returns 0000-00-00
     * @return string A date in format 2017-12-31
     */
    public static function date_to_mysql(?string $date): string{
        
        if(is_null($date) || $date === '' || $date === '0000-00-00'){
			return '0000-00-00';
		}
		$datum = date('Y-m-d', strtotime($date));
		return $datum;
    }

    /**
     * This function reformats the datetime comming from the database
     * @param string $datetime A date in the format 2017-12-31 12:30:15
     * If the date is empty it returns 00-00-0000 00:00:00
     * @return string A date in format 31-21-2017 12:30:15
     */
    public static function datetime_from_mysql(?string $datetime): string{
        
        if(is_null($datetime) || $datetime === '' || $datetime === '0000-00-00 00:00:00'){
			return '00-00-0000 00:00:00';
		}
		
		$datumtijd = date('d-m-Y H:i:s', strtotime($datetime));

        return $datumtijd;	
    }
    
    /**
     * @var $date 'Y-m-d'
     */
    public static function date_long_dutch(string $date): string{
        $naamdag = self::translate_english_day_to_dutch_full( date('D', ($date)) );
        $nummerdag = date('d', ($date));
        $maand = self::translate_english_month_to_dutch_full( date('M', ($date)) );
        $jaar = date('Y', ($date));
        $datum = $naamdag . ' ' .$nummerdag . ' ' . $maand . ' ' . $jaar;
        return $datum;
    }
    
    public static function translate_english_month_to_dutch_short(string $month){
        
        $months = [];
        $months['Jan'] = 'jan';
        $months['Feb'] = 'feb';
        $months['Mar'] = 'mrt';
        $months['Apr'] = 'apr';
        $months['May'] = 'mei';
        $months['Jun'] = 'jun';
        $months['Jul'] = 'jul';
        $months['Aug'] = 'aug';
        $months['Sep'] = 'sep';
        $months['Oct'] = 'okt';
        $months['Nov'] = 'nov';
        $months['Dec'] = 'dec';
        
        return $months[$month];
    }
    
    public static function translate_english_month_to_dutch_full(string $month){
        
        $months = [];
        $months['Jan'] = 'januari';
        $months['Feb'] = 'februari';
        $months['Mar'] = 'maart';
        $months['Apr'] = 'april';
        $months['May'] = 'mei';
        $months['Jun'] = 'juni';
        $months['Jul'] = 'juli';
        $months['Aug'] = 'augustus';
        $months['Sep'] = 'september';
        $months['Oct'] = 'oktober';
        $months['Nov'] = 'november';
        $months['Dec'] = 'december';
        
        return $months[$month];
    }
    
    public static function translate_english_day_to_dutch_short(string $day){
        
        $days = [];
        $days['Mon'] = 'Ma';
        $days['Tue'] = 'Di';
        $days['Wed'] = 'Wo';
        $days['Thu'] = 'Do';
        $days['Fri'] = 'Vr';
        $days['Sat'] = 'Za';
        $days['Sun'] = 'Zo';
        
        return $days[$day];
    }
    
    public static function translate_english_day_to_dutch_full(string $day){
        
        $days = [];
        $days['Mon'] = 'Maandag';
        $days['Tue'] = 'Dinsdag';
        $days['Wed'] = 'Woensdag';
        $days['Thu'] = 'Donderdag';
        $days['Fri'] = 'Vrijdag';
        $days['Sat'] = 'Zaterdag';
        $days['Sun'] = 'Zondag';
        
        return $days[$day];
    }
}
