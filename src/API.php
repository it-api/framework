<?php

namespace Barebone;
error_reporting(E_ALL);
ini_set('display_errors',1);
ini_set('display_startup_errors',1);

use Barebone\API\Interfaces\InterfaceAPI;
use Barebone\API\RouteAPI;

class API implements InterfaceAPI{
	
	private $url = '';
	private $scheme = 'http';
	private $host = 'localhost';
	private $port = 80;
	private $endpoint = '/api/';
	private $routes = array();
	
	function __construct(){
	
	}	

	/**
	 * url
	 * @return string
	 */
	public function getUrl(){
		return $this->url;
	}
	
	/**
	 * url
	 * @param string $url
	 * @return API
	 */
	public function setUrl($url){
		$this->url = $url;
		return $this;
	}
	
	/**
	 * scheme
	 * @return string
	 */
	public function getScheme(){
		return $this->scheme;
	}
	
	/**
	 * scheme
	 * @param string $scheme
	 * @return API
	 */
	public function setScheme($scheme){
		$this->scheme = $scheme;
		return $this;
	}
	
	/**
	 * host
	 * @return string
	 */
	public function getHost(){
		return $this->host;
	}
	
	/**
	 * host
	 * @param string $host
	 * @return API
	 */
	public function setHost($host){
		$this->host = $host;
		return $this;
	}
	
	/**
	 * port
	 * @return integer
	 */
	public function getPort(){
		return $this->port;
	}
	
	/**
	 * port
	 * @param integer $port
	 * @return API
	 */
	public function setPort($port){
		$this->port = $port;
		return $this;
	}
	
	/**
	 * endpoint
	 * @return string
	 */
	public function getEndpoint(){
		return $this->endpoint;
	}
	
	/**
	 * endpoint
	 * @param string $endpoint
	 * @return API
	 */
	public function setEndpoint($endpoint){
		$this->endpoint = $endpoint;
		$this->url = $this->getScheme() . '://' . $this->getHost(). ':' . $this->getPort() . $this->getEndpoint();
		return $this;
	}
	
	/**
	 * routes
	 * @return array
	 */
	public function getRoutes(){
		return $this->routes;
	}
	
	/**
	 * routes
	 * @param array $routes
	 * @return API
	 */
	public function setRoutes($routes){
		$this->routes = $routes;
		return $this;
	}
	
	public function addRoute($method, $path, $comment, $callback){
		
		if(isset($this->routes[$path])) throw new Exception(sprintf('the route "%s" is allready taken.', $path));
		
		$route = new RouteApi();
		$route->setMethod($method);
		$route->setPath($path);
		$route->setComment($comment);
		$route->setCallback($callback);
		$this->routes[$path] = $route;
		
	}
	
	public function listen(){
		
		if(isset($this->routes)){
			
		}
		
	}
	
}
/*
class myAPI extends API{
	
	function __construct(){
		parent::__construct();
	}

}

$api = new myAPI();
$api->setHost('localhost');
$api->setPort(80);
$api->setEndpoint('/api/vlb/v1/');
$api->addRoute('GET', 'users/', 'Return a list of users', 'callbck');
$api->listen();

print_r($api);
*/

