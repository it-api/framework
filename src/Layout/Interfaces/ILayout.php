<?php

namespace Barebone\Layout\Interfaces;

use Barebone\View;

interface ILayout{
    
    function __construct(View $view);
    function set_layout(string $layout): void;
    function get_html(): string;
    function render(): void;
    function title(string $title = '');
    
}
