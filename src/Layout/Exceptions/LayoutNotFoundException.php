<?php

namespace Barebone\Layout\Exceptions;

use Barebone\Router;
use Barebone\Config;

class LayoutNotFoundException extends \Exception {
	
	function __construct($message, $code=0){
	    
	    $router = Router::getInstance();
	    $config = Config::getInstance();
		if($config->getValue('application', 'production') === "false"){
    	    echo '<div class="container-fluid">
                <div class="row-fluid">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
            	    echo '<h1>Layout not found</h1>';
            	    echo '<p>You are trying to render a layout that does not exist.';
            	    echo "<p><img src=/images/tick.png> Application: {$router->application}</p>";
            	    echo "<p><img src=/images/tick.png> Controller: {$router->controller}</p>";
            	    echo "<p><img src=/images/tick.png> Action: {$router->action}</p>";
            	    echo "<p><img src=/images/tick.png> View: {$router->action}.phtml</p>";
            	    echo "<pre>{$message}</pre></div>
                </div>
            </div>";
		}
	}
}