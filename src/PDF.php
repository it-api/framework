<?php

namespace Barebone;

// reference the Dompdf namespace
use Dompdf\Dompdf;

class PDF {

	/**
	 *
	 * @var Dompdf
	 */
	public $dompdf = null;
	private $filename;

	function __construct() {

		// instantiate and use the dompdf class
		$this->dompdf = new Dompdf();
	}

	function setFilename($filename) {
		$this->filename = $filename;
		return $this;
	}

	function getFilename() {
		return $this->filename;
	}

}
