<?php

namespace Barebone;

use Barebone\Component;
use Barebone\View;
use Barebone\AssetsManager;
use Barebone\Layout\Interfaces\ILayout;

/**
 * Layouts in MVC:
 *
 *    In Model-View-Controller (MVC) architecture, layouts play a crucial role in 
 *    maintaining a consistent user experience across your application. 
 *    They serve as templates or wrappers that encapsulate the main structure of your application's web pages.
 *    
 *    Key benefits of using layouts:
 *    
 *        Consistency: Layouts define the common visual elements like headers, footers, navigation bars, and sidebars. 
 *            This ensures a unified look and feel for all your views, enhancing user experience and brand recognition.
 *        Code Reusability: By placing shared elements in a single layout, 
 *            you can significantly reduce code duplication across individual views. 
 *            This simplifies maintenance and promotes easier updates to the application's overall design.
 *        Separation of Concerns: Layouts separate the presentation layer (visual structure) 
 *            from the logic layer (view content). 
 *            This promotes cleaner code organization and improved maintainability.
 *    
 *    Common elements in layouts:
 *    
 *    Layouts typically contain:
 *   
 *       HTML header tags (<head>, <body>)
 *       Navigation elements (links, menus)
 *        Branding elements (logos, headers)
 *        Content placeholders where individual views insert their specific content
 *    
 *    How layouts work:
 *    
 *        Define the layout: 
 *            Create a folder in public/layouts add a file layout.phtml
 *            containing the shared elements and placeholders for view content.
 *        Individual views: 
 *           Each view inherits or includes the layout and provides content 
 *            for the specified placeholders.
 *        Rendering: 
 *            The framework merges the layout with the view's content, 
 *            generating the final HTML response sent to the user's browser.
 *    
 *    Overall, layouts form an essential aspect of the MVC design pattern, 
 *       ensuring a cohesive and maintainable web application experience.
 *    
 *    But this can be done with a couple of mouseclicks in the GUI
 * 
 * @author Frank
 *
 */
class Layout extends Component implements ILayout{

    /**
     * the name of a layout
     * @var string 
     */
    public string $filename = 'layout.phtml';
    /**
     * The folder name of the layout
     * @var string $layout
     */
    private string $layout;
    /**
     *
     * @var Barebone\View $view
     */
    private View $view;
    /**
     * The pagetitle. You can set title from controller this is used in the layout.
     * @var string $title 
     */
    public string $title = 'Untitled';
    /**
     * The assets manager is responsible for the assets 
     * like metatags, javascript and stylesheet you assign in your controller.
     * @var AssetsManager $assets 
     */
    public AssetsManager $assets;
    /**
     * If this is enabled it will show an admin panle to edit controller, view or layout
     * @var bool $admin_helper_enabled 
     */
    private bool $admin_helper_enabled = false;
    /**
     * Nonce can be used by CSP 
     * use in inline attibute on script and stylesheet tags with nonce="<?=$this->nonce;?>"
     * In either views or layouts. Search code for nonce and you will find examples :)
     * @var string $nonce
     */
    public string $nonce = '';
    /**
     * Autoload the js file of the application
     * @var bool $js_autoload
     */
    public bool $js_autoload = true;
    /**
     * Autoload the css file of the application
     * @var bool $css_autoload
     */
    public bool $css_autoload = true;
    
    /**
     * Layout constructer takes one argument the view that it needs to render
     * @param \Barebone\View $view
     */
    function __construct(View $view) {

        $config = Config::getInstance();
        // for use in CSP
        $this->nonce = base64_encode(openssl_random_pseudo_bytes(16));
        // local view assignment
        $this->view = $view;
        // assign nonce to view
        $this->view->nonce = $this->nonce;
        // the name of the default layout as set in the configuration
        $this->layout = $config->getValue('layout', 'default_layout');
        // set the filename to the default layout
        $this->filename = FULL_PUBLIC_PATH . $config->getValue('layout', 'layouts_path') . $this->layout . '/' . $this->filename;
        // the assets manager takes care of javascripts, css, meta tags data
        $this->assets = new AssetsManager();
        
    }
    
    /**
     * Prevent autoloading /js/applications/{app}.js
     * @return Layout
     */
    function disable_js_autoload(): Layout{
        $this->js_autoload = false;
        return $this;
    }
    
    /**
     * Prevent autoloading /css/applications/{app}.css
     * @return Layout
     */
    function disable_css_autoload(): Layout{
        $this->css_autoload = false;
        return $this;
    }
    
    /**
     * Enable admin helper
     * @return void
     */
    function enable_admin_helper(): Layout {
        $this->admin_helper_enabled = true;
        return $this;
    }
    
    
    /**
     * Disable admin helper
     * @return void
     */
    function disable_admin_helper(): void {
        $this->admin_helper_enabled = false;
    }
    
    /**
     * With this function you can set the layout that the controller will use
     * @param string $layout the name of the layout
     * @return void
     */
    function set_layout(string $layout): void {
        $this->layout = $layout;
        if(!defined('LAYOUT_PATH')){
            define('LAYOUT_PATH', "/layouts/$layout/");
        }
    }
    
    /**
     * returns the html of the layout with the view in it
     * @return string
     */
    function get_html(): string {
        
        // start output buffer
        ob_start();
        // render the layout file, this will render the view in it
        $this->render();
        // get the output and clean the buffer
        $output = ob_get_clean();
        // return the output as html
        return $output;
        
    }
    
    /**
     * Renders the layout. 
     * This method sets the layout we render if it does not exists it will throw an Exception
     * It layout exists then add {application}.css and {application}.js to your layout
     * If admin panel is enabled in the controller it will add admin panel
     * It also adds divs used by dialogs and other ajax functions
     * When this reders the layout the view in it is automaticly render too
     * @throws Exception
     */
    function render(): void {
        
        // get the router object for directions
        $router = Router::getInstance();
        
        // get the config object
        $config = Config::getInstance();
        
        // set the filename to the layout we are using this can be different from the constructor
        $this->filename = FULL_PUBLIC_PATH . $config->getValue('layout', 'layouts_path') . $this->layout . '/layout.phtml';
        
        // if the file does not exist throw an exception
        if(!file_exists($this->filename)){
            throw new \Barebone\Layout\Exceptions\LayoutNotFoundException(sprintf('The layout file %s does not exist.', $this->filename));
        }
        
        // if there is a application stylesheet then load it automagicly
        if (file_exists(FULL_PUBLIC_PATH . 'css/applications/' . $router->application . '.css') && $this->css_autoload === true) {
            if(!defined($router->application . '_css')){
                $this->add_stylesheet('/css/applications/' . $router->application . '.css?time='.filectime(FULL_PUBLIC_PATH . 'css/applications/' . $router->application . '.css'));
                define($router->application . '_css', true);
            }
            
        }
        // if there is a application javascript then load it automagicly
        if (file_exists(FULL_PUBLIC_PATH . 'js/applications/' . $router->application . '.js') && $this->js_autoload === true) {
            if(!defined($router->application . '_js')){
                $this->add_javascript('/js/applications/' . $router->application . '.js?time='.filectime(FULL_PUBLIC_PATH . 'js/applications/' . $router->application . '.js'));
                define($router->application . '_js', true);
            }
        }
        
        if(isloggedin() && $this->admin_helper_enabled === true){
            $this->view->add_blokx('/../../barebone/blokx/admin-sidebar', ['view' => $this->view, 'layout' => $this]);
        }
        
        
        libxml_use_internal_errors(true);
        $doc = new \DOMDocument();
        $doc->loadHTMLFile( $this->filename );
        // append a div for #dialog-response and #page-contents
        if(is_null($doc->getElementById('dialog-response'))){
            $this->view->append("\n\n\t".'<div id="dialog-response"><!-- I am automagicly added by the Layout --></div>');
        }
        
        if(is_null($doc->getElementById('page-contents'))){
            $this->view->append("\n\t".'<div id="page-contents"><!-- I am automagicly added by the Layout --></div>');
        }
        // make the properties global
        extract($this->properties);
        
        // include the layout file
        include_once( $this->filename );
    }

    /**
     * Get and Set the title
     * @param string $title if ommited the value will be returned
     */
    function title(string $title = '') {        
        if ($title === ''){
            echo $this->title;
        }else{
            $this->title = $title;
        }
    }
    
    /**
     * Add a metatag to the assets manager thus to the layout
     * @param array $attributes
     * @return Layout
     */
    function add_meta_tag(array $attributes = []): Layout {
        $this->assets->add_meta_tag($attributes);
        return $this;
    }
    
    /**
     * Add a javascript to the assets manager thus to the layout
     * @param string $src
     * @param string $position [head,body] defaults to head
     * @param bool $defer defers the script (adds defer attribute)
     * when position is head the script tag is placed inside the <head> tag
     * when position is body the script tag is places just before end of body tag
     * @return Layout
     */
    function add_javascript(string $src, string $position = 'head', bool $defer = false, bool $async = false): Layout {
        $this->assets->add_javascript($src, $position, $defer, $async, $this->nonce);
        return $this;
    }
    
    /**
     * Add a stylesheet to the assets manager thus to the layout
     * @param string $href
     * @param string $position [head,body] defaults to head
     * @return Layout
     */
    function add_stylesheet(string $href, string $position = 'head'): Layout {
        $this->assets->add_stylesheet($href, $position, $this->nonce);
        return $this;
    }
    
    /**
     * Add a link tag
     * @param array $attributes
     * @return Layout
     */
    function add_link(array $attributes = []): Layout {
        $this->assets->add_link($attributes);
        return $this;
    }
    
    /**
     * This function is used in the layout to render the content of the view
     */
    function get_view_contents(): void {
        try{
            $this->view->render();
        } catch(\Barebone\View\Exceptions\ViewNotFoundException $e){}
    }
    
    /**
     * This function is used in the layout to render the assets that have been assigned in the controller to the assets manager
     */
    function get_assets(string $position = 'head'): void {
        $this->assets->render( $position );
    }

    /**
     * Create a new blokx
     * @param string $blokxname
     * @param mixed array|object $data
     * @return \Barebone\Blokx
     */
    function blokx(string $blokxname, array $data = []) {
        return new Blokx($blokxname, $data);
    }

}
