<?php

namespace Barebone;

/**
 * A Set is a sequence of unique values. 
 * This implementation uses the same hash table as Barebone\Map, 
 * where values are used as keys and the mapped value is ignored. 
 * Barebone 
 * UTF-8
 *
 * @author Frank Teklenburg <frank@it-api.nl>
 * @filesource Set.php
 * @since 16-jan-2017 0:29:59
 * 
 */
class Set {
    //put your code here
}
