<?php

namespace Barebone;

class Model {

    /**
     *
     * @var Database
     */
    public ?Database $db;

    /**
     * View
     */
    public ?View $view = null;

    /**
     * Layout
     */
    public ?Layout $layout = null;

    /**
     * Controller
     */
    public ?Controller $controller = null;
    
    function __construct(Controller $controller = null) {

        if ($controller instanceof Controller) {
            $this->setController($controller);
            $this->setView($controller->view);
            $this->setLayout($controller->layout);
            $this->setDb($controller->db);
        } else {
            $this->setDb(Registry::getService('db'));
        }

    }

    function setController(Controller $controller){
        $this->controller = $controller;
        return $this;
    }

    function getController(): Controller{
        return $this->controller;
    }

    function setDb(Database $db){
        $this->db = $db;
        return $this;
    }

    function getDb(): Database{
        return $this->db;
    }

    function setLayout(Layout $layout){
        $this->layout = $layout;
        return $this;
    }

    function getLayout(): Layout{
        return $this->layout;
    }

    function setView(View $view){
        $this->view = $view;
        return $this;
    }

    function getView(): View{
        return $this->view;
    }
}
