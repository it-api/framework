<?php

/*
  Creator Frank Teklenburg
 */

namespace Barebone\Readers\CSV;

/**
 * Description of CSVReader
 *
 * @author frank
 */
class CSVReader {
	/**
	 * The CSV header rows
	 * @var array
	 */
	private array $headers = [];
	private array $rows = [];

	public function __construct(\SplFileObject $file, string $delimiter = ',', string $enclosure = '"', string $escape = '\\') {
		$i = 0;
		while (!$file->eof()) {
			if($i === 0){
				$this->headers = $file->fgetcsv($delimiter, $enclosure, $escape);
			}else{
				$this->rows[] = $file->fgetcsv($delimiter, $enclosure, $escape);
			}
			$i++;
		}

	}
	/**
	 * Returns the rows in the CSV without the headers
	 * @return array
	 */
	function getRows():array {
		return $this->rows;
	}
	/**
	 * get the column headers
	 * @return type
	 */
	function getColumnHeaders():array {
		return $this->headers;
	}
	/**
	 *  to specify your own headers
	 * @param array $headers
	 * @return $this
	 */
	function setColumnHeaders($headers = []):self {
		$this->headers = $headers;
		return $this;
	}
	/**
	 * Get index of the column by its name
	 * @param string $name
	 * @return integer|null
	 */
	function getColumnIndexByName(string $name): ?int {
	    return array_keys($this->headers, $name)[0] ?? null;
	}
	/**
	 * Filter the rows on column value
	 * @param string $column name of the column
	 * @param string $value value in the column row
	 * @return integer|null
	 * @example
	 *  $file = new \SplFileObject('some-csv.csv');
        $csv = new Barebone\Readers\CSV\CSVReader($file);
        debug(
            $csv
            ->filterRowsOnColumn('Status','Afgekeurd')
            ->filterRowsOnColumn('Some-other-column', 'some-value')
        );
	 */
    function filterRowsOnColumn(string $column, string $value): self{
		// result array
        $result = [];
		// get the index of the column
        $index = $this->getColumnIndexByName($column);
		// if column does not exist return current state
		if(is_null($index)){
			return $this;
		}
		// go over each row
        foreach($this->getRows() as $columns){
			// if a certain column cotains a certain value
            if($columns[$index] === $value){
				// add row to result array
                $result[] = $columns;
            }
        }
        //replace rows with result
        $this->rows = $result;
		// return self for chaining filters
        return $this;
    }
}
