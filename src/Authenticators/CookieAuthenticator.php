<?php

namespace Barebone\Authenticators;
use Barebone\ACL;
use Barebone\ACL\ACL_User;
/**
 * Description of SessionAuthenticator
 * ComposerFW 
 * UTF-8
 *
 * @author Frank
 * @filesource SessionAuthenticator.php
 * @since 14-feb-2017 19:57:48
 * 
 */
class CookieAuthenticator extends IAuthenticator {
    
    /**
     *
     * @var ACL_User
     */
    private $user;
    /**
     * lifetime of the cookie
     * @var string any human readable date 
     */
    public $lifetime = '1 DAY';
    
    public function __construct() {
        $this->user = unserialize($_COOKIE['ACL_USER']);
    }

    
    public function get_auth_user(): ACL_User {
        return $this->user;
    }

    public function is_logged_in(): bool {
        return isset($this->user);
    }

    public function login(ACL_User $user): bool {
        $acl = new ACL();
        $authuser = $acl->valid_user($user->username, $user->password);
        if($authuser){            
            $this->user = $authuser;
            setcookie('ACL_USER', serialize($authuser), strtotime($this->lifetime));
            return true;
        }
        return false;
    }

    public function logout(): bool {        
        unset($_COOKIE['ACL_USER']);
        unset($this->user);
        return setcookie('ACL_USER', null, strtotime('YESTERDAY'));
    }

}
