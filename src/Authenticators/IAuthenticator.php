<?php

namespace Barebone\Authenticators;
use Barebone\ACL\ACL_User;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Frank
 */
abstract class IAuthenticator {
    
    function login(ACL_User $user):bool{}
    function logout():bool{}  
    function is_logged_in():bool{}
    function get_auth_user():ACL_User{}
    
}
