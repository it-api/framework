<?php

namespace Barebone\Authenticators;
use Barebone\ACL;
use Barebone\ACL\ACL_User;
/**
 * Description of SessionAuthenticator
 * ComposerFW 
 * UTF-8
 *
 * @author Frank
 * @filesource SessionAuthenticator.php
 * @since 14-feb-2017 19:57:48
 * 
 */
class SessionAuthenticator extends IAuthenticator {
    
    /**
     *
     * @var ACL_User
     */
    private $user;
    
    public function __construct() {
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
        $this->user = $_SESSION['ACL_USER'] ?? null;
    }

    
    public function get_auth_user(): ACL_User {
        return $this->user;
    }

    public function is_logged_in(): bool {
        return isset($this->user);
    }

    public function login(ACL_User $user): bool {
        $acl = new ACL();
        $authuser = $acl->valid_user($user->username, $user->password);
        if($authuser){            
            $this->user = $authuser;
            unset($authuser->password);
            $_SESSION['ACL_USER'] = $authuser;
            return true;
        }
        return false;
    }

    public function logout(): bool {
        unset($_SESSION['ACL_USER']);
        unset($this->user);
        return session_destroy();
    }

}
