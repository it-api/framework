<?php

namespace Barebone;


use Barebone\Form\Element;

class Form extends Component{
	
	/**
	 * 
	 * @var Element
	 */
	private $form;
	
	
	function __construct($attributes = array()){
		$this->form = new Element('form', $attributes);
		$this->form->setAttribute('role', 'form');
		// if no method is given default to post method
		if(!isset($attributes['method'])){
			$this->form->setAttribute('method', 'POST');
		}
		
		// if no method is given default to post method
		if(!isset($attributes['action'])){
			$router = Router::getInstance();
			$this->form->setAttribute('action', $router->requested_route);
		}
	}
	
	/**
	 * add hidden field to the form for security reasons
	 * <input type="hidden" name="seckey123456" value="987654" />
	 */
	function secure_form(){
		
		// create the unique value
		$formid_value = uniqid();
		
		// save it to a session variable
		$_SESSION['CSFR'] = $formid_value;
		
		$attr = array();
		$attr['type'] = 'hidden';
		$attr['name'] = 'CSFR';
		$attr['value'] = $formid_value;
		
		$this->form->addChild('input', $attr);
	}
	
	public function panelize(Form $form, string $title = '', string $footer = ''): Form\Element{
	    
	    $panel = new \Barebone\Form\Element('div', ['class'=>'panel panel-default']);
	    $panel_heading = new \Barebone\Form\Element('div', ['class'=>'panel-heading']);
	    $panel_body = new \Barebone\Form\Element('div', ['class'=>'panel-body']);
	    $panel_title = new \Barebone\Form\Element('h3', ['class'=>'panel-title','innertext' => $title]);
	    $panel_footer = new \Barebone\Form\Element('div', ['class'=>'panel-footer','innertext' => $footer]);
	    
	    $panel_heading->addChild($panel_title);
	    $panel->addChild($panel_heading);
	    $panel_body->addChild($form->form);
	    $panel->addChild($panel_body);
	    $panel->addChild($panel_footer);
	    return $panel;
	    /*
	    <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Form title</h3>
              </div>
              <div class="panel-body">
                <?php $form->panelize('Form title', 'Form footer')->render();?>
              </div>
              <div class="panel-footer">Form footer</div>
            </div>
        */
	}
	
	public static function create_csfr_token(){
	    
	    // create the unique value
		$formid_value = bin2hex(random_bytes(32));
		
		// save it to a session variable
		$_SESSION['CSFR'] = $formid_value;
		
		echo '<input type="hidden" name="CSFR" value="'.$formid_value.'">';
	}
	
	/**
	 * Shorthand to check if the form secret key is set and valid
	 * @return bool
	 */
	public static function is_valid_csfr(): bool{
		
		return isset($_SESSION['CSFR']) && isset($_POST['CSFR']) && $_POST['CSFR'] === $_SESSION['CSFR'];
		
	}
	
	/**
	 * Shorthand to check if the form is send
	 * @return bool
	 */
	function is_send(): bool{
		
		return strtoupper($_SERVER['REQUEST_METHOD']) === 'POST' && !isset($_GET['bb_faked_posted_request']);
		
	}
	/**
	 * Shorthand to check if the form secret key is set and valid
	 * @return bool
	 */
	function is_valid(): bool{
		
		return $_POST['CSFR'] === $_SESSION['CSFR'];
		
	}
	/**
	 * Shorthand to make the form ajax<br>
	 * <b>The form cannot be ajax when sumitting files !!</b>
	 * @return void
	 */
	function is_ajax(){
		$this->form->setAttribute(
			'onsubmit', 
			"$.php( $(this).attr('action'), $(this).serialize() ); return false;"
		);
	}
	/**
	 * Shorthand to send files with the form
	 * @return void
	 */
	function is_multipart(){
		$this->form->setAttribute('enctype', 'multipart/form-data');
	}
	
	function flash(string $message, string $type = 'success'){
	    $element = new Element('div', [
	        'class'=>'alert alert-'.$type,
	        'innertext'=> $message
	    ]);
	    
	    $this->add($element);
	}
	
	function add(Element $element){
		$this->form->addChild($element);
	}
	
	function addButton($type, $value, $attributes = array()){
		$attr = array();
		$attr['type'] = $type;
		$attr['value'] = $value;
		$attributes = array_merge($attr, $attributes);

		$this->form->addChild('input', $attributes);
	}
	function addInput($type, $label, $name, $attributes = array()){
		
		$formgroup = $this->form->addChild('div', array(
				'class' => 'form-group'
		));
		$label = $formgroup->addChild('label', array(
				'class' => 'col-sm-4',
				'innertext' => $label,
				'for' => $attributes['id']??$name
				
		));		
		$control = $formgroup->addChild('div', array('class' => 'col-sm-8'));		
		$attr = array();
		$attr['type'] = $type;
		$attr['name'] = $name;
		$attr['id'] = $attributes['id']??$name;
		$attributes = array_merge($attr, $attributes);
		return $control->addChild('input', $attributes);
	}
	function addRadioGroup($label, $name, $options = array(), $attributes = array()){
		
		$this->form->addChild('label', array('innertext' => $label));
		
		$attr = array();
		$attr['type'] = 'radio';
		$attr['name'] = $name;
		$attributes = array_merge($attr, $attributes);
		$this->form->addChild('input', $attributes);
	}
	
	function addTextarea($label, $name, $attributes = array()){
		
		$formgroup = $this->form->addChild('div', array(
				'class' => 'form-group'
		));
		$label = $formgroup->addChild('label', array(
				'class' => 'col-sm-4',
				'innertext' => $label
		
		));
		$control = $formgroup->addChild('div', array('class' => 'col-sm-8'));
		$attr = array();
		$attr['name'] = $name;
        $attr['innertext'] = $attributes['value'];
        //$attr['class'] = 'ckeditor';
        $attr['onfocus'] = "$('textarea[name={$name}]').ckeditor({height: 180,toolbar: 'Nihiel'});";
		$attributes = array_merge($attr, $attributes);
		$control->addChild('textarea', $attributes);
	}
	
	function addCheckbox($label, $name, $value, $attributes = array()){
		
		$formgroup = $this->form->addChild('div', array(
				'class' => 'form-group'
		));
		$label = $formgroup->addChild('label', array(
				'class' => 'col-sm-2',
				'innertext' => $label
		
		));		
		
		$attr = array();
		$attr['type'] = 'checkbox';
		$attr['name'] = $name;
		$attr['value'] = $value;
		$attributes = array_merge($attr, $attributes);
		$label->addChild('input', $attributes);
		//$control = $this->form->addChild('input', $attributes);
	}
	
	function addHidden($name, $value, $attributes = array()){
	
		$attr = array();
		$attr['type'] = 'hidden';
		$attr['name'] = $name;
		$attr['value'] = $value;
		$attributes = array_merge($attr, $attributes);
		$element = new Element('input', $attributes);
		$this->add($element);
		//$control = $this->form->addChild('input', $attributes);
	}
	
	function addSelect(string $name, string $label, array $attributes = [], array $options = []){
	    
	    $formgroup = $this->form->addChild('div', array(
				'class' => 'form-group'
		));
		$label = $formgroup->addChild('label', array(
				'class' => 'col-sm-4',
				'innertext' => $label
				
		));		
		$control = $formgroup->addChild('div', array('class' => 'col-sm-8'));		
		$attr = array();
		$attr['name'] = $name;
		$attributes = array_merge($attr, $attributes);
		
		$select = $control->addChild('select', $attributes);
		foreach($options as $option){
		    $option = trim($option);
		    $select->addChild('option', ['value' => $option, 'innertext' => $option]);
		}
		
	}
	
	function get_input_type_from_db_type($propertie){
		//debug($propertie);
		$dictonary = array(
				'int' => 'number',
				'tinyint' => 'checkbox',
				'varchar' => 'text',
				'set' => 'select',
				'enum' => 'select',
				'text' => 'textarea',
				'date' => 'date',
				'datetime' => 'date',

		);
		$aprop = explode('(', $propertie['Type']);
		$fieldtype = $aprop[0];
		
		return $dictonary[$fieldtype];
	}
	
	public function get_json(){
		return $this->form->get_json();
	}
	
	public function get_html(){
		return $this->form->get_html();
	}
	
	final public function render(){
		echo $this->get_html();
	}
	
	public static function get_csrf(){
	    return '<input type="hiden" name="csrf_token" value="' . $_SESSION['csrf_token'] . '">';
	}
	
}
