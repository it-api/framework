<?php

namespace Barebone\Activerecord\Exception;

use Barebone\Config;
/**
 * Record not found exception
 */
class RecordNotFound extends \Exception {

    function __construct($message = 'Record not found', $code = 0, \Exception $previous = null, $path = null) {

        parent::__construct($message, $code, $previous);
        $config = Config::getInstance();
        if ($config->getValue('application', 'production') == "false") {
            
        } else {

            // inform somebody of the error
            // TODO: CREATE A LOGBOOK FOR THIS
        }
    }

}
