<?php

/**
 * Description of Activerecord\Sanitizer
 *
 * @author Frank
 */

namespace Barebone\Activerecord;

class Sanitizer {

	/**
	 * @var \Barebone\Activerecord
	 */
	private \Barebone\Activerecord $activerecord;
	/**
	 * array('email' => 'trim|strtolower')
	 * @var array of field => sanitizer
	 */
	private array $sanitizers = [];
	/**
	 * Pass in your activerecord and the sanitizer rules an call clean on this object<br>
	 * $adres = new \customer();<br>
	 * $adres->get_by_id(1);<br>
	 * $adres_sanitizers = [];<br>
	 * $adres_sanitizers['email'] = 'trim|strtoupper';<br>
	 * $dirty = new Barebone\Activerecord\Sanitizer($adres, $adres_sanitizers);<br>
	 * $dirty<br>
	 * ->addSanitizer('email', 'trim|strtolower')<br>
	 * ->addSanitizer('username', 'trim|strtoupper');<br>
	 * Barebone\Inspector::Inspect($dirty);<br>
	 * Barebone\Inspector::Inspect($dirty->clean()->getActiverecord());<br>
	 * @param \Barebone\Activerecord $activerecord
	 * @param array $sanitizers
	 */
	public function __construct(\Barebone\Activerecord $activerecord, array $sanitizers = []) {

		$this->activerecord = $activerecord;
		$this->sanitizers = $sanitizers;
	}
	/**
	 * 
	 * @return \Barebone\Activerecord
	 */
	public function getActiverecord(): \Barebone\Activerecord {
		return $this->activerecord;
	}
	/**
	 * 
	 * @return array
	 */
	public function getSanitizers(): array {
		return $this->sanitizers;
	}
	/**
	 * 
	 * @param \Barebone\Activerecord $activerecord
	 * @return $this
	 */
	public function setActiverecord(\Barebone\Activerecord $activerecord) {
		$this->activerecord = $activerecord;
		return $this;
	}
	/**
	 * 
	 * @param array $sanitizers
	 * @return $this
	 */
	public function setSanitizers(array $sanitizers) {
		$this->sanitizers = $sanitizers;
		return $this;
	}
	/**
	 * 
	 * @param string $field name of the field in the activerecord
	 * @param string $rules rules to apply to the field eg  trim|ucfirst
	 * @return $this
	 */
	public function addSanitizer($field, $rules) {
		$this->sanitizers[$field] = $rules;
		return $this;
	}
	/**
	 * Sanatize the activerecord fields
	 * @return $this
	 */
	public function sanitize() {
		$sanitizer = new \Barebone\Sanitizer();
		foreach ($this->sanitizers as $field => $rules) {
			$this->activerecord->{$field} = $sanitizer->sanitizeValue($rules, $this->activerecord->{$field});
		}
		return $this;
	}
}

/**
$adres = new \customer();
$adres->get_by_id(1);
$adres_sanitizers = [];
$adres_sanitizers['email'] = 'trim|strtoupper';

$clean = new Barebone\Activerecord\Sanitizer($adres, $adres_sanitizers);
	$clean
 * ->addSanitizer('email', 'trim|strtolower')
 * ->addSanitizer('username', 'trim|strtoupper')
 **/
