<?php
/**
 *
 * @author Frank
 */

namespace Barebone\Activerecord;

interface IActiverecord {
	
	public function create(mixed $data = null);
	public function escape(string $value);
	public function propertie_exists(string $propertie);
	public function get_columns();
	public function get_primary_key();
	public function set_data(mixed $data);
	public function get_data(): array;
	public function get_by_id(int $id);
	public function fetch_all(array $options = []);
	public function all(array $options = []);
	public function first();
	public function last();
	public function find($args);
	public function find_by_sql(string $sql, array $params = [], string $objName = '\stdClass');
	public function insert();
	public function update();
	public function save();
	public function delete();
	public function still_exists();	
	public function toGrid(array $options = [], array $columns = []);
	public function render_form(?\Barebone\Form\Decorators\DecoratorInterface $decorator = null, array $form_attr = []);
	public function get_form(array $attr = []);
	public function get_selected_columns();
	public function toDataTable(\Barebone\View $view, array $options = [], array $formatter = []);
		
}
