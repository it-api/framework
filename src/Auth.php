<?php

namespace Barebone;

use Barebone\Auth\Auth_session;
use Barebone\Auth\Auth_cookie;
use Barebone\Auth\Auth_user;
use Barebone\Auth\IAuth;

class Auth implements IAuth {

	/**
	 * SESSION OR COOKIE
	 * @var string $auth_type
	 */
	public static $auth_type; // session, cookie
	/**
	 * Holds the authenticated user object
	 * @var mixed $auth_user
	 */
	public static $auth_user;
	/**
	 * Holds the authentication type object eg Auth_session, Auth_cookie
	 * @var Auth_session|Auth_cookie $auth
	 */
	public static $auth;

	function __construct($auth_type = 'session') {

		self::$auth_type = $auth_type;

		if (self::$auth_type === 'session') {
			if (session_id() === '')
				session_start();
		}
	}
	/**
	 * Check if user is authenticated
	 * @return bool
	 */
	public static function is_logged_in() {
		if (self::$auth_type === 'session') {
			return isset($_SESSION['BAREBONE']['AUTHUSER']);
		}
		return false;
	}

	public static function login($username, $password) {
		if (!class_exists('\ACL_User')) {
			throw new Exception('You need to create a table with the name "ACL_User" first');
		}
		$ACL_User = new \ACL_User();
		$users = $ACL_User->find_by_sql(
			"SELECT * FROM ACL_User WHERE Username = :Username AND Password = :Password LIMIT 1",
			[
				':Username' => $username,
				':Password' => $password
			]
		);
		if (isset($users[0])) {
			$_SESSION['BAREBONE']['AUTHUSER'] = $users[0];
			self::$auth_user = $_SESSION['BAREBONE']['AUTHUSER'];
		} else {
			return false;
		}
	}
	/**
	 * Log the user out
	 * @return void
	 */
	public static function logout() {
		if (self::$auth_type === 'session') {
			unset($_SESSION['BAREBONE']['AUTHUSER']);
		}
	}
}
