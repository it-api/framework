<?php

namespace Barebone\Collection\Exceptions;
use Barebone\Exception as Exception;
/**
 * Description of KeyInvalidException
 * MYFW 
 * UTF-8
 *
 * @author Frank
 * @filesource KeyInvalidException.php
 * @since 15-jan-2017 20:26:15
 * 
 */
class KeyInvalidException extends \Exception{
    //put your code here
}
