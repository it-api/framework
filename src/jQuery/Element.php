<?php

namespace Barebone\jQuery;

/**
 * jQuery_Element - class for work with jQuery framework
 *
 * @access   public
 * @package  Barebone
 */
class Element
{
    /**
     * selector path
     * @var string
     */
    public $s;
    
    /**
     * methods
     * @var array
     */
    public $m = array();
    
    /**
     * args
     * @var array
     */
    public $a = array();
    
    /**
     * __construct
     * contructor of jQuery
     *
     * @return Element
     */
    public function __construct($selector)
    {
        \Barebone\jQuery::addElement($this); 
        $this->s = $selector;
    }
    
    /**
     * __call
     *
     * @return Element
     */
    public function __call($method, $args)
    {
        array_push($this->m, $method);
        array_push($this->a, $args);
        
        return $this;
    }
    
    /**
     * end
     * need to create new jQuery
     *
     * @return Element
     */
    public function end()
    {
        return new Element($this->s);
    }
}
