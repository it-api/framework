<?php

namespace Barebone\jQuery;

/**
 * Class jQuery_Action
 *
 * Abstract class for any parameter of any action
 * @package  Barebone
 * @version  1.0
 */
class Action
{
    /**
     * add param to list
     * 
     * @param  string $param
     * @param  string $value
     * @return Action
     */
    public $foo;
    public $msg;
    public $callback;
    public $params;

    public function add(string $param, string $value)
    {
        $this->{$param} = $value;
        return $this;
    }
    
}
