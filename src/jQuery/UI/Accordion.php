<?php

/*
  Creator Frank
 */

namespace Barebone\jQuery\UI;
use Barebone\HTML\TElement;
/**
 * Description of Accordion
 *
 * @author Frank
 */
class Accordion extends TElement{
	
	private $_id = null;
	private $sections = [];
	private $options = [];
	
	function __construct($options = []) {
		//create unique id for the accordion
		$this->_id = uniqid('accordion-');
		//create the parent
		parent::__construct('div', ['id' => $this->_id]);
	}
	
	function addSection($caption, $content = ''){
		// create H3 tag
		$h3 = new TElement('h3');
		$h3->innertext($caption);
		$this->append($h3);
		// create the actual content div
		$div = new TElement('div');
		$div->innertext($content);
		$this->sections[] = $div;	
		$this->append($div);
		return $div;
	}
	
	/**
	 * override to string functions
	 * @return type
	 */
	function __toString(){
	    ob_start();
	    $s = parent::__toString();
		if(!defined('UI_ACCORDION_SET')){
			$s .= '<script>$(document).ready(function(){$("#'.$this->_id.'").accordion('. json_encode($this->options).');});</script>';
			define('UI_ACCORDION_SET', true);
		}
	    $s .= ob_get_clean();
	    return $s;
	}

}
