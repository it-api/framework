<?php

/*
  Creator Frank
 */

namespace Barebone\jQuery\UI;
use Barebone\HTML\TElement;

/**
 * Description of Datepicker
 *
 * @author Frank
 */
class Datepicker extends TElement{
	
	private $_id = null;
	private $options = [];
	
	function __construct($name = 'unnamed', $value = '', $attributes = []) {
		parent::__construct('input', $attributes);		
		$this->_id = uniqid('datepicker-');
		$this->type = 'text';
		$this->name = $name;
		$this->value = $value;
		$this->id = $this->_id;
	}
	/**
	 * override to string functions
	 * @return type
	 */
	function __toString(){
	    ob_start();
	    $s = parent::__toString();
		$s .= '<script>$(document).ready(function(){$("#'.$this->_id.'").datepicker('. json_encode($this->options).');});</script>';
	    $s .= ob_get_clean();
	    return $s;
	}
	

}
