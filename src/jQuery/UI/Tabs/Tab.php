<?php

namespace Barebone\jQuery\UI\Tabs;
use Barebone\HTML\TElement;
/**
 * Represents one jquery ui tabs widget tab 
 * <li><a href="#tabs-1">Nunc tincidunt</a></li>
 * @author Frank
 */
class Tab extends TElement{
	
	function __construct($caption, $href = '#') {
		// create a list LI item
		parent::__construct('li');
		// create a link element
		$a = new \Barebone\HTML\TLink('', $href);
		// set the text of the tab
		$a->innertext($caption);
		// set the link of the tab
		$a->href = $href;
		// add the link to the LI element
		$this->append( $a );
	}

}
