<?php

namespace Barebone\jQuery\UI;
use Barebone\HTML\TElement;
use Barebone\jQuery\UI\Tabs\Tab;
/**
 * Description of Tabs
 * Creator Frank
 
 This class represents a jquery ui tabs widget
 
 <div id="tabs">
  <ul>
    <li><a href="#tabs-1">Nunc tincidunt</a></li>
    <li><a href="#tabs-2">Proin dolor</a></li>
  </ul>
  <div id="tabs-1">
    <p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.</p>
  </div>
  <div id="tabs-2">
    <p>Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p>
  </div>
</div>
 *
 * @author Frank
 */
class Tabs extends TElement{
	
	private $_id = null;
	private $tabs = [];
	private $contents = [];
	private $options = [];
	
	function __construct($options = []) {
		//create unique id for the tabs
		$this->_id = uniqid('tabs-');
		//create the parent
		parent::__construct('div', ['id' => $this->_id]);
		// add a ul list that hold the tabs
		$this->tabs = new TElement('ul');
		// add the tabs ul list to the tabs div
		$this->append( $this->tabs );
		// the options for the tab widget
		$this->options = $options;
	}
	/**
	 * Add a tab to the tabs widget
	 * @param string $caption
	 * @param string $href
	 * @return Tab
	 */
	function addTab($caption, $href = '#', $content = ''){
		//if the href == '#' we also want to add a content element
		if(substr($href,0,1) == '#'){
			//$href = '#tab-' . count( $this->tabs );
			$tabcontent = new TElement('div');
			$tabcontent->id = substr($href,1);
			$tabcontent->innertext($content);
			$this->contents[] = $tabcontent;
			$this->append($tabcontent);
		}
		// create a tab object
		$tab = new Tab($caption, $href);
		// add the tab to the tabs ul		
		$this->tabs->append($tab);		
		// return the tab
		return $tab;		
	}
	/**
	 * override to string functions
	 * @return type
	 */
	function __toString(){
	    ob_start();
	    $s = parent::__toString();
		if(!defined('UI_TABS_SET')){
			$s .= '<script>$(document).ready(function(){$("#'.$this->_id.'").tabs('. json_encode($this->options).');});</script>';
			define('UI_TABS_SET', true);
		}
	    $s .= ob_get_clean();
	    return $s;
	}

}
