<?php

/*
  Creator Frank
 */

namespace Barebone\jQuery\UI;
use Barebone\HTML\TElement;
/**
 * Description of Menu
 * <ul id="menu">
  <li class="ui-state-disabled"><div>Toys (n/a)</div></li>
  <li><div>Books</div></li>
  <li><div>Clothing</div></li>
  <li><div>Electronics</div>
    <ul>
      <li class="ui-state-disabled"><div>Home Entertainment</div></li>
      <li><div>Car Hifi</div></li>
      <li><div>Utilities</div></li>
    </ul>
  </li>
  <li><div>Movies</div></li>
 * @author Frank
 */
class Menu extends TElement{
	
	private $_id = null;
	private $options = [];
	
	function __construct($attributes = []) {
		parent::__construct('ul', $attributes);		
		$this->_id = uniqid('menu-');
		$this->id = $this->_id;
		//foreach($items as $item){
		//	$li = new TElement('li');
		//	$div = new TElement('div');
		//	$li->append($div);
		//	$this->append($li);
		//}
	}
	
	function addItem($caption, $attributes = []){
		$li = new TElement('li', $attributes);
		$div = new TElement('div');
		
		$div->innertext($caption);
		
		$li->append($div);
		$this->append($li);
		
		return $div;
	}
	
	function __toString() {
		ob_start();
		$s = parent::__toString();
		$s .= '<script>'
			. '$(document).ready(function(){'
			. '$menu = $(\'#'.$this->_id.'\').menu();'
			//. '$menu.on({"menuselect": function( event, ui ){console.log(event);console.log(ui); alert("item " + $(ui.item).text() + " is clicked");}});'
			. '});'
			. '</script>';
		return $s;
	}

}
