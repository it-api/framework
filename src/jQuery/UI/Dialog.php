<?php

/*
  Creator Frank
 */

namespace Barebone\jQuery\UI;
use Barebone\HTML\TElement;

/**
 * Description of Dialog
 *
 * @author Frank
 */
class Dialog extends TElement{
	
	private $_id = null;
	private $options = [];
	
	function __construct($type, $title, $message, $attributes = [], $options = []) {
		
		parent::__construct('div', $attributes);		
		$this->_id = uniqid('dialog-');
		$this->id = $this->_id;
		$this->title = $title;
		$this->innertext($message);
		$this->options = $options;
		
		if($type == 'info'){
			$img = new \Barebone\HTML\TImage('/assets/thirdparty/zebra-dialog/public/css/flat/information.png');
			$img->css('float', 'left');
			$img->css('width', '48px');
			$img->css('margin-right', '10px');
			$this->append($img);
		}elseif($type == 'error'){
			$img = new \Barebone\HTML\TImage('/assets/thirdparty/zebra-dialog/public/css/flat/error.png');
			$img->css('float', 'left');
			$img->css('width', '48px');
			$img->css('margin-right', '10px');
			$this->append($img);
		}elseif($type == 'question'){
			$img = new \Barebone\HTML\TImage('/assets/thirdparty/zebra-dialog/public/css/flat/question.png');
			$img->css('float', 'left');
			$img->css('width', '48px');
			$img->css('margin-right', '10px');
			$this->append($img);
		}
	}
	
	function open(){
		$s = '$(\'#'.$this->_id.'\').dialog(\'open\');';
		echo $s;
	}
	
	function close(){
		$s = '$(\'#'.$this->_id.'\').dialog(\'close\');';
		echo $s;
	}
	
	/**
	 * override to string functions
	 * @return type
	 */
	function __toString(){
	    ob_start();
	    $s = parent::__toString();
		$s .= '<script>$(document).ready(function(){$("#'.$this->_id.'").dialog('. json_encode($this->options).');});</script>';
	    $s .= ob_get_clean();
	    return $s;
	}

}
