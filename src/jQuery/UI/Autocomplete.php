<?php

/*
  Creator Frank
 */

namespace Barebone\jQuery\UI;
use Barebone\HTML\TElement;
use Barebone\HTML\TInput;
/**
 * Description of Autocomplete
 * <div class="ui-widget">
	<label for="tags">Tags: </label>
	<input id="tags">
  </div>
 *
 * @author Frank
 */
class Autocomplete extends TElement {
	
	private $_id = null;
	private $options = [];
	/**
	 * 
	 * @param text $caption text for the label
	 * @param text $name name of the input element
	 * @param arrayy $attributes
	 */
	function __construct($caption, $name, $attributes = []) {
		parent::__construct('div', $attributes);
		//create unique id for the tabs
		$this->_id = $name;
		$label = new TElement('label');
		$label->innertext($caption);
		$input = new TInput($name);
		$this->append($label);
		$this->append($input);
	}
	
	function setSource($source){
		$this->options['source'] = $source;
	}
	
	/**
	 * override to string functions
	 * @return type
	 */
	function __toString(){
	    ob_start();
	    $s = parent::__toString();
		$s .= '<script>$(document).ready(function(){$("input[name='.$this->_id.']").autocomplete('. json_encode($this->options).');});</script>';
	    $s .= ob_get_clean();
	    return $s;
	}

}
