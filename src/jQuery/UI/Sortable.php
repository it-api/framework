<?php

/*
  Creator Frank
 */

namespace Barebone\jQuery\UI;
use Barebone\HTML\TElement;
/**
 * Description of Sortable
 *
 * @author Frank
 */
class Sortable extends TElement{
	
	private $_id = null;
	private $options = [];
	
	function __construct($attributes = []) {
		parent::__construct('ul', $attributes);		
		$this->_id = uniqid('menu_');
		$this->id = $this->_id;
	}

	function addItem($caption, $attributes = []){
		
		$li = new TElement('li', $attributes);		
		$li->innertext($caption);
		$this->append($li);
		
		return $li;
	}
	
	function __toString() {
		ob_start();
		$s = parent::__toString();
		$s .= '<script>'
			. '$(document).ready(function(){'
			. '$sortable_'.$this->_id.' = $(\'#'.$this->_id.'\').sortable();'
			. '$(\'#'.$this->_id.'\').disableSelection();'
			. '});'
			. '</script>';
		return $s;
	}
}
