<?php

/*
  Creator Frank
 */

namespace Barebone\jQuery\UI;
use Barebone\HTML\TElement;

/**
 * Description of Button
 *
 * @author Frank
 */
class Button extends TElement{
	
	private $_id = null;
	private $options = [];
	
	function __construct($caption, $callback = '') {
		parent::__construct('button');
		//create unique id for the tabs
		$this->_id = uniqid('btn-');
		$this->id = $this->_id;
		$this->innertext($caption);
		$this->on('click', $callback);
	}
	
	/**
	 * override to string functions
	 * @return type
	 */
	function __toString(){
	    ob_start();
	    $s = parent::__toString();
		$s .= '<script>$(document).ready(function(){$("#'.$this->_id.'").button('. json_encode($this->options).');});</script>';
	    $s .= ob_get_clean();
	    return $s;
	}

}
