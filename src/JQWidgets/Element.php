<?php

/*
  Element.php
  UTF-8
  7-mei-2018 22:14:46
  website-backend
  Creator Frank
 */

namespace Barebone\JQWidgets;
use \Barebone\HTML\TElement;

/**
 * Description of Element
 *
 * @author Frank
 */
class Element extends TElement{
	
	public function __construct($tag, $attributtes = []) {
		parent::__construct($tag, $attributtes);
	}
}
