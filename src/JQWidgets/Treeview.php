<?php

/*
  Creator Frank
 */

namespace Barebone\JQWidgets;
use Barebone\HTML\TElement;


/**
 * Description of Treeview
 * 
 * <div id='jqxWidget'>
        <div id='jqxExpander'>
            <div>
                Folders
            </div>
            <div style="overflow: hidden;">
                <div style="border: none;" id='jqxTree'>
                </div>
            </div>
        </div>
    </div>
 *
 * @author Frank
 */
class Treeview extends TElement{
	
	private array $source = [];
	private string $_id = 'jqxTree';
	private string $_nonce = '';
	
	public function __construct(string $id, string $title, string $nonce = '') {
		
		parent::__construct('div');
		
		// CSP nonce
		$this->_nonce = $nonce;
		
		$this->id = 'jqxWidget';
		// create expander
		$expander = new TElement('div');
		$expander->id = 'jqxExpander';
		// add the expander to the widget
		$this->append($expander);
		// create the title div and set the text
		$titlediv = new TElement('div');
		$titlediv->innertext($title);
		// add the title div to the expander
		$expander->append($titlediv);
		// create wrapper tree
		$wrapper = new TElement('div');
		$wrapper->css('overflow', 'hidden');		
		// create the tree
		$treeview = new TElement('div');
		$treeview->css('border', 'none');
		// set id
		$treeview->id = $id;
		$this->_id = $id;
		// add the treeview to the wrapper
		$wrapper->append($treeview);
		// add the wrapper to the expander
		$expander->append($wrapper);
	}
	
	function addItem(string $label, string $icon, string $expanded = 'true', array $items = []){
		
		$item = new \stdClass();
		$item->label = $label;
		$item->icon = $icon;
		$item->expanded = $expanded;
		$item->items = $items;
		$this->source[] = $item;
		//icon: "/assets/thirdparty/jqwidgets4/images/mailIcon.png", label: "Mail", expanded: true, items
	}
	
	function __toString() {
		$r = parent::__toString();
		$r .= '<script nonce="'.$this->_nonce.'" type="text/javascript">'			
			. '$(document).ready(function () {'
			. '		var source = '. \json_encode($this->source) . ';'
			. '		$(\'#jqxExpander\').jqxExpander({ showArrow: false, toggleMode: \'none\', width: \'300px\', height: \'370px\'});'
			. '		$(\'#'.$this->_id.'\').jqxTree({ source: source, width: \'100%\', height: \'100%\'});'
			. '		$(\'#'.$this->_id.'\').jqxTree(\'selectItem\', null);'
			. '});'
			. '</script>';
		return $r;
	}

}
