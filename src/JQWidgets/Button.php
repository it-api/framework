<?php

/*
  Button.php
  UTF-8
  7-mei-2018 22:05:10
  website-backend
  Creator Frank
 */

namespace Barebone\JQWidgets;

/**
 * Description of Button
 *
 * @author Frank
 */
class Button extends Element{
	/**
	 * 
	 * @param string $text text on the badge
	 */
	public function __construct($text) {
		parent::__construct('button');
		$this->type = 'button';
		$this->innertext($text);
	}

}