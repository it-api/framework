<?php

namespace Barebone;

class JSON {

    /**
     * Array to hold the data for the JSON file
     * @var array
     */
    private $data = array();

    /**
     * JSON Constructor takes data and assign it to the data propertie
     * @param mixed $data
     */
    public function __construct($data = null) {
        $this->data = $data;
    }

    /**
     * Return the data array of the JSON object
     * @return mixed array|object
     */
    public function getData($key = '') {
        if ($key === '') {
            return $this->data;
        } elseif (is_array($this->data) && array_key_exists($key, $this->data)) {
            return $this->data[$key];
        } elseif (is_object($this->data) && isset($this->data->$key)) {
            return $this->data->$key;
        } else {
            throw new Exception('Key does not exists in the data.');
        }
    }

    /**
     *  This will overwrite the current data
     * @param array $data
     * @return $this
     */
    public function setData(array $data = []) {
        $this->data = $data;
        return $this;
    }

    /**
     * Merge your data with the JSON object data
     * @param array $data
     * @return $this
     */
    function mergeData(array $data = []) {
        $this->data = array_merge($this->data, $data);
        return $this;
    }

    /**
     * Add an object to the data array
     * @param string $name key value
     * @param object $object value of the key
     */
    public function add_object(string $name, object $object) {
        $this->data[$name] = $object;
    }

    /**
     * Add an array to the data array
     * @param string $name key value
     * @param array $arr value of the key
     */
    public function add_array(string $name, array $arr) {
        $this->data[$name] = $arr;
    }

    /**
     * Add an number to the data array
     * @param string $name key value
     * @param int $num value of the key
     */
    public function add_number(string $name, int $num) {
        $this->data[$name] = $num;
    }

    /**
     * Add a string to the data array
     * @param string $name key value
     * @param string $arr value of the key
     */
    public function add_string(string $name, string $str) {
        $this->data[$name] = $str;
    }

    /**
     * Render the object data as json
     * If return is true you cannot use_headers
     * @param bool $return when return is true this function will return the json string else print it out.
     * @param bool $use_headers when true this function will use the headers to send the json
     * @param mixed $options
     * @return mixed
     */
    function render(bool $return = false, bool $use_headers = false, $options = null) {
        if ($return) {
            return json_encode($this->data, $options);
        } else {

            if ($use_headers) {

                $response = new Response();
                $response->json($this->data, $options);
            } else {

                echo json_encode($this->data, $options);
            }
        }
    }

    /**
     * Save object to disk
     * @param string $filename the name of the file where to save the json
     * @return number the number of bytes written
     */
    public function save_to_disk($filename) {
        return file_put_contents($filename, json_encode($this->data));
    }

    /**
     * Load json from disk and return this JSON object
     * @param string $filename
     * @return \Barebone\JSON
     */
    public function load_from_disk($filename) {
        $this->data = json_decode(file_get_contents($filename));
        return $this;
    }
}
