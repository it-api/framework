<?php

namespace Barebone\Components;
use \Barebone\Layout;
use \Barebone\View;
/**
 * Description of Kanban
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource Kanban.php
 * @since 21-jan-2017 2:00:50
 * 
 */
class Kanban {
    
    public $layout;
    public $view;
    
    public function __construct(Layout $layout, View $view) {
        
        $this->layout = $layout;
        $this->view = $view;
        
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxcore.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxsortable.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxdata.js');        
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxkanban.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxsplitter.js');


        $this->layout->add_stylesheet('/assets/thirdparty/jqwidgets4/jqwidgets/styles/jqx.base.css');
        $this->layout->add_stylesheet('/assets/thirdparty/jqwidgets4/jqwidgets/styles/jqx.energyblue.css');
        
        //$this->view->append('<div id="kanban"></div>');
    }

}
