<?php

namespace Barebone\Components;
use \Barebone\HTML\TElement as TElement;
use \Barebone\Layout as Layout;
use \Barebone\View as View;
/**
 * Description of Scheduler
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource Scheduler.php
 * @since 21-jan-2017 3:20:02
 * 
 */
class Scheduler extends TElement{
    
    public $layout;
    public $view;
    
    public function __construct(Layout $layout, View $view) {
        
        $this->layout = $layout;
        $this->view = $view;
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqx-all.js');
/*
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxcore.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxbuttons.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxscrollbar.js');        
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxdata.js');        
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxdate.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxscheduler.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxscheduler.api.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxdatetimeinput.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxmenu.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxcalendar.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxtooltip.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxwindow.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxcheckbox.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxlistbox.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxdropdownlist.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxnumberinput.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/jqxradiobutton.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/globalization/globalize.js');
        $this->layout->add_javascript('/assets/thirdparty/jqwidgets4/jqwidgets/globalization/globalize.culture.nl-NL.js');   
  */      
        
        $this->layout->add_stylesheet('/assets/thirdparty/jqwidgets4/jqwidgets/styles/jqx.base.css');
        
		//<div id='jqxcalendar'></div>
		parent::__construct('div',['id'=>'scheduler']);
		

    }

	function __toString() {
		$s = parent::__toString();
		$s .= '';
		return $s;
	}
}
