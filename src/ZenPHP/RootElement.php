<?php

namespace Barebone\ZenPHP;

use Barebone\ZenPHP\Element;
/**
 * Barebone\ZenPHP\RootElement Zencoding
 * @author Frank
 *
 */
class RootElement extends Element {
	
	function __construct(){}
	
	function parse($tabCount = 0) {
		$content = '';
		foreach ($this->getChildren() as $child) {
			$content .= $child->parse($tabCount);
		}
		return $content;
	}
}
