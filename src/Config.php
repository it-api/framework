<?php

namespace Barebone;

use Barebone\Filereaders\Json_file;

/**
 * Represents the config/application.json file
 * @author Frank
 *
 */
class Config extends Json_file {

    public static $config;

    public static function getInstance() {
        if (!isset(self::$config)) {
            self::$config = new self();
        }
        return self::$config;
    }

    function __construct(array $config = array()) {

        $this->filename = FULL_BAREBONE_PATH . 'config/application.json';

        if (file_exists($this->filename)) {

            $this->load_file();
        } elseif($this->createNewConfig()){

            $this->load_file();
        } else {

            echo '<p>config file is missing, did you install the framework correctly?</p>';
            echo $this->filename;
        }
    }

    protected function __clone() {
        //Me not like clones! Me smash clones!
    }

    private function createNewConfig(){

        $filename_application_new = FULL_BAREBONE_PATH . 'config/application.new.json';
	    $filename_application = FULL_BAREBONE_PATH . 'config/application.json';
	    if(file_exists($filename_application_new)){
	        rename($filename_application_new, $filename_application);
	        unlink($filename_application_new);

	        $filename_acl_new = FULL_BAREBONE_PATH . 'config/acl.new.json';
    	    $filename_acl = FULL_BAREBONE_PATH . 'config/acl.json';
    	    if(file_exists($filename_acl_new)){
    	        rename($filename_acl_new, $filename_acl);
    	        unlink($filename_acl_new);
    	    }

    	    return true;
	    }

	    return false;
    }
}
