<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Collapse
 *
 * @author Frank
 */
class Collapse extends Element{
    
    public function __construct(Button $btn = null) {
        
        if(!is_null($btn) && $btn->data('controls') === ''){
            die('You need to set the data-controls attribute on the button');
        }
        
        parent::__construct('div');
        $this->add_class('collapse');
        if(!is_null($btn)) {
            $this->id = $btn->data('controls');
        }
    }
    
    function showPreview(){
        $btn = new Button('Hello');
        $btn->data('controls', 'btn-id');
	    $collapse = new Collapse($btn);
        echo $collapse;
	}
    
    function addCard(Card $card){
        $this->append($card);
        return $this;
    }

}