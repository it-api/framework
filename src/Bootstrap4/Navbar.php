<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Navbar
 * <nav class="navbar navbar-expand-lg navbar-light bg-light"></nav>
 * @author Frank
 */
class Navbar extends Element {
    
    private $nav;
    private $collapse;
    
	public function __construct(Navbar\Brand $navbrand = null) {

		parent::__construct('nav');
        $this->add_class('navbar');
        if(!is_null($navbrand)){
            $this->append($navbrand);
        }
        $this->addToggler();
        $this->addCollapse();
        $this->addNav();
	}
    
    function showPreview(){
        echo new Navbar(new Navbar\Brand('My Brand'));
    }
    
    function expand($size = 'lg'){
        $this->add_class("navbar-expand-{$size}");
        return $this;
    }
    
    function navbarColor($color = 'light'){
        $this->add_class("navbar-{$color}");
        return $this;
    }
    
    function bgColor($color = 'light'){
        $this->add_class("bg-{$color}");
        return $this;
    }
    
    function addCollapse(){
        $this->collapse = new Navbar\Collapse();
        $this->append($this->collapse);
        return $this;
    }
    
    function addToggler(){
        $toggler = new Navbar\Toggler();
        $this->append($toggler);
        return $this;
    }
    
    function addNav(){
        $this->nav = new Navbar\Nav();
        $this->collapse->append($this->nav);
    }
    
    function addItem(Navbar\Item $item){
        $this->nav->append($item);
        return $this;
    }
    
    function addDropdown(Dropdown $dropdown){
        $this->nav->append(new Navbar\Dropdown($dropdown));
    }
    
    function fixedTop(){
        $this->add_class("fixed-top");
    }
    
    function fixedBottom(){
        $this->add_class("fixed-bottom");
    }
    
    function stickyTop(){
        $this->add_class("sticky-top");
    }
}
