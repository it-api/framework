<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Splitbutton

  <!-- Split button -->
  <div class="btn-group">
  <button type="button" class="btn btn-danger">Action</button>
  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
  <span class="caret"></span>
  <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu" role="menu">
  <li><a href="#">Action</a></li>
  <li><a href="#">Another action</a></li>
  <li><a href="#">Something else here</a></li>
  <li class="divider"></li>
  <li><a href="#">Separated link</a></li>
  </ul>
  </div>

 * @author Frank
 */
class SplitButton extends Element {

	private $_id;
	private $button;
	private $ddtoggle;
	private $menu;

	public function __construct($caption, $attributes = []) {

		if (!isset($attributes['class'])) {
			$attributes['class'] = 'btn-default';
		}
		// create unique id
		$this->_id = uniqid('ddbtn');

		// <div class="btn-group">
		parent::__construct('div');
		$this->class = 'btn-group';

		// <button type="button" class="btn btn-default">Action</button>
		$this->button = new Element('button', $attributes);
		$this->button->id = $this->_id;
		$this->button->type = 'button';
		$this->button->add_class('btn');
		$this->button->innertext($caption);

		// <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		$this->ddtoggle = new Element('button', $attributes);
		$this->ddtoggle->add_class('btn');
		$this->ddtoggle->add_class('dropdown-toggle');
		$this->ddtoggle->data('toggle', 'dropdown');

		// <span class="caret"></span>
		$caret = new Element('span');
		$caret->class = 'caret';

		// <span class="sr-only">Toggle Dropdown</span>
		$toggle = new Element('span');
		$toggle->class = 'sr-only';

		// add the caret and toggle to the button
		$this->ddtoggle->append($caret);
		$this->ddtoggle->append($toggle);

		// add the button to btn-group
		$this->append($this->button);

		// add the dd toggle to the btn-group
		$this->append($this->ddtoggle);

		// create the dropdown menu
		$this->menu = new Element('ul');
		$this->menu->class = 'dropdown-menu';
		$this->menu->role = 'menu';
		$this->menu->aria('labelledby', $this->_id);

		// add the menu to the btn-group
		$this->append($this->menu);
	}
    
    function showPreview(){
        $splitbutton =  new SplitButton('Splitbutton');
        $splitbutton->addItem('Item 1');
        $splitbutton->addItem('Item 2');
        $splitbutton->addItem('Item 3');
        echo $splitbutton;
    }
    
	/**
	 * Add item to dropdown menu.
	 * @param string $caption text on the item
	 * @param type $href link for the item
	 */
	function addItem($caption, $href = '#') {

		$li = new Element('li');
		$li->role = 'presentation';

		$a = new Element('a');
		$li->role = 'menuitem';
		$li->tabindex = '-1';
		$a->innertext($caption);
		$a->href = $href;

		$li->append($a);

		$this->menu->append($li);
		return $this;
	}

	/**
	 * Add a header to label sections of actions in any dropdown menu.
	 * //<li role="presentation" class="dropdown-header">Dropdown header</li>
	 * @param string $caption
	 */
	function addHeader($caption) {
		$li = new Element('li');
		$li->role = 'presentation';
		$li->class = 'dropdown-header';
		$li->innertext($caption);
		$this->menu->append($li);
		return $this;
	}

	/**
	 * 
	 * @return $this
	 */
	function large() {
		$this->button->add_class('btn-lg');
		$this->ddtoggle->add_class('btn-lg');
		return $this;
	}

	/**
	 * 
	 * @return $this
	 */
	function small() {
		$this->button->add_class('btn-sm');
		$this->ddtoggle->add_class('btn-sm');
		return $this;
	}

	/**
	 * 
	 * @return $this
	 */
	function extrasmall() {
		$this->button->add_class('btn-xs');
		$this->ddtoggle->add_class('btn-xs');
		return $this;
	}

	function dropup() {
		$this->add_class('dropup');
	}

}
