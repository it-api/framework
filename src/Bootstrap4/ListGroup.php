<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Listgroup
 *
 * @author Frank
 */
class ListGroup extends Element {

	/**
	 * Listgroup constructor
	 */
	public function __construct() {
		parent::__construct('ul');
		$this->add_class('list-group');
	}
    
    function showPreview(){
        $listgroup = new ListGroup();
        $listgroup->addItem('Item 1');
        $listgroup->addItem('Item 2');
        $listgroup->addItem('Item 3');
        echo $listgroup;
    }
    
	/**
	 * 
	 * @param string $caption text on the list-item
	 * @param string $bagde bagde text
	 * @param string $href link
	 * @param string $contextual [null,default,primary,secondary,info,success,warning,danger,light,dark,active,disabled]
	 * @return $this
	 */
	function addItem($caption, $contextual = null, $href = null, $bagde = null) {

		// if a link is given in
		if (!is_null($href)) {

			// transform the ul to div
			$this->tag = 'div';
			// create the link
			$listitem = new Element('a');
			$listitem->add_class('list-group-item');
            $listitem->add_class('list-group-item-action');
			$listitem->innertext($caption);
			$listitem->href = $href;
		} else {

			// no link so we use standard li element
			$listitem = new Element('li');
			$listitem->class = 'list-group-item';
			$listitem->innertext($caption);
		}

		// set the contextual color
		if($contextual === 'disabled'){
            $listitem->add_class("disabled");
        } elseif($contextual === 'active'){
            $listitem->add_class("active");
        } elseif (!is_null($contextual)) {
            $listitem->add_class("list-group-item-{$contextual}");
        }

		// add a badge to the list-group-item
		if (!is_null($bagde)) {
			$b = new Badge($bagde);
			$listitem->append($b);
		}
		// add the listitem to the parent
		$this->append($listitem);
		return $this;
	}
    
    function flush(){
        $this->add_class("list-group-flush");
    }
    
    function horizontal(){
        $this->add_class("list-group-horizontal");
    }

}
