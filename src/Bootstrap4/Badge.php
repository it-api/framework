<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Badge
 *
 * @author Frank
 */
class Badge extends Element {

	/**
	 * 
	 * @param string $text text on the badge
	 */
	public function __construct($text, $color = Colors::INFO) {
		parent::__construct('span');
		$this->add_class('badge');
        $this->add_class('badge-'.$color);
		$this->innertext($text);
	}
	
	function showPreview(){
	    echo new Badge('Badge');
	}
    
    function pill(){
        $this->add_class('badge-pill');
        return $this;
    }
    
    function link($href){
        $this->tag = 'a';
        $this->href = $href;
        return $this;
    }

}
