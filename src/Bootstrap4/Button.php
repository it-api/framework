<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Button
 *
 * @author Frank
 */
class Button extends Element {

	/**
	 * 
	 * @param string $caption text for button
	 * @param type $contextual [default,info,success,warning,danger]
	 */
	public function __construct($caption, $contextual = 'primary', $type = 'button') {

		parent::__construct('button');
		$this->type = $type;
		$this->innertext($caption);
        $this->add_class("btn btn-{$contextual}");
	}
	
	function showPreview(){
	    echo new Button('Caption 1');
	}

	function large() {
		$this->add_class('btn-lg');
		return $this;
	}

	function small() {
		$this->add_class('btn-sm');
		return $this;
	}
    
    function addSpinner(string $text = 'Loading...', $position = 'begin'){
        //<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        //<span class="sr-only">Loading...</span>
        
        if($position == 'begin'){
            $loading = new Element('span');
            $loading->innertext($text);
            $loading->add_class('sr-only');
            $this->prepend($loading);
        }
        
        $spinner = new \Barebone\Bootstrap\Element('span');
        $spinner->add_class('spinner-border');
        $spinner->add_class('spinner-border-sm');
        $spinner->role = 'status';
        $spinner->aria('hidden', 'true');
        $this->append($spinner);
        
        if($position == 'end'){
            $loading = new Element('span');
            $loading->innertext($text);
            $loading->add_class('sr-only');
            $this->prepend($loading);
        }
    }

}
