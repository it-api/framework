<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Breadcrumb
 *
 * @author Frank
 */
class Breadcrumb extends Element {

	/**
	 * 
	 * @param string $caption text for crumb
	 * @param type $href link for crumb
     * @param bool $active
	 */
	public function __construct($caption, $href = '#', $active = false) {
		parent::__construct('li');
        $this->add_class('breadcrumb-item');
        
		
		
        if($active === true){
            $this->add_class('active');
            $this->innertext($caption);
        } else {
            $a = new Element('a');        
            $a->href = $href;
            $a->innertext($caption);
            $this->append($a);
        }
        
        
	}
	
	function showPreview(){
	    echo new Breadcrumb('Caption 1', '#', true);
	}

}
