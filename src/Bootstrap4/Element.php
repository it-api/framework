<?php

namespace Barebone\Bootstrap4;

use Barebone\HTML\TElement;

/**
 * Description of Element
 *
 * @author Frank
 */
class Element extends TElement {

	public function __construct($tag, $attributtes = []) {
		parent::__construct($tag, $attributtes);
	}
    
    /**
	 * Muted context
	 * @param string $context [text, bg]
	 * @return $this
	 */
	function btn($color = 'primary') {
		$this->add_class("btn");
        $this->add_class("btn-{$color}");
		return $this;
	}

	/**
	 * Muted context
	 * @param string $context [text, bg]
	 * @return $this
	 */
	function muted($context = 'text') {
		$this->add_class("{$context}-muted");
		return $this;
	}

	/**
	 * Primary context
	 * @param string $context [text, bg]
	 * @return $this
	 */
	function primary($context = 'text') {
		$this->add_class("{$context}-primary");
		return $this;
	}
    
    /**
	 * Secondary context
	 * @param string $context [text, bg]
	 * @return $this
	 */
	function secondary($context = 'text') {
		$this->add_class("{$context}-secondary");
		return $this;
	}

	/**
	 * Success context
	 * @param string $context [text, bg]
	 * @return $this
	 */
	function success($context = 'text') {
		$this->add_class("{$context}-success");
		return $this;
	}

	/**
	 * Info context
	 * @param string $context [text, bg]
	 * @return $this
	 */
	function info($context = 'text') {
		$this->add_class("{$context}-info");
		return $this;
	}

	/**
	 * Warning context
	 * @param string $context [text, bg]
	 * @return $this
	 */
	function warning($context = 'text') {
		$this->add_class("{$context}-warning");
		return $this;
	}

	/**
	 * Danger context
	 * @param string $context [text, bg]
	 * @return $this
	 */
	function danger($context = 'text') {
		$this->add_class("{$context}-danger");
		return $this;
	}
    
    /**
	 * Light context
	 * @param string $context [text, bg]
	 * @return $this
	 */
	function light($context = 'text') {
		$this->add_class("{$context}-light");
		return $this;
	}
    
    /**
	 * Dark context
	 * @param string $context [text, bg]
	 * @return $this
	 */
	function dark($context = 'text') {
		$this->add_class("{$context}-dark");
		return $this;
	}
    
    /**
	 * White context
	 * @param string $context [text, bg]
	 * @return $this
	 */
	function white($context = 'text') {
		$this->add_class("{$context}-white");
		return $this;
	}
    
    /**
	 * Transparent context
	 * @param string $context [text, bg]
	 * @return $this
	 */
	function transparent($context = 'text') {
		$this->add_class("{$context}-transparent");
		return $this;
	}

	/**
	 * Horizontal align element<br>
	 * @param string $alignement [left, center, right, {sm,md,lg,xl}-{$alignment} eg text-sm-left]
	 * @return $this
	 */
	function align($alignement = 'left') {
		$this->add_class("text-{$alignement}");
		return $this;
	}
    /**
     * Vertical alignment<br>
     * Easily change the vertical alignment of inline, inline-block, inline-table, and table cell elements.<br>
     * @param type $alignment
     * Choose from baseline, top, middle, bottom, text-bottom and text-top as needed.
     */
    function valign($alignment = ''){
        $this->add_class("align-{$alignment}");
		return $this;
    }
    /**
     * Text utilities<br>
     * Left aligned text on viewports sized SM (small) or wider.<br>
     * ->text('sm-left')<br>
     * Left aligned text on viewports sized MD (medium) or wider.<br>
     * ->text('md-left')<br>
     * Left aligned text on viewports sized LG (large) or wider.<br>
     * ->text('lg-left')<br>
     * Left aligned text on viewports sized XL (extra-large) or wider.<br>
     * ->text('xl-left')<br>
     * ->text('wrap')<br>
     * ->text('nowrap')<br>
     * ->text('truncate')<br>
     * @param type $util [justify, break, wrap, nowrap, truncate, lowercase, uppercase, capitalize, reset]
     * @return $this
     */
    function text($util){
        $this->add_class("text-{$util}");
		return $this;
    }
    /**
     * 
     * @param string $style [bold, bolder,normal, light, lighter, italic]
     * @return $this
     */
    function font($style){ 
        if($style === 'italic'){
            $this->add_class("font-{$style}");
        } else {
            $this->add_class("font-weight-{$style}");
        }
		return $this;
    }
    /**
     * add or remove underline from element
     * @param string $none
     * @return $this
     */
    function underline($none = ''){
        if($none === 'none'){
            $this->add_class("text-decoration-none");
        } else {
            $this->css('text-decoration','underline');
        }
		return $this;
    }
	/**
	 * Hide element
	 * @return $this
	 */
	function hide() {
        $this->remove_class("show");
		$this->add_class("hidden");
		return $this;
	}

	/**
	 * Show element
	 * @return $this
	 */
	function show() {
        $this->remove_class('hidden');
		$this->add_class("show");
		return $this;
	}

	/**
	 * Add tooltip to the element
	 * @param string $tooltip
	 * @param string $placement [left,top,right,bottom]
	 */
	function tooltip($tooltip, $placement = 'top') {
		// data-toggle="tooltip" data-placement="left" title="Tooltip on left">
		$this->data('toggle', 'tooltip');
		$this->data('placement', $placement);
		$this->title = $tooltip;
        return $this;
	}

	function popover($title = '', $content = '', $placement = 'top') {
		//data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus faucibus."
		$this->data('container', 'body');
		$this->data('toggle', 'popover');
		$this->data('placement', $placement);
		$this->data('title', $title);
		$this->data('content', $content);
        return $this;
	}
    /**
     * Add border around element
     * @param string $border empty for all borders, top, left, bottom, right
     * @param string $color [primary,secondary,info,success,warning,danger,light,dark,white]
     */
    function border(string $border = '', string $color = ''){
        if(strstr($border, ',')){
            $borders = explode(',', $border);
            foreach($borders as $usedborder){
                $classname = "border-{$usedborder}";
                $this->add_class($classname);  
            }
        } else {
            $classname = $border == '' ? 'border' : "border-{$border}";
            $this->add_class($classname);  
        }
        if($color != ''){            
            $this->add_class("border-{$color}");
        }
        
        return $this;
    }
    /**
     * Add rounded corners to an element
     * @param string $rounded [top,right,bottom,left,circle,pill,0]
     * @param string $size [sm,lg] for larger or smaller border-radius
     */
    function roundCorners(string $rounded = '', string $size = ''){
        if(strstr($rounded, ',')){
            $borders = explode(',', $rounded);
            foreach($borders as $usedborder){
                $classname = "rounded-{$usedborder}";
                $this->add_class($classname);  
            }
        } else {
            $classname = $rounded == '' ? 'rounded' : "rounded-{$rounded}";        
            $this->add_class($classname);
        }
        
        if($size != ''){
            $this->add_class("rounded-{$size}"); 
        }
        return $this;
    }
    /**
     * Where sides is one of:<br>
        t - for classes that set margin-top or padding-top<br>
        b - for classes that set margin-bottom or padding-bottom<br>
        l - for classes that set margin-left or padding-left<br>
        r - for classes that set margin-right or padding-right<br>
        x - for classes that set both *-left and *-right<br>
        y - for classes that set both *-top and *-bottom<br>
        blank - for classes that set a margin or padding on all 4 sides of the element<br>
     * @param int $padding
     * @param string $sides
     * @return $this
     */
    function padding($padding, string $sides = ''){
        
        if($padding == 'auto'){            
        }elseif($padding < 0 || $padding > 5){
            die('Padding needs to be number from 0 to 5 or auto');
        }
        
        if($sides == ''){
            $this->add_class('p-'.$padding);
        } else {            
            $this->add_class("p{$sides}-".$padding);
        }
        return $this;
    }
    /**
     * Put left padding on element
     * @param int $padding
     * @return $this
     */
    function paddingLeft(int $padding){
        $this->padding($padding, 'l');        
        return $this;
    }
    /**
     * Put right padding on element
     * @param int $padding
     * @return $this
     */
    function paddingRight(int $padding){
        $this->padding($padding, 'r');        
        return $this;
    }
    /**
     * Put bottom padding on element
     * @param int $padding
     * @return $this
     */
    function paddingBottom(int $padding){        
        $this->padding($padding, 'b');        
        return $this;
    }
    /**
     * Put top padding on element
     * @param int $padding
     * @return $this
     */
    function paddingTop(int $padding){
        $this->padding($padding, 't');        
        return $this;
    }
    /**
     * Put left and right padding on element
     * @param int $padding
     * @return $this
     */
    function paddingX(int $padding){
        $this->padding($padding, 'x');        
        return $this;
    }
    /**
     * Put top and bottom padding on element
     * @param int $padding
     * @return $this
     */
    function paddingY(int $padding){
        $this->padding($padding, 'y');        
        return $this;
    }
    /**
     * Where sides is one of:<br>
        t - for classes that set margin-top or padding-top<br>
        b - for classes that set margin-bottom or padding-bottom<br>
        l - for classes that set margin-left or padding-left<br>
        r - for classes that set margin-right or padding-right<br>
        x - for classes that set both *-left and *-right<br>
        y - for classes that set both *-top and *-bottom<br>
        blank - for classes that set a margin or padding on all 4 sides of the element<br>

     * @param type $margin
     * @param type $sides
     * @return $this
     * ->margin(3)
     * ->margin(3, 't')
     */
    function margin($margin, string $sides = ''){
        if($margin == 'auto'){            
        }elseif($margin < 0 || $margin > 5){
            die('Margin needs to be number from 0 to 5 or auto');
        }
        if($sides == ''){
            $this->add_class('m-'.$margin);
        } else {            
            $this->add_class("m{$sides}-".$margin);
        }
        return $this;
    }
    /**
     * Put left margin on element
     * @param int $margin
     * @return $this
     */
    function marginLeft(int $margin){       
        $this->margin($margin, 'l');        
        return $this;
    }
    /**
     * Put right margin on element
     * @param int $margin
     * @return $this
     */
    function marginRight(int $margin){       
        $this->margin($margin, 'r');        
        return $this;
    }
    /**
     * Put bottom margin on element
     * @param int $margin
     * @return $this
     */
    function marginBottom(int $margin){       
        $this->margin($margin, 'b');        
        return $this;
    }
    /**
     * Put bottom margin on element
     * @param int $margin
     * @return $this
     */
    function marginTop(int $margin){       
        $this->margin($margin, 't');        
        return $this;
    }
    /**
     * Put left and right margin on element
     * @param int $margin
     * @return $this
     */
    function marginX(int $margin){       
        $this->margin($margin, 'x');        
        return $this;
    }
    /**
     * Put top and bottom margin on element
     * @param int $margin
     * @return $this
     */
    function marginY(int $margin){       
        $this->margin($margin, 'y');        
        return $this;
    }
    /**
     * Position the element
     * @param type $position [static,relative,absolute,fixed,sticky]
     * @return $this
     */
    function position($position){
        $this->add_class('position-'.$position);
        return $this;
    }
    
    function stickyTop(){
        $this->add_class('sticky-top');
        return $this;
    }

    function fixTop(){
        $this->add_class('fixed-top');
        return $this;
    }
    
    function fixBottom(){
        $this->add_class('fixed-bottom');
        return $this;
    }
    
    function centered($width = null){
        $this->add_class('mx-auto');
        if(!is_null($width)){
            $this->css('width', $width);
            $this->css('display', 'block');
        }
        return $this;
        
        //class="mx-auto" style="width: 200px;
    }
    /**
	 * visible can be used to toggle only the visibility of an element, 
	 * meaning its display is not modified and the element can still affect the flow of the document.
	 * @return $this
	 */
    function visible(){
        $this->add_class('visible');
        $this->remove_class('invisible');
        return $this;
    }
    /**
	 * invisible can be used to toggle only the visibility of an element, 
	 * meaning its display is not modified and the element can still affect the flow of the document.
	 * @return $this
	 */
    function invisible(){
        $this->add_class('invisible');
        $this->remove_class('visible');
        return $this;
    }
    /**
     * Width relative to the parent<br>
     * Includes support for 25%, 50%, 75%, 100%, and auto by default.<br>
     * Modify those values as you need to generate different utilities here.<br>
     * @param type $width
     * @return $this
     */
    function width($width){
        $this->add_class('w-' . $width);
        return $this;
    }
    /**
     * Height relative to the parent<br>
     * Includes support for 25%, 50%, 75%, 100%, and auto by default.<br>
     * Modify those values as you need to generate different utilities here.<br>
     * @param type $height
     * @return $this
     */
    function height($height){
        $this->add_class('h-' . $height);
        return $this;
    }
    /**
     * Add a shadow around an element
     * @param string $type [none, sm, lg]
     * @return $this
     */
    function shadow(string $type = ''){
        $classname = $type === '' ? 'shadow' : "shadow-{$type}";
        $this->add_class($classname);
        return $this;
    }
    
    function bg($context){
        $this->add_class('bg-' . $context);
        return $this;
    }
    /**
     * Here are all the support classes;<br>
     * left<br>
     * right<br>
     * none<br>
     * sm-left<br>
     * sm-right<br>
     * sm-none<br>
     * md-left<br>
     * md-right<br>
     * md-none<br>
     * lg-left<br>
     * lg-right<br>
     * lg-none<br>
     * xl-left<br>
     * xl-right<br>
     * xl-none<br>
     * @param type $float
     * @return $this
     */
    function float($float){        
        $this->add_class('float-' . $float);
        return $this;
    }
    
    function animation($animation, $time){
        $this->add_class($animation);
        $this->add_class('show');
        //$this->data('animation-' . $animation, 'true');
        //$this->data('delay', $time);
    }
    
    function addHelp($text, $contextual = 'muted', $id = null){
        
        //<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        
        $small = new Element('small');
        $small->innertext($text);
        if(!is_null($id)){        
            $small->id = $id;
        }
        $small->add_class('form-text text-' . $contextual);
        $this->append($small);
    }
        
    function addError(string $error){
        //<div class="invalid-feedback">Error message here...</div>
        $invalid = new Element('div');
        $invalid->add_class('invalid-feedback');
        $invalid->innertext($error);
        $this->append($invalid);
    }
}
