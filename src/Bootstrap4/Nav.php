<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Nav
  <ul class="nav nav-tabs">
  <li class="active"><a href="#">Home</a></li>
  <li><a href="#">Profile</a></li>
  <li><a href="#">Messages</a></li>
  </ul>

 * @author Frank
 */
class Nav extends Element {

	public function __construct() {
		parent::__construct('ul');
        $this->add_class('nav');
	}
    
    function showPreview(){
        echo new Nav();
    }
    
	/**
	 * Nav as tabs Add nav-tabs class
	 */
	function tabs() {
        if( ! $this->class_exists('nav'))
            $this->add_class('nav');
		$this->add_class('nav-tabs');
		return $this;
	}

	/**
	 * Nav as pills Add nav-pills class
	 */
	function pills() {
		if( ! $this->class_exists('nav'))
            $this->add_class('nav');
		$this->add_class('nav-pills');
		return $this;
	}

	/**
	 * Add nav-justified class
	 * @return $this
	 */
	function justified() {
		$this->add_class('nav-justified');
		return $this;
	}
    
    function justifyCentered(){
        $this->add_class('justify-content-center');
    }
    
    function justifyRight(){
        $this->add_class('justify-content-end');
    }
    
    function vertical(){
        $this->add_class('flex-column');
    }
    
    function fill(){
        $this->add_class('nav-fill');
    }

	/**
	 * Add a nav item
	 * @param string $caption text for the item
	 * @param string $href the link for the navitem
	 * @param bool $active is the item active
	 * @param string|Badge $badge string or a badge object
	 */
	function addItem($caption, $href = '#', $active = false, $badge = null) {
		$li = new Element('li');
		if ($active === true) {
			$li->class = 'active';
		}

		$a = new Element('a');
		$a->innertext($caption);
		$a->href = $href;

		if (!is_null($badge) && !$badge instanceof Badge) {
			$b = new Badge($badge);
			$a->append($b);
		} elseif (!is_null($badge) && $badge instanceof Badge) {
			$a->append($badge);
		}

		$li->append($a);
		$this->append($li);
		return $this;
	}

	/**
	 * Add a form to the nav element
	 * @param \Barebone\Bootstrap\Form $form
	 * @return $this
	 */
	function addForm(Form $form) {
		$form->add_class('navbar-form');
		$this->append($form);
		return $this;
	}

	function addDropDown($caption, Dropdown $dropdown) {
		// <li class="nav-item dropdown">
        //<a class="nav-link dropdown-toggle" data-toggle="true" href="" role="button" aria-haspopup="true" aria-expanded="false">DD</a>
        //<div class="dropdown-menu">
        //<a class="dropdown-item" href="#">Action 1</a>
        //<a class="dropdown-item" href="#">Action 2</a>
        //<a class="dropdown-item" href="#">Action 3</a>
        //</div>
        //</li>
		$li = new Element('li');
		$li->class = 'nav-item dropdown';
		$a = new Element('a');
		$a->class = "nav-link dropdown-toggle";
		$a->data('toggle', 'dropdown');
		$a->innertext($caption);
		$a->href = '#';
        $a->role= 'button';
        $a->aria('haspopup', 'true');
        $a->aria('expanded', 'false');
        $li->append($a);
        $this->append($li);
     
		// dropdown items
		$newdropdown = new Element('div');
		$newdropdown->class = 'dropdown-menu';
		foreach ($dropdown->getMenu()->get_children() as $child) {
            //print_r($child);
			//$dditem = new Element('li');
            
			//$link = new Element('a');
            $child->add_class('dropdown-item');
			//$link->href = (string) $child->href;
			//$link->innertext($child->innertext);
			//$dditem->append($link);
			$newdropdown->append($child);
		}
		$li->append($newdropdown);

		
	}

}
