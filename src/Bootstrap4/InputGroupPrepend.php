<?php

namespace Barebone\Bootstrap4;

/**
 * Description of InputGroupPrepend
 *
 * @author Frank
 */
class InputGroupPrepend  extends Element{
    
    public function __construct($item, $id = null) {
        
        parent::__construct('div');
        $this->add_class('input-group-prepend');
        
        if(is_string($item)){            
            $span = new Element('span');  
            $span->id =$id;
            $span->add_class('input-group-text');
            $span->innertext($item);
            $this->append($span);
        } else if($item instanceof Element){
            $this->append($item);
        }
    }
    
    function showPreview(){
	    echo new InputGroupPrepend('Group prepend');
	}
}
