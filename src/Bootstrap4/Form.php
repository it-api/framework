<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Form
 *
 * @author Frank
 */
class Form extends Element {
    /**
	 * Array that will hold the rows
	 * @var type 
	 */
    private $rows = [];
	/**
	 * 
	 * @param type $attributes
	 */
	function __construct(array $attributes = []) {
		parent::__construct('form', $attributes);
		$this->role = 'form';
	}
	
	function showPreview(){
	    echo new Form();
	}

	/**
	 * Set the form to inline
	 * @return $this
	 */
	function inline() {
		$this->add_class('form-inline');
		return $this;
	}

	/**
	 * Set the form to horizontal
	 * @return $this
	 */
	function horizontal() {
		$this->add_class('form-horizontal');
		return $this;
	}

	/**
	 * Add a formgroup
	 * @param \Barebone\Bootstrap\FormGroup $formgroup
	 * @return \Barebone\Bootstrap\FormGroup
	 */
	function addFormGroup($formgroup = []) {
		if (empty($formgroup)) {
			$formgroup = new FormGroup();
		}
		$this->append($formgroup);
		return $formgroup;
	}
    
    function addRow(){
        
        $row = new Element('div');
        $row->add_class('row');
        $this->append($row);
        $this->rows[] = $row;
        
        return $row;
    }

    function addColumn(Element $row, $size = null, $element = null){
        
        $column = new Element('div');
        if(!is_null($size)){
            $column->add_class('col-'. $size);
        } else {
            $column->add_class('col');
        }
        if(!is_null($element)){
            $column->append($element);
        }
        $row->append($column);
        return $column;
    }
    
    function addToColumn(Element $column, Element $element){
        $column->append($element);
        return $this;
    }
    
    function prependToColumn(Element $column, Element $element){
        $column->prepend($element);
        return $this;
    }    
    
    function needsValidation(){
        $this->add_class('needs-validation');
        $this->novalidate = '';
        $this->append(new Form\HTML5ValidationScript());
    }
}
