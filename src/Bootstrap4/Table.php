<?php

namespace Barebone\Bootstrap4;

use Barebone\HTML\TTable;

/**
 * Description of Table
 *
 * @author Frank
 */
class Table extends TTable {

	public function __construct(array $attributes = []) {
		parent::__construct($attributes);
		$this->class = 'table';
	}
	
	function showPreview(){
	    echo new Table();
	}

	function stripped() {
		$this->add_class('table-stripped');
		return $this;
	}

	function condensed() {
		$this->add_class('table-condensed');
		return $this;
	}

	function bordered() {
		$this->add_class('table-bordered');
		return $this;
	}

	function hover() {
		$this->add_class('table-hover');
		return $this;
	}

}
