<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Accordion
 *
 * @author Frank
 */
class Accordion extends Element{
    
    public function __construct() {
        
        parent::__construct('div');
        $this->add_class('accordion');

    }
    
    function showPreview(){
	    echo new Accordion();
	}
    
    function addCard(Card $card){
        $this->append($card);
        return $this;
    }

}