<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Dropdown
  <div class="dropdown">
  <button class="btn dropdown-toggle sr-only" type="button" id="dropdownMenu1" data-toggle="dropdown">
  Dropdown
  <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
  <li role="presentation" class="divider"></li>
  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
  </ul>
  </div>

 *
 * @author Frank
 */
class Dropdown extends Element {

	private $_id;
	private $button;
	private $menu;

	/**
	 * 
	 * @param string $caption
	 * @param string $contextual [info,primary,success,warning,danger]
	 */
	public function __construct($caption = '', $contextual = 'default') {

		// create unique id
		$this->_id = uniqid('ddbtn');

		parent::__construct('div');
		$this->add_class('dropdown');

		$this->button = new Element('button');
		$this->button->add_class("btn btn-$contextual dropdown-toggle");
		$this->button->type = 'button';
		$this->button->id = $this->_id;
		$this->button->data('toggle', 'dropdown');
		$this->button->innertext($caption);
		$this->button->append(new Element('span', ['class' => 'caret']));

		$this->append($this->button);

		$this->menu = new Element('ul');
		$this->menu->class = 'dropdown-menu';
		$this->menu->role = 'menu';
		$this->menu->aria('labelledby', $this->_id);

		$this->append($this->menu);
		
	}
	
	function showPreview(){
	    $dropdown = new Dropdown('Dropdown', 'info');
	    $dropdown->addItem('Item 1');
	    $dropdown->addItem('Item 2');
	    $dropdown->addItem('Item 3');
	    echo $dropdown;
	}

	/**
	 * Add item to dropdown menu.
	 * @param string $caption text on the item
	 * @param type $href link for the item
	 */
	function addItem($caption, $href = '#', bool $active = false, bool $disabled = false) {

		$a = new Element('a');
		$a->innertext($caption);
		$a->href = $href;
        $a->add_class('dropdown-item');

        if($active === true){
            $this->add_class('active');
        }
        
        if($disabled === true){
            $this->add_class('disabled');
        }

		$this->menu->append($a);
		return $this;
        
	}
    
    function addDivider(){
        //<div class="dropdown-divider"></div>
        $divider = new Element('div');
        $divider->add_class('dropdown-divider');
        $this->append($divider);
    }

	/**
	 * 
	 * @return $this
	 */
	function large() {
		$this->add_class('btn-group-lg');
		return $this;
	}

	/**
	 * 
	 * @return $this
	 */
	function small() {
		$this->add_class('btn-group-sm');
		return $this;
	}

	/**
	 * Return as btn-group
	 * @return $this
	 */
	function asBtnGroup() {
		$this->class = 'btn-group';
		return $this;
	}

	function dropup() {
		$this->add_class('dropup');
	}
    
    public function getMenu() {
        return $this->menu;
    }

    public function setMenu($menu) {
        $this->menu = $menu;
        return $this;
    }
    
    public function getButton() {
        return $this->button;
    }

    public function setButton($button) {
        $this->button = $button;
        return $this;
    }



}
