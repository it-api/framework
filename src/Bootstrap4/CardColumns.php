<?php

namespace Barebone\Bootstrap4;

/**
 * Description of CardColumns
 *
 * @author Frank
 */
class CardColumns extends Element{
    
    public function __construct() {
        parent::__construct('div');
        $this->add_class('card-columns');
    }
    
    function showPreview(){
        
	    $cc = new CardColumns();
	    
	    $img = new Image('/images/100-procent-service-van-de-Vastelastenbond.png');
        $link = new Link('Caption link', '#');
	    $card = new Card('Card title', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>', $img, $link);
	    $card->css('width', '200px');
	    $card->card_body->css('color','black');
	    $card->addHeader('Card header')->css('color', 'green');
	    $card->addFooter('Card footer')->css('color', 'red');
	    $card->margin(3);
	    $cc->addCard($card);
	    
	    $img = new Image('/images/100-procent-service-van-de-Vastelastenbond.png');
        $link = new Link('Caption link', '#');
	    $card = new Card('Card title', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>', $img, $link);
	    $card->css('width', '200px');
	    $card->card_body->css('color','black');
	    $card->addHeader('Card header')->css('color', 'green');
	    $card->addFooter('Card footer')->css('color', 'red');
	    $card->margin(3);
	    $card->marginLeft(5);
	    $cc->addCard($card);
	    
	    echo $cc;
	    
	}
    
    function addCard(Card $card){
        $this->append($card);
        return $this;
    }

}