<?php

namespace Barebone\Bootstrap4\Navbar;

/**
 * Description of NavbarToggler
 *<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
 * @author Frank
 */
class Toggler extends \Barebone\Bootstrap4\Element{
    
    public function __construct() {
        
        parent::__construct('button');
        $this->add_class('navbar-toggler');
        $this->type = 'button';
        $this->data('toggle', 'collapse');
        $this->data('target', '#navbarSupportedContent');
        $this->aria('controls','navbarSupportedContent');
        $this->aria('expanded','false');
        $this->aria('label','Toggle navigation');
        
        $span = new \Barebone\Bootstrap4\Element('span');
        $span->add_class('navbar-toggler-icon');
        
        $this->append($span);
    }
}
