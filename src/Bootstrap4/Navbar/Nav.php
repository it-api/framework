<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Bootstrap4\Navbar;

/**
 * Description of navbarNav
 *<ul class="navbar-nav mr-auto"></ul>
 * @author Frank
 */
class Nav extends \Barebone\Bootstrap4\Element{
    
    public function __construct() {
        
        parent::__construct('ul');
        $this->add_class('navbar-nav');
        $this->add_class('mr-auto');
    }
}
