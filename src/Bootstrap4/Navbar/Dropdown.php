<?php

namespace Barebone\Bootstrap4\Navbar;

/**
 * Description of Dropdown
 *
 * @author Frank
 */
class Dropdown extends \Barebone\Bootstrap4\Element{
    
    public function __construct(\Barebone\Bootstrap4\Dropdown $dropdown) {
        
        parent::__construct('li');
        $this->add_class('nav-item');
        $this->add_class('dropdown');
        
        $link = new \Barebone\Bootstrap4\Element('a');
        $link->add_class('nav-link');
        $link->add_class('dropdown-toggle');
        $link->href = $dropdown->getButton()->href;
        $link->role = 'button';
        $link->data('toggle','dropdown');
        $link->aria('haspopup', 'true');
        $link->aria('expanded', 'false');
        $link->innertext($dropdown->getButton()->innertext());
        $this->append($link);
        
        $dropdownmenu = new \Barebone\Bootstrap4\Element('div');
        $dropdownmenu->add_class('dropdown-menu');
        $dropdownmenu->aria('labelledby', 'navbarDropdown');
        foreach($dropdown->getMenu()->get_children() as $child){
            $dropdownmenu->append($child);
        }
        $this->append($dropdownmenu);

    }
}
