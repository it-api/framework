<?php

namespace Barebone\Bootstrap4\Navbar;
/**
 * Description of NavbarBrand
 * <a class="navbar-brand" href="#">Navbar</a>
 * @author Frank
 */
class Brand extends \Barebone\Bootstrap4\Element{
    
    public function __construct(string $text = 'Navbar', string $href = '#') {
        
        parent::__construct('a');
        $this->add_class('navbar-brand');
        $this->innertext($text);
        $this->href = $href;
    }
}
