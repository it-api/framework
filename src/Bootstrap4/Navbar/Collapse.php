<?php

namespace Barebone\Bootstrap4\Navbar;

/**
 * Description of Collapse
 * <div class="collapse navbar-collapse" id="navbarSupportedContent"></div>
 * @author Frank
 */
class Collapse  extends \Barebone\Bootstrap4\Element{
    
    public function __construct() {
        
        parent::__construct('div');
        $this->add_class('collapse');
        $this->add_class('navbar-collapse');
        $this->id = 'navbarSupportedContent';
    }
}
