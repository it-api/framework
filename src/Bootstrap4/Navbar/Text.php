<?php

namespace Barebone\Bootstrap4\Navbar;

/**
 * Description of NavbarText
 * <span class="navbar-text">Navbar text with an inline element</span>
 * @author Frank
 */
class Text extends \Barebone\Bootstrap4\Element{
    
    public function __construct(string $text) {
        
        parent::__construct('span');
        $this->add_class('navbar-text');
        $this->innertext($text);
        
    }
}
