<?php

namespace Barebone\Bootstrap4\Navbar;

/**
 * Description of NavbarItem
 *<li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
 * @author Frank
 */
class Item extends \Barebone\Bootstrap4\Element{
    
    public function __construct(string $text = 'Link', string $href = '#') {
        
        parent::__construct('li');
        $this->add_class('nav-item');
        
        $link = new \Barebone\Bootstrap4\Element('a');
        $link->add_class('nav-link');
        $link->innertext($text);
        $link->href = $href;
        
        $this->append($link);
    }
}
