<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Breadcrumbs
 *
 * @author Frank
 */
class Breadcrumbs extends Element {

	/**
	 * Breadcrumbs Constructor
	 * @param array $crumbs array of [caption => '#']
	 */
	public function __construct($crumbs = []) {
		parent::__construct('ul');
		$this->add_class('breadcrumb');
		if (!empty($crumbs)) {
			foreach ($crumbs as $caption => $href) {
				$this->addCrumb($caption, $href);
			}
		}
	}
    
    function showPreview(){
	    echo new Breadcrumbs(['caption1'=>'#', 'caption2'=>'#', 'caption3'=>'#']);
	}
	
	/**
	 * Adds a crumb to the breadcrumbs
	 * @param string $caption
	 * @param string $href
     * @param bool $active
	 */
	function addCrumb($caption, $href, $active = false) {
		$this->append(new Breadcrumb($caption, $href, $active));
	}

}
