<?php

namespace Barebone\Bootstrap4;

/**
 * Description of CardDeck
 *
 * @author Frank
 */
class CardDeck extends Element{
    
    public function __construct() {
        parent::__construct('div');
        $this->add_class('card-deck');
    }
    
    function showPreview(){
	    echo new CardDeck();
	}
    
    function addCard(Card $card){
        $this->append($card);
        return $this;
    }

}