<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Well
 *
 * @author Frank
 */
class Well extends Element {

	public function __construct($content) {
		parent::__construct('div');
		$this->add_class('well');
		$this->innertext($content);
	}
    
    function showPreview(){
	    echo new Well('<p>Well, well, well!</p>');
	}
	
	function large() {
		$this->add_class('well-lg');
	}

	function small() {
		$this->add_class('well-sm');
	}

}
