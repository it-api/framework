<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Progress
  <div class="progress">
  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
  <span class="sr-only">60% Complete</span>
  </div>
  </div>

 *
 * @author Frank
 */
class Progress extends Element {

	private $value = 0;
	private $valuemin = 0;
	private $valuemax = 100;
    /**
	 *
	 * @var Barebone\HTML\TElement
	 */
	private $bar;
	/**
	 *
	 * @var Barebone\HTML\TElement
	 */
	private $complete;
    
	/**
	 * 
	 * @param int $value
	 * @param int $valuemin
	 * @param int $valuemax
	 * @param string $contextual [info,success,warning,danger]
	 */
	public function __construct(int $value, int $valuemin = 0, int $valuemax = 100, string $contextual = null) {

		$this->value = $value;
		$this->valuemin = $valuemin;
		$this->valuemax = $valuemax;

		parent::__construct('div');
		$this->class = 'progress';

		$this->bar = new Element('div');
		$this->bar->class = 'progress-bar';
		$this->bar->css('width', $this->value . '%');
		$this->bar->role = 'progressbar';
		$this->bar->aria('valuenow', $this->value);
		$this->bar->aria('valuemin', $this->valuemin);
		$this->bar->aria('valuemax', $this->valuemax);

		if (!is_null($contextual)) {
            $this->bar->add_class("bg-{$contextual}");
		}

		$this->complete = new Element('span');
		$this->complete->class = 'sr-only';
		$this->complete->innertext($this->value . '% Complete');
		// building it togheter

		$this->bar->append($this->complete);
		$this->append($this->bar);
	}
	
	function showPreview(){
        echo new Progress(10,0, 100);
    }
    
    public function getBar(): Element {
        return $this->bar;
    }

    public function getComplete(): Element {
        return $this->complete;
    }

    public function setBar(Element $bar) {
        $this->bar = $bar;
        return $this;
    }

    public function setComplete(Element $complete) {
        $this->complete = $complete;
        return $this;
    }

    
	/**
	 * Show a label in the progress bar
	 * @return $this
	 */
	function show_label() {
		$this->complete->remove_class('sr-only');
		return $this;
	}

	/**
	 * Stripped progress bar
	 * @return $this
	 */
	function striped() {
		$this->getBar()->add_class('progress-bar-striped');
		return $this;
	}

	/**
	 * Animated progress bar
	 * @return $this
	 */
	function animated() {
		$this->getBar()->add_class('progress-bar-animated');
		return $this;
	}

}
