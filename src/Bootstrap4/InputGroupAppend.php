<?php

namespace Barebone\Bootstrap4;

/**
 * Description of InputGroupAppend
 *
 * @author Frank
 */
class InputGroupAppend  extends Element{
    
    public function __construct($item, $id = null) {
        
        parent::__construct('div');
        $this->add_class('input-group-append');
        
        if($item instanceof Element){
            $this->append($item);
        } elseif(is_string($item)){            
            $span = new Element('span');
            $span->id =$id;
            $span->add_class('input-group-text');
            $span->innertext($item);
            $this->append($span);
        }

    }
    
    function showPreview(){
	    echo new InputGroupAppend('Group append');
	}
}
