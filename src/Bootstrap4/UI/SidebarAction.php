<?php

namespace Barebone\Bootstrap4\UI;

class SidebarAction{
    
    public string $caption = '';
    public string $icon = '';
    public string $callback = '';
    public ?SidebarActionSlideOut $slideout;
    
    public function __construct($caption, $icon, $callback, SidebarActionSlideOut $slideout = null){
        
        $this->caption = $caption;
        $this->icon = $icon;
        $this->callback = $callback;
        $this->slideout = $slideout;
        
    }
    
}