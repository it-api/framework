<?php

namespace Barebone\Bootstrap4\UI;
use \Barebone\Bootstrap4\Element;

class SidebarActionSlideOut{
    
    public string $id;
    public string $title = '';
    public string $width = '';
    private array $toolbar = []; // array of action
    private array $items = []; // array of Element
    private $slideout;
    public function __construct($title, $width = '40%'){
        $this->id = uniqid('slideout-');
        $this->title = $title;
        $this->width = $width;
        
        $slideout = new Element('div', ['class'=>'sidebar-action-slideout', 'style'=>"width: {$this->width}"]);
        $slideout->id = $this->getId();
        $slideout->css('display','none');
        $this->slideout = $slideout;
    }
    
    public function addToolbarItem(string $icon, string $callback = "alert('clicked')"){
        $this->toolbar[] = new SlideOutToolbarItem($icon, $callback);
        return $this;
    }
    
    function addItem(Element $item){
        $this->items[] = $item;
        return $this;
    }
    
    function show(){
        echo "<script>$( '#".$this->id."' ).show( 'slide' , { direction: 'right' }, 150);</script>";
    }
    
    function getId(){
        return $this->id;
    }
    
    function getCallback(){ 
        return "$( '#".$this->id."' ).show( 'slide' , { direction: 'right' }, 150);return false;";
    }
    
    public function render(){
        
        
        
        $closeBtn = new Element('a');
        $closeBtn->add_class('sidebar-action-slideout-close');
        $closeBtn->href = '#close';
        $closeBtn->on('click', "$('#".$this->getId()."').hide( 'slide' , { direction: 'right' }, 150); return false;");
        $this->slideout->append($closeBtn);
        
        $title = new Element('div', ['class'=>'sidebar-action-slideout-title']);
        $title->innertext($this->title);
        $this->slideout->append($title);
        
        $toolbar = new Element('div', ['class'=>'sidebar-action-slideout-toolbar']);
        foreach($this->toolbar as $tbitem){
            $item = new Element('div', ['class'=>'slideout-toolbar-button']);
            $icon = new Element('div', ['class'=>'slideout-toolbar-icon']);
            $icon->add_class($tbitem->icon);
            $callback  = new Element('a');
            $callback->href='#';
            $callback->on('click', $tbitem->callback);
            $callback->append($icon);
            $item->append($callback);
            $toolbar->append($item);
        }
        $this->slideout->append($toolbar);
        
        foreach($this->items as $item){
            
            //$soitem = new Element('div', ['class'=>'slideout-item']);
            $this->slideout->append($item);
        }
        $this->slideout->render();
    }
    
}