<?php

namespace Barebone\Bootstrap4\UI;

class SlideOutToolbarItem{
    
    public string $icon = '';
    public string $callback = '';

    public function __construct($icon, $callback){
        $this->icon = $icon;
        $this->callback = $callback;
    }
}