<?php

namespace Barebone\Bootstrap4\UI;
use \Barebone\Bootstrap4\Element;

class Sidebar{
    
    public string $title = '';
    public string $width = '';
    public $position = 'right'; // right or left
    private array $actions = []; // array of action
    protected Element $element;
    
    public function __construct($title, $width = '260px'){
        $this->title = $title;
        $this->width = $width;
    }
    
    public function addAction(string $caption, string $icon, string $callback = "alert('clicked')", SidebarActionSlideOut $slideout = null){
        $this->actions[] = new SidebarAction($caption, $icon, $callback, $slideout);
    }
    
    public function render(){
        // main wrapper
        $this->element = new Element('div', ['class'=>'sidebar', 'style'=>'width:'.$this->width]);
        // title element
        $titleElement = new Element('div', ['class'=>'sidebar-title']);
        $titleElement->innertext($this->title);
        $this->element->append($titleElement);
        $actionsElement = new Element('div', ['class'=>'sidebar-actions']);
        foreach($this->actions as $action){
            
            $actionElement = new Element('div', ['class'=>'sidebar-action']);
            
            $actionElementIcon = new Element('div', ['class'=>'sidebar-action-icon']);
            $actionElementIcon->add_class($action->icon);
            
            $actionElementLink = new Element('a', ['class'=>'sidebar-action-link']);
            $actionElementLink->innertext($action->caption);
            $actionElementLink->href='#';
            
            $actionElementLink->append($actionElementIcon);
            $actionElement->append($actionElementLink);
            $actionsElement->append($actionElement);
            
            // if there is a slideout
            if($action->slideout instanceof SidebarActionSlideOut){
                $action->slideout->render();
            } 
            $actionElementLink->on('click', $action->callback);
        }
        $this->element->append($actionsElement);
        
        $this->element->render();
    }
}