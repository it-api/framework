<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Link
 *
 * @author Frank
 */
class Link extends Element {

	public function __construct(string $caption, string $href, array $attributes = []) {
		parent::__construct('a', $attributes);
		$this->innertext($caption);
		$this->href = $href;
	}
    
    function showPreview(){
        echo new Link('Caption', '#link');
    }
    
	function addBadge($text) {
		$this->append(new Badge($text));
		return $this;
	}

}
