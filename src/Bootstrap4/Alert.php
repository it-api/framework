<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Alert
 *
 * @author Frank
 */
class Alert extends Element {
    
    const INFO = 'info';
    const SUCCESS = 'success';
    const WARNING = 'warning';
    const DANGER = 'danger';
    const PRIMARY = 'primary';
    const SECONDARY = 'secondary';
    const LIGHT = 'light';
    const DARK = 'dark';
    
    /**
	 * Constructor of the alert message
	 * @param string $message
	 * @param string $type [info,success,waring,danger]
	 */
	public function __construct($message = '', $type = self::INFO, $dismissable = false) {
		parent::__construct('div');
		if ($message !== '') {
            
            $this->role = 'alert';
            
			if ($dismissable === true) {
				$this->dismissable();
			}
			$this->innertext($message);
			$this->class = "alert alert-$type";
		}
	}
	
	function showPreview(){
	    echo new Alert('Test message', self::INFO, true);
	}

	function dismissable() {
		//<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		$btn = new Element('button');
		$btn->class = 'close';
		$btn->data('dismiss', 'alert');
		$btn->{'aria-label'} = 'Close';
		$btn->innertext('&times;');
		$this->append($btn);
		$this->add_class('alert-dismissible');
		return $this;
	}
    
    function fade(){
        $this->add_class('fade');
        return $this;
    } 
    
    function show(){
        $this->add_class('show');
        return $this;
    }
}
