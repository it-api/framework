<?php


namespace Barebone\Bootstrap4;

/**
 * Description of Media
 *
 * @author Frank
 */
class Media extends Element {
    
    public function __construct($body, Image $img = null, $img_position = 'Before') {
        
        parent::__construct('div');
        $this->add_class('media');
        if(!is_null($img) && $img_position == 'Before'){
            $this->append($img);
        }
        
        $bodyelement = new Element('div');
        $bodyelement->add_class('media-body');
        $bodyelement->innertext($body);
        
        $this->append($bodyelement);
        
        if(!is_null($img) && $img_position == 'After'){
            $this->append($img);
        }
    }
    
    function showPreview(){
        $img = new Image('/images/edit.png');
        $media = new Media('Media body', $img);
        echo $media;
    }

}
