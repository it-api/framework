<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Label
 *
 * @author Frank
 */
class Label  extends Element{
    
    public function __construct($text) {
        
        parent::__construct('label');
        $this->innertext($text);

    }
    
    function showPreview(){
        echo new Label('Label');
    }
    
    function large(){
        //col-form-label-sm/
        $this->add_class('col-form-label-lg');
    }
    
    function small(){
        //col-form-label-sm/
        $this->add_class('col-form-label-sm');
    }
}
