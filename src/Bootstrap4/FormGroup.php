<?php

namespace Barebone\Bootstrap4;

use Barebone\HTML\TElement;

/**
 * Description of FormGroup
 *
 * @author Frank
 */
class FormGroup extends Element {

	/**
	 *
	 * @var Barebone\HTML\TElement 
	 */
	private $label;

	/**
	 *
	 * @var Barebone\HTML\TElement 
	 */
	private $input;
	private $error;

	/**
	 * 
	 * @param string $caption text on the label
	 * @param string $input_name name of the input
	 * @param array $input_attributes for input
	 */
	function __construct($caption = null, $input_name = null, $input_attributes = []) {

		parent::__construct('div');
		$this->add_class('form-group');
		if (!is_null($caption)) {
			// create the label
			$label = new \Barebone\HTML\TLabel();
			$label->add_class('control-label');
			// set the caption
			$label->innertext($caption);
			// if id isset then add for attr to label else use input_name
			if (isset($input_attributes['id'])) {
				$label->for = $input_attributes['id'];
			} else {
				$label->for = $input_name;
			}
			$this->setLabel($label);
			$this->append($label);
		}
		if (!is_null($input_name)) {
			$input = new \Barebone\HTML\TInput($input_name, $input_attributes);
			$input->add_class('form-control');
			if (!isset($input_attributes['id'])) {
				$input->id = $input_name;
			}
			$this->setInput($input);
			$this->append($input);
		}
	}

	public function getLabel(): \Barebone\HTML\TElement {
		return $this->label;
	}

	public function getInput(): \Barebone\HTML\TElement {
		return $this->input;
	}

	public function setLabel(\Barebone\HTML\TElement $label) {
		$this->label = $label;
		return $this;
	}

	public function setInput(\Barebone\HTML\TElement $input) {
		$this->input = $input;
		return $this;
	}

	function enable() {
		unset($this->getInput()->disabled);
		return $this;
	}

	function disable() {
		$this->getInput()->disabled = 'disabled';
		return $this;
	}

	function setError() {
		$this->add_class('has-error has-feedback');
		//<span class="glyphicon glyphicon-ok form-control-feedback"></span>
		$this->append(new TElement('span', ['class' => 'glyphicon glyphicon-remove form-control-feedback']));
		return $this;
	}

	function setWarning() {
		$this->add_class('has-warning has-feedback');
		$this->append(new TElement('span', ['class' => 'glyphicon glyphicon-warning-sign form-control-feedback']));
		return $this;
	}

	function setSuccess() {
		$this->add_class('has-success has-feedback');
		$this->append(new TElement('span', ['class' => 'glyphicon glyphicon-ok form-control-feedback']));
		return $this;
	}       

}
