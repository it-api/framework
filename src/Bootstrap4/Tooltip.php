<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Tooltip
 * <button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
    Tooltip on top
   </button>
 * @author Frank
 */
class Tooltip  extends Element{
    /**
     * 
     * @param type $text
     * @param type $placement [top,left,bottom,right]
     * @param type $color [primary,secondary,success,danger,warning,etc]
     */
    public function __construct(string $text, string $tooltip, string $placement, string $color = Colors::PRIMARY) {
        
        parent::__construct('button');
        $this->add_class('btn');
        $this->add_class('btn-secondary');
        $this->data('toggle', 'tooltip');
        $this->data('placement', $placement);
        $this->data('html', 'true');
        $this->title = $tooltip;
        $this->innertext($text);
    }
    
    function showPreview(){
	    echo new Tooltip('<p>Well, well, well!</p>', 'I am a tooltip!', 'right');
	}
}
