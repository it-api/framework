<?php

namespace Barebone\Bootstrap4;

/**
 * Description of CustomFileInput
 *<div class="custom-file">
    <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
  </div>
 * @author Frank
 */
class CustomFileInput  extends Element{
    
    public function __construct(Input $input, $label = 'Choose file') {
        
        parent::__construct('div');
        $this->add_class('custom-file');        
        
        $input->add_class('custom-file-input');
        $input->type = 'file';
        
        $this->append($input);
        $lbl = new Label($label);
        $lbl->add_class('custom-file-label');
        if(isset($input->id)){
            $lbl->for = $input->id;
        }
        $this->append($lbl);
    }
    
    function showPreview(){
	    echo new CustomFileInput(new Input(['name'=>'files']));
	}
}
