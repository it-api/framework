<?php

namespace Barebone\Bootstrap4\Form;

/**
 * Description of Option
 *
 * @author Frank
 */
class Option extends \Barebone\Bootstrap4\Element{
    
    public function __construct($text, $value = null, $selected = false, $disabled=false, $attributes = []) {
        
        parent::__construct('option', $attributes);
        
        $this->innertext($text);
        
        if($selected === true){
            $this->selected = 'selected';
        }
        if($disabled === true){
            $this->disabled = 'disabled';
        }        
        
        if(is_null($value) || $value === ''){
            $this->value = $text;
        } else {
            $this->value = $value;
        }
    }

}
