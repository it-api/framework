<?php

namespace Barebone\Bootstrap4\Form;

/**
 * Description of Select
 * <select class="custom-select custom-select-lg">
    <option selected>Open this select menu</option>
    <option value="1">One</option>
    <option value="2">Two</option>
    <option value="3">Three</option>
  </select>
 * @author Frank
 */
class Select extends \Barebone\Bootstrap4\Element{
    
    public function __construct($attributes = []){
        parent::__construct('select', $attributes);
        $this->add_class('custom-select');
    }
    
    function large(){
        $this->add_class('custom-select-lg');
    }
    function small(){
        $this->add_class('custom-select-sm');
    }
    
    function multiple(){
        $this->multiple = 'multiple';
    }
    
    function size($size){
        $this->size = $size;
    }
    
    function addOption($text, $value = '', $selected = false, $disabled = false, $attributes = []){
        $this->append(new Option($text, $value, $selected, $disabled, $attributes));
    }

}
