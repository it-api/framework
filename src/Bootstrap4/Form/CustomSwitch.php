<?php

namespace Barebone\Bootstrap4\Form;

/**
 * Description of CustomSwitch
 *<div class="custom-control custom-switch">
    <input type="checkbox" class="custom-control-input" id="customSwitch1">
    <label class="custom-control-label" for="customSwitch1">Toggle this switch element</label>
  </div>
  <div class="custom-control custom-switch">
    <input type="checkbox" class="custom-control-input" disabled id="customSwitch2">
    <label class="custom-control-label" for="customSwitch2">Disabled switch element</label>
  </div>
 * @author Frank
 */
class CustomSwitch extends \Barebone\Bootstrap4\Element{
    
    public function __construct($label, $name, $value = '1') {
        parent::__construct('div');
        $this->add_class('custom-control');
        $this->add_class('custom-switch');
        
        $control = new \Barebone\Bootstrap4\Element('input');
        $control->add_class('custom-control-input');
        $control->id = $name;
        $control->type = 'checkbox';
        $control->name = $name;        
        $control->value = $value;
        
        $lbl = new \Barebone\Bootstrap4\Element('label');
        $lbl->add_class('custom-control-label');
        $lbl->innertext($label);
        $lbl->for = $name;
        
        $this->append($control);
        $this->append($lbl);
    }

}
