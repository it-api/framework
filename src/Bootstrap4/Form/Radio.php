<?php

namespace Barebone\Bootstrap4\Form;

/**
 * Description of Radio
 * <div class="form-check">
    <input class="form-check-input" type="radio" value="" id="defaultCheck1">
    <label class="form-check-label" for="defaultCheck1">
      Default checkbox
    </label>
  </div>
 * @author Frank
 */
class Radio extends \Barebone\Bootstrap4\Element{
    
    public function __construct($label, $name, $value = '1') {
        
        parent::__construct('div');
        $this->add_class('form-group');
        $this->add_class('form-check');
        
        $control = new \Barebone\Bootstrap4\Element('input');
        $control->add_class('form-check-input');
        $control->id = $name;
        $control->type = 'radio';
        $control->name = $name;        
        $control->value = $value;
        
        $lbl = new \Barebone\Bootstrap4\Element('label');
        $lbl->add_class('form-check-label');
        $lbl->innertext($label);
        $lbl->for = $name;
        
        $this->append($control);
        $this->append($lbl);
        
    }
    
    function inline(){
        $this->add_class('form-check-inline');
    }
}
