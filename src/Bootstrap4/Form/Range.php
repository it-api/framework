<?php

namespace Barebone\Bootstrap4\Form;

/**
 * Description of Range
 * <label for="customRange3">Example range</label>
   <input type="range" class="custom-range" min="0" max="5" step="0.5" id="customRange3">
 * @author Frank
 */
class Range extends \Barebone\Bootstrap4\Element{
    
    public function __construct(string $label = null, string $name = '', int $value = 0, int $min = 0, int $max = 100, int $step = 10, array $attributes = []) {
        parent::__construct('div', $attributes);
        
        $control = new \Barebone\Bootstrap4\Element('input');
        $control->type = 'range';
        $control->add_class('custom-range');
        $control->min = $min;
        $control->max = $max;
        $control->name = $name;
        $control->id = $name;
        
        $lbl = new \Barebone\Bootstrap4\Element('label');
        $lbl->innertext($label);
        $lbl->for = $name;
        
        $this->append($lbl);
        $this->append($control);        
    }
    
    function min($min){
        $this->children[0]->min = $min;
    }
    function max($max){
        $this->children[0]->max = $max;
    }
    function step($step){
        $this->children[0]->step = $step;
    }
    function value($value){
        $this->children[0]->value = $value;
    }

}
