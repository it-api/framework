<?php

namespace Barebone\Bootstrap4;

/**
 * Description of FormCheck
 *<div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div>
 * @author Frank
 */
class FormCheck extends Element{
    
    public function __construct($text, $name) {
        
        parent::__construct('div');
        $this->add_class('form-group');
        $this->add_class('form-check');
        
        $input = new Element('input');
        $input->add_class('form-check-input');
        $input->type = 'checkbox';
        $input->id = $name;
        $input->name = $name;
        
        $label = new Element('label');
        $label->innertext($text);
        $label->add_class('form-check-label');
        $label->for = $name;
        
        $this->append($input);
        $this->append($label);
        
    }
    
    function showPreview(){
	    echo new FormCheck('Check 1', 'one-check');
	}
}
