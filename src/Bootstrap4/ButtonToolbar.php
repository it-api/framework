<?php

namespace Barebone\Bootstrap4;

/**
 * Description of ButtonToolbar
 *
 * @author Frank
 */
class ButtonToolbar extends Element {

	public function __construct() {
		parent::__construct('div');
		$this->add_class('btn-toolbar');

	}
	
	function showPreview(){
	    $toolbar = new ButtonToolbar();
	    $toolbar->append(new Button('I', '#'));
	    $toolbar->append(new Button('B', '#'));
	    $toolbar->append(new Button('U', '#'));
	    echo $toolbar;
	}
}
    
