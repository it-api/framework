<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Column
 *
 * @author Frank
 */
class Column extends Element {

	private $rows = [];

	/**
	 * 
	 * @param type $text
	 * @param type $attributes
	 */
	function __construct(string $text, array $attributes = []) {
		parent::__construct('div', $attributes);
		$this->innertext($text);
	}
	
	function showPreview(){
	    echo new Column('Column');
	}

}
