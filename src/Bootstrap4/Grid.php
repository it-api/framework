<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Grid
 *
 * @author Frank
 */
class Grid extends Element {

	/**
	 * Array that will hold the rows
	 * @var type 
	 */
	private $rows = [];

	/**
	 * 
	 * @param string $attributes
	 */
	function __construct(array $attributes = []) {
		if (empty($attributes)) {
			$attributes = ['class' => 'container'];
		}
		parent::__construct('div', $attributes);
	}
    
    function showPreview(){
	    echo new Grid();
	}
	
	/**
	 * Make grid fluid
	 * @return $this
	 */
	function fluid() {
		$this->class = 'container-fluid';
		return $this;
	}

	/**
	 * Add a row to the grid
	 * @return \Barebone\Bootstrap\Row
	 */
	function addRow() {

		$row = new Row();
		$this->rows[] = $row;
		$this->append($row);
		return $row;
	}

	/**
	 * 
	 * @param array $array
	 */
	function appendArrayOfElements($array) {
		foreach ($array as $element) {
			$this->append($element);
		}
	}

}
