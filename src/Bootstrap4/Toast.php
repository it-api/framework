<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Toast
 * <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
  <div class="toast-header">
    <img src="..." class="rounded mr-2" alt="...">
    <strong class="mr-auto">Bootstrap</strong>
    <small>11 mins ago</small>
    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="toast-body">
    Hello, world! This is a toast message.
  </div>
</div>
 * @author Frank
 */
class Toast extends Element{
    
    public function __construct(Image $img = null, string $title = '', string $timestamp = '', string $text = '', $type = 'default') {
        
        parent::__construct('div');
        $this->add_class('toast');
        $this->role = 'alert';
        $this->aria('live', 'assertive');
        $this->aria('atomic', 'true');
        $this->addHeader($img, $title, $timestamp, $type);
        
        $body = new Element('div');
        $body->add_class('toast-body');
        $body->innertext = $text;
        $this->append($body);
    }
    
    function showPreview(){
	    $img = new Image('/images/information.png');
	    $toast = new Toast($img, 'I am the title', '2 minutes ago..', 'I am a toast message');
	}
    
    function autoClose($interval = 3){
        $interval = $interval * 1000;
        $this->data('autohide', 'true');
        $this->data('delay', $interval);
    }
    
    function addHeader(Image $img = null, string $title = '', string $timestamp = '', $type = 'default'){
        
        $header = new Element('div');
        $header->add_class('toast-header');
        
        if($type === 'success'){
            $header->add_class("bg-success text-white");
        } elseif($type === 'warning'){
            $header->add_class("bg-warning text-white");
        } elseif($type === 'danger'){
            $header->add_class("bg-danger text-white");
        } elseif($type === 'primary'){
            $header->add_class("bg-primary text-black");
        } elseif($type === 'secondary'){
            $header->add_class("bg-secondary text-white");
        }
        
        if(!is_null($img)){
            $header->append($img);
        }
        
        if($title != ''){
            $strong = new Element('strong');
            $strong->innertext = $title;
            $strong->add_class('mr-auto');
            $header->append($strong);
        }
        if($timestamp != ''){
            $small = new Element('small');
            $small->add_class('text-muted');
            $small->innertext = $timestamp;
            $header->append($small);
        }
        
        //close button
        //<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
        //    <span aria-hidden="true">&times;</span>
        //</button>
        $btn = new Element('button');
        $btn->add_class('ml-2');
        $btn->add_class('mb-1');
        $btn->add_class('close');
        $btn->data('dismiss', 'toast');
        $btn->aria('label', 'Close');
        
        $span = new Element('span');
        $span->aria('hidden', 'true');
        $span->innertext = '&times;';
        $btn->append($span);
        
        $header->append($btn);
        $this->append($header);
        
        return $this;
    }

}