<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Option
 *
 * @author Frank
 */
class Option  extends Element{
    
    public function __construct($text, $value = '') {
        
        parent::__construct('option');
        $this->innertext($text);
        if($value === ''){
            $this->value = $text;
        } else {
            $this->value = $value;
        }
        
    }
    
    function showPreview(){
        echo new Option('Option', 'Opt value');
    }
}
