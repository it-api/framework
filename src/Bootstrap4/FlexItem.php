<?php

namespace Barebone\Bootstrap4;

/**
 * Description of FlexItem
 *
 * @author Frank
 */
class FlexItem extends Element {

	public function __construct(array $attributtes = []) {
		parent::__construct('div', $attributtes);		
	}
	
	function showPreview(){
	    echo new FlexItem();
	}
    /**
     * Responsive variations also exist for align-self.

    .align-self-start<br>
    .align-self-end<br>
    .align-self-center<br>
    .align-self-baseline<br>
    .align-self-stretch<br>
    .align-self-sm-start<br>
    .align-self-sm-end<br>
    .align-self-sm-center<br>
    .align-self-sm-baseline<br>
    .align-self-sm-stretch<br>
    .align-self-md-start<br>
    .align-self-md-end<br>
    .align-self-md-center<br>
    .align-self-md-baseline<br>
    .align-self-md-stretch<br>
    .align-self-lg-start<br>
    .align-self-lg-end<br>
    .align-self-lg-center<br>
    .align-self-lg-baseline<br>
    .align-self-lg-stretch<br>
    .align-self-xl-start<br>
    .align-self-xl-end<br>
    .align-self-xl-center<br>
    .align-self-xl-baseline<br>
    .align-self-xl-stretch<br>

     * @param type $align
     */
    function alignSelf($align){
        $this->add_class("align-self-{$align}");        
        return $this;
    }
    /**
     * Responsive variations also exist for flex-fill.
     * @param type $fill [fill,sm-fill,md-fill,lg-fill,xl-fill]
     * @return $this
     */
    function fill($fill){
        $classname = $fill === '' ? 'flex-fill' : "flex-fill-{$fill}";
        $this->add_class($classname);        
        return $this;
    }
    
    function grow($grow){
        $this->add_class("flex-grow-{$grow}");        
        return $this;
    }
    
    function shrink($shrink){
        $this->add_class("flex-shrink-{$shrink}");        
        return $this;
    }
    /**
     * 
     * @param int $order [0 to 12]
     * @param string $device [sm, md, lg, xl]
     * @return $this
     */
    function order(int $order , string $device = ''){
        if($device != ''){
            $this->add_class("order-{$device}-{$order}");
        } else {
            $this->add_class("order-{$order}");
        }
        return $this;
    }
    
}