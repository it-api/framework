<?php

namespace Barebone\Bootstrap4;

/**
 * Description of CustomSelect
 * <select class="custom-select">
    <option selected>Choose...</option>
    <option value="1">One</option>
    <option value="2">Two</option>
    <option value="3">Three</option>
  </select>
 *
 * @author Frank
 */
class CustomSelect  extends Element{
    
    public function __construct(array $options = [], array $attributes = []) {
        
        parent::__construct('select', $attributes);
        $this->add_class('custom-select');
        foreach ($options as $option) {
            $this->append($option);
        }
    }
    
    function showPreview(){
	    echo new CustomSelect([new Option('Opt 1', 1), new Option('Opt 2', 2)]);
	}
}
