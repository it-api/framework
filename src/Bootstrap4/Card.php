<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Card
 *
 * @author Frank
 */
class Card extends Element {
    
    public $card_header = null;
    public $card_body = null;
    public $card_footer = null;
    
    public function __construct($title, $text, Image $img = null, Link $link = null) {
        
        parent::__construct('div');
        $this->add_class('card');
        
        if(!is_null($img)){
            $img->add_class('card-img-top');
            $this->append($img);
        }
        
        $this->card_body = new Element('div');
        $this->card_body->add_class('card-body');
        
        $card_title = new Element('h4');
        $card_title->add_class('card-title');
        $card_title->innertext = $title;
        $this->card_body->append($card_title);
        
        $card_text = new Element('p');
        $card_text->innertext = $text;
        $this->card_body->append($card_text);
        
        if(!is_null($link)){
            $this->card_body->append($link);
        }
        
        $this->append($this->card_body);
        
    }
    
    function showPreview(){
        $img = new Image('/images/100-procent-service-van-de-Vastelastenbond.png');
        $link = new Link('Caption link', '#');
	    $card = new Card('Card title', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>', $img, $link);
	    $card->css('width', '300px');
	    $card->card_body->css('color','black');
	    $card->addHeader('Card header')->css('color', 'green');
	    $card->addFooter('Card footer')->css('color', 'red');
	    echo $card;
	}
    
    function body(){
        return $this->card_body;
    }
    
    function addHeader($text){
        $this->card_header = new Element('div');
        $this->card_header->add_class('card-header');
        $this->card_header->innertext = $text;
        $this->prepend($this->card_header);
        return $this->card_header;
    }

    function addFooter($text){
        $this->card_footer = new Element('div');
        $this->card_footer->add_class('card-footer');
        $this->card_footer->innertext = $text;
        $this->append($this->card_footer);
        return $this->card_footer;
    }
}
