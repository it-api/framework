<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Bootstrap4;

/**
 * Description of Input
 *
 * @author Frank
 */
class Input extends Element{
    
    public function __construct($attributes = []) {
        
        parent::__construct('input', $attributes);
        $this->add_class('form-control');
    }
    
    function showPreview(){
	    echo new Input(['type'=>'text']);
	}
    
}
