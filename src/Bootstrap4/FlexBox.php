<?php

namespace Barebone\Bootstrap4;

/**
 * Description of FlexBox
 *
 * @author Frank
 */
class FlexBox extends Element {
    /**
     * Responsive variations also exist for .d-flex and .d-inline-flex.<br>
     * flex<br>
     * inline-flex<br>
     * sm-flex<br>
     * sm-inline-flex<br>
     * md-flex<br>
     * md-inline-flex<br>
     * lg-flex<br>
     * lg-inline-flex<br>
     * xl-flex<br>
     * xl-inline-flex<br>
     * @param type $variation
     */
	public function __construct(string $variation = 'flex') {

		parent::__construct('div');
        $this->add_class("d-{$variation}");
	}
	
	function showPreview(){
	    echo new FlexBox();
	}
    /**
     * 
     * @param bool $reverse
     * @param string $device [sm, md, lg, xl]
     * @return $this
     */
    function row(bool $reverse = false, string $device = ''){
        
        if($device != '' && $reverse === true){
            $this->add_class("flex-{$device}-row-reverse");
        } elseif($device != '') {
            $this->add_class("flex-{$device}-row");
        } elseif($reverse){
            $this->add_class("flex-row-reverse");
        } else {
            $this->add_class("flex-row");
        }
        return $this;
        
    }
    
    /**
     * flex-{device}-column[-reverse]
     * @param bool $reverse
     * @param string $device [sm, md, lg, xl]
     * @return $this
     */
    function column(bool $reverse = false, string $device = ''){
        
        if($device != '' && $reverse === true){
            $this->add_class("flex-{$device}-column-reverse");
        } elseif($device != '') {
            $this->add_class("flex-{$device}-column");
        } elseif($reverse){
            $this->add_class("flex-column-reverse");
        } else {
            $this->add_class("flex-column");
        }
        return $this;

    }
    /**
     * Responsive variations also exist for flex-direction.<br>
     Skip the .flex- part ;)<br>
    .flex-row<br>
    .flex-row-reverse<br>
    .flex-column<br>
    .flex-column-reverse<br>
    .flex-sm-row<br>
    .flex-sm-row-reverse<br>
    .flex-sm-column<br>
    .flex-sm-column-reverse<br>
    .flex-md-row<br>
    .flex-md-row-reverse<br>
    .flex-md-column<br>
    .flex-md-column-reverse<br>
    .flex-lg-row<br>
    .flex-lg-row-reverse<br>
    .flex-lg-column<br>
    .flex-lg-column-reverse<br>
    .flex-xl-row<br>
    .flex-xl-row-reverse<br>
    .flex-xl-column<br>
    .flex-xl-column-reverse
     * @param string $direction
     */
    function direction($direction){
        $this->add_class("flex-{$direction}");        
        return $this;
    }
    /**
     * Use justify-content utilities on flexbox containers to change the alignment<br>
     * of flex items on the main axis (the x-axis to start, y-axis if flex-direction: column).<br>
     * Choose from start (browser default), end, center, between, or around.<br>
     * @param type $justify [start, end, center, between, around]
     * @return $this
     */
    function justify($justify){
        $this->add_class("justify-content-{$justify}");        
        return $this;
    }
    /**
     * Use align-items utilities on flexbox containers to change the alignment<br> 
     * of flex items on the cross axis (the y-axis to start, x-axis if flex-direction: column). <br>
     * Choose from start, end, center, baseline, or stretch (browser default).<br>
     * esponsive variations also exist for align-items.
     Skip the part .align-items-
    .align-items-start<br>
    .align-items-end<br>
    .align-items-center<br>
    .align-items-baseline<br>
    .align-items-stretch<br>
    .align-items-sm-start<br>
    .align-items-sm-end<br>
    .align-items-sm-center<br>
    .align-items-sm-baseline<br>
    .align-items-sm-stretch<br>
    .align-items-md-start<br>
    .align-items-md-end<br>
    .align-items-md-center<br>
    .align-items-md-baseline<br>
    .align-items-md-stretch<br>
    .align-items-lg-start<br>
    .align-items-lg-end<br>
    .align-items-lg-center<br>
    .align-items-lg-baseline<br>
    .align-items-lg-stretch<br>
    .align-items-xl-start<br>
    .align-items-xl-end<br>
    .align-items-xl-center<br>
    .align-items-xl-baseline<br>
    .align-items-xl-stretch<br>

     */
    function alignItems($align){
        $this->add_class("align-items-{$align}");        
        return $this;
    }
    
    function addItem(FlexItem $item){
        $this->append($item);
    }
    /**
     * Responsive variations also exist for flex-wrap. 
     * @param type $wrap One of [ nowrap, sm-nowrap, md-nowrap, lg-nowrap, xl-nowrap ]
     * @param bool $reverse adds -reverse
     * @return $this
     */
    function wrapItems($wrap = 'wrap', bool $reverse = false){
        if($reverse){
            $this->add_class("flex-{$wrap}-reverse");
        }elseif($wrap == 'wrap'){
            $this->add_class("flex-wrap");
        } else {
            $this->add_class("flex-{$wrap}");
        }
        return $this;
    }
    /**
     *
     * @param type $wrap One of [ nowrap, sm-nowrap, md-nowrap, lg-nowrap, xl-nowrap ]
     * @return $this
     */
    function nowrapItems($wrap = 'nowrap'){
        if($wrap == 'nowrap'){
            $this->add_class("flex-nowrap");
        } else {
            $this->add_class("flex-{$wrap}");
        }
        return $this;
    }
    /**
     * Responsive variations also exist for align-content.

    .align-content-start
    .align-content-end
    .align-content-center
    .align-content-around
    .align-content-stretch
    .align-content-sm-start
    .align-content-sm-end
    .align-content-sm-center
    .align-content-sm-around
    .align-content-sm-stretch
    .align-content-md-start
    .align-content-md-end
    .align-content-md-center
    .align-content-md-around
    .align-content-md-stretch
    .align-content-lg-start
    .align-content-lg-end
    .align-content-lg-center
    .align-content-lg-around
    .align-content-lg-stretch
    .align-content-xl-start
    .align-content-xl-end
    .align-content-xl-center
    .align-content-xl-around
    .align-content-xl-stretch

     * @param string $align [start, end, center, around, stretch]
     * @param string $device [sm, md, lg, xl]
     */
    function alignContent(string $align, string $device = ''){
        if($device != ''){
            $this->add_class("align-content-{$device}-{$align}");
        } else {
            $this->add_class("align-content-{$align}");
        }
        return $this;
    }
    
}