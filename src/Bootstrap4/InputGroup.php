<?php

namespace Barebone\Bootstrap4;

/**
 * Description of InputGroup
 *
 * @author Frank
 */
class InputGroup  extends Element{
    
    public $text = null;
    public $input = null;
    private $position = null;
    private $atext = '';
    private $aid = '';
    private $textright = '';
    private $idright = '';
    
    /**
     *<div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="basic-addon1">@</span>
        </div>
        <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
      </div>
     * @param type $position
     * @param type $text
     * @param type $id
     */
    public function __construct($position = null, $text = '', $id = '', $textright = '', $idright = '') {
        
        parent::__construct('div'); //<div class="input-group">
        $this->add_class('input-group');
        
        if(is_null($position)){
            $this->input = new Element('input'); //<input type="text" class="form-control" aria-describedby="basic-addon1">
            $this->input->type = 'text';
            $this->input->add_class('form-control');
            $this->input->aria('describedby', $id);               

            $this->position = $position;  
            $this->atext = $text;
            $this->aid = $id;
            $this->textright = $textright;
            $this->idright = $idright;
        }
    }
    
    function showPreview(){
	    echo new InputGroup();
	}
    
    public function getText() {
        return $this->text;
    }

    public function getInput() {
        return $this->input;
    }

    public function setText($text) {
        $this->text = $text;
        return $this;
    }

    public function setInput($input) {
        $this->input = $input;
        return $this;
    }
    function useTextarea(){
        $textarea = new Element('textarea');
        $textarea->id = $this->input->id;
        $textarea->add_class('form-control');
        $textarea->placeholder = $this->input->placeholder;
        $this->setInput($textarea);
        return $this;
    }
    /**
     *<div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1">@</span>
      </div>
      <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
     */
    function tagLeft($text, $id){
        
        $this->text = new InputGroupPrepend($text, $id);
        $this->append($this->text);
        return $this;
    }
    
    /**
     *<input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
      <div class="input-group-append">
        <span class="input-group-text" id="basic-addon1">@</span>
      </div>      
     */
    function tagRight($text, $id){
        
        $this->text = new InputGroupAppend($text, $id);
        $this->append($this->text);
        return $this;
    }
    
    function addItem(Element $item){
        $this->append($item);
        if(!is_null($this->input)){
            $this->input = null;
        }
    }
    
    function small(){
        $this->add_class('input-group-sm');
    }
    
    function large(){
        $this->add_class('input-group-lg');
    }
    
    function flexNoWrap(){
        $this->add_class('flex-nowrap');
    }
    
    function __toString(): string {
        
        if($this->position == 'left'){ 
            if(!$this->has_children()){
                $this->tagLeft($this->atext, $this->aid);
                $this->append($this->input);      
            }
                  
        } elseif($this->position == 'right'){            
            
            if(!$this->has_children()){
                $this->append($this->input);
                $this->tagRight($this->atext, $this->aid);            
            }
        } elseif($this->position == 'both'){
            if(!$this->has_children()){
                $this->tagLeft($this->atext, $this->aid);
                $this->append($this->input);
                $this->tagRight($this->textright, $this->idright);
            }
            
        }
        return parent::__toString();
        //return $this;
    }

}
