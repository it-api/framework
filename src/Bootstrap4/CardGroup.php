<?php

namespace Barebone\Bootstrap4;

/**
 * Description of CardGroup
 *
 * @author Frank
 */
class CardGroup extends Element{
    
    public function __construct() {
        parent::__construct('div');
        $this->add_class('card-group');
    }
    
    function showPreview(){
	    echo new CardGroup();
	}
    
    function addCard(Card $card){
        $this->append($card);
        return $this;
    }

}
