<?php

namespace Barebone\Bootstrap4;

/**
 * Description of Colors
 *
 * @author Frank
 */
class Colors {
    const INFO = 'info';
    const SUCCESS = 'success';
    const WARNING = 'warning';
    const DANGER = 'danger';
    const PRIMARY = 'primary';
    const SECONDARY = 'secondary';
    const LIGHT = 'light';
    const DARK = 'dark';
    
    function __construct(){}
    
    function showPreview(){
	    echo 'This is a helper class with all colors';
	}
}
