<?php

namespace Barebone;

class SQLLite3{
	
	public $db;
	
	function __construct( $filename, $flags = SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE, $encryption_key = '' ){
		$this->db = new \SQLite3($filename, $flags, $encryption_key);
	}
}
