<?php

namespace Barebone;

/**
 * Description of Cache
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource Cache.php
 * @since 23-jan-2017 19:19:07
 * 
 */
class Cache {

    static private string $cache_path = FULL_BAREBONE_PATH . 'cache/';
    static private int $lifetime = 30;

    private static function setProperties() {

        $config = Config::getInstance();
        self::$cache_path = FULL_BAREBONE_PATH . $config->getValue('cache', 'cache_path');
        self::$lifetime = $config->getValue('cache', 'lifetime');
    }
    /**
     * Clean the cache
     */
    public static function clean(): void {
        self::setProperties();
        $fs = new Filesystem();
        $files = $fs->get_Files_list(self::$cache_path);

        foreach ($files as $file) {
            $fs->unlink(self::$cache_path . $file);
        }
    }

    /**
     * Read data from the cache. If the data is not in the cache yet it will be created
     * @param mixed $data
     * @return string
     */
    public static function read($data): string {
        self::setProperties();
        $filename = self::$cache_path . sha1($data);

        if (file_exists($filename) && filemtime($filename) < (time() - self::$lifetime)) {
            unlink($filename);
        }

        if (file_exists($filename)) {

            //echo 'load from cache file...';
            return file_get_contents($filename);
        } else {

            //echo 'no cache file yet...';
            file_put_contents($filename, $data);
            return $data;
        }
    }
}
