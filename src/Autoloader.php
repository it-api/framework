<?php

namespace Barebone;

/**
 * This class will autoload all classes from the library and database models and everything you add offcourse
 * @author Frank
 *
 */
class Autoloader {

    public static $loader;

    public static function getInstance() {
        if (!isset(self::$loader)) {
            self::$loader = new self();
        }
        return self::$loader;
    }

    public function __construct() {

        spl_autoload_register(array($this, 'models'));
        spl_autoload_register(array($this, 'app_models'));
        spl_autoload_register(array($this, 'database_models'));
    }

    /**
     * autoload classes from the global models directory
     * @param string $class the name of the class to load
     */
    public function models($class) {

        $model_path     = FULL_BAREBONE_PATH . 'models/';
        $filename     = $model_path . $class . '.php';
        $filename     = str_replace("\\", '/', $filename);
        if (file_exists($filename)) {
            require $filename;
        }
    }

    /**
     * autoload classes from the application/models/ directory
     * @param string $class the name of the class to load
     */
    public function app_models($class) {

        $config = Config::getInstance();
        // get appplications folder path
        $apps_path = FULL_BAREBONE_PATH . $config->getValue('application', 'applications_path');
        // gat array with all apps
        $apps = glob($apps_path.'*');
        // loop over apps array
        foreach($apps as $app_path){
            $app          = basename($app_path);
            $model_path   = $apps_path . $app . '/models/';
            $filename     = $model_path . $class . '.php';
            $filename     = str_replace("\\", '/', $filename);
    
            if (file_exists($filename)) {
                require $filename;
            }
        }
    }
    /**
     * autoload classes from the barebone/models/database/ directory
     * @param string $class the name of the class to load
     */
    public function database_models($class) {

        $dbname = Config::getInstance()->getValue('database', 'dbname');

        $db_models_path = FULL_BAREBONE_PATH . 'models/database/' . $dbname . '/';
        $filename     = $db_models_path . $class . '.php';
        $filename     = str_replace("\\", '/', $filename);

        if (file_exists($filename)) {
            require $filename;
        }
    }
}
