<?php

namespace Barebone;

/**
 * Description of Map
 * MYFW 
 * UTF-8
 *
 * @author Frank
 * @filesource Map.php
 * @since 15-jan-2017 23:18:37
 * 
 */
class Map {

    private array $items = [];

    function __construct(array $items = []) {
        $this->items = $items;
        foreach ($items as $name => $value) {
            $pair = new Pair($name, $value);
            $this->items[$name] = $pair;
        }
    }

    function getValueByName(string $name) {
        foreach ($this->items as $key => $pair) {
            if ($pair->getName() === $name) {
                return $pair->getValue();
            }
        }
    }

    function getNameByValue(string $value) {
        foreach ($this->items as $name => $pair) {
            if ($pair->getValue() === $value) {
                return $pair->getName();
            }
        }
    }
}
