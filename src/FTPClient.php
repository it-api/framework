<?php

/*
  FTPClient.php
  UTF-8
  31-okt-2018 10:24:25
  website-backend
  Creator Frank
 */

namespace Barebone;

/**
 * Description of FTPClient
 *
 * @author Frank
 */
class FTPClient {
	
	private $username = null;
	private $password = null;
	private $server = null;
	private $passive = false;
	
	private $connection = null;
	
	public function __construct(string $username, string $password, string $server, bool $passive = false) {
		$this->username = $username;
		$this->password = $password;
		$this->server = $server;
		$this->passive = $passive;
	}

	public function getUsername() {
		return $this->username;
	}

	public function getPassword() {
		return $this->password;
	}

	public function getServer() {
		return $this->server;
	}

	public function getPassive() {
		return $this->passive;
	}

	public function setUsername($username) {
		$this->username = $username;
		return $this;
	}

	public function setPassword($password) {
		$this->password = $password;
		return $this;
	}

	public function setServer($server) {
		$this->server = $server;
		return $this;
	}

	public function setPassive($passive) {
		$this->passive = $passive;
		return $this;
	}
	
	public function Connect(){
		
		// set up basic connection
		$this->connection = ftp_connect($this->server);
		
		if($this->connection === false){
			throw new Exception('Cannot connect.');
		}

		// login with username and password
		$login_result = ftp_login($this->connection, $this->username, $this->password);
		
		if($login_result === false){
			throw new Exception('Cannot login.');
		}
		
		// turn passive mode on/off
    	ftp_pasv($this->connection, $this->passive);
		
		return $login_result;
		
	}
	
	function CloseConnection(){		
		// close this connection
		ftp_close($this->connection);		
	}

	public function getFilesList(){		
		if($this->Connect()){
			// get contents of the current directory
			$contents = ftp_nlist($this->connection, ".");
			// close connection
			$this->CloseConnection();
			// return $contents
			return $contents;			
		}		
	}
	
	public function getFile($server_file, $local_file){		
		if($this->Connect()){
			
			// try to download $server_file and save to $local_file
			if (ftp_get($this->connection, $local_file, $server_file, FTP_BINARY)) {
				echo "Successfully written to $local_file\n";
			} else {
				echo "There was a problem\n";
			}
			
			$this->CloseConnection();
		}		
	}
}
