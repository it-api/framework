<?php

namespace Barebone\Form;

class Element{

	
	const TAG_OPEN = '<';
	const TAG_CLOSE= '>';
	const INLINE_CLOSE = ' />';
	
	private string $tag;
	private array $attributes = array();
	private static array $non_closing_tags = array(
		'area', 'base', 'br', 'col', 'command', 'embed', 'hr',
        'img', 'input', 'keygen', 'link', 'meta', 'param', 'source', 'track', 'wbr'
	);
	/**
	 * @var array child element of this element
	 */
	private array $children = array();
	private string $innertext = '';
	
	function __construct($tag, $attributes = array()){
		
		$this->setTag($tag);		
		if(count($attributes) > 0){
			foreach($attributes as $attr_name => $attr_value){
				$this->attributes[] = new Attribute($attr_name, $attr_value);
			}
		}
	}
	function addChild($tag, $attributes = array()){
            if($tag instanceof Element){
                $this->children[] = $tag;
				return $tag;
            }else{
                $child = new Element($tag, $attributes);
                $this->children[] = $child;
				return $child;
            }
            
	}
	public function hasChildren(){
		return count($this->children) > 0;
	}
	public function getChildren(){
		return $this->children;
	}
	public function getTag() {
		return $this->tag;
	}
	public function setTag($tag) {
		$this->tag = $tag;
		return $this;
	}
	public function isClosingTag(){
		return in_array(strtolower($this->getTag()), self::$non_closing_tags);
	}
	public function hasAttribute($name){
		return isset( $this->attributes[$name] );
	}
	public function getAttribute($name){
		return $this->attributes[$name];
	}
	public function getAttributes() {
		return $this->attributes;
	}
	public function setAttributes($attributes) {
		$this->attributes = array_merge($this->attributes, $attributes);
		return $this;
	}
	public function setAttribute($name, $value){
		$attribute = new Attribute($name, $value);
		array_push($this->attributes, $attribute);
	}
	public function get_json(){
		
		$element = new \stdClass();
		$element->tag = $this->getTag();
		if($this->hasAttribute('type')){
			$element->type = $this->getAttribute('type');
		}
		foreach($this->getAttributes() as $attribute){
			debug($attribute);
			//$element->attributes[ $attribute->getName() ] = $attribute->getValue();
		}
		//$element->attributes = $this->getAttributes();
		
		return json_encode($element);
	}
	public function get_html(){
		// open the tag
		$html = '<' . $this->getTag();
		// add the attributes
		foreach($this->getAttributes() as $attribute){
			if($attribute->getName() === 'innertext') 
				$this->innertext = $attribute->getValue();
			else
				$html .= $attribute->get_html();
		}
		// how do we close the tag
		if($this->isClosingTag())
			$html .= ' />' . "\n";
		else 
			$html .= '>'. "\n";
		// if the element has children then render them to
		if($this->hasChildren()){
			foreach($this->getChildren() as $child ){
				$html .= $child->get_html();
			}
		}
		if(strlen($this->innertext) > 0){
			$html .= $this->innertext;
		}
		// how do we close the tag
		if(!$this->isClosingTag())
			$html .= '</'.$this->getTag().'>'. "\n";
		
		return $html;
	}	
	public function render(){
		echo $this->get_html();
	}
	
}
