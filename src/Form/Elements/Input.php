<?php

namespace Barebone\Form\Elements;

use Barebone\Form\Element;

class Input extends Element{
	
	function __construct( $type, $attributes = array() ){
		
		$this->setTag('input');
		$attr = array('type' => $type);
		$attr = array_merge($attr, $attributes);
		
		$this->setAttributes( $attr );
		
	}
}
