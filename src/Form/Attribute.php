<?php

namespace Barebone\Form;

class Attribute{
	
	private $name;
	private $value;
	
	function __construct($name, $value){
		$this->setName($name);
		$this->setValue($value);
	}
	
	public function getName() {
		return $this->name;
	}
	public function setName($name) {
		$this->name = $name;
		return $this;
	}
	public function getValue() {
		return $this->value;
	}
	public function setValue($value) {
		$this->value = $value;
		return $this;
	}
	public function get_html(){
		return ' ' . $this->getName() . '="' . $this->getValue() . '"';
	}
	
	public function render(){
		echo $this->get_html();
	}
	
}
