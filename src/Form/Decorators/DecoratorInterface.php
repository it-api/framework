<?php

/*
  DecoratorInterface.php
  UTF-8
  23-nov-2018 18:26:26
  backend_vastelastenbond
  Creator Frank
 */

namespace Barebone\Form\Decorators;

/**
 *
 * @author Frank
 */
interface DecoratorInterface {
	
	function __construct( \Barebone\Form $form );
	function render();
	
}
