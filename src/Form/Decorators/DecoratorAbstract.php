<?php

/*
  DecoratorAbstract.php
  UTF-8
  23-nov-2018 18:45:13
  backend_vastelastenbond
  Creator Frank
 */

namespace Barebone\Form\Decorators;

/**
 * Description of DecoratorAbstract
 *
 * @author Frank
 */
abstract class DecoratorAbstract implements DecoratorInterface{
	
	protected $form = null;
	
	public function __call($function, $args){
	    
	    $this->form->{$function}($args);
	}

}
