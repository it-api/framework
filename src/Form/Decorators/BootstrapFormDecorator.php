<?php

/*
  BootstrapFormDecorator.php
  UTF-8
  23-nov-2018 18:25:14
  backend_vastelastenbond
  Creator Frank
 */

namespace Barebone\Form\Decorators;

/**
 * Description of BootstrapFormDecorator
 *
 * @author Frank
 */
class BootstrapFormDecorator extends DecoratorAbstract implements DecoratorInterface{
	
	function __construct( \Barebone\Form $form = null ) {
		$this->form = $form;
	}
	
	function render(){
		debug($this->form);
	}
}
