<?php

namespace Barebone;

/**
 * If you’ve ever been in a line at the supermarket checkout,
 * then you’ll know that the first person in line gets served first.
 * In computer terminology, a queue is another abstract data type,
 * which operates on a first in first out basis, or FIFO.
 * Inventory is also managed on a FIFO basis,
 * particularly if such items are of a peristable nature.
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource Queue.php
 * @since 16-jan-2017 0:27:14

   @example

        // create kassa rij
	    $registerline = new \Barebone\Queue();
	    // stel 3 klanten op voor de kassa
	    $registerline->enqueue('Gast1');
	    $registerline->enqueue('Gast2');
	    $registerline->enqueue('Gast3');
	    // eerste klant is geholpen
	    echo $registerline->dequeue(); //Gast1
	    // next
	    echo $registerline->top(); //Gast2
 * 
 */
class Queue {

    protected $items;
    protected $limit;

    function __construct($limit = 10){
        $this->init($limit);
    }
    // – create the queue.
    function init($limit = 10){

        $this->items = [];
        $this->limit = $limit;

    }
    //– add an item to the “end” (tail) of the queue.
    function enqueue($item){
        // trap for Queue overflow
        if (count($this->items) < $this->limit) {
            // prepend item to the end of the array
            $this->items[]= $item;
        } else {
            throw new \RunTimeException('Queue is full!');
        }
    }
    //– remove an item from the “front” (head) of the queue.
    function dequeue(){
        if ($this->isEmpty()) {
            // trap for stack underflow
	        throw new \RunTimeException('Stack is empty!');
	    } else {
            // pop item from the start of the array
            return array_shift($this->items);
        }
    }
    // – look at the first item
    function top(){
        return current($this->items);
    }
    //– return whether the queue contains no more items.
    function isEmpty(){
        return empty($this->items);
    }

}
