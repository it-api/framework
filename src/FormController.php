<?php

namespace Barebone;

use Barebone\Controller;
					
class FormController extends Controller{
    
    /**
     * @var Form
     */
    public $form;
    
	function __construct(){
	    
	    // create parent controller
	    parent::__construct();
	    // create the form
	    $this->form = new Form();
	    // assign the form to the view
	    $this->view->form = $this->form;
	}
}
