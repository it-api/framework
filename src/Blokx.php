<?php

namespace Barebone;

/**
 * Blokx handler this will load blokx from the blokx folder in your app, just like a view.
 * Only blokx are re-usable static content no PHP allowed, use the view helper for that
 * @author Frank
 *
 */
class Blokx extends Component {

    private string $filename = '';
    public string $nonce = '';

    function __construct(string $blokxname, array $data = []) {

        // get the Config object
        $config = Config::getInstance();

        // get the Router object
        $router = Router::getInstance();

        // find applications folder
        $applications_path = FULL_BAREBONE_PATH . $config->getValue('application', 'applications_path');
        if (!is_dir($applications_path)) {
            return false;
        }

        // find the application
        $application = $applications_path . $router->application . '/';
        if (!is_dir($application)) {
            return false;
        }

        // find the blokx folder
        $application_blokx = $application . 'blokx/';
        if (!is_dir($application_blokx)) {
            return false;
        }

        // find the blokx file for this action
        $this->filename = $application_blokx . $blokxname . '.phtml';
        
        // if the file does not exist show that blokx file is missing
        if(!file_exists($this->filename)){
            $backtrace = debug_backtrace()[1];
            echo '<pre>' . sprintf(
'The blokx file "%s" does not exist in de application "%s".
Called from class: "%s" with method: "%s"
on line: "%s" in file: "%s"', 
                $blokxname . '.phtml', 
                $router->application, 
                $backtrace['class'],
                $backtrace['function'],
                $backtrace['line'],
                $backtrace['file'])
                
                . '</pre>';
            return false;
        }
        
        if (!empty($data)) {
            $this->setData($data);
        }

        if (isset($data['layout'])) {
            $this->nonce = $data['layout']->nonce;
        } elseif (isset($data['view'])) {
            $this->nonce = $data['view']->nonce;
        }

    }

    /**
     * Create a new blokx
     * @param string $blokxname
     * @param mixed array|object $data
     * @return \Barebone\Blokx
     */
    function blokx(string $blokxname, array $data = []): Blokx {

        return new Blokx($blokxname, $data);
    }
    /**
     * Get the HTML of the Blokx
     * @throws \Barebone\Exception
     * @return string
     */
    function get_html(): string {

        // if there is a correpsonding Blokx file in the Blokx folder
        if (file_exists($this->filename)) {
            ob_start();
            // then extract the properties so the Blokx has acces to them
            extract($this->properties);
            // then include the Blokx any code therein will be executed
            include($this->filename);

            $html = ob_get_contents();
            ob_end_clean();
            return $html;
        } else {

            throw new Exception('File not found: ' . $this->filename);
        }
    }

    /**
     * Render the Blokx
     * @throws \Barebone\Exception
     * @return void
     */
    function render(): void {

        // if there is a correpsonding Blokx file in the Blokx folder
        if (file_exists($this->filename)) {

            // then extract the properties so the Blokx has acces to them
            extract($this->properties);
            // then include the Blokx any code therein will be executed
            include($this->filename);
        } else {

            throw new Exception('File not found: ' . $this->filename);
        }
    }
    /**
     * Summary of requestAction
     * @param string $route
     * @param array $options
     * @return mixed
     */
    function requestAction(string $route, array $options = []) {

        $route_array = explode('/', $route);

        $application = $route_array[0];
        $controller = ($route_array[1] . 'Controller');
        $action = $route_array[2];

        $file = FULL_BAREBONE_PATH . "applications/{$application}/controllers/{$controller}.php";

        if(!file_exists($file)){
            throw new \Exception(sprintf('Controller %s does not exist.', $controller));
        }
        
        require($file);
        $class = new $controller();
        if (method_exists($class, $action . 'Action')) {
            return (call_user_func_array(array($class, $action . 'Action'), $options));
        } elseif (method_exists($class, $action)) {
            return (call_user_func_array(array($class, $action), $options));
        } else {
            throw new \Exception(sprintf(
                'Cannot dispatch to requestAction %s. Method %s does not exist in controller %s.', 
                $route, 
                $action, 
                $controller
            ));
        }
    }
}
