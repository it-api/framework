<?php

namespace Barebone\View\Interfaces;

interface ViewInterface{
    
    function get_html(): string;
    function render(): void;
    
}
