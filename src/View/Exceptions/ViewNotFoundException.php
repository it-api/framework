<?php

/*
  ViewNotFoundException.php
  UTF-8
  26-mrt-2024 19:30:50
  Creator Frank
 */

namespace Barebone\View\Exceptions;

use Barebone\Router;
use Barebone\Config;
/**
 * Description of ViewNotFoundException
 *
 * @author Frank
 */
class ViewNotFoundException extends \Exception {
	
	function __construct($message, $code=0){
	    $router = Router::getInstance();
	    $config = Config::getInstance();
		if($config->getValue('application', 'production') === "false"){
    	    echo '<div class="container-fluid">
                <div class="row-fluid">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
            	    echo '<h1>View not found</h1>';
            	    echo '<p>You are trying to render a view that does not exist.';
            	    echo "<p><img src=/images/tick.png> Application: {$router->application}</p>";
            	    echo "<p><img src=/images/tick.png> Controller: {$router->controller}</p>";
            	    echo "<p><img src=/images/tick.png> Action: {$router->action}</p>";
            	    echo "<p><img src=/images/error.png> View: {$router->action}.phtml</p>";
            	    echo "<pre>{$message}</pre></div>
                </div>
            </div>";
		}   
	}
}
