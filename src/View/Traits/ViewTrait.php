<?php

namespace Barebone\View\Traits;

use Barebone\View\Interfaces\ViewInterface;

trait ViewTrait{
    
    protected ViewInterface $view;
    
    public function setView(ViewInterface $view) {
        $this->view = $view;
    }
    
    public function getView() {
        return $this->view;
    }
}