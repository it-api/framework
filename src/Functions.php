<?php

namespace Barebone;

class Functions {

    /**
     *
     * Generate a random password
     * @param integer $length
     */
    public static function generatePassword(int $length = 8): string {
        $chars = 'abdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $numChars = strlen($chars);

        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }
        return $string;
    }

    /**
     * Remove HTML tags, including invisible text such as style and
     * script code, and embedded objects.  Add line breaks around
     * block-level tags to prevent word joining after tag removal.
     * @param string $text
     */
    public static function html2txt(string $text): string {

        $text = preg_replace(
            array(
                // Remove invisible content
                '@<head[^>]*?>.*?</head>@siu',
                '@<style[^>]*?>.*?</style>@siu',
                '@<script[^>]*?.*?</script>@siu',
                '@<object[^>]*?.*?</object>@siu',
                '@<embed[^>]*?.*?</embed>@siu',
                '@<applet[^>]*?.*?</applet>@siu',
                '@<noframes[^>]*?.*?</noframes>@siu',
                '@<noscript[^>]*?.*?</noscript>@siu',
                '@<noembed[^>]*?.*?</noembed>@siu',
                // Add line breaks before and after blocks
                '@</?((address)|(blockquote)|(center)|(del))@iu',
                '@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
                '@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
                '@</?((table)|(th)|(td)|(caption))@iu',
                '@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
                '@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
                '@</?((frameset)|(frame)|(iframe))@iu',
            ),
            array(
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
                "\n\$0", "\n\$0",
            ),
            $text
        );

        return strip_tags($text);
    }

    /**
     * Shorthand for print_r() with a <pre></pre> tag around it
     * @param mixed object / array $data
     * @param bool $exit
     */
    public static function debug(mixed $data, bool $exit = false) {
?>
        <h2 style="color:#78ce17;margin-left:10px">Inspector</h2>
        <?php \Barebone\Inspector::Inspect($data); ?>

        <h2 style="color:#78ce17;margin-left:10px">Backtrace</h2>
        <?php
        $e = new \Exception();
        \Barebone\Inspector::Inspect($e->getTraceAsString());
        ?>

        <?php if (is_object($data)) : ?>
            <h2 style="color:#78ce17;margin-left:10px">Properties in the class</h2>
            <?php \Barebone\Inspector::Inspect(get_class_vars(get_class($data))); ?>
            <h2 style="color:#78ce17;margin-left:10px">Methods in the class: "<?= get_class($data) ?>":</h2>
            <?php \Barebone\Inspector::Inspect(get_class_methods(get_class($data))); ?>
        <?php endif; ?>

<?php
        if ($exit === true) {
            exit();
        }
    }

    /**
     * Check if a radio or checkbox input should have the attribue checked
     * @example &lt;input type="radio" &lt;?=is_checked($a, $b);?> name="etc">
     * @param string $val1
     * @param string $val2
     * @return string
     */
    public static function is_checked(string $val1, string $val2): string {
        if ($val1 === $val2) {
            return 'checked="checked"';
        } else {
            return '';
        }
    }

    /**
     * Check if a select element should have the attribue selected
     * @example &lt;select &lt;?=is_selected($a, $b);?> name="etc"></select>
     * @param string $val1
     * @param string $val2
     * @return string
     */
    public static function is_selected(string $val1, string $val2): string {
        if ($val1 === $val2) {
            return 'selected="selected"';
        } else {
            return '';
        }
    }

    public static function bool_to_string(bool $bool): string {
        if ($bool === true) {
            return 'Ja';
        } else {
            return 'Nee';
        }
    }

    /**
     * Create a slug for use in a url
     * @param string $text
     * @return
     */
    public static function create_slug(string $text, string $char = '-') {

        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', $char, $text);

        // trim
        $text = trim($text, $char);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    /**
     * transfer an array to an object
     * @param array $array
     */
    public static function array_to_object(array $array = [], string $clsname = '') {
        if ($clsname === '') {
            $cls = new \stdClass();
        } else {
            $cls = new $clsname();
        }

        if (!empty($array)) {
            foreach ($array as $key => $val) {
                $cls->$key = $val;
            }
        }
        return $cls;
    }

    /**
     * transfer an object to an array
     * @param object $object
     */
    public static function object_to_array(object $object): array {

        $arr = [];

        if (!is_object($object)) {
            return $arr;
        }

        foreach ($object as $key => $val) {
            $arr[$key] = $val;
        }
        return $arr;
    }
    /**
     * function defination to convert array to xml
     * @param mixed $data
     * @param mixed $xml_data
     * @return void
     */
    public static function array_to_xml($data, &$xml_data) {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                if (is_numeric($key)) {
                    $key = 'item' . $key; //dealing with <0/>..<n/> issues
                }
                $subnode = $xml_data->addChild($key);
                self::array_to_xml($value, $subnode);
            } else {
                $xml_data->addChild("$key", htmlspecialchars("$value"));
            }
        }
    }


    /**
     * 
     * @param object $d
     * @return array
     * @example 
     * // Create new stdClass Object
      $init = new stdClass;

      // Add some test data
      $init->foo = "Test data";
      $init->bar = new stdClass;
      $init->bar->baaz = "Testing";
      $init->bar->fooz = new stdClass;
      $init->bar->fooz->baz = "Testing again";
      $init->foox = "Just test";

      // Convert array to object and then object back to array
      $array = objectToArray($init);
      $object = arrayToObject($array);

      // Print objects and array
      print_r($init);
      echo "\n";
      print_r($array);
      echo "\n";
      print_r($object);
     */
    public static function objectToArray(object $d): array {

        if (is_object($d)) {
            // Gets the properties of the given object
            // with get_object_vars function
            $d = get_object_vars($d);
        }

        // Return array
        return $d;
    }

    /**
     * 
     * @param array $d
     * @return object
     * @example 
     * // Create new stdClass Object
      $init = new stdClass;

      // Add some test data
      $init->foo = "Test data";
      $init->bar = new stdClass;
      $init->bar->baaz = "Testing";
      $init->bar->fooz = new stdClass;
      $init->bar->fooz->baz = "Testing again";
      $init->foox = "Just test";

      // Convert array to object and then object back to array
      $array = objectToArray($init);
      $object = arrayToObject($array);

      // Print objects and array
      print_r($init);
      echo "\n";
      print_r($array);
      echo "\n";
      print_r($object);
     */
    public static function arrayToObject(array $d = []): object {

        if (is_array($d)) {
            /*
    	     * Return array converted to object
    	     * Using __FUNCTION__ (Magic constant)
    	     * for recursive call
    	     */
            return (object) array_map(__FUNCTION__, $d);
        }

        return new \stdClass;
    }

    public static function barebone_application_exists(string $application): bool {
        return is_dir(FULL_BAREBONE_PATH . 'applications/' . $application);
    }

    public static function apache_request_headers() {
        $arh = array();
        $rx_http = '/\AHTTP_/';
        foreach ($_SERVER as $key => $val) {
            if (preg_match($rx_http, $key)) {
                $arh_key = preg_replace($rx_http, '', $key);
                $rx_matches = array();
                // do some nasty string manipulations to restore the original letter case
                // this should work in most cases
                $rx_matches = explode('_', $arh_key);
                if (count($rx_matches) > 0 and strlen($arh_key) > 2) {
                    foreach ($rx_matches as $ak_key => $ak_val)
                        $rx_matches[$ak_key] = ucfirst($ak_val);
                    $arh_key = implode('-', $rx_matches);
                }
                $arh[$arh_key] = $val;
            }
        }
        return ($arh);
    }

    public static function get_segment(int $nr): string {
        $router = Router::getInstance();
        return $router->request[$nr] ?? '';
    }

    public static function get_new_guid(): string {
        $db = Registry::getService('db');
        $uuid = $db->doquery("SELECT UUID() as GUID");
        return $uuid->row->GUID;
    }

    /**
      Create a friendly url

      $myFriendlyURL = friendlyURL("Barca rejects FIFA statement on Olympics row", ".html");
      echo $myFriendlyURL; // will echo barca-rejects-fifa-statement-on-olympics-row.html
     */
    public static function friendlyURL(string $string, string $extension = '', bool $restore = false) {

        if ($restore) {

            $string = str_replace("-", " ", $string);
            $string = str_replace("_", " ", $string);
            $string = ucfirst($string);
            return $string;
        } else {

            $string = str_replace("&", "en", $string);
            $string = preg_replace("`\[.*\]`U", "", $string);
            $string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i', '-', $string);
            $string = htmlentities($string, ENT_COMPAT, 'utf-8');
            $string = preg_replace("`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i", "\\1", $string);
            $string = preg_replace(array("`[^a-z0-9]`i", "`[-]+`"), "-", $string);
            return strtolower(trim($string, '-')) . $extension;
        }
    }

    /*
      tableize_array(array(
      'cell1'=>'cell2',
      'cell3'=>'cell4'
      ));

      class test{
      public $name = 'test';
      public $value = "test";
      public $cells = array('cell5'=>'cell6');
      }
      $intpack = new test;
      tableize_array($intpack);

     */

    public static function tableize_array(array $array) {
        echo '<table width="100%" class="ui-table">';

        foreach ($array as $key => $val) {
            if (is_array($val) || is_object($val)) {
                echo '<tr><td>' . self::tableize_array($val) . '</td></tr>';
            } else {
                echo '<tr><td>' . $key . '</td><td>' . $val . '</td></tr>';
            }
        }
        echo '</table>';
    }

    public static function table_list_array(array $rows) {
        echo '<table width="100%" class="ui-table">';
        foreach ($rows as $cols) {
            echo '<tr>';
            foreach ($cols as $key => $val) {

                if (is_array($val) || is_object($val)) {
                    echo '<td>' . self::table_list_array($val) . '</td>';
                } else {
                    echo '<td>' . $val . '</td>';
                }
            }
            echo '<tr>';
        }
        echo '</table>';
    }

    public static function echo_ui_table(array $array, string $title = 'Your data', int $colspan = 2) {
        echo '<table width="100%" class="ui-table">';
        echo '<tr><td colspan="' . $colspan . '">' . $title . '</td></tr>';
        foreach ($array as $key => $val) {
            if (is_array($val) || is_object($val)) {
                echo '<tr><td>' . self::tableize_array($val) . '</td></tr>';
            } else {
                echo '<tr><td>' . $key . '</td><td>' . $val . '</td></tr>';
            }
        }
        echo '</table>';
        echo "<script>
    	    $('table.ui-table tbody td').addClass('ui-widget-content').css('font-size','16px');
	        $('table.ui-table tbody tr:first-child')
		        .addClass('ui-widget-header')
		        .find('td')
		        .removeClass('ui-widget-content');
    	</script>";
    }

    /**
     * creates a readable table from an multidimensional array or object 
     * @param array $array
     * @param bool $return_as_strng
     * @param bool $collapsable
     * @param bool $collapsed
     * @param string $width
     * @return mixed
     */
    public static function create_table_from_array(
        array $array, 
        bool $return_as_strng = true, 
        bool $collapsable = true, 
        bool $collapsed = true, 
        string $width = '100%'
    ) {

        $html = array();
        $html[] = '<table style="width: ' . $width . '" class="ui-widget table" id="master-table-from-array" cellpadding="4">';

        if ($collapsable && !defined('onlyonce')) {
            $html[] = '<tr>';
            $html[] = '<td valign="top" class="ui-state-default" style="cursor:pointer" onclick="$(\'#master-table-from-array td\').each(function(){$(this).show();})">Show all &gt; &gt; </td>';
            $html[] = '</tr>';
            define('onlyonce', true);
        }
        if ($array)
            foreach ($array as $key => $val) {
                $html[] = '<tr>';
                if ($collapsable) {
                    $html[] = '<td valign="top" class="ui-state-default" style="cursor:pointer" onclick="$(this).next().toggle()">' . $key . ' &gt; </td>';
                    $html[] = '<td valign="top" class="ui-state-active" ' . ($collapsed ? 'style="display:none"' : '') . '>';
                } else {
                    $html[] = '<td valign="top" class="ui-state-default">' . $key . '</td>';
                    $html[] = '<td valign="top" class="ui-state-active">';
                }
                if (is_object($val) || is_array($val)) {
                    $html[] = self::create_table_from_array($val, $return_as_strng, $collapsable, $collapsed);
                } else {
                    $html[] = stripslashes($val);
                }
                $html[] = '</td>';
                $html[] = '</tr>';
            }
        $html[] = '</table>';

        $result = implode("\n", $html);

        if ($return_as_strng === true) {
            return $result;
        } else {
            echo $result;
        }
    }
    /**
     * Opens a utf8 encoded file for windows machines
     * @param string $fileName
     * @return resource
     */
    public static function utf8_fopen_read(string $filename) {
        
        if(!file_exists($filename)){
            throw new \Barebone\Filesystem\Exception\FileNotFoundException();
        }
        
        $fc = iconv('windows-1250', 'utf-8//IGNORE', file_get_contents($filename));
        $handle = fopen("php://memory", "rw");
        fwrite($handle, $fc);
        fseek($handle, 0);
        return $handle;
    }

    /**
     * Remove the Byte Order Mark from a file
     * @param string $string
     * @return string
     * @example
     * 	$string = file_get_contents('/full/path/to/utf8-file.csv');<br>
      $string = rmBOM($string);<br>
      file_put_contents('/full/path/to/utf8-file.csv', $string);
     */
    public static function removem_BOM(string $string) {
        if (substr($string, 0, 3) === pack('CCC', 0xef, 0xbb, 0xbf)) {
            $string = substr($string, 3);
        }
        return $string;
    }

    public static function removem_BOM_from_file(string $filename) {
        
        if(!file_exists($filename)){
            throw new \Barebone\Filesystem\Exception\FileNotFoundException();
        }
        
        $string = file_get_contents($filename);
        if (substr($string, 0, 3) === pack('CCC', 0xef, 0xbb, 0xbf)) {
            $string = substr($string, 3);
        }
        file_put_contents($filename, $string);
        return $string;
    }

    /**
     * wordwrap() with utf-8 support
     * to avoid crazy backtracking when words are longer than $width
     * @param string $string the string to wrap
     * @param integer $width the nr of characters before wrap occurs
     * @param $break the break character use can also be: &lt;br&gt;
     * @param $cut
     */
    public static function utf8_wordwrap(string $string, int $width = 75, string $break = "\n", bool $cut = false) {
        if ($cut) {
            // Match anything 1 to $width chars long followed by whitespace or EOS,
            // otherwise match anything $width chars long
            $search = '/(.{1,' . $width . '})(?:\s|$)|(.{' . $width . '})/uS';
            $replace = '$1$2' . $break;
        } else {
            // Anchor the beginning of the pattern with a lookahead
            // to avoid crazy backtracking when words are longer than $width
            $pattern = '/(?=\s)(.{1,' . $width . '})(?:\s|$)/uS';
            $replace = '$1' . $break;
        }
        return preg_replace($search, $replace, $string);
    }

    public static function csv_to_array(string $filename = '', string $delimiter = ',') {

        if(!file_exists($filename)){
            throw new \Barebone\Filesystem\Exception\FileNotFoundException();
        }

        $header = NULL;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }

    public static function getApplicationConfigFile(string $configfile) {
        $router = new \Barebone\Router();
        $filename = FULL_BAREBONE_PATH . 'applications/' . $router->application . '/config/' . $configfile;
        $file_content = \file_get_contents($filename);
        return \json_decode($file_content);
    }
}
