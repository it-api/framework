<?php

namespace Barebone;
use Barebone\ORM\Bone;

/**
 * Description of ORM
 * ComposerFW 
 * UTF-8
 *
 * @author Frank
 * @filesource ORM.php
 * @since 18-feb-2017 21:43:00
 * 
 */
class ORM extends \RedBeanPHP\R{
 
    /**
     * setup connection only once
     * @var boolean 
     */
    private static bool $setup = false;
    
    /**
     * The name of the database to connect to
     * If ommitted it will use the database in the Configuration
     * @param string $dbname
     */
    public static function init($dbname = null){
        
        $config = Config::getInstance();
        $dbhost = $config->getValue('database', 'dbhost');
        
        if(empty($dbhost)){
            throw new Exception('There is no database configured.');
        }
        
        if(is_null($dbname)){
            $dbname = $config->getValue('database', 'dbname');
        }
        if(self::$setup === false){
            self::setup( 
                    'mysql:host='.$dbhost.';dbname='.$dbname, 
                    $config->getValue('database', 'dbuser'),
                    $config->getValue('database', 'dbpass')
            );
            self::$setup = true;
        }
        //for version 5.3 and higher
        //optional but recommended
        self::useFeatureSet( 'novice/latest' );
    }
    
    /**
     * This will create a table in the database based on the name of the bone and its properties
     * @param string $name
     * @param array $properties
     * @return Bone
     */
    public static function CreateBone($name, $properties = []){
        
        // this will create an Bone and then run the create_models on the database
        $bone = new Bone($name, $properties);
        self::PersistBone($bone);
        return $bone;
        
    }

    /**
     * Get a Bone from the database
     * @param string $tablename the tablename
     * @param integer $id the primary key id
     * @return \RedBeanPHP\OODBBean
     */
    public static function fetchBone($tablename, $id){
        self::init();
        $bone = self::load( $tablename, $id ); //reloads our $tablename
        return $bone;
    }
    
    /**
     * Update a bone in the database
     * @param string $tablename
     * @param integer $id
     * @param array $properties
     * @return Bone
     */
    public static function UpdateBone($tablename, $id, $properties){
        
        self::init();
        $bone = self::fetchBone($tablename, $id);
        foreach($properties as $key => $value){
            $bone->{$key} = $value;
        }
        self::store( $bone );
        self::CreateActiverecords();
        return new Bone($tablename, $properties, $id);
    }
    
    /**
     * This will drop a row from a table in the the database
     * @param string $tablename the name of the table
     * @param integer $id The id of the row
     */
    public static function CrushBone($tablename, $id){
        
        self::init();
        self::trash( $tablename, $id );
        self::CreateActiverecords();
        return true;
        
    }
    
    /**
     * This will drop a table from the database
     * @param string $tablename
     */
    public static function CrushBones($tablename){
        
        self::init();
        self::wipe( $tablename );
        self::exec("DROP TABLE `$tablename`");
        self::CreateActiverecords();
        return true;
        
    }
    /**
     * Store a bone in the database
     * @param Bone $bone
     * @return integer the id of the created row (bone)
     */
    public static function PersistBone(Bone $bone){
        
        self::init();
        $object = self::dispense( $bone->getName() );
        foreach($bone->getProperties() as $key => $value){
            $object->{$key} = $value;
        }
        $id = self::store( $object );
        
        self::CreateActiverecords();
        
        return $id;
    }
    
    /**
     * Create a database table based on the structure of the class
     * @param string $tablename
     * @param object $object
     */
    function PersistObject(string $tablename, $object){
        
        if(!is_object($object)){
            throw new Exception('$object is not an object');
        }
        
        $obj = self::dispense( $tablename );
        foreach($object as $key => $value){
            $obj->{$key} = $value;
        }
        
        $id = self::store( $obj );
        self::CreateActiverecords();
        
        return $id;
    }
    
    /**
     * Creates the activerecords corresponding to the database
     */
    public static function CreateActiverecords(){
        
        $db = new Database();
        $db->drop_activerecords();
        $db->create_activerecords(true, false, false);
    }

}
/**
// this will create an Bean and then run the create_models on the database
$bone = new Bone('Customer', [
    'Username' => 'Tester',
    'Password' => md5('123456verysecurypassword'),
    'Email' => 'frank@test.nl'
]);

$bone = ORM::CreateBone('Customer', [
    'Username' => 'Tester',
    'Password' => md5('123456verysecurypassword'),
    'Email' => 'frank@test.nl'
]);

ORM::PersistBone($bone);

*/