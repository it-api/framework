<?php

/**
 * Doctrine 1.x follows the active record pattern for working with data,
 * where a class corresponds with a database table. For instance,
 * if a programmer wanted to create a new "User" object in a database,
 * he/she would no longer need to write SQL queries,
 * but instead could use the following PHP code:
 *
 * $user = new User();
 * $user->name = "john";
 * $user->password = "doe";
 * $user->save();
 * echo "The user with id $user->id has been saved.";
 *
 */

namespace Barebone;

use Barebone\Database\Connectors\PDO;
use Barebone\Exception;
use Barebone\Validator;

/**
 *
 * @author Frank
 * @uses
 * 		@link Barebone\Database\Connectors\PDO;
 * 		@link Barebone\Activerecord\Exception\RecordNotFound;
 * @package Barebone
 */
class Activerecord implements \Barebone\Activerecord\IActiverecord {

    /**
     * The name of the class id the name of the table
     * @var string
     */
    private string $tablename = '';

    /**
     * There needs to be a primary key available for the Activerecord to work
     * @var string
     */
    private string $primary_key = '';

    /**
     * handle to the pdo of the Database 
     * @var PDO
     */
    private ?PDO $pdo;

    /**
     * Want some more information while you develop turn this on and it will log all query's to the error_log()
     * @var boolean
     */
    protected bool $debug = false;

    /**
     * Save each join as assosiative array;
     * @var array
     */
    protected array $joins = [];
    protected array $selected_columns = [];

    function __construct(mixed $data = null) {
        // the controller allready put a Database object with a pdo handle in registry
        $this->pdo = Registry()->getService('db')->pdo;
        // enable or disable debuggin with configuration file
        if (Config()->getValue('database', 'debug') === true || Config()->getValue('database', 'debug') === true) {
            $this->debug = true;
        }
        // check if we have a connection
        if ($this->pdo === null) {
            throw new Exception('There is no database connection available.');
        }
        // set encoding
        $this->pdo->exec("SET NAMES utf8;");
        // this will extract the name of the table from the class that extends this Activerecord class
        $this->tablename = stristr('\\', get_class($this)) ? substr(strrchr(get_class($this), '\\'), 1) : get_class($this);
        // this will try to fetch the rpimary key of the table
        $this->primary_key = $this->get_primary_key();
        // if the primary key is not found throw an exception
        if (is_null($this->primary_key)) {
            throw new \Exception('The "' . $this->tablename . '" table has no primary key defined.');
        }
        // there is any data set it to corresponding fields
        if (!is_null($data)) {
            $this->set_data($data);
        }
    }

    private function _throw_exception(\PDOStatement $stmt, ?string $sql) {

        $error_info = $stmt->errorInfo();
        error_log(print_r($error_info, true));
        error_log('Could not excecute statement: ' . $sql);
        throw new Exception('Could not excecute statement: ' . $sql . '<p>' . $error_info[2] . '</p>');
    }

    private function _pdo_debug_StrParams($stmt) {
        ob_start();
        $stmt->debugDumpParams();
        $r = ob_get_contents();
        ob_end_clean();
        return $r;
    }

    private function is_protected_propertie(string $propertie) {
        return in_array($propertie, ['pdo', 'selected_columns', 'debug', 'joins', 'primary_key', 'tablename']);
    }

    public function debug(bool $debug): Activerecord {
        $this->debug = $debug;
        return $this;
    }

    public function create(mixed $data = null) {

        if (!is_null($data)) {
            $this->set_data($data);
        }

        return $this->save();
    }

    /**
     * find_by_[columnname].
     * get[columnname]()
     * set[columnname]($value)
     * function call find_by_id or find_by_name find_by_sort_order<br/>
     * @param string $name
     * @param array $arguments
     * @throws Exception if propertie does not exist
     * @return mixed
     */
    function __call(string $name, array $arguments) {

        // function call get{columnname}()
        if (substr($name, 0, 3) === 'get') {
            $columnname = substr($name, 3);
            if (!$this->propertie_exists($columnname)) {
                throw new Exception(sprintf('The column "%s" does not exist in the table "%s"', $columnname, $this->tablename));
            }
            return $this->$columnname;

            // function call set{columnname}($value)
        } elseif (substr($name, 0, 3) === 'set') {
            $columnname = substr($name, 3);
            if (!$this->propertie_exists($columnname)) {
                throw new Exception(sprintf('The column "%s" does not exist in the table "%s"', $columnname, $this->tablename));
            }
            $this->$columnname = $arguments[0];
            return $this;

            // function call find_by_id or find_by_name find_by_sort_order	
        } elseif (substr($name, 0, 8) === 'find_by_') {

            $columnname = str_replace('find_by_', '', $name);

            if (!$this->propertie_exists($columnname)) {
                throw new Exception(sprintf('The column "%s" does not exist in the table "%s"', $columnname, $this->tablename));
            }
            return $this->find($columnname, $arguments[0]);
        }
    }

    /**
     * Magic function to assign properties
     * @param string $name
     * @param string $value
     */
    function __set(string $name, ?string $value) {
        if ($this->propertie_exists($name)) {
            $this->{$name} = $value;
        }
    }
    /**
     * Prevention of Uncaught PDOException: You cannot serialize or unserialize PDO instances
     */
    public function __sleep() {
        unset($this->pdo);
        $properties = [];
        foreach ($this as $key => $val) {
            if ($key != 'pdo') {
                $properties[] = $key;
            }
        }
        return $properties;
    }

    /**
     * replace any non-ascii character with its hex code.
     * @param string $value value to escape
     */
    public function escape(string $value) {
        $return = '';
        for ($i = 0; $i < strlen($value); ++$i) {
            $char = $value[$i];
            $ord = ord($char);
            if ($char !== "'" && $char !== "\"" && $char !== '\\' && $ord >= 32 && $ord <= 126) {
                $return .= $char;
            } else {
                $return .= '\\x' . dechex($ord);
            }
        }
        return $return;
    }

    /**
     * Check if a given propertie exists on the object and is public
     * @param string $propertie
     * @return boolean
     */
    public function propertie_exists(string $propertie) {

        $reflect = new \ReflectionClass($this);
        $props = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC);
        foreach ($props as $key => $columnname) {
            if ($columnname->name === $propertie) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retrieve the columnnames from $this->tablename
     * To have less overhead the columns are only retrieved once per hour from database
     * It uses a cache file if one exists and is not older then one hour
     * @return array of columns of a table
     */
    public function get_columns() {
        // check if var directory exists else create it
        if (!is_dir(FULL_VAR_PATH)) {
            mkdir(FULL_VAR_PATH);
        }
        // check if var/tmp directory exists else create it
        if (!is_dir(TEMP_PATH)) {
            mkdir(TEMP_PATH);
        }
        // check if var/tmp/activerecords directory exists else create it
        if (!is_dir(TEMP_PATH . 'activerecords')) {
            mkdir(TEMP_PATH . 'activerecords');
        }
        // name of the cache file
        $cachefile = TEMP_PATH . 'activerecords/' . $this->tablename . '.json';
        // rmeove cache file if anything is wrong with it
        if (file_exists($cachefile) && is_null(json_decode(file_get_contents($cachefile), TRUE))) {
            unlink($cachefile);
        }
        // create result array
        $columnnames = array();
        // if cache file exists and is not older then 1 day we use this to speed up proces
        if (file_exists($cachefile) && filemtime($cachefile) > time() - 3600) {

            $columnnames = json_decode(file_get_contents($cachefile), TRUE);
            // otherwise we need to (re)create cache file
        } else {
            
            try{
                
                // query to get columns
                $sql = "SHOW FULL COLUMNS  FROM `{$this->tablename}`";
                $stmt = $this->pdo->prepare($sql);
                $stmt->execute();
                $result = $stmt->fetchAll(PDO::FETCH_OBJ);
                // add each row to result array
                foreach ($result as $row) {
                    $columnnames[] = ($row);
                }
                // create cache file
                file_put_contents($cachefile, json_encode($columnnames));
                
            } catch(\PDOException $pde){
                throw new Exception($pde->getMessage());
            }
            
        }
        // return result array
        return $columnnames;
    }

    /**
     * Retrieve a single columnn from $this->tablename
     * @return mixed column of a table
     */
    public function get_column($columnname) {

        $sql = "SHOW FULL COLUMNS  FROM `{$this->tablename}` WHERE Field = '{$columnname}'";

        foreach ($this->pdo->query($sql) as $row) {
            $columnnames[] = ($row);
        }

        return $columnnames[0] ?? false;
    }

    /**
     * Returns the primary key if found, else NULL
     * @return string|NULL
     */
    public function get_primary_key() {

        $columns = $this->get_columns();

        if (is_array($columns)) {

            foreach ($columns as $column) {

                if (is_object($column)) {

                    if ($column->Key === 'PRI') {
                        return $column->Field;
                    }
                } elseif (is_array($column)) {

                    if ($column['Key'] === 'PRI') {
                        return $column['Field'];
                    }
                } else {

                    throw new Exception('Invalid columntype');
                }
            }
        }

        return null;
    }

    /**
     * Will set the properties of the active record to the values of the data array
     * @param mixed $data
     * @param bool $clean cleans the input with htmlentities
     * @return Activerecord
     * 
     * @example
     * $data = array('Id' => 1, 'Username' => 'Tester')<br/>
     * $user = new User();<br/>
     * $user->set_data($data); <br/>
     * now the user object has the properties of the data array | object
     */
    public function set_data(mixed $data = [], bool $clean = true) {

        if (!empty($data) && (is_object($data) || is_array($data))) {
            foreach ($data as $key => $val) {
                if (!$this->is_protected_propertie($key) && property_exists($this, $key)) {
                    if ($clean === true) {
                        $this->{$key} = clean_input((string) $val);
                    } else {
                        $this->{$key} = $val;
                    }
                }
            }
        }
        return $this;
    }
    /**
     * return array with data of the activerecord. this will loose functionality of class
     * @return array 
     */
    function get_data(): array {

        $result = [];
        foreach ($this as $key => $value) {
            if ($this->is_protected_propertie($key)) {
                continue;
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    /**
     * Will set the properties of the active record to the found record
     * @param integer $id
     * @return mixed
     */
    public function get_by_id(int $id) {

        if (!is_numeric($id)) {
            throw new Exception('$id IS NOT NUMERIC');
        }
        $result = false;
        $sql = "SELECT * FROM `{$this->tablename}` WHERE `{$this->primary_key}` = :id LIMIT 1";

        if ($this->debug === true) {
            error_log('Query before PDO prepare statement.');
            error_log($sql);
        }

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);

        if ($stmt->execute() === false) {
            $this->_throw_exception($stmt, $sql);
        }
        $result = $stmt->fetchAll(PDO::FETCH_CLASS, $this->tablename);

        if (!isset($result[0])) {

            if ($this->debug === true) {
                error_log('Not found');
            }
            return false;
        } else {

            if ($this->debug === true) {
                error_log('Executed query');
                error_log(print_r($result[0], true));
            }
        }

        $this->set_data($result[0]);

        $stmt->closeCursor();

        return $this;
    }

    /**
     * Fetch all rows from a database table
     * @param array $options options of optional options every options can be omitted
     * @example $options
     * $options['select'] = 'UserID, Username'; // default
     * 
     * $options['from'] = 'TableUsers'; // default name of activerecord class
     * 
     * $options['conditions'] = 'UserID > 20'; // default null
     * 
     * $options['conditions'] = 'UserID = :userid'; // placeholders
     * 
     * $options['conditions_values'] = [':userid' => $user->Id]
     * 
     * $options['offset'] = 0; // default null
     * 
     * $options['limit'] = 10; // default null
     * 
     * $options['order'] = 'UserID DESC'; // default null
     * 
     * $options['having'] = 'Username NOT IS_NULL'; // default null     
     * 
     * will give you the following query
     * SELECT UserID, Username FROM TableUsers WHERE UserID > 20 LIMIT 0,10 ORDER BY UserID DESC HAVING Username NOT IS_NULL
     * 
     * {select|*} FROM {from} WHERE {conditions} LIMIT {offset},{limit} ORDER BY {order} HAVING {having}
     * 
     * @return array of Activerecord objects
     */
    function fetch_all(array $options = []) {

        $select = '';
        $from = '';
        $where = '';
        $joins = '';
        $limit = '';
        $orderby = '';
        $group = '';
        $having = '';

        if (isset($options['select'])) {

            $select = $options['select'];
        } else {

            $select = '*';
        }

        if (isset($options['from'])) {

            $from = $options['from'];
        } else {

            $from = $this->tablename;
        }

        if (isset($options['limit']) && is_numeric($options['limit'])) {

            if (isset($options['offset']) && !is_null($options['offset'])) {
                $limit = "LIMIT {$options['offset']}, {$options['limit']}";
            } else {
                $limit = "LIMIT 0, {$options['limit']}";
            }
        }

        if (isset($options['order'])) {
            $orderby = "ORDER BY {$options['order']}";
        }

        if (isset($options['group'])) {
            $group = "GROUP BY {$options['group']}";
        }

        if (isset($options['having'])) {
            $having = "HAVING {$options['having']}";
        }

        if (isset($options['joins'])) {
            $joins = "{$options['joins']}";
        } elseif (count($this->joins) > 0) {
            $joins = $this->create_join_strings();
        }

        if (isset($options['conditions'])) {
            $where = "WHERE {$options['conditions']}";
        }

        $result = array();
        $sql = "SELECT $select FROM {$from} {$joins} {$where} {$group} {$orderby} {$limit} {$having};";

        if ($this->debug) {
            \error_log($sql);
        }
        $stmt = $this->pdo->prepare($sql);

        if (isset($options['conditions_values']) && count($options['conditions_values']) > 0) {
            foreach ($options['conditions_values'] as $placeholder => $value) {
                if (gettype($value) === 'integer') {
                    $stmt->bindValue($placeholder, $value, PDO::PARAM_INT);
                } else {
                    $stmt->bindValue($placeholder, $value, PDO::PARAM_STR);
                }
            }
        }

        if ($stmt->execute() === false) {
            $this->_throw_exception($stmt, $sql);
        }
        if (!isset($options['joins']) && count($this->joins) === 0) {
            $result = $stmt->fetchAll(PDO::FETCH_CLASS, get_class($this));
        } else {
            $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        }
        $stmt->closeCursor();

        if ($this->debug) {

            ob_start();
            $stmt->debugDumpParams();
            $buffer = ob_get_contents();
            ob_end_clean();
            echo "<pre>{$buffer}</pre>";
        }

        return $result;
    }
    /**
     * Shorthand for fetch_all
     */
    public function all(array $options = []) {
        return $this->fetch_all($options);
    }
    /**
     * Get first row in table
     */
    public function first() {

        $result = array();
        $sql = "SELECT * FROM `{$this->tablename}` ORDER BY {$this->primary_key} ASC LIMIT 1";

        $stmt = $this->pdo->prepare($sql);
        if ($stmt->execute() === false) {
            $this->_throw_exception($stmt, $sql);
        }
        $result = $stmt->fetchAll(PDO::FETCH_CLASS, $this->tablename);
        $stmt->closeCursor();

        if (isset($result[0])) {

            $this->set_data($result[0]);

            return $this;
        } else {

            return null;
        }
    }
    /**
     * Get last row in table
     */
    public function last() {

        $result = array();
        $sql = "SELECT * FROM `{$this->tablename}` ORDER BY {$this->primary_key} DESC LIMIT 1";

        $stmt = $this->pdo->prepare($sql);
        if ($stmt->execute() === false) {
            $this->_throw_exception($stmt, $sql);
        }
        $result = $stmt->fetchAll(PDO::FETCH_CLASS, $this->tablename);
        $stmt->closeCursor();

        if (isset($result[0])) {

            $this->set_data($result[0]);

            return $this;
        } else {

            return null;
        }
    }

    /**
     * Find certain records in the active record table
     * @param mixed $args
     * 
     * @example $user = new User();<br/>
     * $found = $user->find('id', '1'); will return an array<br/>
     * creates sql SELECT * FROM `User` WHERE `id` = '1'<br/>
     * $foundalso = $user->find('UserID', '>', '1')<br/>
     * creates sql SELECT * FROM `User` WHERE `UserID` > '1'
     * 
     * @example ->find('first'); returns the first row in the table
     * @example ->find('last'); returns the last row in the table
     * @example ->find('all'); returns all the rows in the table BE CAREFULL WITH THIS!!!!
     * @example ->find(1); returns the row with primary key of 1 out the table
     * 
     * @example options WHERE IN
     * $options = [];
     * $options['conditions'] = array('id IN (?)', array(1,2,3)); // WHERE Id IN (1,2,3)
     * ->find($options); returns the rows with primary key 1, 2 and 3 out the table
     * 
     * @example options WHERE
     * $options = [];
     * $options['conditions'] = array('id < ?', 3); // WHERE Id < 3
     * ->find($options); returns the rows with primary key smaller then 3 out the table
     * 
     * 
     */
    public function find(mixed $args) {

        $result = array();

        /**
         * @example
         * $user = new User();<br/>
         * $user->find(1); will set the data of the table row to the activerecord object<br/>
         * now you can echo $user->Username ( if that column would exist in your table ) 
         * creates sql statement "SELECT * FROM `User` WHERE `id` = 1"<br/>
         */
        if (count($this->joins) > 0) {
            $joins = $this->create_join_strings();
        } else {
            $joins = '';
        }
        // last, first $ar->find('first'), $ar->find('last')
        // when 1 numeric argument is passed into this function and the argument is last or first
        if (func_num_args() === 1 && ($args === 'last') || $args === 'first') {

            if ($args === 'last') {
                return $this->last();
            } elseif ($args === 'first') {
                return $this->first();
            }

            // by primary key $ar->find(1)
            // when 1 numeric argument is passed into this function 
        } elseif (func_num_args() === 1 && is_numeric($args)) {

            $key = $this->get_primary_key();
            $val = $args;
            $sql = "SELECT * FROM `{$this->tablename}` {$joins} WHERE `{$this->tablename}`.`{$key}` = :value ;";

            if ($this->debug === true) {
                error_log('Query before PDO prepare statement.');
                error_log($sql);
            }
            $stmt = $this->pdo->prepare($sql);
            if (gettype($val) === 'integer') {
                $stmt->bindValue(':value', $val, PDO::PARAM_INT);
            } else {
                $stmt->bindValue(':value', $val, PDO::PARAM_STR);
            }
            if ($stmt->execute() === false) {
                $this->_throw_exception($stmt, $sql);
            }
            $result = $stmt->fetchAll(PDO::FETCH_CLASS, $this->tablename);
            $stmt->closeCursor();

            $this->set_data($result[0]);
            return $this;
            // when 1 argument is passed into this function      
        } elseif (func_num_args() === 1) {

            $key = func_get_arg(0);
            if (is_array($key)) {

                /* SELECT * FROM Table WHERE [conditions] */
                if (array_key_exists('conditions', $key)) {

                    $where = '';
                    $conditions = $key['conditions'];

                    if (is_array($conditions)) {

                        if (stristr(strtoupper($conditions[0]), 'IN (?)')) {

                            // $conditions = array('conditions' => array('id IN (?)', array(1,2,3)));
                            $inQuery = implode(',', array_fill(0, count($conditions[1]), '?'));
                            $where = str_replace('?', $inQuery, $conditions[0]);
                        } else {

                            // $conditions = array('conditions' => array('id < ?', 3));
                            $where = "{$conditions[0]}";
                        }
                    } else {

                        // $conditions = array('conditions' => 'id < 3');
                        $where = $conditions;
                    }


                    $sql = "SELECT * FROM `{$this->tablename}` WHERE $where";
                    if ($this->debug === true) {
                        error_log('Query before PDO prepare statement.');
                        error_log($sql);
                    }
                    $stmt = $this->pdo->prepare($sql);

                    $i = 1;
                    if (is_array($conditions)) {

                        if (is_array($conditions[1])) {

                            foreach ($conditions[1] as $condition) {

                                if (is_numeric($condition)) {
                                    $stmt->bindValue($i, $condition);
                                } else {
                                    $stmt->bindValue($i, "$condition");
                                }
                                $i++;
                            }
                        } else {

                            $stmt->bindValue($i, $conditions[1], PDO::PARAM_INT);
                        }
                    }

                    if ($stmt->execute() === false) {
                        $this->_throw_exception($stmt, $sql);
                    }
                    $result = $stmt->fetchAll(PDO::FETCH_CLASS, $this->tablename);

                    $stmt->closeCursor();
                    return $result;
                }
            } else {

                /* SELECT * FROM Table */
                if (strtolower($key) === 'all') {

                    return $this->fetch_all();
                }
            }
            // when 2 arguments are passed into this function     
        } elseif (func_num_args() === 2) {

            $key = func_get_arg(0);
            $val = func_get_arg(1);

            if (strtolower($key) === 'all') {

                return $this->fetch_all($val);
            } else {

                $sql = "SELECT * FROM `{$this->tablename}` {$joins} WHERE `{$this->tablename}`.`{$key}` = :value ;";
                if ($this->debug === true) {
                    error_log('Query before PDO prepare statement.');
                    error_log($sql);
                }
                $stmt = $this->pdo->prepare($sql);
                if (gettype($val) === 'integer') {
                    $stmt->bindValue(':value', $val, PDO::PARAM_INT);
                } else {
                    $stmt->bindValue(':value', $val, PDO::PARAM_STR);
                }
                if ($stmt->execute() === false) {
                    $this->_throw_exception($stmt, $sql);
                }
                if ($joins === '') {
                    $result = $stmt->fetchAll(PDO::FETCH_CLASS, $this->tablename);
                } else {
                    $result = $stmt->fetchAll(PDO::FETCH_CLASS);
                }
                $stmt->closeCursor();
                return $result;
            }
            // when 3 arguments are passed into this function    
        } elseif (func_num_args() === 3) {

            $key      = func_get_arg(0);
            $operator = func_get_arg(1);
            $val      = func_get_arg(2);

            $sql = "SELECT * FROM `{$this->tablename}` WHERE `{$this->tablename}`.`{$key}` {$operator} :value ;";
            if ($this->debug === true) {
                error_log('Query before PDO prepare statement.');
                error_log($sql);
            }
            //echo $sql;
            $stmt = $this->pdo->prepare($sql);
            if (gettype($val) === 'integer') {
                $stmt->bindValue(':value', $val, PDO::PARAM_INT);
            } else {
                $stmt->bindValue(':value', $val, PDO::PARAM_STR);
            }

            if ($stmt->execute() === false) {
                $this->_throw_exception($stmt, $sql);
            }
            $result = $stmt->fetchAll(PDO::FETCH_CLASS, $this->tablename);
            $stmt->closeCursor();
            return $result;
        }
    }

    /**
     * If, for some reason, you need to create a complicated SQL query beyond the capacity of finder options,
     * then you can pass a custom SQL query through Model::find_by_sql().
     * @param string $sql
     * @return array of objects: this->tablename
     */
    public function find_by_sql(string $sql, array $params = [], string $objName = '\stdClass') {

        $stmt = $this->pdo->prepare($sql);

        foreach ($params as $paramName => $paramValue) {
            $stmt->bindValue($paramName, $paramValue, PDO::PARAM_STR);
        }
        if ($stmt->execute() === false) {
            $this->_throw_exception($stmt, $sql);
        }

        $result = $stmt->fetchAll(PDO::FETCH_CLASS, $objName);
        $stmt->closeCursor();
        return $result;
    }

    /**
     * "SELECT * FROM customer WHERE id IN (1,2)"
     * @param string $key key you want to find in
     * @param array $array items to find
     * @param string $type [int, string] If you are paranoid about the values you can walk them first.
     * @param boolean $quote add '' quotes If you need to quote wrap them in ''.
     * @return array
     */
    public function find_in(string $key, array $array, string $type = 'int', bool $quote = false) {

        // If you are paranoid about the values you can walk them first. For example, if they must be integers...
        if ($type === 'int') {
            array_walk($array, function (&$value) {
                $value = (int) $value;
            });
        } elseif ($type === 'string') {
            array_walk($array, function (&$value) {
                $value = (string) $value;
            });
        }
        // If you need to quote wrap them in ''.
        // And yes, that's one of the rare instances where you'd need to use PDO::quote()
        if ($quote === true) {
            array_walk($array, function (&$value) {
                $value = $this->pdo->quote($value);
            });
        }

        $options = [];
        $options['select'] = '*';
        $options['conditions'] = array($key . ' IN (?)', $array);
        return $this->find($options);
    }

    /**
     * Insert this object into the database
     * @throws Exception on error prepare statement
     * @return object \Barebone\Activerecord newly created object
     */
    public function insert() {

        // initialize a string
        $fields = '';

        // create prepared statement
        $sql = "INSERT INTO `$this->tablename` SET ";
        foreach ($this as $key => $val) {

            if (!$this->is_protected_propertie($key) && $key !== $this->primary_key && $this->propertie_exists($key)) {
                //$fields .= "`" . $key . "` = " . $this->pdo->quote($val) . " ,";
                $fields .= "`{$key}` = :{$key} ,";
            }
        }

        $fields = rtrim($fields, ' ,');

        // combine the query
        $sql = $sql . $fields;

        if ($this->debug === true) {
            error_log('Query before PDO prepare statement.');
            error_log($sql);
        }

        // prepare the statement
        $stmt = $this->pdo->prepare($sql);
        foreach ($this as $key => $val) {
            if (!$this->is_protected_propertie($key) && $key !== $this->primary_key && $this->propertie_exists($key)) {
                if (gettype($val) === 'integer') {
                    $stmt->bindValue(':' . $key, $val, PDO::PARAM_INT);
                } else {
                    $stmt->bindValue(':' . $key, $val, PDO::PARAM_STR);
                }
            }
        }
        if ($this->debug === true) {
            error_log('Executing query');
        }

        if ($stmt->execute() === false) {
            $this->_throw_exception($stmt, $sql);
        }

        if ($this->debug === true) {
            error_log('Executed query');
            echo '<pre>' . htmlspecialchars($this->_pdo_debug_StrParams($stmt)) . '</pre>';
        }
        // close the cursor
        $stmt->closeCursor();

        if ($this->debug === true) {
            error_log('Cursor closed');
        }
        // get the insertid
        $insert_id = $this->pdo->lastInsertId();

        if ($this->debug === true) {
            error_log('Insertid: ' . $insert_id);
        }

        // return the newly created row
        return $this->get_by_id($insert_id);
    }

    /**
     * Update this object in the database
     * @return boolean true on succes
     * @throws \Exception on error
     */
    public function update() {

        // initialize a string
        $fields = '';

        // get the primary key name of the table
        $primary_key = $this->primary_key;

        // the unique id of the row in the table
        $id = $this->{$primary_key};

        // create prepared statement
        $sql = "UPDATE `{$this->tablename}` SET ";
        foreach ($this as $key => $val) {

            if (!$this->is_protected_propertie($key) && $key !== $this->primary_key && $this->propertie_exists($key)) {
                //$fields .= "`" . $key . "` = " . $this->pdo->quote($val) . " ,";
                $fields .= "`{$key}` = :{$key} ,";
            }
        }

        $fields = rtrim($fields, ' ,');

        // make sure only the row with this id get updated
        $where = " WHERE `{$this->tablename}`.`{$primary_key}` = :uid LIMIT 1";

        // combine the query
        $sql = $sql . $fields . $where;

        if ($this->debug === true) {
            error_log('Query before PDO prepare statement.');
            error_log($sql);
        }

        // prepare the statement
        $stmt = $this->pdo->prepare($sql);
        foreach ($this as $key => $val) {
            if (!$this->is_protected_propertie($key) && $key !== $this->primary_key && $this->propertie_exists($key)) {
                if (gettype($val) === 'integer') {
                    $stmt->bindValue(':' . $key, $val, PDO::PARAM_INT);
                } else {
                    $stmt->bindValue(':' . $key, $val, PDO::PARAM_STR);
                }
            } elseif ($key === $this->primary_key) {
                $stmt->bindValue(':uid', $id, PDO::PARAM_INT);
            }
        }

        if ($this->debug === true) {
            error_log('Executing query');
        }

        // execute the query
        if ($stmt->execute() === false) {
            $this->_throw_exception($stmt, $sql);
        }

        if ($this->debug === true) {
            error_log('Executed query');
            echo '<pre>' . htmlspecialchars($this->_pdo_debug_StrParams($stmt)) . '</pre>';
        }

        // close the cursor
        $stmt->closeCursor();

        if ($this->debug === true) {
            error_log('Cursor closed');
        }

        return true;
    }

    /**
     * Save this object to the database either update if it exists else insert a new row
     * @return mixed
     */
    public function save(): mixed {
        try {
            $primary_key = $this->primary_key;
            if ((int) $this->{$primary_key} > 0 && $this->still_exists()) {
                return $this->update();
            } else {
                return $this->insert();
            }
        } catch (\Exception $e) {
            if ($this->debug === true) {
                print_r($e);
            }
        }
    }

    /**
     * Delete this row from the database
     * @return Activerecord
     */
    public function delete() {
        // get the primary key name
        $primary_key = $this->primary_key;
        // get the unique id from this object
        $id = $this->{$primary_key};
        // delete query
        $sql = "DELETE FROM {$this->tablename} WHERE `{$primary_key}` = :id";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        if ($stmt->execute() === false) {
            $this->_throw_exception($stmt, $sql);
        }
        $stmt->closeCursor();

        return $this;
    }

    /**
     * Count the number of rows in the table
     * @return int
     */
    public function count(string $where = ''): int {

        if (!empty($where) && substr($where, 0, 6) !== 'WHERE ') {
            throw new \Exception('$where argument should begin with the text "WHERE "');
        }

        $sql = "SELECT COUNT(*) AS Total FROM `{$this->tablename}` {$where};";
        $stmt = $this->pdo->prepare($sql);
        if ($stmt->execute() === false) {
            $this->_throw_exception($stmt, $sql);
        }
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        return (int) $result[0]->Total;
    }

    /**
     * Check if this object still exist in the database
     * @return boolean
     */
    public function still_exists(): bool {

        $primary_key = $this->primary_key;
        $id = (int) $this->{$primary_key};
        $where = "WHERE `{$this->tablename}`.`{$primary_key}` = {$id}";
        return $this->count($where) > 0;
    }

    /**
     * This function builds the join function added Activerecords
     * @return string
     */
    private function create_join_strings(): string {

        $jointstring = '';
        $join_join = '';
        // go over each of the added joins
        foreach ($this->joins as $join) {
            // get the name of this table wich is the name of this activerecord
            $tablename = get_class($this);
            // get the join key this is named primary but can be any column
            $primary_key = $join['primary_key'];
            // get the name of the foreign table
            $foreign_table = get_class($join['foreign_table']);
            // get the key of the foreign table
            $foreign_key = $join['foreign_key'];
            // get the jointype
            $jointype = $join['join_type'];
            // if the foreign table has joins get the joins and build a string
            if (count($join['foreign_table']->joins) > 0) {
                $join_join = $join['foreign_table']->create_join_strings();
            }
            // now we have a join string
            $jointstring .= "$jointype JOIN `$foreign_table` ON `$tablename`.`$primary_key` = `$foreign_table`.`$foreign_key`\n$join_join";
        }

        return $jointstring;
    }

    /**
     * join another active record on the one you allready have
     * @param Activerecord $foreign_table
     * @param string $primary_key the key of the Activerecord you allready have
     * @param string $foreign_key the foreign table key
     * @param string $join_type he type of join, eg: INNER, OUTER, LEFT, RIGHT
     */
    public function join(\Barebone\Activerecord $foreign_table, string $primary_key, string $foreign_key, string $join_type = 'INNER') {

        $this->joins[] = array(
            'foreign_table' => $foreign_table,
            'primary_key' => $primary_key,
            'foreign_key' => $foreign_key,
            'join_type' => $join_type
        );
    }
    /**
     * left join another active record on the one you allready have
     * @param Activerecord $foreign_table
     * @param string $primary_key the key of the Activerecord you allready have
     * @param string $foreign_key the foreign table key
     */
    public function left_join(\Barebone\Activerecord $foreign_table, string $primary_key, string $foreign_key) {

        $this->joins[] = array(
            'foreign_table' => $foreign_table,
            'primary_key' => $primary_key,
            'foreign_key' => $foreign_key,
            'join_type' => 'LEFT '
        );
    }
    /**
     * right join another active record on the one you allready have
     * @param Activerecord $foreign_table
     * @param string $primary_key the key of the Activerecord you allready have
     * @param string $foreign_key the foreign table key
     */
    public function right_join(\Barebone\Activerecord $foreign_table, string $primary_key, string $foreign_key) {

        $this->joins[] = array(
            'foreign_table' => $foreign_table,
            'primary_key' => $primary_key,
            'foreign_key' => $foreign_key,
            'join_type' => 'RIGHT '
        );
    }
    /**
     * inner join another active record on the one you allready have
     * @param Activerecord $foreign_table
     * @param string $primary_key the key of the Activerecord you allready have
     * @param string $foreign_key the foreign table key
     */
    public function inner_join(\Barebone\Activerecord $foreign_table, string $primary_key, string $foreign_key) {

        $this->joins[] = array(
            'foreign_table' => $foreign_table,
            'primary_key' => $primary_key,
            'foreign_key' => $foreign_key,
            'join_type' => 'INNER '
        );
    }
    /**
     * outer join another active record on the one you allready have
     * @param Activerecord $foreign_table
     * @param string $primary_key the key of the Activerecord you allready have
     * @param string $foreign_key the foreign table key
     */
    public function outer_join(\Barebone\Activerecord $foreign_table, string $primary_key, string $foreign_key) {

        $this->joins[] = array(
            'foreign_table' => $foreign_table,
            'primary_key' => $primary_key,
            'foreign_key' => $foreign_key,
            'join_type' => 'INNER '
        );
    }
    /**
     * Put in an array of validators and validate the activerecord
     * @param array $validators
     * @return Validator
     */
    public function is_valid(array $validators) {

        $validator = new Validator(\Barebone\Functions::object_to_array($this), $validators);
        $validator->execute();
        return $validator;
    }

    /**
     * 
     * @param array $options options array
     * @param array $columns cooked columns array
     * @return string Grid ouput
     */
    public function toGrid(array $options = [], array $columns = [], Controller $controller = null) {

        $grid = new \Barebone\JQGrid5Model($controller);

        $grid->select_command = "SELECT * FROM `{$this->tablename}`";
        $grid->table = $this->tablename;

        $cols = []; // columns of the grid
        $opts = []; // options of the grid
        foreach ($this->getProperties() as $propertieName => $propVal) {
            $columns[] = [
                "title" => $propertieName, // caption of column, can use HTML tags too
                "name" => $propertieName, // grid column name, same as db field or alias from sql
                //"width" => 20,
                "editable" => true,
                "key" => $propertieName === $this->primary_key ? true : false
            ];
        }

        $opt = array_merge([], $options);
        $opt["caption"] = $this->tablename . " Data";
        $opt['autowidth'] = true;
        $opt['height'] = 'auto';
        $opt["rownumbers"] = true;
        $opt["rownumWidth"] = 30;
        $opt["forceFit"] = true;
        $opt['rowNum'] = 10;
        $opt['resizable'] = true;

        $opt["add_options"] = array('width' => '620');
        $opt["edit_options"] = array('width' => '620');
        $opt["view_options"] = array('width' => '620');

        $grid->set_options($opt);

        $grid->set_actions(array(
            "add" => true,
            "edit" => true,
            "clone" => true,
            "bulkedit" => true,
            "delete" => true,
            "view" => true,
            "rowactions" => true,
            "export_csv" => true,
            "export_excel" => true,
            "autofilter" => true,
            "search" => "advance",
            "inlineadd" => true,
            "showhidecolumns" => true
        ));

        if (count($columns) > 0) {
            $grid->set_columns($columns, true);
        }

        return $grid->render(get_class($this));
    }

    public function render_form(?\Barebone\Form\Decorators\DecoratorInterface $decorator = null, array $form_attr = []) {

        // create a form
        $form = new Form([
            'id' => $form_attr['id'] ?? '',
            'method' => 'POST',
            'action' => $form_attr['action']
        ]);

        if (strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {

            $this->set_data($_POST);
            if ($this->save()) {
                $form->flash($options['savedMessage'] ?? 'Form is saved to the database.', 'success');
            } else {
                $form->flash($options['errorSaveMessage'] ?? 'There was an error while saving the form to the database.', 'danger');
            }
        }

        // go over each column of the record
        foreach ($this->get_columns() as $propertie) {
            // get the type from dictonary
            $type = $form->get_input_type_from_db_type($propertie);
            // the label for this form field
            $label = $propertie['Field'];
            // the name of the form field
            $name = $propertie['Field'];
            // the attributes of the element
            $attributes = array();
            $attributes['id'] = $propertie['Field'];
            // the value attribute
            $attributes['value'] = htmlentities($this->{$propertie['Field']});
            // create new or edit with hidden field for primary
            if ($propertie['Key'] === 'PRI' && $this->{$propertie['Field']} === '') {
                // if the field is the primary key and the field is empty continue
                continue;
            } elseif ($propertie['Key'] === 'PRI' && $this->{$propertie['Field']} > 0) {
                // if the field is the primary key and the field is not empty create hidden field
                $form->addHidden($name, $attributes['value'], $attributes);
                continue;
            }

            if ($type === 'textarea') {
                $attributes['innertext'] = htmlentities($this->{$propertie['Field']});
                $attributes['class'] = 'ckeditor';
                $form->addTextarea($label, $name, $attributes);
            } else {
                $form->addInput($type, $label, $name, $attributes);
            }
        }
        $form->addButton('submit', 'Save', ['class' => 'btn btn-success']);


        // decorate the form if decorator is given in
        if (!is_null($decorator)) {
            $form = new $decorator($form);
        }

        $form->secure_form();
        $form->render();
    }

    /**
     * Create a form from this activerecord
     * @param array $attr form attributes
     * @return \Barebone\Form
     */
    public function get_form(array $form_attr = []) {

        // create a form
        $form = new Form([
            'id' => $form_attr['id'] ?? '',
            'method' => $form_attr['method'] ?? 'POST',
            'action' => $form_attr['action'] ?? ''
        ]);

        // go over each column of the record
        foreach ($this->get_columns() as $propertie) {
            //debug($propertie);
            // get the type from dictonary
            $type = $form->get_input_type_from_db_type($propertie);
            // the label for this form field
            $label = $propertie['Field'];
            // the name of the form field
            $name = $propertie['Field'];
            // the attributes of the element
            $attributes = array();
            $attributes['id'] = $propertie['Field'];
            $attributes['style'] = 'width: 100%;';
            $attributes['class'] = 'form-control';
            // the value attribute
            $attributes['value'] = htmlentities($this->{$propertie['Field']});
            // create new or edit with hidden field for primary
            if ($propertie['Key'] === 'PRI' && $this->{$propertie['Field']} === '') {
                // if the field is the primary key and the field is empty continue
                continue;
            } elseif ($propertie['Key'] === 'PRI' && $this->{$propertie['Field']} > 0) {
                // if the field is the primary key and the field is not empty create hidden field
                $form->addHidden($name, $attributes['value'], $attributes);
                continue;
            }

            if ($type === 'real-textarea') {
                $attributes['innertext'] = htmlentities($this->{$propertie['Field']});
                $attributes['style'] = 'width: 100%; height:300px;padding:10px;';
                $form->addTextarea($label, $name, $attributes);
            } elseif ($type === 'textarea') {
                $attributes['innertext'] = htmlentities($this->{$propertie['Field']});
                //$attributes['class'] = 'ckeditor';
                $form->addTextarea($label, $name, $attributes);
            } else {
                if ($type === 'checkbox') {
                    $attributes['value'] = 1;
                    if ((string) $this->{$name} === '1') {
                        $attributes['checked'] = 'checked';
                    }
                }
                $form->addInput($type, $label, $name, $attributes);
            }
        }

        $form->addButton('submit', 'Save', ['class'=>'btn btn-success', 'style'=>'margin-top: 20px;margin-left: 15px;']);
        

        return $form;
    }

    public function get_selected_columns() {
        return $this->selected_columns;
    }

    /**
     * Create datatable from activerecord. Requires JS files to be loaded.
     * @param View $view the view from the action
     * @param array $options fetch_all options
     * @param array $formatter array of row formatters
     * @return View
     */
    public function toDataTable(\Barebone\View $view, array $options = [], array $formatter = []) {
        ob_start();
        $this->selected_columns = explode(',', $options['select']);
        $columns = $this->get_selected_columns();
?>


        <table id="myTable" class="display" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <?php foreach ($columns as $column) : ?>
                        <th><?= $column ?></th>
                    <?php endforeach; ?>

                </tr>
            </thead>
            <tfoot>
                <tr>
                    <?php foreach ($columns as $column) : ?>
                        <th><?= $column ?></th>
                    <?php endforeach; ?>
                </tr>
            </tfoot>
            <tbody>
                <?php foreach ($this->fetch_all($options) as $row) : ?>
                    <tr>
                        <?php foreach ($columns as $column) : ?>
                            <?php if (isset($formatter[$column])) : ?>
                                <td><?= sprintf($formatter[$column], $row->{$column}, $row->{$column}) ?></td>
                            <?php else : ?>
                                <td><?= $row->{$column} ?></td>
                            <?php endif; ?>

                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <script>
            $(document).ready(function() {
                $('#myTable').DataTable();
            });
        </script>
<?php
        $result = ob_get_clean();
        return $view->append($result);
    }

    /**
     * Return the properties of this object when withdata is true it will also return the data of the object
     * 
     * WARNING: this will return a stdClass object not an Activerecord object
     * @param bool $withdata
     * @return \stdClass
     */
    function getProperties(bool $withdata = false): \stdClass {

        $props = new \stdClass;
        foreach ($this as $key => $val) {
            if (!$this->is_protected_propertie($key)) {
                if ($withdata === true) {
                    $props->{$key} = $val;
                } else {
                    $props->{$key} = "";
                }
            }
        }
        return $props;
    }
    
    function toArray(){
        return (array) $this->getProperties(withdata: true);
    }
    
    function toObject(){
        return $this->getProperties(withdata: true);
    }

    /**
     * Return the properties of this object as a json string
     * @param bool $withdata
     * @return bool|string
     */
    function toJSON(bool $withdata = true): bool|string {
        return json_encode($this->getProperties($withdata), JSON_PRETTY_PRINT);
    }
    
    /**
     * Return the object as \Barebone\HTML\TTable
     * @param array $options
     * @return \Barebone\HTML\TTable
     */
    function toTable(array $options = []): HTML\TTable {

        $table = new \Barebone\HTML\TTable(['class' => 'table']);
        $data = $this->fetch_all($options);
        $tr = $table->addRow();
        foreach ($this->get_columns() as $propertie) {
            $tr->addHeader($propertie['Field']);
        }

        foreach ($data as $object) {
            $tr = $table->addRow();
            foreach ($this->get_columns() as $propertie) {
                if ($propertie['Field'] === $this->primary_key) {
                    $tr->addCell(new HTML\TLink($object->{$propertie['Field']}, '../read/?id=' . $object->{$propertie['Field']}));
                } else {
                    $tr->addCell($object->{$propertie['Field']});
                }
            }
        }

        return $table;
    }
    
    /**
     * Return the object as MD5 string
     * @return string
     */
    function getHash(): string {
        // file deepcode ignore InsecureHash: <No need for more secure hash>
        return md5(serialize($this->toObject()));
    }
    
    function drop_column(string $columnname){
        
        $sql = "ALTER TABLE {$this->tablename} DROP COLUMN `{$columnname}`;";
        
        $stmt = $this->pdo->prepare($sql);
        if ($stmt->execute() === false) {
            $this->_throw_exception($stmt, $sql);
        }
        $stmt->closeCursor();
        return true;
    }
}
