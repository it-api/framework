<?php

/*
  Creator Frank
 */

namespace Barebone;
use \Barebone\Controller;

/**
 * Description of RibbonController
 *
 * @author Frank
 */
class RibbonController extends Controller{
	
	/**
	 * The title of the ribbon
	 * @var string
	 */
	public $ribbon_window_title;
	
	/**
	 * Backstage object
	 * @var Barebone\Ribbon\Backstage
	 */
	public $backstage;
	
	/**
	 * The tabs of the ribbon 
	 * @var array of YaZen_UI_Tabs
	 */
	public $ribbon_tabs = array();	
	
	function __construct( $title = '' ){

		parent::__construct();
		
		$this->layout->set_layout('ribbon-gui');
		
		// the ribbon style sheets
		$this->layout->add_stylesheet('/css/ribbon2010/ribbon.css');
		$this->layout->add_stylesheet('/css/ribbon2010/soft_button.css');
		// the ribbon javascripts
		$this->layout->add_javascript('/js/ribbon2010/ribbon.js');
		
		// we are gonna need the validation library
		$this->layout->add_javascript('/assets/thirdparty/jquery.validate.min.js');
		// php jquery
		$this->layout->add_javascript('/assets/thirdparty/jquery-php.js');
		
		$this->backstage = new \Barebone\Ribbon\Backstage();
		$this->layout->ribbon = $this;
		
		// in the controller you place a function load_ribbon
		if(method_exists($this, 'load_ribbon'))
			$this->load_ribbon();
		
		return $this;
	}
	
	function add_ribbon_tab( $name ){
		$ribbon_tab = new \Barebone\Ribbon\Tab( $name );
		$this->ribbon_tabs[] = $ribbon_tab;
		return $ribbon_tab;
	}	
}
