<?php

namespace Barebone;

/**
 * /**
 * URI Syntax (RFC 3986).
 *
 * <pre>
 * scheme  user  password  host  port  basePath   relativeUrl
 *   |      |      |        |      |    |             |
 * /--\   /--\ /------\ /-------\ /--\/--\/----------------------------\
 * http://john:x0y17575@nette.org:8042/en/manual.php?name=param#fragment  <-- absoluteUrl
 *        \__________________________/\____________/^\________/^\______/
 *                     |                     |           |         |
 *                 authority               path        query    fragment
 * </pre>
 *
 * - authority:   [user[:password]@]host[:port]
 * - hostUrl:     http://user:password@nette.org:8042
 * - basePath:    /en/ (everything before relative URI not including the script name)
 * - baseUrl:     http://user:password@nette.org:8042/en/
 * - relativeUrl: document.php
 * 
 * This will help dealing with urls
 * 
 * @author Frank
 * URL: [http|https]://[user[:pass]@][host][:port][/path]?name=param[#fragment] <-- absoluteUrl
 * @example $url = new URL('http://www.github.com')
 */
class URL {

	public static array $defaultports = [ 		
		'http' 	=> 80,
		'https' => 443,
		'ftp' 	=> 21,
		'sftp' 	=> 22,
		'ssh' 	=> 22,
		'smtp' 	=> 25,
		'news' 	=> 119,
		'nntp' 	=> 119 
	];
	
	/** @var string */
	private string $scheme = '';
	
	/** @var string */
	private string $user = '';
	
	/** @var string */
	private string $password = '';
	
	/** @var string */
	private string $host = '';
	
	/** @var string */
	private ?int $port = 0;
	
	/** @var string */
	private string $path = '';
	
	/** @var string */
	private mixed $query = '';
	
	/** @var string */
	private string $fragment = '';
	
	/** @var read-only string */
	private string $absoluteUrl = '';
	
 	/** @var read-only string */
	private string $authority = '';
	
 	/** @var read-only string */
	private string $hostUrl = '';
	
 	/** @var read-only string */
	private string $basePath = '';
	
 	/** @var read-only string */
	private string $baseUrl = '';
	
	/** @var read-only string */
	private string $relativeUrl = '';
	/** @var read-only string */
	private string $fullUrl = '';
	/** @var read-only string */
	private array $queryParameters = array ();
	
	public function __construct(mixed $url = NULL) {

		if (is_string ( $url )) {
			
			$this->fullUrl = $url;
			
			$p = @parse_url ( $url ); // @ - is escalated to exception
			
			if ($p === FALSE) {
				
				throw new \InvalidArgumentException ( "Malformed or unsupported URI '$url'." );
				
			}
			
			$this->scheme = isset ( $p ['scheme'] ) ? $p ['scheme'] : '';
			$this->port = isset ( $p ['port'] ) ? $p ['port'] : NULL;
			$this->host = isset ( $p ['host'] ) ? rawurldecode ( $p ['host'] ) : '';
			$this->user = isset ( $p ['user'] ) ? rawurldecode ( $p ['user'] ) : '';
			$this->password = isset ( $p ['pass'] ) ? rawurldecode ( $p ['pass'] ) : '';
			$this->setPath ( isset ( $p ['path'] ) ? $p ['path'] : '' );
			$this->setQuery ( isset ( $p ['query'] ) ? $p ['query'] : [ ] );
			$this->fragment = isset ( $p ['fragment'] ) ? rawurldecode ( $p ['fragment'] ) : '';
		
		} elseif ($url instanceof self) {
			
			foreach ( $this as $key => $val ) {
				$this->$key = $url->$key;
			}
			
		}
	
	}

	/**
	 * Let's build a URI from an array (why not :)
	 * @example: array('frontend', 'index', 'index') return /frontend/index/index/
	 * @param array $segments        	
	 */
	public static function build(array $segments = []) {

		return '/' . implode('/', $segments) . '/';
	}

	/**
	 * defaultports
	 * 
	 * @return string 
	 */
	public function getDefaultports() {

		return $this->defaultports;
	
	}

	/**
	 * defaultports
	 * 
	 * @param unkown $defaultports        	
	 * @return self
	 */
	public function setDefaultports($defaultports) {

		$this->defaultports = $defaultports;
		return $this;
	
	}

	/**
	 * scheme
	 * 
	 * @return string 
	 */
	public function getScheme() {

		return $this->scheme;
	
	}

	/**
	 * scheme
	 * 
	 * @param unkown $scheme        	
	 * @return self
	 */
	public function setScheme(string $scheme) {

		$this->scheme = $scheme;
		return $this;
	
	}

	/**
	 * user
	 * 
	 * @return string 
	 */
	public function getUser() {

		return $this->user;
	
	}

	/**
	 * user
	 * 
	 * @param unkown $user        	
	 * @return self
	 */
	public function setUser(string $user) {

		$this->user = $user;
		return $this;
	
	}

	/**
	 * password
	 * 
	 * @return string 
	 */
	public function getPassword() {

		return $this->password;
	
	}

	/**
	 * password
	 * 
	 * @param unkown $password        	
	 * @return self
	 */
	public function setPassword(string $password) {

		$this->password = $password;
		return $this;
	
	}

	/**
	 * host
	 * 
	 * @return string 
	 */
	public function getHost() {

		return $this->host;
	
	}

	/**
	 * host
	 * 
	 * @param unkown $host        	
	 * @return self
	 */
	public function setHost(string $host) {

		$this->host = $host;
		return $this;
	
	}

	/**
	 * port
	 * 
	 * @return string 
	 */
	public function getPort() {

		return $this->port
             ? $this->port
             : (isset($this->defaultPorts[$this->scheme]) ? $this->defaultPorts[$this->scheme] : NULL);
	
	}

	/**
	 * port
	 * 
	 * @param unkown $port        	
	 * @return self
	 */
	public function setPort(int $port) {

		$this->port = $port;
		return $this;
	
	}

	/**
	 * path
	 * 
	 * @return string 
	 */
	public function getPath() {

		return $this->path;
	
	}

	/**
	 * path
	 * 
	 * @param unkown $path        	
	 * @return self
	 */
	public function setPath(string $path) {

		$this->path = (string) $path;
		if ($this->host && substr($this->path, 0, 1) !== '/') {
			$this->path = '/' . $this->path;
		}
		return $this;
	
	}
	
	/**
	 * queryParameters
	 *
	 * @return string 
	 */
	public function getQueryParameters() {
	
		return $this->queryParameters;
	
	}

	/**
	 * query
	 * 
	 * @return string 
	 */
	public function getQuery() {

		return http_build_query($this->query, '', '&', PHP_QUERY_RFC3986);
	
	}

	/**
	 * query
	 * 
	 * @param mixed $query        	
	 * @return self
	 */
	public function setQuery(mixed $query) {

		$this->query = is_array($query) ? $query : self::parseQuery($query);
		return $this;
	
	}
	
	/**
	 * Appends the query part of URI.
	 * @param  mixed $query
	 * @return self
	 */
	public function appendQuery(mixed $query) {
		
		$this->query = is_array($query)
			? $query + $this->query
			: self::parseQuery($this->getQuery() . '&' . $query);
		
		return $this;
	}
	
	/**
	 * @return string
	*/
	public function getQueryParameter(string $name, ?string $default = NULL){
		
		return isset($this->query[$name]) ? $this->query[$name] : $default;
		
	}
	
	public function setQueryParameter(string $name, string $value){
		
         $this->query[$name] = $value;
         return $this;
         
    }

	/**
	 * fragment
	 * 
	 * @return string 
	 */
	public function getFragment() {

		return $this->fragment;
	
	}

	/**
	 * fragment
	 * 
	 * @param unkown $fragment        	
	 * @return self
	 */
	public function setFragment(string $fragment) {

		$this->fragment = $fragment;
		return $this;
	
	}

	/**
	 * absoluteUrl
	 * 
	 * @return string 
	 */
	public function getAbsoluteUrl(): string {

		return $this->getHostUrl() . $this->path
            . (($tmp = $this->getQuery()) ? '?' . $tmp : '')
            . ($this->fragment === '' ? '' : '#' . $this->fragment);
	
	}

	/**
	 * authority
	 * 
	 * @return string 
	 */
	public function getAuthority(): string {

		return $this->host === ''
             ? ''
             : ($this->user !== '' && $this->scheme !== 'http' && $this->scheme !== 'https'
                 ? rawurlencode($this->user) . ($this->password === '' ? '' : ':' . rawurlencode($this->password)) . '@'
                 : '')
             . $this->host
             . ($this->port && (!isset($this->defaultPorts[$this->scheme]) || $this->port !== $this->defaultPorts[$this->scheme])
                 ? ':' . $this->port
                 : '');
	
	}

	/**
	 * hostUrl
	 * 
	 * @return string 
	 */
	public function getHostUrl(): string {

		return ($this->scheme ? $this->scheme . ':' : '') . '//' . $this->getAuthority();
	
	}

	/**
	 * basePath
	 * 
	 * @return string 
	 */
	public function getBasePath(): string {

		$pos = strrpos($this->path, '/');
        return $pos === FALSE ? '' : substr($this->path, 0, $pos + 1);
	
	}

	/**
	 * Returns the base-URI.
	 * 
	 * @return string
	 */
	public function getBaseUrl(): string {

		return $this->getHostUrl() . $this->getBasePath();
	
	}

	/**
	 * Returns the relative-URI
	 * 
	 * @return string
	 */
	public function getRelativeUrl(): string {

		return (string) substr($this->getAbsoluteUrl(), strlen($this->getBaseUrl()));
	
	}
	
	/**
      * URL comparison.
      * @param  string|self
      * @return bool
      */
     public function isEqual($url): bool {
     	
         $url = new self($url);
         $query = $url->query;
         ksort($query);
         $query2 = $this->query;
         ksort($query2);
         $http = in_array($this->scheme, ['http', 'https'], TRUE);
         return $url->scheme === $this->scheme
             && !strcasecmp($url->host, $this->host)
             && $url->getPort() === $this->getPort()
             && ($http || $url->user === $this->user)
             && ($http || $url->password === $this->password)
             && self::unescape($url->path, '%/') === self::unescape($this->path, '%/')
             && $query === $query2
             && $url->fragment === $this->fragment;
         
     }
     
     /**
      * Transforms URL to canonical form.
      * @return self
      */
     public function canonicalize() {
     	
         $this->path = preg_replace_callback(
             '#[^!$&\'()*+,/:;=@%]+#',
             function ($m) { return rawurlencode($m[0]); },
             self::unescape($this->path, '%/')
         );
         $this->host = strtolower($this->host);
         return $this;
     }
     
     /**
      * @return string
      */
     public function __toString(): string {
     	
         return $this->getAbsoluteUrl();
         
     }
     
     /**
      * @return string
      */
     public function jsonSerialize(): string {
     	
         return $this->getAbsoluteUrl();
         
     }
     
	/**
	* Similar to rawurldecode, but preserves reserved chars encoded.
	* @param string $s string to decode
	* @param string $reserved string reserved characters
	* @return string
	*/
     public static function unescape(string $s, string $reserved = '%;/?:@&=+$,'): string {
     	
        // reserved (@see RFC 2396) = ";" | "/" | "?" | ":" | "@" | "&" | "=" | "+" | "$" | ","
        // within a path segment, the characters "/", ";", "=", "?" are reserved
        // within a query component, the characters ";", "/", "?", ":", "@", "&", "=", "+", ",", "$" are reserved.
        if ($reserved !== '') {
            $s = preg_replace_callback(
                '#%(' . substr(chunk_split(bin2hex($reserved), 2, '|'), 0, -1) . ')#i',
                function ($m) { return '%25' . strtoupper($m[1]); },
                $s
            );
        }
        return rawurldecode($s);
    }


    /**
     * Parses query string.
     * @return array
     */
    public static function parseQuery(string $s): array {
    	
         parse_str($s, $res);
         return $res;
    }
    
    public function contains(string $s): bool{
        return str_contains($this->fullUrl, $s);
    }
    
    public function beginsWith(string $s): bool{
        return str_starts_with($this->fullUrl, $s);
    }
    
    public function endsWith(string $s): bool{
        return str_ends_with($this->fullUrl, $s);
    }
    
    public function indexOf(string $s): mixed{
        return stripos($this->fullUrl, $s);
    }
}
