<?php

namespace Barebone;

/**
 * 
    $data = ['test1' => 1, 'test2' => 'Hello world'];
    
    echo (new Codegenerator)->activerecord('\\tbl_admins')->getJSON();
    //{"UserID":null,"GroupID":null,"Username":null,"Password":null [...]}
    echo (new Codegenerator)->activerecord('\\tbl_admins')->wrapIn('data')->getJSON();
    //{"data":{"UserID":null,"GroupID":null,"Username":null,"Password":null,"Language":null [...]} 
    
    echo (new Codegenerator)->data($data)->getJSON();
    //{"UserID":null,"GroupID":null,"Username":null,"Password":null [...]}
    echo (new Codegenerator)->data($data)->wrapIn('hereitis')->wrapIn('mydata')->getJSON(); // this is in reverse order of WrapIn
    //{"data":{"UserID":null,"GroupID":null,"Username":null,"Password":null,"Language":null [...]} 
    
    echo htmlentities((new Codegenerator)->data($data)->getTable());
 */

class Codegenerator {

    private $data;

    function activerecord(string $classname) {
        $this->data = new $classname;
        return $this;
    }

    function data(array|object $data) {
        $this->data = $data;
        return $this;
    }

    function getJSON() {
        return json_encode($this->data);
    }

    function wrapIn(string $name) {
        $data[$name] = $this->data;
        $this->data = $data;
        return $this;
    }

    function getTable() {
        $table = '<table>';
        $table .= '<tr>';
        foreach ($this->data as $key => $val) {

            $table .= '<td>';
            $table .= $key;
            $table .= '</td>';
        }
        $table .= '</tr>';
        $table .= '</table>';
        return $table;
    }
}
