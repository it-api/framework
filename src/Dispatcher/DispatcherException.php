<?php
namespace Barebone\Dispatcher;

use Barebone\Config;
use Barebone\Router;

class DispatcherException extends \RuntimeException{

	function __construct($message, $code=0){
	    
	    $router = Router::getInstance();
	    $config = Config::getInstance();
		if($config->getValue('application', 'production') === "false"){
    	    echo '<div class="container-fluid">
                <div class="row-fluid">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
            	    echo '<h1>Dispatcher Exception</h1>';
            	    echo '<p>Cannot dispatch the route: "'.$router->route->getPath().'" to an action.';
            	    echo '<p>Searched the controller for actiontypes: {'.$router->action.'}[Action|AJAX|JQUERY|JSON|XML] but could not find matching action.</p>';
            	    echo "<p><img src=/images/tick.png> Application: {$router->application}</p>";
            	    echo "<p><img src=/images/tick.png> Controller: {$router->controller}</p>";
            	    echo "<p><img src=/images/error.png> Action: {$router->action}</p>";
            	    echo "<p><img src=/images/error.png> View: {$router->action}.phtml</p>";
            	    echo "<pre>{$message}</pre></div>
                </div>
            </div>";
            exit();
		}
	}
}
