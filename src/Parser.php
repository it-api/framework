<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone;

/**
 * Description of Variables
 *
 * @author Frank
 */
class Parser {

	private $text;

	function __construct(string $text) {
		$this->text = $text;
	}

	function get_text(): string {
		return $this->text;
	}

	/**
	 * Replace variables of the text
	 * 
	 * @param string $search        	
	 * @param string $replace        	
	 */
	function replace_variable(string $search, string $replace) {
		$this->text = str_replace('{' . $search . '}', $replace, $this->text);
		return $this->text;
	}

	/**
	 * Pass in an object or an array and replace the {VARIABLE}
	 * 
	 * @param object|array $data        	
	 */
	function replace_variables(object|array $data): string {
		foreach ($data as $search => $replace) {
			$this->replace_variable($search, $replace);
		}
		return $this->text;
	}
	
	function parse(): string{
		return $this->replace_variables($this);
	}

}
