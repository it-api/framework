<?php

namespace Barebone;

/**
 * Standaard component, A component has properties you can set and get
 * @author Frank
 *
 */
class Component {

	/**
	 * array of properties you set with the magic functions on the object $object->propertie = 'value';
	 * @var array $properties
	 */
	public array $properties = [];

	/**
	 * Construct a component.
	 * @param mixed $data array|object The properties of the component
	 */
	function __construct(mixed $data = null) {
		if (!is_null($data)) {
			$this->setData($data);
		}
	}
	/**
	 * Set properties of this object
	 * @param mixed object|array $data
	 * @example 
	 * $obj->setData(array('one' => 1, 'two' => 2, 'three' => 3));<br/>
	 * echo $obj->one; // 1<br/>
	 * echo $obj->two; // 2
	 */
	function setData(mixed $data = []) {
		if (is_array($data) || is_object($data)) {
			foreach ($data as $key => $val) {
				$this->{$key} = $val;
			}
		}
	}

	/**
	 * Return the properties of this component as an array
	 * @return array
	 */
	function toArray(): array {
		return $this->properties;
	}

	/**
	 * Print out this object or any data as readable format
	 * @param mixed $data
	 * @param bool $vardump
	 */
	function debug(mixed $data = null, bool $vardump = false, $cls_pre = 'prettyprint') {

		echo '<pre class="'.$cls_pre.'">';
		if (!is_null($data))
			if ($vardump)
				var_dump($data);
			else
				print_r($data);
		else
			print_r($this);

		echo '</pre>';
	}

	/**
	 * Set a propertie on this object
	 * @param string $name
	 * @param mixed $value
	 */
	function __set(string $name, mixed $value) {

		if ($name === 'properties') {
			throw new \Barebone\Exception('properties is a reserved word, cannot assign to "' . get_class($this) . '"');
		}
		$this->properties[$name] = $value;
	}

	/**
	 * Get a propertie of this object
	 * @param string $name
	 * @return multitype:
	 */
	function __get(string $name) {
		return $this->properties[$name] ?? null;
	}

	/**
	 * Check if a propertie isset on this object
	 * @param string $name
	 */
	function __isset(string $name) {
		return isset($this->properties[$name]);
	}
	
	function sanitize_array(array $data){
	    $clean = [];
	    foreach($data as $key => $val){
	        if(is_array($val) || is_object($val)){
	            $clean[$key] = $this->sanitize_array($val);
	        } elseif(is_string($val)){
	            $clean[$key] = safehtml($val);
	        }
	    }
	    return $clean;
	}
}
