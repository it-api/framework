<?php
namespace Barebone;

use Barebone\Request\Post;
use Barebone\Request\Get;
use Barebone\Request\Cookie;
use Barebone\Request\Files;
use Barebone\Request\Server;
use Barebone\Request\Session;
use Barebone\Request\Headers;

/**
 * Request object. 
 * 
 * Holds all global variables like post, get, session, headers, files, cookie, server
 * 
 * @author Frank
 *
 */
class Request {

	/**
	 * 
	 * @var Barebone\Request\Post
	 */
	public Post $post;
	/**
	 *
	 * @var Barebone\Request\Get
	 */
	public Get $get;	
	/**
	 *
	 * @var Barebone\Request\Cookie
	 */
	public Cookie $cookie;	
	/**
	 *
	 * @var Barebone\Request\Files
	 */
	public Files $files;	
	/**
	 *
	 * @var Barebone\Request\Server
	 */
	public Server $server;	
	/**
	 *
	 * @var Barebone\Request\Session
	 */
	public Session $session;
	/**
	 *
	 * @var Barebone\Request\Headers
	 */
	public Headers $headers;
	/**
	 *
	 * @var string $request_method
	 */
	public string $request_method;
	/**
	 *
	 * @var string $request_uri
	 */
	private string $request_uri;
	/**
	 *
	 * @var array $segments
	 */
	public array $segments;
	
	/**
	 * Constructor
	 * This will set all global variables to this object
	 */
	function __construct(){
	
		$this->post 		= new Post();
		$this->get 		    = new Get();
		$this->cookie 		= new Cookie();
		$this->files 		= new Files();
		$this->server 		= new Server();
		$this->session 		= new Session();
		$this->headers 	    = new Headers();
		
		$this->request_method = $this->server('REQUEST_METHOD');
		$this->request_uri = $this->getURI();
		//$this->headers = Functions::apache_request_headers();
		
		$request_array = explode('/', ltrim($this->request_uri, '/'));
		for($i = 3;$i < count($request_array); $i++){
			$this->segments[] = new URISegment($request_array[$i]);
		}
	}
	
	function is_post(): bool{
		return strtoupper($this->request_method) === 'POST' && !isset($_REQUEST['bb_faked_posted_request']);
	}
	
	function is_get(): bool{
		return strtoupper($this->request_method) === 'GET';
	}
	
	function is_put(): bool{
		return strtoupper($this->request_method) === 'PUT';
	}
        
    function is_ajax(): bool{
        
        /* AJAX check  */
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {
			/* special ajax here */
			return true;
        }
        
        return false;
    }
	
	function getURI(): string {
		
		if($this->server('PATH_INFO') != ''){
			
			return $this->server('PATH_INFO');
		
		}elseif($this->server('PHP_SELF') != '' && $this->server('SCRIPT_NAME') != ''){
			
			return str_replace($this->server('SCRIPT_NAME'), '', $this->server('PHP_SELF'));
		}
		
		return '';
	}
	
	/**
	 * Index starts at 0 so getSegment(0) will return the first parameter after /app/controller/method/param1
	 * @param int $index
	 * @param string $default if the index not is found you can return a default value
	 * @return URISegment
	 */
	function getSegment( int $index, string $default_value = '' ): string {
		
		return isset($this->segments[$index]) ? $this->segments[$index] : $default_value; 
	}
	
	/**
	 * Return a segment name value pair based on key
	 * 
	 * @example uri: /app/cntrl/method/foo:123/ ->getSegmentByName('foo');
	 * @param int $index
	 * @param string $default if the index not is found you can return a default value
	 * @return URISegment
	 */
	function getSegmentByName( string $name, string $default_value = '' ): string {
		
		foreach ($this->segments as $segment){
			if($segment->getName() === $name){
				return $segment;
			}
		}
		return '';
	}
	
	/**
	 * @return array
	 */
	function getSegments(): array{
		return $this->segments;
	}
	/**
	 * @param string $param
	 * @return string
	 */
	function getQueryParam( string $param ): string {
		return $this->get( $param );
	}
	/**
	 * @return array
	 */
	function getQueryParams(): array{
		return $this->get();
	}
	/**
	 * Object of $_POST.
	 * If param $key is passed this will return the post[$key] in question else it will return the post object
	 * @param string $key
	 * @return boolean|mixed
	 */
	function post( string $key = '' ){
		
		if(empty($key)){
			return $this->post ?? false;
		}else{
			return $this->post->{$key} ?? false;
		}
	}

	/**
	 * Object of $_GET.
	 * If param $key is passed this will return the get[$key] in question else it will return the get object
	 * @param string $key
	 * @return boolean|mixed
	 */
	function get( string $key = '' ){
	
		if(empty($key)){
			return $this->get ?? false;
		}else{
			return $this->get->{$key} ?? false;
		}
	}

	/**
	 * Object of $_COOKIE.
	 * If param $key is passed this will return the cookie[$key] in question else it will return the cookie object
	 * @param string $key
	 * @return boolean|mixed
	 */
	function cookie( string $key = '' ){
	
		if(empty($key)){
			return $this->cookie ?? false;
		}else{
			return $this->cookie->{$key} ?? false;
		}
	}

	/**
	 * Object of $_FILES.
	 * If param $key is passed this will return the files[$key] in question else it will return the files object
	 * @param string $key
	 * @return boolean|mixed
	 */
	function files( string $key = '' ){
	
		if(empty($key)){
			return $this->files ?? false;
		}else{
			return $this->files->{$key} ?? false;
		}
	}

	/**
	 * Object of $_SERVER.
	 * If param $key is passed this will return the server[$key] in question else it will return the server object
	 * @param string $key
	 * @return boolean|mixed
	 */
	function server( string $key = '' ){
	
		if(empty($key)){
			return $this->server ?? false;
		}else{
			return $this->server->{$key} ?? false;
		}
	}

	/**
	 * Object of $_SESSION.
	 * If param $key is passed this will return the session[$key] in question else it will return the session object
	 * @param string $key
	 * @return boolean|mixed
	 */
	function session( string $key = '' ){
	
		if(empty($key)){
			return $this->session ?? false;
		}else{
			return $this->session->{$key} ?? false;
		}
	}

	/**
	 * Object with all the request headers.
	 * If param $key is passed this will return the header in question else it will return the headers object
	 * @param string $key
	 * @return boolean|mixed
	 */
	function headers( string $key = '' ){
	
		if(empty($key)){
			return $this->headers ?? false;
		} else {
			return $this->headers->toArray()[$key] ?? false;
		}
	}

}
