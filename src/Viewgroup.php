<?php

namespace Barebone;

use Barebone\View;

/**
 * This class is a wrapper for more the one view
 * eg 
 * header, navigation, content, footer
 * @author Frank
 *
 */
class Viewgroup{
	
	private array $views = [];
	
    /**
     * 
     * @param array $views
     */
	function __construct(array $views = []){
        
		$router = Router::getInstance();
		
        foreach ($views as $viewname){
            $view = new View();
            $view->filename = $router->get_application_action_view_path() . $viewname . '.phtml';
            $this->addView($view, $viewname);
        }
		
		
	}
	
    /**
     * Add view to the VIewgroup
     * @param mixed $view string|View
     * @param string $viewname
     * @return void
     */
	function addView(mixed $view, string $viewname): void{
		if(!$view instanceof View){
			$view = new View();
			$view->filename = $view;
		}
		$this->views[$viewname] = $view;
	}
    
    /**
     * Get a view from the Viewgroup
     * @param string $viewname
     * @return View
     * @throws \RunTimeException
     */
    function getView(string $viewname): View{
        if(isset($this->views[$viewname])){
            return $this->views[$viewname];
        }else{
            throw new \RunTimeException(sprintf('View %s does not exist!', $viewname));
        }
    }

    /**
     * views
     * @return array
     */
    public function getViews(): array{
        return $this->views;
    }

    /**
     * views
     * @param array $views
     * @return is
     */
    public function setViews($views): Viewgroup{
        $this->views = $views;
        return $this;
    }
    
    public function render(): void{
    	foreach($this->views as $view){
    		$view->render();
    	}
    }

}
