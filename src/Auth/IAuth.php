<?php

namespace Barebone\Auth;

interface IAuth{
	
	public static function login($username, $password);
	
	public static function is_logged_in();
	
	public static function logout();
}
