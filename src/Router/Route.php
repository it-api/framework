<?php

namespace Barebone\Router;


/**
 * Barebone Route
 * @author Frank
 *
 */
class Route {

	/**
	 * The path to route
	 * @example /blog/article/show/123
	 * @param string
	 */
	private $path;

	/**
	 * @return string $path
	 */
	public function getPath() {

		return $this->path;
	}

	/**
	 * @param string $path
	 */
	public function setPath($path) {

		$this->path = $path;
	}

	/**
	 * Constructor
	 * @param string $path
	 * @example /blog/article/show/123
	 */
	function __construct($path) {

		$this->path = $path;
	}
}
