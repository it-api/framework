<?php

namespace Barebone\Router;

/**
 * Barebone Routes
 * @author Frank
 *
 */
class Routes {

    private array $routes = [];
    
    function __construct(){
        
    }
    
    function addRoute(string $uri, $callback): void {
        
        if($this->routeExists($uri)){
            throw new RouterExeption(sprintf('The Route: %s allready exists ', $uri));
        }
        
        $this->routes[$uri] = $callback;
    }
    
    function routeExists(string $uri): bool {
        return isset($this->routes[$uri]);
    }
    
    function dispatchRoute(string $uri){
        echo $uri . ' => ' . $this->routes[$uri];
    }

}
