<?php

namespace Barebone\Router;

use Barebone\Config;

/**
 * Barebone RouterExeption
 * @author Frank
 *
 */
class RouterExeption  extends \RuntimeException {

	function __construct($message, $code = 0, \Exception $previous = null, $path = null) {

		$config = Config::getInstance();
		if ($config->getValue('application', 'production') === "false") {

?>
<html>

<head>
	<title>Exception</title>

	<!-- the meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- the styles -->
	<link href="/css/bootstrap/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css">
	<link href="/css/styles.css" media="screen" rel="stylesheet" type="text/css">
	<link href="/css/bootstrap/css/bootstrap-responsive.min.css" media="screen" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="/js/google-code-prettify/prettify.css">
	<?php if (stristr($config->getValue('barebone-gui', 'skin'), 'dark') || stristr($config->getValue('barebone-gui', 'skin'), 'black')) : ?>
		<link rel="stylesheet" type="text/css" href="/js/google-code-prettify/theme-black.css">
		<style>
			body {
				background-color: gray
			}
		</style>
	<?php endif; ?>
	<!-- the scripts -->
	<script type="text/javascript" src="/js/jquery/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="/js/bootstrap/bootstrap.min.js"></script>


</head>

<body>
	<div class="container">
		<?php echo '<pre class="prettyprint lang-php">' . $message . '</pre>'; ?>
		<pre class="prettyprint lang-php"><?= $this->getTraceAsString() ?></pre>
	</div>
	<script src="/js/google-code-prettify/prettify.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {

			// make code pretty
			window.prettyPrint && prettyPrint()

		});
	</script>
</body>

</html>
<?php
			exit();
		} else {

			// inform somebody of the error
			// TODO: CREATE A LOGBOOK FOR THIS
		}
	}
}
