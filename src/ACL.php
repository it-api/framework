<?php

namespace Barebone;

use Barebone\ACL\ACL_Group;
use Barebone\ACL\ACL_User;
use Barebone\ACL\ACL_Rule;

class ACL {

    /**
     * Groups collection
     * @var array $groups
     */
    public array $groups = [];

    /**
     * Users collection
     * @var array $users
     */
    public array $users = [];

    /**
     * Rules collection
     * What are users and groups allowed to do
     * @var array $rules
     */
    public array $rules = [];
    /**
     * @var string hashing algoritme
     * If you change this all passwords are invalidated immediatly
     * This ACL is by no means to be used in a production enviroment!
     * It is just giving the illusion of security :)
     */
    private string $hashing = 'md5|md5|sha1';

    /**
     * The constructor will load the access control list
     * So the groups, users and rules get loaded into the properties
     */
    public function __construct() {
        
        $this->load_access_control_list();

    }
    /**
     * Destructor VOID
     */
    function __destruct() {
        //$this->save_access_control_list();
    }
    
    public function encode(string $string): string{
        // this is by no means to be used in a production enviroment!
        $output = $string;
        $hashes = explode('|', $this->hashing);
        
        foreach($hashes as $hashfunc){
            $output = $hashfunc($output);
        }
        return $output;
    }
    private function decode(string $string){
        // not possible
    }
    
    /**
     * Saves the acces control list to disk
     */
    function save_access_control_list(){
        
        $filename = FULL_BAREBONE_PATH . 'config/acl.json';
        
        $data = [];
        $data['users'] = (array) $this->getUsers();
        $data['groups'] = (array) $this->getGroups();
        $data['rules'] = (array) $this->getRules();
        
        $json = json_encode($data);
        file_put_contents($filename, $json, JSON_PRETTY_PRINT);
    }
    
    /**
     * Loads the acces control list from disk
     */
    function load_access_control_list(){
        
        $filename = FULL_BAREBONE_PATH . 'config/acl.json';
        if(file_exists($filename)){
            $json = file_get_contents($filename);
            $data = json_decode($json);
            
            if(!isset($data->groups) && !isset($data->users) && !isset($data->rules)){
                throw new Exception('You are trying to load an invalid ACL');
            }
            
            $this->setGroups((array) $data->groups);
            $this->setUsers((array) $data->users);
            $this->setRules((array) $data->rules);
        }
    }
    /**
     * Check if a user is valid against the ACL
     * @param string $username
     * @param string $password
     * @return boolean
     */
    function valid_user($username, $password){
        
        foreach ($this->getUsers() as $acluser){
            if($username === $acluser->username && $this->encode($password) === $acluser->password){

                return new ACL_User($acluser);
            }
        }
        return false;
    }
    
    /**
     * Check is a user is denied
     * @param ACL_User $user
     * @return boolean
     */
    function isDenied(ACL_User $user){
        
        $router = Router::getInstance();
        $application = $router->application;
        $controller = $router->controller;
        $action = $router->action;
        
        // go over each rule in the rules book :)
        foreach ($this->getRules() as $rule) {
            
            // if there is a deny rule we process that first
            if ($user->id === $rule->user_id) {
                if (
                        ($rule->application === $application) 
                        && ($rule->controller === $controller)
                        && ($rule->action === $action)
                        && ($rule->deny === true)
                ){
                    return true;
                }
            }
        }
        // we checked al the rules none was denied access, all good.
        return false;
    }

    /**
     * Check is a user is allowed to use the resource
     * Deny goes before Allow
     * @param ACL_User $user
     * @return boolean
     */
    function IsAllowed(ACL_User $user) {
        
        // first check denied access
        if($this->isDenied($user) ){
            return false;
        }
        
        $router = Router::getInstance();
        $application = $router->application;
        $controller = $router->controller;
        $action = $router->action;
        // go over each rule in the rules book :)
        foreach ($this->getRules() as $rule) {

            // if there is a rule for this user			
            if ($user->id === $rule->user_id) {
                
                if($rule->application === $application || $rule->application === '*'){
                    if($rule->controller === $controller || $rule->controller === '*'){
                        if($rule->action === $action || $rule->action === '*'){
                            return true;
                        }
                    }
                }
                
            // else if there is a rule for the group
            } elseif ($user->group_id === $rule->group_id) {
                
                if($rule->application === $application || $rule->application === '*'){
                    if($rule->controller === $controller || $rule->controller === '*'){
                        if($rule->action === $action || $rule->action === '*'){
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Getter groups
     * @return array
     */
    public function getGroups(): array {
        return $this->groups;
    }
    
    /**
     * Getter users
     * @return array
     */
    public function getUsers(): array {
        return $this->users;
    }

    /**
     * Getter rules
     * @return array
     */
    public function getRules(): array {
        return $this->rules;
    }

    /**
     * Setter groups
     * @return ACL
     */
    public function setGroups(array $groups) {
        foreach ($groups as $group){
            $this->groups[$group->id] = $group;
        }
        return $this;
    }

    /**
     * Setter users
     * @return ACL
     */
    public function setUsers(array $users) {
        foreach ($users as $user){
            $this->users[$user->id] = $user;
        }
        return $this;
    }

    /**
     * Setter rules
     * @return ACL
     */
    public function setRules(array $rules) {
        foreach ($rules as $rule){
            $this->rules[$rule->id] = $rule;
        }
        $this->rules = $rules;
        return $this;
    }

    /**
     * Add a group
     * @return ACL
     */
    function addGroup(ACL_Group $group){    
        if($group->id === null){
            $group->id = uniqid('group_');
        }
        $this->groups[$group->id] = $group;
        $this->save_access_control_list();
        return $this;
    }
    
    /**
     * Add a user
     * @return ACL
     */
    function addUser(ACL_User $user){
        if($user->id === null){
            $user->id = uniqid('user_');
        }
        $user->password = $this->encode($user->password);
        $this->users[$user->id] = $user;
        $this->save_access_control_list();
        return $this;
    }
    
    /**
     * Add a rule
     * @return ACL
     */
    function addRule(ACL_Rule $rule){
        if($rule->id === null){
            $rule->id = uniqid('rule_');
        }
        $this->rules[$rule->id] = $rule;
        $this->save_access_control_list();
        return $this;
    }
    
    /**
     * Save a group to the ACL
     * @return ACL
     */
    function saveGroup(ACL_Group $group){
        if($group->id === ''){
            $group->id = uniqid('group_');
            $this->groups[$group->id] = $group;
        }else{
            $aclgroup = $this->getGroupByID($group->id);
            $aclgroup->name = $group->name;
        }
        
        $this->save_access_control_list();
        return $this;
    }
    
    /**
     * Save a user to the ACL
     * @return ACL
     */
    function saveUser(ACL_User $user){
        if($user->id === ''){
            $user->id = uniqid('user_');
            $user->group_id = $user->group_id;
            $user->username = $user->username;
            $user->password = $this->encode($user->password);
            $user->email = $user->email;
            $this->users[$user->id] = $user;
        }else{
            $acluser = $this->getUserByID($user->id);
            $acluser->group_id = $user->group_id;
            $acluser->username = $user->username;
            $acluser->password = $this->encode($user->password);
            $acluser->email = $user->email;
            $this->users[$user->id] = $acluser;
        }
        
        $this->save_access_control_list();
        return $this;
    }
    
    /**
     * Save a rule to the ACL
     * @return ACL
     */
    function saveRule(ACL_Rule $rule){
        if($rule->id === ''){
            $rule->id = uniqid('rule_');
            $this->rules[$rule->id] = $rule;
        }else{
            $aclrule = $this->getRuleByID($rule->id);
            $aclrule->user_id = $rule->user_id;
            $aclrule->group_id = $rule->group_id;
            $aclrule->application = $rule->application;
            $aclrule->controller = $rule->controller;
            $aclrule->action = $rule->action;
            $aclrule->allow = $rule->allow;
            $aclrule->deny = $rule->deny;
        }
        
        $this->save_access_control_list();
        return $this;
    }
    
    /**
     * Get a group by the id
     * @param string $id
     * @return ACL_Group
     */
    function getGroupByID($id){
        foreach ($this->getGroups() as $group){
            if($id === $group->id){
                return new ACL_Group($group);
            }
        }
        
        return false;
    }
    
    /**
     * Get a user by the id
     * @param string $id
     * @return ACL_User
     */
    function getUserByID($id): ACL_User{
        foreach ($this->getUsers() as $user){
            if($id === $user->id){
                return new ACL_User($user);
            }
        }
        
        return false;
        
    }
    
    /**
     * Get a rule by its id
     * @param string $id
     * @return ACL_Rule
     */
    function getRuleByID($id){
        foreach ($this->getRules()as $rule){
            if($id === $rule->id){
                return new ACL_Rule($rule);
            }
        }
        
        return false;
    }
    
    /**
     * Delete an item from the ACL
     * @param string $item on of [users,groups,rules]
     * @param string $id The id of the item to delete
     */
    function delete($item, $id){
        switch ($item) {
            case 'users':
                $this->deleteUser($id);
                break;
            case 'groups':
                $this->deleteGroup($id);
                break;
            case 'rules':
                $this->deleteRule($id);
                break;
            default:
                break;
        }
    }
    
    /**
     * Delete a user from the ACL
     * @param string $id
     * @return $this
     */
    function deleteUser($id){        
        foreach ($this->getUsers() as $key => $user){
            if($id === $user->id){
                unset($this->users[$key]);
            }
        }        
        $this->save_access_control_list();
        return $this;
    }
    
    /**
     * Delete a group from the ACL
     * @param string $id
     * @return $this
     */
    function deleteGroup($id){
        foreach ($this->getGroups() as $key => $group){
            if($id === $group->id){
                unset($this->groups[$key]);
            }
        }
        $this->save_access_control_list();
        return $this;
    }
    
    /**
     * Delete a rule from the ACL
     * @param string $id
     * @return $this
     */
    function deleteRule($id){
        foreach ($this->getRules() as $key => $rule){
            if($id === $rule->id){
                unset($this->rules[$key]);
            }
        }
        
        $this->save_access_control_list();
        return $this;
    }
    
    public static function getAuthUser(){
        return $_SESSION['ACL_USER'] ?? null;
    }
}
