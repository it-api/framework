<?php

/*
  Creator Frank
 */

namespace Barebone\Math;

/**
 * Description of OperationNode
 *
 * @author Frank
 */
interface InterfaceOperation {
	
	public function evaluate();
	
}
