<?php

/*
  Creator Frank
 */

namespace Barebone\Math;

/**
 * Description of AdditionNode
 *
 * @author Frank
 * $a = new AdditionNode(1, 3);
 * echo $a->evaluate();
 */
class Addition implements InterfaceOperation{
	
	private $left;
	private $right;
	public function __construct($left, $right) {
		$this->left = $left;
		$this->right = $right;
	}

	public function evaluate() {
		return $this->left + $this->right;
	}

}
