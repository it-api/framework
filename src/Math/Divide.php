<?php

/*
  Creator Frank
 */

namespace Barebone\Math;

/**
 * Description of DivideNode
 *
 * @author Frank
 */
class Divide implements InterfaceOperation{
	
	private $left;
	private $right;
	public function __construct($left, $right) {
		$this->left = $left;
		$this->right = $right;
	}

	public function evaluate() {
		return ($this->left / $this->right);
	}

}
