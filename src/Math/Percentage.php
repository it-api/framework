<?php

/*
  Creator Frank
 */

namespace Barebone\Math;

/**
 * Description of Percentage
 *
 * @author Frank
 */
class Percentage implements InterfaceOperation{
	
	private $left;
	private $right;
	
	public function __construct($left, $right) {
		$this->left = $left;
		$this->right = $right;
	}

	public function evaluate() {
		$percentage = ( $this->left / $this->right ) * 100;
		return $percentage;
	}
}
