<?php

/*
  Creator Frank
 */

namespace Barebone\Math;

/**
 * Description of SubtractNode
 *
 * @author Frank
 */
class Subtract implements InterfaceOperation{
	
	private $left;
	private $right;
	
	public function __construct($left, $right) {
		$this->left = $left;
		$this->right = $right;
	}

	public function evaluate() {
		return $this->left - $this->right;
	}

}
