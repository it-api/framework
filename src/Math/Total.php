<?php

/*
  Creator Frank
 */

namespace Barebone\Math;

/**
 * Description of Total
 *
 * @author Frank
 */
class Total implements InterfaceOperation{
	
	private $objects = [];
	
	public function __construct() {
		$args = func_get_args();
		$this->objects = $args;
	}

	
	public function evaluate() {
		$result = 0;
		foreach($this->objects as $object){
			if(is_object($object) && method_exists($object, 'evaluate')){
				$result += $object->evaluate();
			}elseif(is_numeric($object) || is_float($object)){
				$result += $object;
			}
		}
		return $result;
	}

}
