<?php

namespace Barebone;

use Barebone\Application\Abstracts\ApplicationAbstract;
use Barebone\ACL;
use Barebone\ACL\ACL_User;

/**
 * This is what makes Barebone tick the Application object
 * 
 * @author Frank
 *
 */
class Application extends ApplicationAbstract{

    /**
     * Holds the router object
     * @var Router $router
     */
    public Router $router;
    /**
     * Turn debugging on/off
     */
    private bool $debug = false;
    /**
     * Application constructor.
     * If you have a single application, lets say just a blog
     * You set this as the base app
     * @param string|null $app_name
     */
    function __construct(?string $app_name = null) {

        Autoloader::getInstance();
        
        // load the FULL_APPS_PATH from the config file
        define('FULL_APPS_PATH', FULL_BAREBONE_PATH . Config()->getValue('application', 'applications_path'));

        $this->router = Router::getInstance();
        // set the routers default application
        if (!is_null($app_name)) {
            $this->router->application = $app_name;
        }
        $this->setName($this->router->application);
        // if no session is started, start a session and set Guest access
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
      
        /**
         * If the ACL is enabled in the configuration
         */
        if (Config()->getValue('ACL', 'enabled') === 'true') {
            // and if config enables auto_create_guest_session
            if(Config()->getValue('ACL', 'auto_create_guest_session') === 'true'){
                // create guest in session
                if(!isset($_SESSION['ACL_USER'])){
                    
                    $guest = new ACL_User();
                    $guest->username = 'guest';
                    $guest->password = '********';
                    $guest->group_id = 'group_000000000000';
                    $guest->id = 'user_00000000000';
                    $guest->hash = (new \Barebone\ACL())->encode(serialize($guest));
                    $_SESSION['ACL_USER'] = $guest;
                }
            }            
            /**
             * If the ACL is enabled in the configuration 
             * we check the access rights before anything else
             */
            $this->validate_access();
        }

        /**
         * If the ORM is frozen
         * If frozen it will prevent changes by ORM Beans
         */
        if (
            Config()->getValue('barebone-gui', 'configured') === 'true'
            && Config()->getValue('ORM', 'freeze') === 'true'
            && Config()->getValue('database', 'dbname') !== ''
        ) {
            ORM::init();
            ORM::freeze();
        }
    }

    /**
     * Validate access to the resource
     * @return void
     */
    function validate_access() {

        $request = new Request();
        
        // we need to exclude the login form here else it will be imposible to login
        // if the current url is not /auth/index/ or /api (most likely has it own authentication)
        if (
            substr($_SERVER['REQUEST_URI']??'', 0, 12) !== '/auth/index/' 
            && substr($_SERVER['REQUEST_URI']??'', 0, 4) !== '/api') 
        {

            // and the user is not authenticated
            if (!isset($_SESSION['ACL_USER'])) {

                if ($request->is_ajax() && str_contains($request->headers('Accept'), 'application/json, text/javascript, */*; q=0.01')) {

                    jQuery::init();
                    jQuery::evalScript('autosave_stop();$.messager.alert({title: "Oops1!", msg: "'.sprintf("No access allowed in this ajax call: '%s'. You are not logged in anymore. Log in again.",$request->getURI()).'", icon: "warning", width: "400"})');
                    jQuery::getResponse();
                    
                } elseif ($request->is_ajax() && str_contains($request->headers('Accept'), '*/*')){
                    echo '<script>autosave_stop();$.messager.alert({title: "Oops2!", msg: "'.sprintf("No access allowed in this ajax call: '%s'. You are not logged in anymore. Log in again.",$request->getURI()).'", icon: "warning", width: "400"});</script>';
                    die();
                } else {

                    header('Location: /auth/index/?referer=' . $_SERVER['REQUEST_URI']);
                    exit;
                }
            }

            // since we are authenticated does not mean we have access to the resource
            // there is the ACL for
            $acl = new ACL();
            $acluser = $_SESSION['ACL_USER'];
           
            // check if the logged in user is allowed to access the resource
            if ($acl->IsAllowed($acluser) === false) {
     
                if ($request->is_ajax() && stristr($request->headers('Accept'), 'application/json, text/javascript, */*; q=0.01')) {

                    jQuery::init();
                    jQuery::evalScript('$.messager.alert({title: "Oops3!", msg: "'.sprintf("No access allowed in this ajax call: '%s'. You are not logged in anymore. Log in again.",$request->getURI()).'", icon: "warning", width: "400"});autosave_stop();');
                    jQuery::getResponse();
                    
                } elseif ($request->is_ajax() && stristr($request->headers('Accept'), '*/*')){
                    echo '<script>autosave_stop();$.messager.alert({title: "Oops4!", msg: "'.sprintf("No access allowed in this ajax call: '%s'. You are not logged in anymore. Log in again.",$request->getURI()).'", icon: "warning", width: "400"});</script>';
                    die();
                } else {

                    header('Location: /auth/index/?referer=' . $_SERVER['REQUEST_URI']);
                    exit;
                }
            }
        }        
    }
    /**
     * Run the application
     * @return void
     */
    function run() {
        
        // register a fatal error handler
        //register_shutdown_function(array($this, 'fatal_error_handler'));
        
        date_default_timezone_set(Config()->getValue('application','timezone'));
        
        // if the router knows the way the dispatcher has to go
        if ($this->router->can_resolve_route()) {

            // if we can resolve the route we dispatch the request
            Dispatcher::dispatch();
        } else {

            // if we can't resolve the route we dispatch the request to 404 function in the errors controller
            header('HTTP/1.1 404 Not Found');
            Dispatcher::reroute('frontend/errors/index/404');
        }
    }
    /**
     * Register fatal error handler will show error page
     * You could change this to sending a mail on production servers
     * To monitor if there are errors in your code
     * @return void
     */
    function fatal_error_handler(): void {

        if (is_array($e = error_get_last())) {
            //if (isset($e['type']) && $e['type'] == 1) {
                header("Status: 500 Internal Server Error");
                header("HTTP/1.0 500 Internal Server Error");

                echo '<h1>500 Internal Server Error</h1>';

                $body = '<pre>' . wordwrap(print_r($e, true), 70, "\r\n") . '</pre>';
                echo $body;
                exit();
            //}
        }
    }
    /**
     * Enable the debugger will show all error messages except E_NOTICE
     * @return void
     */
    function enableDebugger(): void {

        error_reporting(E_ALL ^ (E_NOTICE));
        ini_set('display_errors', 1);
        
        $this->debug = true;
        Config()->setValue('application', 'debug', 'true');
        Config()->setValue('application', 'production', 'false');
    }
    /**
     * Disable the debugger will prevent error message from accedentily being shown
     * @return void
     */
    function disableDebugger(): void {

        error_reporting(0);
        ini_set('display_errors', 0);
        $this->debug = false;
        Config()->setValue('application', 'debug', 'false');
        Config()->setValue('application', 'production', 'true');
    }
}
