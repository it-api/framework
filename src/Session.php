<?php

namespace Barebone;

class Session {
	
	/**
	 * Discard session array changes and finish session
	 */
    function abort() : bool{
        return session_abort();
    }
    /**
     * Get and/or set current cache expire
     * @param int $value
     * @return string|false
     */
    function cache_expire(int $value = null){
        if(is_null($value)){
            return session_cache_expire();
        } else {
            return session_cache_expire($value);
        }
    }
    /**
     * Get and/or set the current cache limiter
     * Possible values
     
        public 	

        Expires: (sometime in the future, according session.cache_expire)
        Cache-Control: public, max-age=(sometime in the future, according to session.cache_expire)
        Last-Modified: (the timestamp of when the session was last saved)
        
        private_no_expire 	
        
        Cache-Control: private, max-age=(session.cache_expire in the future), pre-check=(session.cache_expire in the future)
        Last-Modified: (the timestamp of when the session was last saved)
        
        private 	
        
        Expires: Thu, 19 Nov 1981 08:52:00 GMT
        Cache-Control: private, max-age=(session.cache_expire in the future), pre-check=(session.cache_expire in the future)
        Last-Modified: (the timestamp of when the session was last saved)
        
        nocache 	
        
        Expires: Thu, 19 Nov 1981 08:52:00 GMT
        Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0
        Pragma: no-cache

     * @param string $value
     * @return string|false
     */
    function cache_limiter(string $value = null){
        if(is_null($value)){
            return session_cache_limiter();
        } else {
            return session_cache_limiter($value);
        }
    }
    /**
     * Alias of session_write_close()
     */
    function commit() : bool{
        return session_commit();
    }
    /**
     * Create new sesion id
     * @param string $prefix
     * @return string|false
     */
    function create_id(string $prefix = ""){
        return session_create_id($prefix);
    }
    /**
     * Decodes the serialized session data provided in $data, and populates the $_SESSION superglobal with the result. 
     * @param string $data
     */
    function decode(string $data) : bool{
        return session_decode($data);
    }
    /**
     * Destroys all data registered to a session
     */
    function destroy() : bool{
        return session_destroy();
    }
    /**
     * Encodes the current session data as a session encoded string
     */
    function encode() : string{
        return session_encode();
    }
    /**
     * Perform session data garbage collection
     */
    function gc(){
        return session_gc();
    }
    /**
     * Get the session cookie parameters
     */
    function get_cookie_params() : array{
        return session_get_cookie_params();
    }
    /**
     * Get and/or set the current session id
     * @param string $id
     * @return string|false
     */
    function id(string $id = null){
        if(is_null($id)){
            return session_id();
        } else {
            return session_id($id);
        }
    }
    /**
     * Get and/or set the current session module
     * @param string $module
     * @return string|false
     */
    function module_name(string $module = null){
        if(is_null($module)){
            return session_module_name();
        } else {
            return session_module_name($module);
        }
    }
    /**
     * Get and/or set the current session name
     * @param string $module
     * @return string|false 
     */
    function name(string $name = null){
        if(is_null($name)){
            return session_name();
        } else {
            return session_name($name);
        }
    }
    /**
     * Update the current session id with a newly generated one, and keep the current session information.
     * @param bool $delete_old_session
     * @return bool
     */
    function regenerate_id(bool $delete_old_session = false) : bool{
         return session_regenerate_id($delete_old_session);
    }
    /**
     * Session shutdown function
     */ 
    function register_shutdown() : void{
        session_register_shutdown();
    }
    /**
     * reinitializes a session with original values stored in session storage. 
     * This function requires an active session and discards changes in $_SESSION. 
     * 
     */
    function reset(){
        return session_reset();
    }
    /**
     * Get and/or set the current session save path
     * @param string $path
     * @return string|false
     */
    function save_path(string $path = null){
         if(is_null($path)){
             return session_save_path();
         } else {
             return session_save_path($path);
         }
    }
    /**
     * Set the session cookie parameters
     * @param int $lifetime_or_options
     * @param string $path
     * @param string $domain
     * @param string $secure
     * @param string $httponly
     * @return bool
     */
    function set_cookie_params($lifetime_or_options, string $path = null,string $domain = null,bool $secure = null,bool $httponly = null) : bool{
        if(is_array($lifetime_or_options)){
            //debug($lifetime_or_options);
            return session_set_cookie_params($lifetime_or_options);
        } else {
            return session_set_cookie_params(
                $lifetime_or_options,
                $path,
                $domain,
                $secure,
                $httponly
            );
        }
    }
    /**
     * Sets user-level session storage functions
     * @param string $open The open callback works like a constructor in classes and is executed when the session is being opened. It is the first callback function executed when the session is started automatically or manually with session_start(). Return value is true for success, false for failure. 
     * @param string $close The close callback works like a destructor in classes and is executed after the session write callback has been called. It is also invoked when session_write_close() is called. Return value should be true for success, false for failure. 
     * @param string $read The read callback must always return a session encoded (serialized) string, or an empty string if there is no data to read. 
     * @param string $write  The write callback is called when the session needs to be saved and closed. This callback receives the current session ID a serialized version the $_SESSION superglobal. The serialization method used internally by PHP is specified in the session.serialize_handler ini setting.
     * @param string $destroy This callback is executed when a session is destroyed with session_destroy() or with session_regenerate_id() with the destroy parameter set to true. Return value should be true for success, false for failure. 
     * @param string $gc The garbage collector callback is invoked internally by PHP periodically in order to purge old session data. The frequency is controlled by session.gc_probability and session.gc_divisor. The value of lifetime which is passed to this callback can be set in session.gc_maxlifetime. Return value should be true for success, false for failure. 
     * @param string $create_sid This callback is executed when a new session ID is required. No parameters are provided, and the return value should be a string that is a valid session ID for your handler. 
     * @param string $validate_sid This callback is executed when a session is to be started, a session ID is supplied and session.use_strict_mode is enabled. The key is the session ID to validate. A session ID is valid, if a session with that ID already exists. The return value should be true for success, false for failure. 
     * @param string $update_timestamp This callback is executed when a session is updated. key is the session ID, val is the session data. The return value should be true for success, false for failure. 
     */
    function set_save_handler(
        callable $open,
        callable $close,
        callable $read,
        callable $write,
        callable $destroy,
        callable $gc,
        callable $create_sid,
        callable $validate_sid,
        callable $update_timestamp
    ) : bool{
         return session_set_save_handler(
            $open,
            $close,
            $read,
            $write,
            $destroy,
            $gc,
            $create_sid,
            $validate_sid,
            $update_timestamp
        );
    }
    /**
     * 
     */
    function start() : bool{
        return session_start();
    }
    /**
     * Return Values
     * PHP_SESSION_DISABLED if sessions are disabled.
     * PHP_SESSION_NONE if sessions are enabled, but none exists.
     * PHP_SESSION_ACTIVE if sessions are enabled, and one exists.
     */
    function status() : int{
        return session_status();
    }
    /**
     * The session_unset() function frees all session variables currently registered. 
     */
    function unset() : bool{
         return session_unset();
    }
    /**
     * Write session data and end session
     */
    function write_close() : bool{
         return session_write_close();
    }

}
