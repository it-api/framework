<?php
namespace Barebone\Filesystem\Exception;

/**
 * Exception interface for all exceptions thrown by the component.
 *
 * @api
 */
interface ExceptionInterface
{
}
