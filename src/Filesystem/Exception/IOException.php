<?php

namespace Barebone\Filesystem\Exception;

/**
 * Exception class thrown when a filesystem operation failure happens
 *
 * @api
 */
class IOException extends \RuntimeException implements IOExceptionInterface
{
    private $path;

    public function __construct($message, $code = 0, \Exception $previous = null, $path = null)
    {
        $this->path = $path;

        parent::__construct($message, $code, $previous);
    }

    /**
     * Returns the associated path for the exception
	 *
	 * @return string The path.
     */
    public function getPath()
    {
        return $this->path;
    }
}
