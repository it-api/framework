<?php

namespace Barebone\ACL;
use Barebone\ACL\ACL_User;
use \Barebone\Authenticators\IAuthenticator;
/**
 * Description of ACL_Authenticator 
 * UTF-8
 *
 * @author Frank
 * @filesource ACL_Authenticator.php
 * @since 14-feb-2017 19:40:24
 * 
 */
class ACL_Authenticator {
   
    /**
     *
     * @var IAuthenticator
     */
    private $authenticator;
    
    function __construct(IAuthenticator $authenticator) {
        $this->authenticator = $authenticator;
    }
    /**
     * Log any attempt made to login
     * @param ACL_User $user the user attempting to login/logout
     * @param string $type login/logout
     */
    function log_attempt(ACL_User $user, string $type = 'login'){
        
		$filename = FULL_BAREBONE_PATH . 'config/acl_log.json';
        $logs = json_decode(file_get_contents($filename))->logs;
        $data = [];
        $data['type'] = $type;
        $data['timestamp'] = date('d-m-Y H:i:s');
        $data['user'] = $user;
        $logs[] = $data;
        file_put_contents($filename, json_encode(['logs' => $logs]));
    }
    
    /**
     * Log a user in against the ACL
     * @param ACL_User $user
     * @return bool
     */
    function login(ACL_User $user) : bool{
        $this->log_attempt($user, 'login');
        return $this->authenticator->login($user);
    }
    
    /**
     * Log a user off
     * @param ACL_User $user
     * @return bool
     */
    function logout() : bool{
        $this->log_attempt($this->get_auth_user(), 'logout');
        return $this->authenticator->logout();
    }
    
    /**
     * Check if user is logged in
     * @param ACL_User $user
     * @return bool
     */
    function is_logged_in() : bool{
        return $this->authenticator->is_logged_in();
    }
    
    /**
     * Get the current loggedin user.
     * This could be a Guest user object
     * @return ACL_User
     */
    function get_auth_user() : ?ACL_User{
        return $this->authenticator->get_auth_user();
    }
}
