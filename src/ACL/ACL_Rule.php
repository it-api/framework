<?php

namespace Barebone\ACL;

/**
 * Database model ACL_Rule reflects the database table 
 * @since 31-12-2015
 * @author Frank 
 */
class ACL_Rule {

    /**
     * @var string 
     */
    public string $id;

    /**
     * @var string 
     */
    public string $group_id;

    /**
     * @var string 
     */
    public string $user_id;

    /**
     * Does the rule allow the requested action
     * @var bool
     */
    public bool $allow;

    /**
     * Does the rule deny the requested action
     * @var bool 
     */
    public bool $deny;

    /**
     * Taken from the router uri, first segment
     * @var string 
     */
    public string $application;

    /**
     * Taken from the router uri, second segment
     * @var string 
     */
    public string $controller;

    /**
     * Taken from the router uri, third segment
     * @var string
 
     */
    public string $action;
    
    /**
     * Constructor
     * @param mixed $data
     */
    public function __construct(mixed $data = []) {

        // if data is an array convert to object
        if (is_array($data) && !empty($data)) {
            $data = \Barebone\Functions::array_to_object($data);
        }

        // we should have an object    
        if (is_object($data)) {

            $this->id = (string) $data->id;
            $this->group_id = (string) $data->group_id;
            $this->user_id = (string) $data->user_id;
            $this->allow = (bool) $data->allow;
            $this->deny = (bool) $data->deny;
            $this->application = (string) $data->application;
            $this->controller = (string) $data->controller;
            $this->action = (string) $data->action;
            
        }
    }
}
