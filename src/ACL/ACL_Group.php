<?php

namespace Barebone\ACL;

/**
 * Database model ACL_Group reflects the database table 
 * @since 31-12-2015
 * @update 16-01-2023
 * @author Frank  
 */
class ACL_Group {

    /**
     * @var string
     */
    public string $id;

    /**
     * @var string
     */
    public string $name;

    /**
     * Constructor
     * @param mixed $data
     */
    public function __construct(mixed $data = []) {

        // if data is an array convert to object
        if (is_array($data) && !empty($data)) {
            $data = \Barebone\Functions::array_to_object($data);
        }

        // we should have an object    
        if (is_object($data)) {

            $this->id = (string) $data->id;
            $this->name = (string) $data->name;
            
        }
    }
    /**
     * Get the id of the group
     * @return string
     */
    public function getId() {
        
        return $this->id;
        
    }

    /**
     * Get the name of the group
     * @return string
     */
    public function getName() {
        
        return $this->name;
        
    }

    /**
     * Set the id of the group
     * @param string $id
     * @return $this
     */
    public function setId(string $id) {
        
        $this->id = $id;
        return $this;
        
    }

    /**
     * Set the name of the group
     * @param string $name
     * @return $this
     */
    public function setName(string $name) {
        $this->name = $name;
        return $this;
    }


}
