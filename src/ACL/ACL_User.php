<?php

namespace Barebone\ACL;

/**
 * ACL_User is used by ACL
 * @since 31-12-2015
 * @author Frank 
 */
class ACL_User {

    /**
     * id
     * @var string 
     */
    public string $id = '';

    /**
     * The group this user belongs to
     * @var string 
     */
    public string $group_id = '';

    /**
     * The username is used for authenticatin the user
     * @var string 
     */
    public string $username = '';

    /**
     * The password is used in combination with username for authenticatin the user
     * @var string
     */
    public string $password = '';

    /**
     * Fo sending lost password
     * @var string
     */
    public string $email = '';
    
    public string $hash = '';

    /**
     * @param mixed $data
     */
    public function __construct(mixed $data = []) {

        // if data is an array convert to object
        if (is_array($data) && !empty($data)) {
            $data = \Barebone\Functions::array_to_object($data);
        }

        // we should have an object    
        if (is_object($data)) {

            $this->id = (string) $data->id;
            $this->group_id = (string) $data->group_id;
            $this->username = (string) $data->username;
            $this->password = (string) '********';
            $this->email = (string) $data->email ?? '';
            
            $this->hash = (new \Barebone\ACL())->encode(serialize($this));
        }
    }
    /**
     * Check if the session hash is equal to object->hash
     * @return bool
     **/
    function hashIsValid(): bool{
        //echo $_SESSION['ACL_USER']->hash.'<br>'.$this->hash;
        return $_SESSION['ACL_USER']->hash === $this->hash; // if both ar equal its valid
    }
    
}
