<?php

namespace Barebone;

use Barebone\Component;
use Barebone\View\Interfaces\ViewInterface;

/**
 * Class representing the view in the model/view/controller concept
 * @author Frank
 *
 */
class View extends Component implements ViewInterface {

	/**
	 * Filename of the view file
	 * @var string $filename
	 */
	public string $filename;
	/**
	 * Will hold message to show on top of the view
	 * @var array $flash
	 */
	private array $flash = [];
	/**
	 * Allows you to display an Confirmation
	 * @var Component $confirm
	 */
	private ?Component $confirm = null;
	/**
	 * Holds some html added to the end of the view before rendering
	 * @var string $_extra_html
	 */
	private string $_extra_html = '';
	/**
	 * Holds the output of the layout
	 * @var string $_output
	 */
	private string $_output = '';
	/**
	 * Used with Content-Security-Policy.
	 * This will create a nonce you can use for your inline script tags
	 * @var string $nonce
	 */
	public string $nonce = '';
	/**
	 * Constructor of a View.
	 * This will search for a view file in /[application]/views/[controller]/[method].phtml
	 * @return void
	 */
	function __construct(?string $filename = NULL) {

		if (!is_null($filename)) {

			$this->filename = FULL_BAREBONE_PATH . 'applications/' . $filename . '.phtml';
            
		} else {
            
			// get the Config object
			$config = Config::getInstance();

			// get the Router object
			$router = Router::getInstance();

			// find applications folder
			$applications_path = FULL_BAREBONE_PATH . $config->getValue('application', 'applications_path');
			if (!is_dir($applications_path)) {
				return false;
			}

			// find the application
			$application = $applications_path . $router->application . '/';
			if (!is_dir($application)) {
				return false;
			}

			// find the views folder
			$application_views = $application . 'views/';
			if (!is_dir($application_views)) {
				return false;
			}

			// find the views folder for the controller
			$controller_views = $application_views . $router->controller . '/';
			if (!is_dir($controller_views)) {
				return false;
			}

			// find the view file for this action
			$this->filename = $controller_views . $router->action . '.phtml';
		}
	}

	/**
	 * returns the html of the the view 
	 * @return string
	 */
	function get_html(): string {

		if ($this->_output === '') {
			// start output buffer
			ob_start();
			// render the view
			$this->render();
			// get the output and clean the buffer
			$this->_output = ob_get_clean();
			// return the output as html
			return $this->_output;
		} else {
			return $this->_output;
		}
	}

	/**
     * Render the view
     * @return void
     * @throws \Exception
     */
	function render(): void {
        
        
		if (!is_null($this->confirm) && isset($this->confirm->message)) {
			?>
			<div class="alert alert-warning span12">
				<h4 class="alert-heading">Warning!</h4>
				<p><?= $this->confirm->message ?></p>
				<?php if(isset($this->confirm->buttons) && count($this->confirm->buttons) > 0):?>
				    <?php foreach($this->confirm->buttons as $caption => $attributes):?>
    				    <?php $attr_string = '';?>
        				<?php foreach($attributes as $name => $value):?>
        				    <?php $attr_string .= " {$name}=\"{$value}\"";?>
        				<?php endforeach;?>
        				<button<?=$attr_string;?>><?=$caption;?></button>
				    <?php endforeach;?>
				<?php endif;?>
			</div>
			<?php
		} elseif (count($this->flash) > 0) {
			?>
			<div class="container">
			<div class="row-fluid">
			<?php
			foreach ($this->flash as $flash):
				if ($flash->type === 'warning'):
					?>
						<div class="alert alert-warning span12">
							<h4 class="alert-heading">Warning!</h4>
							<p><?= $flash->message ?></p>
						</div>
					<?php
				elseif ($flash->type === 'success'):
					?>
						<div class="alert alert-success span12">
							<h4 class="alert-heading">Success!</h4>
							<p><?= $flash->message ?></p>
						</div>
					<?php
				elseif ($flash->type === 'error' || $flash->type === 'danger'):
					?>
						<div class="alert alert-danger span12">
							<h4 class="alert-heading">Error!</h4>
							<p><?= $flash->message ?></p>
						</div>
					<?php
				elseif ($flash->type === 'info'):
					?>
						<div class="alert alert-info span12">
							<h4 class="alert-heading">Information!</h4>
							<p><?= $flash->message ?></p>
						</div>
					<?php
				endif;
			endforeach;
			?>
			</div>
			</div>
				<?php
			}
			
			// if there is a correpsonding view file in the views folder
			if (file_exists($this->filename)) {
				// then extract the properties so the view has access to them
				extract($this->properties);
				// then include the view any php code therein will be executed
				include($this->filename);
				
				if (strlen($this->_extra_html) > 0) {
    				echo $this->_extra_html;
    			}
			} else {
				throw new \Barebone\View\Exceptions\ViewNotFoundException(sprintf('Missing file: "%s".', $this->filename));
			}
		}
        
        /**
         * Add blokx to the blokx stack
         * @param string $blokxname
         * @param array $data
         */
		function add_blokx(string $blokxname, array $data = []) {
			$blokx = $this->blokx($blokxname, $data);
			$this->append($blokx->get_html());
		}

		/**
		 * Create a new blokx
		 * @param string $blokxname
		 * @param mixed array|object $data
		 * @return \Barebone\Blokx
		 */
		function blokx(string $blokxname, array $data = []) {

			return new Blokx($blokxname, $data);
		}

		/**
		 * Flash a message on top of the view
		 * @param string $message the message to flash
		 * @param $type [warning, error, success, info]
		 */
		function flash(string $message, string $type) {

			$flash = new Component();
			$flash->message = $message;
			$flash->type = $type;
			$this->flash[] = $flash;
		}

		/**
		 * Confirm dialog. eg: Are you sure? [yes, no]
		 * @param string $message the message to flash
		 * @param string $type [warning, error, success, info]
		 * @param array $buttons array with buttons
		 */
		function confirm(string $message, string $type = 'warning', array $buttons = []) {

			$confirm = new Component();
			$confirm->message = $message;
			$confirm->type = $type;
			$confirm->buttons = $buttons;
			$this->confirm = $confirm;
		}
        
        /**
         * Display contianrer with errors
         * @param array $errors array with errors
         */
		function errorcontainer(array $errors = []) {
			
			$message = '';
			foreach ($errors as $field => $errmessage) {
				$message .= '<div>The field: ' . $field . ' contains an error.</div>';
				$message .= '<div>' . $errmessage[0] . '</div>';
			}
			$flash = new Component();
			$flash->message = $message;
			$flash->type = 'error';
			$this->flash[] = $flash;
		}

		/**
		 * Append a string to the end of the view
		 */
		function append(string $html) {
			$this->_extra_html .= $html;
		}
        
        /**
         * Loads the view into the identifier
         * @param string $identifier DOM identifier
         */
		function getjQueryResponse(string $identifier) {

			// initalize jQuery
			jQuery::init();
			jQuery($identifier)->html($this->get_html());

			// get response for jQuery
			jQuery::getResponse();
		}

	}
	
