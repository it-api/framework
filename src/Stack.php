<?php

namespace Barebone;

/**
 * A Stack is a “last in, first out” or “LIFO” collection 
 * that only allows access to the value at the top of the structure 
 * and iterates in that order, destructively. 
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource Stack.php
 * @since 16-jan-2017 0:28:33

@example
$books = new \Barebone\Stack(3);
$books->push('Harry potter1');
$books->push('Harry potter2');
echo $books->top(); //Harry potter2
echo $books->pop(); // remove top book
echo $books->top(); Harry potter1
echo $books->pop(); //remove top book
echo $books->top(); // [empty]
echo $books->pop(); // RunTimeException

 * 
 */
class Stack {

    protected $items;
    protected $limit;

    function __construct($limit = 10){

        $this->init($limit);
    }

    //  – create the stack.
    function init($limit){

        // initialize the stack
        $this->items = [];
        // stack can only contain this many items
        $this->limit = $limit;
    }
    // – add an item to the top of the stack.
    function push($item){
        // trap for stack overflow
        if (count($this->items) < $this->limit) {
            // prepend item to the start of the array
            array_unshift($this->items, $item);
        } else {
            throw new \RunTimeException('Stack is full!');
        }
    }
    // – remove the last item added to the top of the stack.
    function pop(){
        if ($this->isEmpty()) {
            // trap for stack underflow
	        throw new \RunTimeException('Stack is empty!');
	    } else {
            // pop item from the start of the array
            return array_shift($this->items);
        }
    }
    // – look at the item on the top of the stack without removing it.
    function top(){
        return current($this->items);
    }
    // – return whether the stack contains no more items.
    function isEmpty(){
        return empty($this->items);
    }

}
