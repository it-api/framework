<?php

/*
  IMAPOverview.php
  UTF-8
  2-nov-2018 12:53:52
  website-backend
  Creator Frank
 */

namespace Barebone\IMAP;

/**
 * Description of IMAPOverview
 *
 * @author Frank
 */
class IMAPOverview extends \ArrayObject
{
    private $mailbox;
    public function __construct(IMAPMailbox $mailbox, $sequence)
    {
        $result = imap_fetch_overview($mailbox->getStream(), $sequence);
        if (FALSE === $result) {
            throw new \Exception('Overview failed: ' . imap_last_error());
        }
        $this->mailbox = $mailbox;
        foreach ($result as $overview)
        {
            if (!isset($overview->subject)) {
                $overview->subject = '';
            } else {
                $overview->subject = IMAPUtility::decodeToUTF8($overview->subject);
            }
        }
        parent::__construct($result);
    }
    /**
     * @return IMAPMailbox
     */
    public function getMailbox()
    {
        return $this->mailbox;
    }
}
