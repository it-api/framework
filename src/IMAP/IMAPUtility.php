<?php

/*
  IMAPUtility.php
  UTF-8
  2-nov-2018 12:55:24
  website-backend
  Creator Frank
 */

namespace Barebone\IMAP;

/**
 * Description of IMAPUtility
 *
 * @author Frank
 */
class IMAPUtility {
	/**
     *
     * =?x-unknown?B?
     * =?iso-8859-1?Q?
     * =?windows-1252?B?
     *
     * @param string $stringQP
     * @param string $base (optional) charset (IANA, lowercase)
     * @return string UTF-8
     */
    public static function decodeToUTF8($stringQP, $base = 'windows-1252')
    {
        $pairs = array(
            '?x-unknown?' => "?$base?"
        );
        $stringQP = strtr($stringQP, $pairs);
        return imap_utf8($stringQP);
    }
}
