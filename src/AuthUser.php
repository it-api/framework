<?php

namespace Barebone;

/**
 *
 * @author Frank
 * @filesource AuthUser.php
 * @since 14-feb-2017 17:15:44
 * 
 */
class AuthUser {
    
    public $user  = null;
    
    function __construct(){
        
        $this->user = isset($_SESSION['ACL_USER']) ? $_SESSION['ACL_USER'] : null ;
    }
    
    public static function get_auth_user(){
        return $_SESSION['ACL_USER'];
    }
}
