<?php

namespace Barebone;

use Barebone\AuthController;

class JQGrid_controller extends AuthController{
	
	public $db_conf = array();
	/**
	 * This function gets called before all other functions in this controller.
	 * So if you want to add stuff to the controller this is the place to do it.
	 */
	function __construct(){
	    
		parent::__construct();
		$this->layout->set_layout('ribbon');
		
		if(!defined('JQGRID_LOADED')){
    		//<!-- Stylesheets JQGrid -->
    		//$this->layout->add_stylesheet('/assets/thirdparty/jqgrid/themes/redmond/jquery-ui.custom.css');
    		$this->layout->add_stylesheet('/assets/thirdparty/jqgrid4/jqgrid/css/ui.jqgrid.css');
    		//<!-- Javascripts JQGrid -->
    		//$this->layout->add_javascript('/assets/thirdparty/jqgrid/jquery.min.js');
    		$this->layout->add_javascript('/assets/thirdparty/jqgrid4/jqgrid/js/i18n/grid.locale-nl.js');
    		$this->layout->add_javascript('/assets/thirdparty/jqgrid4/jqgrid/js/jquery.jqGrid.min.js');
    		//$this->layout->add_javascript('/assets/thirdparty/jqgrid/themes/jquery-ui.custom.min.js');
    		define('JQGRID_LOADED', true);
		}
		
		// include db config
		include_once(FULL_VENDOR_PATH . "jqgridphp/config.php");
        
		$this->db_conf["type"] = "mysqli";
		$this->db_conf["server"] = 'localhost'; // or you mysql ip
		$this->db_conf["user"] = $this->config->getValue('database', 'dbuser'); // username
		$this->db_conf["password"] =  $this->config->getValue('database', 'dbpass'); // password
		$this->db_conf["database"] =  $this->config->getValue('database', 'dbname'); // database
	  
		// include and create object
		include(PHPGRID_LIBPATH."inc/jqgrid_dist.php");
        
		if(isset($_REQUEST['grid_id'])){
		    $this->auto_render = false;
		}
	}
}
