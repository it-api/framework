<?php

namespace Barebone\Console;

use Barebone\Config;
use Barebone\Router;
use Barebone\Database;

/**
 * Description of Controller
 * ComposerFW 
 * UTF-8
 *
 * @author Frank
 * @filesource Controller.php
 * @since 18-feb-2017 19:53:57
 * 
 */
class Controller {

    /**
     * Configuration object
     * @var Config
     */
    public $config;

    /**
     * Router object exposed to the active controller
     * @var Router
     */
    public $router;

    /**
     * This is your database object
     * @var Database object
     */
    public $db;

    /**
     * This variable will hold your appmodel in either your application/models diectory
     * or in the global/models directory
     * @example create a new application > add model > application model
     * @var mixed object
     */
    public $appmodel;

    /**
     * This variable will hold your controller model in either your application/models diectory
     * or in the global/models directory
     * @example create a new application > add model > controller model
     * @var mixed object
     */
    public $model;

    /**
     * Controller contructor.
     * This class is for you to extend from with your controllers.
     * Here you put your application wide functions that you want your controllers to have
     */
    function __construct() {

        if(php_sapi_name() != 'cli'){
            exit('THIS PROGRAM NEEDS TO RUN FROM THE COMMANDLINE');
            return;
        }
        $this->config = Config::getInstance();
        $this->router = Router::getInstance();
        $this->db = new Database();

        //$this->appmodel;
        //$this->model;
        // this will load a application/model with the same name as the application into the appmodel propertie
        // this will give all controllers in an app access to this model
        $app_name = $this->router->application;
        $appmodel_name = $app_name . 'Model';
        $controller_name = $this->router->controller;
        $model_name = $controller_name . 'Model';
        $app_model_name = FULL_APPS_PATH . $app_name . '/models/' . $app_name . 'Model.php';
        if (file_exists($app_model_name)) {
            $this->appmodel = new $appmodel_name($this);
        }

        // this will load a application/model with the same name as the controller into the model propertie
        // this will give all controllers in an app access to this model
        $app_model_name = FULL_APPS_PATH . $app_name . '/models/' . $controller_name . 'Model.php';
        if (file_exists($app_model_name)) {
            $this->model = new $model_name($this);
        }

        // this will load a global/model with the same name as the application into the appmodel propertie
        // BUT ONLY IF THERE IS NO APPLICATION MODEL		
        $controller_model_name = FULL_BAREBONE_PATH . 'models/' . $app_name . 'Model.php';
        if (file_exists($controller_model_name) && !isset($this->appmodel)) {
            $this->appmodel = new $appmodel_name($this);
        }

        // this will load a global/model with the same name as the controller into the model propertie
        // BUT ONLY IF THERE IS NO APPLICATION MODEL
        $controller_model_name = FULL_BAREBONE_PATH . 'models/' . $controller_name . 'Model.php';
        if (file_exists($controller_model_name) && !isset($this->model)) {
            $this->model = new $model_name($this);
        }

        // this executes the init function in the controller if it exists. 
        if (method_exists($this, 'init')) {
            $this->init();
        }
    }

    /**
     * alias for write_data
     * @param type $data
     */
    function debug($data) {
        $this->write_data($data);
    }

    /**
     * alias for write_data
     * @param type $data
     */
    function write_line($data, $fgcolor = 'white', $bgcolor = 'black') {
        $colors = new \Barebone\Colors();
        $this->write_data($colors->getColoredString($data, $fgcolor, $bgcolor)) . "\n";
    }

    /**
     * If $data is an array or an object the print_r() function is used
     * else if $data is a string echo the $data
     * else var_dump (null, bool, etc)
     * @param type $data
     */
    function write_data($data) {
        $colors = new \Barebone\Colors();
        if (is_object($data) || is_array($data)) {
            \print_r($data) . "\n";
        } elseif (is_string($data)) {
            echo $colors->getColoredString($data, 'white', 'green') . "\n";
        } else {
            \var_dump($data);
            echo "\n";
        }
    }

    function question($question) {
        //$colors = new \Barebone\Colors();
        $this->write_line('**************************************************************************************', 'white', 'blue');
        $this->write_line('** '.$question.' **', 'white', 'blue');
        $this->write_line('**************************************************************************************', 'white', 'blue');
        //$this->write_line($colors->getColoredString($question, 'white', 'green'));
        $line = fgets(STDIN);
        return trim($line, "\n");
    }

    function write_menu($menu = []) {
        $colors = new \Barebone\Colors();
        foreach ($menu as $key => $value) {
            // the number of the menu
            $number = $colors->getColoredString($key . '.)', 'white', 'blue');
            // the command of the menu
            $command = $colors->getColoredString(ucfirst(str_replace('_', ' ', $value)), 'black', 'cyan');
            // write the entry
            $this->write_line($number . $command);
        }

        $line = fgets(STDIN);
        return trim($line, "\n");
    }

    /*

      //////////////////////////////////////////////////////

      // FUNCTION: draw_text_table ($table)

      // Accepts an array ($table) and returns a text table

      // Array must be of the form:

      $table[1]['id']       = '1';
      $table[1]['make']     = 'Citroen';      
      $table[1]['model']    = 'Saxo';
      $table[1]['version']  = '1.4 West Coast';


      $table[2]['id']       = '2';
      $table[2]['make']     = 'Honda';
      $table[2]['model']    = 'Civic';
      $table[2]['version']  = '1.6 VTi';

      $table[3]['id']       = '3';
      $table[3]['make']     = 'BMW';
      $table[3]['model']    = '3 Series';
      $table[3]['version']  = '328 Ci';

      //////////////////////////////////////////////////////

     */

    function draw_text_table($table) {
        
        $cell_lengths = [];
        
        // Work out max lengths of each cell
        foreach ($table AS $row) {

            $cell_count = 0;
            $cell_lengths = [];

            foreach ($row AS $key => $cell) {

                $cell_length = strlen($cell);

                $cell_count++;

                if (!isset($cell_lengths[$key]) || $cell_length > $cell_lengths[$key]){
                    $cell_lengths[$key] = $cell_length;
                }
            }
        }



        // Build header bar
        $bar = '+';
        $header = '|';
        $i = 0;

        foreach ($cell_lengths AS $fieldname => $length) {

            $i++;
            $bar .= str_pad('', $length + 2, '-') . "+";
            $name = $i . ") " . $fieldname;
            
            if (strlen($name) > $length) {
                // crop long headings
                $name = substr($name, 0, $length - 1);
            }

            $header .= ' ' . str_pad($name, $length, ' ', STR_PAD_RIGHT) . " |";
        }
        
        $output = '';
        $output .= $bar . "\n";
        $output .= $header . "\n";
        $output .= $bar . "\n";


        // Draw rows
        foreach ($table AS $row) {

            $output .= "|";

            foreach ($row AS $key => $cell) {

                $output .= ' ' . str_pad($cell, $cell_lengths[$key], ' ', STR_PAD_RIGHT) . " |";
            }

            $output .= "\n";
        }

        $output .= $bar . "\n";

        return $output;
    }

}
