<?php

namespace Barebone\Console;
use Barebone\Dispatcher;

/**
 * Description of ConsoleApplication
 * ComposerFW 
 * UTF-8
 *
 * @author Frank
 * @filesource ConsoleApplication.php
 * @since 18-feb-2017 19:26:13
 * 
 */
class Application {
    
    
    function run(){
        global $argv;
       
        $route = isset($argv[1]) ? $argv[1] : 'console/app/index';
       
        $dispatcher = Dispatcher::getInstance();
        $dispatcher->dispatch_cli($route);
    }
}
