<?php

/*
  Creator Frank
 */
namespace Barebone\Ribbon;
/**
 * Description of Section
 *
 * @author Frank
 */
class Section {
	
	public $buttons = array();
	public $caption = '';
	/**
	 * 
	 * @param string $caption text on the button
	 * @return $this
	 */
	function __construct( $caption ){
		$this->caption = $caption;
		return $this;
	}
	/**
	 * Add a button to the ribbon
	 * @param string $id #id of the element
	 * @param string $text text on the button
	 * @param string $event event on the button
	 * @param string $img img on the button
	 * @param string $help attr title on the button
	 */
	function add_ribbon_button($id, $text, $event, $img, $help = ''){
		$this->buttons[] = new Button($id, $text, $event, $img, $help);
	}
	
}
