<?php

/*
  Creator Frank
 */
namespace Barebone\Ribbon;
/**
 * Description of Event
 *
 * @author Frank
 */
class Event {
	public $type;
	public $action;
	
	function __construct($action, $type = 'onclick'){
		$this->type = $type;
		$this->action = str_replace('"',"'",$action);
	}
	
	function get_inline_code(){
		
		return ' ' . $this->type . '="'.$this->action.'"';
	}
}
