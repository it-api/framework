<?php

/*
  Creator Frank
 */
namespace Barebone\Ribbon;
/**
 * Description of Tab
 *
 * @author Frank
 */
class Tab {
	
	public $name;
	public $sections = array();
	/**
	 * Tab constructor
	 * @param string $name text on the tab
	 * @return $this
	 */
	function __construct( string $name ){
		$this->name = $name;
	}
	/**
	 * Add asection to the ribbon
	 * @param string $caption name of the section
	 * @return \Barebone\Ribbon\Section
	 */
	function add_ribbon_section( string $caption ) : Section{
		$section = new Section( $caption );
		$this->sections[] = $section;
		return $section;
	}
}
