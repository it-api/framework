<?php

/*
  Creator Frank
 */
namespace Barebone\Ribbon;
/**
 * Description of Button
 *
 * @author Frank
 */
class Button {
	
	public $id;
	public $text;
	public $event;
	public $img;
	public $help;
	
	/**
	 * Add a button to the ribbon
	 * @param string $id #id of the element
	 * @param string $text text on the button
	 * @param string $event event on the button
	 * @param string $img img on the button
	 * @param string $help attr title on the button
	 */
	function __construct( $id, $text, $event, $img, $help = '' ){
		$this->id = $id;
		$this->text = $text;
		if($event instanceof \Barebone\Ribbon\Event){
			$this->event = $event->get_inline_code();
		}else{
			$event = new \Barebone\Ribbon\Event( str_replace('"',"'",$event) );
			$this->event = $event->get_inline_code();
		}
		$this->img = $img;
		$this->help = $help;
		return $this;
	}
}
