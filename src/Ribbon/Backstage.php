<?php

/*
  Creator Frank
 */
namespace Barebone\Ribbon;
/**
 * Description of Backstage
 *
 * @author Frank
 */
class Backstage {
	
	public $title;
	public $buttons = array();
	/**
	 * 
	 * @param string $title title on the backstage
	 */
	function __construct( $title = 'This is the Backstage.' ){
		
		$this->title = $title;
		
	}	
	/**
	 * Add a button to the backstage
	 * @param string $id #id of the element
	 * @param string $text text on the button
	 * @param string $event event on the button
	 * @param string $img img on the button
	 * @param string $help attr title on the button
	 */
	function add_button($id, $text, $event, $img, $help){
		
		$this->buttons[] = new Button($id, $text, $event, $img, $help);
		
	}
}
