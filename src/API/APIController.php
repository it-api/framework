<?php

/*
  Creator Frank
 */

namespace Barebone\API;

use Barebone\Controller;
/**
 * Description of APIController
 *
 * @author Frank
 */
class APIController extends Controller{
	
	/**
     * Holds the request headers from apache_request_headers()
     * @var array
     */
    private $request_headers = array();

    /**
     * Type of request eg POST GET DELETE PUT
     * @var string
     */
    private $request_type = '';

    /**
     * After validation this gets set to the partner
     * @var User $partner
     */
    private $partner;

    /**
     * The requested URI
     * @var string
     */
    public $request_url = '';
	
	function __construct() {
		
		parent::__construct();
		$this->auto_render = false;
		// get the request headers
        $this->request_headers = $this->apache_request_headers();
        // get the request type
        $this->request_type = strtoupper($_SERVER['REQUEST_METHOD']);
        $this->request_url = $_SERVER['REQUEST_URI'];
        // get the logincredentials from the headers
        $apikey = $this->request_headers['APIKEY'];
        $password = $this->request_headers['PASSWORD'];

        // if the user does not send any login credentials then deny access
        if (!isset($apikey) && !isset($password)) {

            $this->response_error('Access denied! Gebruik de verstrekte logingegevens', '403');
        } else {

            // check the user against the users data
            if (!$this->validate($apikey, $password)) {

                $this->response_error('Access denied! Onjuiste logingegevens', '403');
            }
        }
	}
	
	function apache_request_headers() {
		
        $arh = array();
        $rx_http = '/\AHTTP_/';
        foreach ($_SERVER as $key => $val) {
            if (preg_match($rx_http, $key)) {
                $arh_key = preg_replace($rx_http, '', $key);
                $rx_matches = array();
                // do some nasty string manipulations to restore the original letter case
                // this should work in most cases
                $rx_matches = explode('_', $arh_key);
                if (count($rx_matches) > 0 and strlen($arh_key) > 2) {
                    foreach ($rx_matches as $ak_key => $ak_val){
                        $rx_matches[$ak_key] = ucfirst($ak_val);
                    }
                    $arh_key = implode('-', $rx_matches);
                }
                $arh[$arh_key] = $val;
            }
        }
        return( $arh );
    }
	
	function response($data) {
        header('Content-Type: text/json');
        echo json_encode($data);
    }

    /**
     * Validates a apikey and password
     * @param string $apikey
     * @param string $password
     */
    function validate($apikey, $password) {

        if (!is_null($apikey) && !is_null($password))
            return false;

        $this->partner = $this->db->getrow('Users', array('AanmeldCode' => $apikey, 'Password' => $password));

        return (isset($this->partner->Id) && $this->partner->Id > 0);
    }

    /**
     * Returns a response 
     * @param array $response
     */
    function response_succes($response) {

        $this->response(array('response' => $response));
        // stop further proccesing
        exit();
    }

    /**
     * Response an error
     * @param string $msg
     * @param integer $code
     * @param varchar(200) $context_id
     * @return json_object
     */
    function response_error($msg, $code, $context_id = null) {

        switch ($code) {
            case '400':
                header('HTTP/1.1 400 Bad Request');
                break;

            case '403':
                header('HTTP/1.1 403 Forbidden');
                break;

            case '404':
                header('HTTP/1.1 404 Not Found');
                break;

            case '405':
                header('HTTP/1.1 405 Method Not Allowed');
                break;

            case '500':
                header('HTTP/1.1 500 Internal Server Error');
                break;
        }

        $this->response(array('response' => array(
                'error' => $msg,
                'code' => $code,
                'context_id' => $context_id
        )));
        // stop further proccesing
        exit();
    }
}
