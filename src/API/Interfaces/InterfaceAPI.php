<?php

namespace Barebone\API\Interfaces;

/**
 * API Interface
 */
interface InterfaceAPI {

    /**
     * This should return the enpoint of the API
     */
    function getEndpoint();
}
