<?php

namespace Barebone\API\Interfaces;

/**
 *
 * @author Frank
 */
interface HeaderInterface {
	
	function __construct(string $name, string $value);
	function getName();
	function getValue();
	
}
