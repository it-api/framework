<?php

namespace Barebone\API\Interfaces;
use \Barebone\API\Header;
/**
 *
 * @author Frank
 */
interface RequestInterface {
	
	public function getHeaders();
	public function setHeaders(array $headers);
	public function addHeader(Header $header);
	public function getMethod();
	public function setMethod(string $method);
	public function getEndpoint();
	public function setEndpoint(string $endpoint);
	
}
