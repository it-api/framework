<?php

namespace Barebone\API\Interfaces;

/**
 *
 * @author Frank
 */
interface MethodsInterface {
	
	const API_METHOD_GET = 'GET';
	const API_METHOD_POST = 'POST';
	const API_METHOD_PUT = 'PUT';
	const API_METHOD_DELETE = 'DELETE';
	const API_METHOD_OPTIONS = 'OPTIONS';
	const API_METHOD_PATCH = 'PATCH';
	const API_METHOD_HEAD = 'HEAD';
	
	public function getMethod();
	public function setMethod($method);
	
}
