<?php

/*
  Creator Frank
 */

namespace Barebone\API\Interfaces;

/**
 *
 * @author Frank
 */
interface ResponseInterface {
	
	function getResponseCode() : int;
	function setResponseCode(int $code);
	function getResponseBody() : string;
	function setResponseBody(string $body);
	
}
