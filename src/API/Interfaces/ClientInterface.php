<?php

namespace Barebone\API\Interfaces;

/**
 *
 * @author Frank
 */
interface ClientInterface {
	
	const version = '1.0.0.0';
	
	public function getEndpoint();
	public function setEndpoint(string $endpoint);
	public function getHeaders();
	public function setHeaders(array $headers);
	public function addHeader(string $name, string $value);	
	public function request(string $path, $data = []);
	public function get(string $path);
	public function head(string $path);
	public function post(string $path, $data);
	public function put(string $path, $data);
	public function patch(string $path, $data);
	public function options(string $path);
	public function delete(string $path);
	
}
