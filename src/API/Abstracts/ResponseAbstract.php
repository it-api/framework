<?php

/*
  Creator Frank
 */

namespace Barebone\API\Abstracts;
use Barebone\API\Interfaces\ResponseInterface;
/**
 * Description of ResponseAbstract
 *
 * @author Frank
 */
abstract class ResponseAbstract implements ResponseInterface{
	
	/**
	 * The response code van http request
	 * @var integer
	 */
	private $code = 0;
	/**
	 * The response body of the http request
	 * @var string
	 */
	private $body = null;
	/**
	 * The Response headers
	 * @var array
	 */
	private $headers = null;
	/**
	 * Constructor van Response object
	 * @param int $code
	 * @param string $body
	 */
	public function __construct(int $code = 0, string $body = '') {
		$this->code = $code;
		$this->body = $body;
	}
	/**
	 * Curl sets the response headers
	 * @param array $curl_info
	 * @return $this
	 */
	public function setResponseHeaders(array $curl_info){
		$this->headers = $curl_info;
		return $this;
	}
	/**
	 * When passing in the headername it will return one header else the array of headers
	 * @param string $header
	 * @return mixed Header|array
	 */
	public function getResponseHeaders(string $header = ''){
		if(empty($header)){
			return $this->headers;
		}else{
			return $this->headers[$header];
		}
	}	
	/**
	 * Set the response body
	 * @param string $body
	 * @return $this
	 */
	public function setResponseBody(string $body) {
		$this->body = $body;
		return $this;
	}
	/**
	 * Get the response body
	 * @return string
	 */
	public function getResponseBody() : string {
		return $this->body;
	}
	/**
	 * Get the response code
	 * @return int
	 */
	public function getResponseCode() : int {
		return $this->code;
	}
	/**
	 * Set the response code
	 * @param int $code
	 * @return $this
	 */
	public function setResponseCode(int $code) {
		$this->code = $code;
		return $this;
	}
	
	function getData(){
		return \json_decode($this->body);
	}
}
