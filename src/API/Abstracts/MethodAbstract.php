<?php

/*
  Creator Frank
 */

namespace Barebone\API\Abstracts;

/**
 * Description of MethodAbstract
 *
 * @author Frank
 */
abstract class MethodAbstract implements \Barebone\API\Interfaces\MethodsInterface{
	/**
	 * Request method
	 * @var string
	 */
	private $method;
	/**
	 * Returns the request method
	 * @return string
	 */
	public function getMethod() : string {
		return $this->method;
	}
	/**
	 * Set the request method
	 * @param type $method
	 * @return $this
	 * @throws \Barebone\Exception
	 */
	public function setMethod($method) {
		
		$refl = new \ReflectionClass('\Barebone\API\Interfaces\MethodsInterface');
		$constants = $refl->getConstants();
		if(!in_array($method, $constants)){
			throw new \Barebone\Exception('Not a valid method');
		}
		$this->method = $method;
		return $this;
		
	}



}
