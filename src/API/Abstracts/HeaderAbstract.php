<?php

/*
  Creator Frank
 */

namespace Barebone\API\Abstracts;

/**
 * Description of HeaderAbstract
 *
 * @author Frank
 */
abstract class HeaderAbstract  implements \Barebone\API\Interfaces\HeaderInterface{
	
	/**
	 * Name of the header
	 * @var string
	 */
	private $name;
	/**
	 * Value of the header
	 * @var string
	 */
	private $value;
	/**
	 * Returns the name of the header
	 * @return string
	 */
	public function getName() : string {
		return $this->name;
	}
	/**
	 * Returns the value of the header
	 * @return string
	 */
	public function getValue() : string {
		return $this->value;
	}
	/**
	 * Header constructor
	 * @param string $name
	 * @param string $value
	 */
	public function __construct(string $name, string $value) {
		
		$this->name = $name;
		$this->value = $value;
	}

}
