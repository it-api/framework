<?php

namespace Barebone\API\Abstracts;
use Barebone\API\Interfaces\RequestInterface;
use Barebone\API\Interfaces\MethodsInterface;
use Barebone\API\Header;

/**
 * Description of RequestAbstract
 *
 * @author Frank
 */
abstract class RequestAbstract implements RequestInterface{
	
	/**
	 * Username for Authentication Header
	 * @var string
	 */
	private $username = '';
	/**
	 * Password for Authentication Header
	 * @var string
	 */
	private $password = '';
	/**
	 * Authentication method
	 * @var string 
	 */
	private $authmethod = '';
	/**
	 * api endpoint
	 * @var string
	 */
	private $endpoint;
	/**
	 * full url of the request
	 * @var string 
	 */
	private $full_url;
	/**
	 * Array of Barebone\API\Header
	 * @var array 
	 */
	private $headers = [];
	/**
	 * The request method
	 * @var string
	 */
	private $method = MethodsInterface::API_METHOD_GET;
	
	public function getUsername() : string {
		return $this->username;
	}

	public function getPassword() : string {
		return $this->password;
	}

	public function getAuthmethod() : string {
		return $this->authmethod;
	}

	public function setUsername(string $username) {
		$this->username = $username;
		return $this;
	}

	public function setPassword(string $password) {
		$this->password = $password;
		return $this;
	}

	public function setAuthmethod(string $authmethod) {
		$this->authmethod = $authmethod;
		return $this;
	}

		/**
	 * Build the full url endpoint + path
	 * @param string $path
	 * @return string
	 */
	public function buildURL(string $path) : string{
		$full_url = $this->getEndpoint() . $path;
		$this->full_url = $full_url;
		return $full_url;
	}
	/**
	 * Get the full url of the request
	 * @return string
	 */
	public function getFull_url() : string {
		return $this->full_url;
	}
	/**
	 * 
	 * @param string $full_url
	 * @return $this
	 */
	public function setFull_url($full_url) {
		$this->full_url = $full_url;
		return $this;
	}
	
	function headerExists($headername, $headervalue){
	    foreach($this->headers as $header){
	        if($headername == $header->getName() && $headervalue == $header->getValue()){
	            return true;
	        }
	    }
	    return false;
	}
	/**
	 * Add a header to the headers of the request
	 * @param Header $header
	 */
	public function addHeader(Header $header) {
		$this->headers[] = $header;
	}
	/**
	 * Returns an array of Barebone\API\Header
	 * @return array
	 */
	public function getHeaders() : array {
		return $this->headers;
	}
	/**
	 * Returns the request method
	 * @return string
	 */
	public function getMethod() : string {
		return $this->method;
	}
	/**
	 * Set the request headers This should be an array of Barebone\API\Header
	 * @param array $headers
	 * @return $this
	 */
	public function setHeaders(array $headers) {
		$this->headers = $headers;
		return $this;
	}
	/**
	 * Set the method of the request
	 * @param string $method
	 * @return $this
	 */
	public function setMethod(string $method) {
		$this->method = $method;
		return $this;
	}
	/**
	 * Get the endpoint of the request
	 * @return type
	 */
	public function getEndpoint() {
		return $this->endpoint;
	}
	/**
	 * Set the endpoint of the request
	 * @param string $endpoint
	 * @return $this
	 */
	public function setEndpoint(string $endpoint) {
		$this->endpoint = $endpoint;
		return $this;
	}

}
