<?php

namespace Barebone\API\Abstracts;
use Barebone\API\Request;
use Barebone\API\Response;
use Barebone\API\Header;
use Barebone\API\Abstracts\RequestAbstract;
use Barebone\API\Abstracts\ResponseAbstract;

/**
 * Description of ClientAbstract
 *
 * @author Frank
 */
abstract class ClientAbstract implements \Barebone\API\Interfaces\ClientInterface{
	
	/**
	 *
	 * @var RequestAbstract
	 */
	private $request;
	/**
	 *
	 * @var ResponseAbstract
	 */
	private $response;
	
	public function __construct() {
		$this->request = new Request();
		$this->response = new Response();
	}
	/**
	 * Get the Responde object
	 * @return \Barebone\API\Abstracts\RequestAbstract
	 */
	public function getRequest(): RequestAbstract {
		return $this->request;
	}
	/**
	 * Get the Response object
	 * @return \Barebone\API\Abstracts\ResponseAbstract
	 */
	public function getResponse(): ResponseAbstract {
		return $this->response;
	}
	/**
	 * Set the Response object
	 * @param \Barebone\API\Abstracts\RequestAbstract $request
	 * @return $this
	 */
	public function setRequest(RequestAbstract $request) {
		$this->request = $request;
		return $this;
	}
	/**
	 * Set the Response object
	 * @param \Barebone\API\Abstracts\ResponseAbstract $response
	 * @return $this
	 */
	public function setResponse(ResponseAbstract $response) {
		$this->response = $response;
		return $this;
	}
	/**
	 * Builds the full_url endpoint + path
	 * @param string $path
	 * @return string returns the endpoint + path
	 */
	public function buildURL(string $path){
		return $this->request->buildURL($path);
	}
	/**
	 * Get the endpoint from the request object
	 * @return type
	 */
	public function getEndpoint() {
		return $this->request->getEndpoint();
	}
	/**
	 * Set the endpoint to the request object
	 * @param string $endpoint
	 * @return $this
	 */
	public function setEndpoint(string $endpoint) {
		$this->request->setEndpoint($endpoint);
		return $this;
	}
	/**
	 * Returns an array with the request headers
	 * @return array of Header
	 */
	public function getHeaders() : array {
		return $this->request->getHeaders();
	}
	/**
	 * Set the headers to the request object
	 * @param array $headers
	 * @return $this
	 */
	public function setHeaders(array $headers) {
		$this->request->setHeaders($headers);
		return $this;
	}
	/**
	 * Add a header to the request object
	 * @param string $name Name of the header
	 * @param string $value Value of the header
	 * @return $this
	 */
	public function addHeader(string $name, string $value) {
		$header = new Header($name, $value);
		$this->request->addHeader($header);		
		return $this;
	}
	/**
	 * Get the method of the request object
	 * @return string Returns request methodtype
	 */
	public function getMethod() {
		return $this->request->getMethod();
	}
	/**
	 * Set the method of the Request object
	 * @param string $method
	 * @return $this
	 */
	public function setMethod(string $method) {
		$this->request->setMethod( $method );
		return $this;
	}

}
