<?php

namespace Barebone\API;
use Barebone\API\Interfaces\MethodsInterface;
use Barebone\API\Abstracts\ClientAbstract;
use Barebone\API\Response;
/**
 * Description of Client
 *
 * @author Frank
 * @example 
 *	$apiclient = new \Barebone\API\Client();
	$apiclient->getRequest()->setAuthmethod('Basic');
	$apiclient->getRequest()->setUsername('username');
	$apiclient->getRequest()->setPassword('password');
	// custom headers
	$apiclient->addHeader('my-header-x', 'XXXX');
	$apiclient->addHeader('my-header-y', 'YYYY');
	$apiclient->addHeader('Content-Type','application/json');
	$apiclient->setEndpoint('https://example.org/api');
		
	$result = $apiclient->get('/index/index/APIFilters/1');
		
	Barebone\Inspector::Inspect($apiclient); // result 10 rows
	Barebone\Inspector::Inspect($result); // result 10 rows
 */
class Client extends ClientAbstract{
	/**
	 * The actual call to the api is happening here.
	 * @param string $path
	 * @param array $data
	 * @return string
	 */
	public function request(string $path, $data = []){
		
		$url = $this->buildURL($path);		
		$ch = curl_init($url);
		
		if($this->getRequest()->getAuthmethod() === 'Basic'){			
			$username = $this->getRequest()->getUsername();
			$password = $this->getRequest()->getPassword();			
			$basicauth = \base64_encode("{$username}:{$password}");
			$this->addHeader( 'Authorization', 'Basic ' . $basicauth );
		}
		
		$curl_headers = [];
		foreach($this->getHeaders() as $header){
			$hdr = "{$header->getName()}: {$header->getValue()}";
			$curl_headers[] = $hdr;
		}
		
		\curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
				
		\curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    \curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	    \curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    \curl_setopt($ch, CURLINFO_HEADER_OUT, true);
	    \curl_setopt($ch, CURLOPT_VERBOSE, true);
	    \curl_setopt($ch, CURLOPT_TIMEOUT, 5);

	    if($this->getMethod() === MethodsInterface::API_METHOD_POST || $this->getMethod() === MethodsInterface::API_METHOD_PUT){		    
			if((is_array($data)) && $this->getRequest()->headerExists('Content-Type', 'application/json')){
				$data = json_encode($data);
			}
		    \curl_setopt($ch, CURLOPT_POSTFIELDS, $data);			
	    }elseif($this->getMethod() === MethodsInterface::API_METHOD_OPTIONS){	       
			\curl_setopt($ch, CURLOPT_CUSTOMREQUEST, MethodsInterface::API_METHOD_OPTIONS);			
	    }elseif($this->getMethod() === MethodsInterface::API_METHOD_PUT){	        
			\curl_setopt($ch, CURLOPT_CUSTOMREQUEST, MethodsInterface::API_METHOD_PUT);			
	    }elseif($this->getMethod() === MethodsInterface::API_METHOD_PATCH){	        
			\curl_setopt($ch, CURLOPT_CUSTOMREQUEST, MethodsInterface::API_METHOD_PATCH);			
	    }elseif($this->getMethod() === MethodsInterface::API_METHOD_HEAD){	        
			\curl_setopt($ch, CURLOPT_CUSTOMREQUEST, MethodsInterface::API_METHOD_HEAD);			
	    }elseif($this->getMethod() === MethodsInterface::API_METHOD_GET){	        
			\curl_setopt($ch, CURLOPT_CUSTOMREQUEST, MethodsInterface::API_METHOD_GET);			
	    }		
		
		$result = \curl_exec($ch);
		
		$code = curl_getinfo($ch)['http_code'];
		$this->setResponse(new Response($code, $result));
		$this->getResponse()->setResponseHeaders( curl_getinfo($ch) );
		return $this->getResponse()->getResponseBody();
		
	}
	
	public function delete(string $path) {
		$this->setMethod( MethodsInterface::API_METHOD_DELETE );
		return $this->request($path);
	}

	public function get(string $path) {
		$this->setMethod( MethodsInterface::API_METHOD_GET );
		return $this->request($path);
	}

	public function head(string $path) {
		$this->setMethod( MethodsInterface::API_METHOD_HEAD );
		return $this->request($path);
	}

	public function options(string $path) {
		$this->setMethod( MethodsInterface::API_METHOD_OPTIONS );
		return $this->request($path);
	}

	public function patch(string $path, $data) {
		$this->setMethod( MethodsInterface::API_METHOD_PATCH );
		return $this->request($path, $data);
	}

	public function post(string $path, $data) {
		$this->setMethod( MethodsInterface::API_METHOD_POST );
		return $this->request($path, $data);
	}

	public function put(string $path, $data) {
		$this->setMethod( MethodsInterface::API_METHOD_PUT );
		return $this->request($path, $data);
	}
	public function execute(){
		if($this->getMethod() === MethodsInterface::API_METHOD_POST){
			
		}
	}
}
