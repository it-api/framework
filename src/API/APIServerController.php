<?php

namespace Barebone\API;
use Barebone\Controller;

/**
 * Description of APIServer
 *
 * @author Frank
 */
class APIServerController extends Controller {
	
	/**
	 * Holds the request headers from apache_request_headers()
	 * @var array
	 */
	private $request_headers = array();
	private $allow_users = array(
		array(
			'Username' => 'test',
			'Password' => 'test'
		),
		array(
			'Username' => 'admin',
			'Password' => 'admin'
		)
	);

	private function user_allowed($username, $password) {

		foreach ($this->allow_users as $user) {
			if ($user['Username'] === $username && $user['Password'] === $password) {
				return true;
			}
		}

		return false;
	}

	/**
	 * This function gets called before all other functions in this controller.
	 * So if you want to add stuff to the controller this is the place to do it.
	 */
	function __construct() {

		parent::__construct();
		$this->request_headers = \Barebone\Functions::apache_request_headers();
		//print_r($this->request_headers);
		// Authorization: Basic hdfksdf89ds89fdsf8df80dsf80df8ds8f8dsfds8fsd8d8fd8fd8=
		if (isset($this->request_headers['Authorization'])) {
			//print_r($this->request_headers);
			$s = explode(' ', $this->request_headers['Authorization']);
			$basic = $s[0]; //Basic
			$base64 = explode(':', base64_decode($s[1]));
			$username = $base64[0];
			$password = $base64[1];
			//echo $username;
			if (!$this->user_allowed($username, $password)) {
				$this->auto_render = false;
				die(json_encode(array('error' => 'You are not allowed to use this API')));
			}
		} elseif (
			$this->request_headers['USERNAME'] != 'test' 
			|| $this->request_headers['PASSWORD'] != 'test-123-09837645-0kkjfg990'
		) {
			$this->auto_render = false;
			die(json_encode(array('error' => 'You are not allowed to use this API')));
		}
	}
	
	function get_input() {
        //debug($_FILES);
		// get the input
		$json = file_get_contents('php://input');
		//echo $json;
		// convert to object
		$obj = json_decode($json);
		//var_dump($obj);
		// check for valid object
		if (!is_object($obj->data)) {
			$this->view->data = array('error' => 'No JSON object found.');
			return false;
		}
		return $obj->data;
	}
	
	function get_data(){
	    
	    $json = file_get_contents('php://input');
	    $data = json_decode($json);
	    return $data;
	    
	}
	
	function get_input_as_object(){
		
		// get the input
		$json = file_get_contents('php://input');
		// convert to object
		$object = json_decode($json);

		return $object;
	}
}
