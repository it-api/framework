<?php

namespace Barebone\API;

class RouteAPI{
	
	private $method;
	private $path;
	private $comment;
	private $callback;	
	

    /**
     * get method
     * @return string
     */
    public function getMethod(){
        return $this->method;
    }

    /**
     * set method
     * @param string $method
     * @return RouteAPI
     */
    public function setMethod($method){
        $this->method = $method;
        return $this;
    }

    /**
     * get path
     * @return string
     */
    public function getPath(){
        return $this->path;
    }

    /**
     * set path
     * @param string $path
     * @return RouteAPI
     */
    public function setPath($path){
        $this->path = $path;
        return $this;
    }

    /**
     * get comment
     * @return string
     */
    public function getComment(){
        return $this->comment;
    }

    /**
     * set comment
     * @param string $comment
     * @return RouteAPI
     */
    public function setComment($comment){
        $this->comment = $comment;
        return $this;
    }

    /**
     * get callback
     * @return string
     */
    public function getCallback(){
        return $this->callback;
    }

    /**
     * set callback
     * @param string $callback
     * @return RouteAPI
     */
    public function setCallback($callback){
        $this->callback = $callback;
        return $this;
    }

}
