<?php

namespace Barebone;

use Barebone\Model;
use Barebone\Layout;

class JQGrid5Model extends Model {


    public string $select_command = '';
    public string $table = '';
    public string $url = '';
    public string $datatype = 'json';
    /* Subgrid */
    public bool $subGrid = false;
    public string $subgridtype = 'json';
    public string $subGridUrl = '';
    public string $subGridTable = '';

    private array $colModel = [];
    private array $columns = [];
    private array $options = [];
    private array $actions = [];

    public function __construct(Controller $controller) {
        parent::__construct();
        $this->controller = $controller;
    }

    public function select_command($sql) {
        $this->select_command = $sql;
    }

    public function set_columns(array $columns) {
        $this->columns = $columns;
    }

    public function set_options(array $options) {
        $this->options = $options;
    }

    public function set_actions(array $actions) {
        $this->actions = $actions;
    }

    private function build_js_grid($grid_id) {

        ob_start();

?>
        <div>
            <table id="<?= $grid_id; ?>"></table>
            <div id="<?= $grid_id; ?>_jqGridPager"></div>
        </div>
<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function(){
            
        $("#<?= $grid_id; ?>").jqGrid({
            url: '<?= $this->url; ?>',
            datatype: "<?= $this->datatype; ?>",
            editurl: '<?= $this->url; ?>',
            colModel: [
<?php foreach ($this->columns as $column) : ?> 
{
<?php foreach ($column as $field => $fvalue) : ?>
<?php if ($fvalue !== '') : ?>
<?php if (is_numeric($fvalue)) : ?>
<?= $field; ?>: <?= $fvalue; ?>,
<?php elseif (is_bool($fvalue) || $fvalue === 'false' ||  $fvalue === 'true') : ?>
<?= $field; ?>: <?= ($fvalue === true || $fvalue === 'true') ? 'true' : 'false'; ?>,
<?php elseif (is_array($fvalue)) : ?>
<?=$field;?>: [<?=implode(',', $fvalue)?>],
<?php elseif (is_string($fvalue)) : ?>
<?= $field; ?>: '<?= $fvalue; ?>',
<?php elseif (is_object($fvalue)) : ?>
<?= $field; ?>: {
<?php foreach($fvalue as $k=>$v):?>
<?=$k;?>: '<?=$v;?>',
<?php endforeach;?>
},
<?php endif; ?>
<?php endif; ?>
<?php endforeach; ?>
},
<?php endforeach; ?>
],
<?php foreach ($this->options as $option => $ovalue) : ?>
<?php if ($ovalue != '') : ?>
<?php if (is_numeric($ovalue)) : ?>
<?= $option; ?>: <?= $ovalue; ?>,
<?php elseif (is_bool($ovalue)) : ?>
<?= $option; ?>: <?= $ovalue == 1 ? 'true' : 'false'; ?>,
<?php elseif (is_string($ovalue)) : ?>
<?= $option; ?>: "<?= $ovalue; ?>",
<?php elseif (is_array($ovalue)) : ?>
<?=$option;?>: [<?=implode(',', $ovalue)?>],
<?php elseif (is_object($ovalue)) : ?>
<?= $option; ?>: {
<?php foreach($ovalue as $k=>$v):?>
<?=$k;?>: '<?=$v;?>',
<?php endforeach;?>
},
<?php endif; ?>
<?php endif; ?>
<?php endforeach; ?>
pager: "#<?= $grid_id; ?>_jqGridPager",

<?php if ($this->subGrid === true) : ?>
    subGrid: true, // set the subGrid property to true to show expand buttons for each row
    subgridtype: '<?= $this->subgridtype; ?>', // set the subgrid type
    subGridUrl: '<?= $this->subGridUrl; ?>',
    subGridRowExpanded: function(subgrid_id, rowId) {
        console.log(subgrid_id, rowId)
        $.php('/jqgrid5/index/subgrid/', {
            'subgrid_id': subgrid_id,
            'rowId': rowId
        });
    }
<?php endif; ?>
});

$('#<?= $grid_id; ?>').navGrid('#<?= $grid_id; ?>_jqGridPager',
// the buttons to appear on the toolbar of the grid
{
<?php foreach ($this->actions as $action => $avalue) : ?>
<?php if ($avalue != '') : ?>
<?php if (is_numeric($avalue)) : ?>
<?= $action; ?>: <?= $avalue; ?>,
<?php elseif (is_bool($avalue)) : ?>
<?= $action; ?>: <?= $avalue == 1 ? 'true' : 'false'; ?>,
<?php elseif (is_string($avalue)) : ?>
<?= $action; ?>: '<?= $avalue; ?>',
<?php elseif (is_array($ovalue)) : ?>
<?=$option;?>: [<?=implode(',', $ovalue)?>],
<?php elseif (is_object($ovalue)) : ?>
<?= $option; ?>: {
<?php foreach($ovalue as $k=>$v):?>
<?=$k;?>: '<?=$v;?>', 
<?php endforeach;?>
<?php endif; ?>
<?php endif; ?>
<?php endforeach; ?>
},
        // options for the Edit Dialog
        {
            height: 'auto',
            width: 620,
            editCaption: "The Edit Dialog",
            recreateForm: true,
            checkOnUpdate: true,
            checkOnSubmit: true,
            closeAfterEdit: true,
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Add Dialog
        {
            closeAfterAdd: true,
            recreateForm: true,
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Delete Dailog
        {
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            }
        }, {
            multipleSearch: true,
            multipleGroup: true,
            showQuery: true
        });

    jQuery("#<?= $grid_id; ?>").jqGrid('filterToolbar', {
        autosearch: true,
        searchOperators: true,
    });
    
    }, 500);
});
</script>
<?php
        $output = ob_get_clean();
        return $output;
    }

    private function normalize_query(string $query): string {
        $result = [];
        $cmds_array = explode(' ', trim($query));
        foreach ($cmds_array as $key => $value) {
            if (!empty(trim($value)))
                $result[$key] = trim($value);
        }
        //return str_replace("    "," ",str_replace("\n"," ",str_replace("\t"," ",$query)));
        return implode(' ', $result);
    }

    function guess_tablename(string $query) {
        $result = [];
        $cmds_array = explode(' ', trim($query));
        foreach ($cmds_array as $key => $value) {
            if (!empty(trim($value)))
                $result[$key] = trim($value);
        }
        foreach ($result as $k => $v) {
            if ($v === 'FROM') {
                return $result[$k + 1];
            }
        }
    }
    
    public function get_dropdown_values($sql){
        $return = '';
        $result = $this->db->doquery($sql)->getRows();
        foreach($result as $row){
            $return .= "{$row->k}:{$row->v};";
        }
        return ':;'.rtrim($return, ';');
    }

    public function render($grid_id) {
        // Table needs to be defined
        if ($this->table === '') {
            $this->table = $this->guess_tablename($this->normalize_query($this->select_command));
            //var_dump($this->normalize_query($this->select_command));
            //$cmds_array = explode(' ', $this->normalize_query($this->select_command));
            //debug($cmds_array);
            //die('Define a table first on the grid class. eg: $grid->table = "some_table_in_database";');
        }
        // If select_command is empty create one
        if ($this->select_command === '') {
            $this->select_command = "SELECT * FROM `{$this->table}`";
        }
        // if url is empty then set to current page
        if ($this->url === '') {
            $this->url = $_SERVER['REQUEST_URI'];
        }
        // shorthand to the table for creating activerecords
        $table = $this->table;

        // if no columns ar provided we need to build te colModel
        if (count($this->columns) === 0) {
            // create activerecord of the table
            $ar = new $table();
            // go over each column in the table
            foreach ($ar->get_columns() as $key => $column) {
                // create grid column
                $col = [];
                $col["title"] = $column['Field']; // caption of column, can use HTML tags too
                $col["name"] = $column['Field']; // grid column name, same as db field or alias from sql
                $col["width"] = "20"; // width on grid
                $col["editable"] = true;
                $col["key"] = $ar->get_primary_key() === $column['Field'] ? true : false;
                // add column to grid columns array
                $this->columns[] = $col;
            }
        }
        // grid parameters
        $_search = $_REQUEST['_search'] ?? 'false';
        $limit = $_REQUEST['rows'] ?? '10';
        $page = $_REQUEST['page'] ?? '1';
        $sidx = $_REQUEST['sidx'] ?? '';
        $sord = $_REQUEST['sord'] ?? 'asc';
        $oper = $_REQUEST['oper'] ?? 'view';
        $page = $_REQUEST['page'] ?? 1;
        $offset = $page > 1 ? (((int) $limit * (int) $page) - $limit) : 0;
        // if sort column is empty
        if ($sidx === '') {
            // use the first column from colModel
            $sidx = $this->columns[0]['name'];
        }
        // multisearch filters
        $filters = isset($_REQUEST['filters']) ? json_decode(urldecode($_REQUEST['filters'])) : null;
        // DELETE ROW
        // oper	"del"
        // id	"2"
        if ($oper === 'del') {
            $ids = explode(',', clean_input($_REQUEST['id']));
            $ar = new $table();
            foreach ($ids as $id) {
                $ar->get_by_id($id);
                $ar->delete();
            }
        }

        // EDIT ROW
        // oper	"edit"
        if ($oper === 'edit') {
            unset($_REQUEST['oper']);
            unset($_REQUEST['id']);
            //$id = $_REQUEST['id'];
            $ar = new $table();
            //$ar->get_by_id($id);
            $ar->set_data($_POST);
            $ar->save();
        }
        // ADD ROW
        // oper	"add"
        if ($oper === 'add') {
            unset($_REQUEST['oper']);
            $ar = new $table();
            $ar->set_data($_POST);
            $ar->save();
        }

        // filters search
        if ($_search === 'true' && !is_null($filters) && count($filters->rules) > 0) {

            $where = $this->create_where_from_filters($filters);

            header('Content-Type: application/json');
            $this->controller->auto_render = false;
            try {
                $dbrows = $this->db->doquery("{$this->select_command} {$where} ORDER BY {$sidx} {$sord} LIMIT {$offset},{$limit}")->getRows();
                $ar = new $table();
                $response = [];
                $response['view'] = 'filters';
                $response['records'] = (int) $ar->count($where);
                $response['page'] = (int) $page;
                $response['total'] = (int) ceil($response['records'] /  $limit);
                $response['rows'] = $dbrows;
                $response['query'] = "{$this->select_command} {$where} ORDER BY {$sidx} {$sord} LIMIT {$offset},{$limit}";
                echo json_encode($response);
                return;
            } catch (\Exception $e) {
                // file deepcode ignore ServerLeak: <please specify a reason of ignoring this>
                echo (sprintf("cannot execute query %s",  "{$this->select_command} {$where} ORDER BY {$sidx} {$sord} LIMIT {$offset},{$limit}"));
            }
        }

        // SEARCH
        // _search	"true"
        // nd	"1708165412789"
        // rows	"20"
        // page	"1"
        // sidx	"id"
        // sord	"asc"
        // filters	""
        // searchField	"id"
        // searchString	"1"
        // searchOper	"eq"
        if ($_search === 'true') {

            $searchField = $_REQUEST['searchField'];
            $searchString = $_REQUEST['searchString'];
            $searchOper = $_REQUEST['searchOper'];
            $where = $this->create_where_statement($searchField, $searchString, $searchOper);
            header('Content-Type: application/json');
            $this->controller->auto_render = false;
            try {
                $dbrows = $this->db->doquery("{$this->select_command} {$where} ORDER BY {$sidx} {$sord} LIMIT {$offset},{$limit}")->getRows();
                $ar = new $table();
                $response = [];
                $response['view'] = 'search';
                $response['records'] = (int) $ar->count($where);
                $response['page'] = (int) $page;
                $response['total'] = (int) ceil($response['records'] /  $limit);
                $response['rows'] = $dbrows;
                $response['query'] = "{$this->select_command} {$where} ORDER BY {$sidx} {$sord} LIMIT {$offset},{$limit}";
                echo json_encode($response);
                return;
            } catch (\Exception $e) {
                echo (sprintf("cannot execute query %s",  "{$this->select_command} {$where} ORDER BY {$sidx} {$sord} LIMIT {$offset},{$limit}"));
            }
        }

        if (isset($_REQUEST['page'])) {
            header('Content-Type: application/json');
            $this->controller->auto_render = false;
            try {
                $dbrows = $this->db->doquery("{$this->select_command} ORDER BY {$sidx} {$sord} LIMIT {$offset},{$limit}")->getRows();

                $ar = new $table();
                $response = [];
                $response['view'] = 'pages';
                $response['records'] = (int) $ar->count();
                $response['page'] = (int) $page;
                $response['total'] = (int) ceil($response['records'] /  $limit);
                $response['rows'] = $dbrows;
                $response['query'] = "{$this->select_command} ORDER BY {$sidx} {$sord} LIMIT {$offset},{$limit}";
                echo json_encode($response);
                return;
            } catch (\Exception $e) {
                echo (sprintf("cannot execute query %s",  "{$this->select_command} ORDER BY {$sidx} {$sord} LIMIT {$offset},{$limit}"));
            }
        }

        return $this->build_js_grid($grid_id);
    }

    private function get_operator_value($op, $searchString) {
        $operators = [
            'eq' => " = '{$searchString}'", // equal
            'ne' => " <> '{$searchString}'", // not equal
            'lt' => " < '{$searchString}'", // less then
            'gt' => " > '{$searchString}'", // greater then
            'bw' => "LIKE '{$searchString}%'", // begins with
            'ew' => "LIKE '%{$searchString}'", // ends with
            'cn' => "LIKE '%{$searchString}%'", // contains
            'en' => "NOT LIKE '%{$searchString}%'", // ends not with
            'nc' => "NOT LIKE '%{$searchString}%'", // not contains
            'nu' => "{$searchString} IS NULL",
            'nn' => "{$searchString} IS NOT NULL",
            'in' => "IN ({$searchString})"
        ];
        return $operators[$op];
    }

    private function add_group($group, $groups) {

        if (isset($group->groupOp)) {

            $where = " {$group->groupOp} ";
            $where .= '(';
            foreach ($group->rules as $key => $rule) {

                $searchString = $rule->data;
                $value = $this->get_operator_value($rule->op, $searchString);
                $where .= "`{$rule->field}` {$value}";
                if ($key < count($group->rules) - 1) {
                    $where .= " {$group->groupOp} ";
                }
            }

            $where .= ')';
            $groups[] = $where;
        }

        if (isset($group->groups) && is_array($group->groups) && count($group->groups) > 0) {
            foreach ($group->groups as $grp) {
                $groups = $this->add_group($grp, $groups);
            }
        }

        return array_reverse($groups);
    }

    private function create_where_from_filters($filters) {
        $groups = [];
        $groups = $this->add_group($filters, $groups);
        if (substr($groups[0], 0, 4) === ' OR ') {
            $groups[0] = substr($groups[0], 3);
        } elseif (substr($groups[0], 0, 5) === ' AND ') {
            $groups[0] = substr($groups[0], 4);
        }
        $where = implode('', $groups);
        return 'WHERE ' . $where;
    }

    private function create_where_statement(string $searchField, string $searchString, string $searchOper) {

        $operators = [
            'eq' => " = '{$searchString}'", // equal
            'ne' => " <> '{$searchString}'", // not equal
            'lt' => " < '{$searchString}'", // less then
            'gt' => " > '{$searchString}'", // greater then
            'bw' => "LIKE '{$searchString}%'", // begins with
            'ew' => "LIKE '%{$searchString}'", // ends with
            'cn' => "LIKE '%{$searchString}%'", // contains
            'en' => "NOT LIKE '%{$searchString}%'", // ends not with
            'nc' => "NOT LIKE '%{$searchString}%'", // not contains
            'nu' => "{$searchString} IS NULL",
            'nn' => "{$searchString} IS NOT NULL",
            'in' => "IN ({$searchString})"
        ];

        $operator = $operators[$searchOper];
        return "WHERE `{$searchField}` {$operator}";
    }
}
