<?php

namespace Barebone\Javascript;

class PHPArrayToJSArray{
    
    private $phparray = [];
    
    function __construct(array $phparray){
        $this->phparray = $phparray;
    }
    
    function __toString(){
        $result_string = '[';
        foreach($this->phparray as $key => $value){
            $result_string .= "{$value},";
        }
        $result_string = substr($result_string, 0, strlen($result_string)-1);
        $result_string .= ']';
        return $result_string;
    }
}