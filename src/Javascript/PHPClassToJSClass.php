<?php
/**
 * Create a javascript class from a php class
 * 
 * @author Frank 
 * @copyright iT-APi 
 * @email barebone@it-api.nl 
 * @version 1.0.0.0 
 * @since 2019-12-28 13:33:43
 * @package Barebone
 *
 */

namespace Barebone\Javascript;
 
class PHPClassToJSClass{
    
    private $php_class = null;
    private $create_getset = false;
    
	/**
	* @var $php_class a valid php class
	*/
	public function __construct( $php_class, $create_getset = true ){
		$this->php_class = $php_class;
		$this->create_getset = $create_getset;
	}
	/**
	 * @var $filename the .js filename we save to
	 */
	public function SaveToDisk($filename){
	    
	    try {
	        file_put_contents($filename, $this);
	    } catch (\Exception $e) {
	    }
	}
	
	private function objectToString($obj){
	    $str = '{';
	    foreach($obj as $key => $val){
	        if(!is_object($val)){
	            $str .= "\n\t\t" . $key . " = '" . $val . "',";
	        }
	    }
	    $str = rtrim($str, ",\n");
	    $str .= "\n\t}";
	    return $str;
	}
	
	private function create_getter($prop){
	    return "\n\tget$prop() {\n\t\treturn this._$prop;\n\t}";
	}
	
	private function create_setter($prop){
	    return "\n\tset$prop( $prop ) {\n\t\tthis._$prop = $prop;\n\t}";
	}
	
	public function __toString(){
	    
        $classname = (new \ReflectionClass($this->php_class))->getShortName();
        $props   = (new \ReflectionClass($this->php_class))->getProperties();
        $jsobj = [];
        foreach($props as $prop){
            $jsobj[$prop->getName()] = $prop->getValue(new $this->php_class);
        }
        
        $str = '';
        $str .= 'class '.$classname." {\n";
        foreach($jsobj as $jskey => $jsval){
            if(is_array($jsval)){
                //$jsval = $this->objectToString($jsval);
                $str .= "\t" . $jskey . " = [];\n";
            } elseif(is_object($jsval)){
                $jsval = $this->objectToString($jsval);
                $str .= "\t" . $jskey . " = " . $jsval . ";\n";
            } else {
                $str .= "\t" . $jskey . " = '" . $jsval . "';\n";
            }
            
        }
        
        //$str = rtrim($str, ",\n");
        
        if($this->create_getset === true){
            foreach($jsobj as $jskey => $jsval){
                if(!is_object($jsval)){
                    $str .= $this->create_getter($jskey);
                    $str .= $this->create_setter($jskey);
                }
            }
        }
        
        $str .= "\n}";
        return $str;
	}
}
 