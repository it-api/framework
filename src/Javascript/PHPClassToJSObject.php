<?php
/**
 * Create a javascript object from a php class
 * 
 * @author Frank 
 * @copyright iT-APi 
 * @email barebone@it-api.nl 
 * @version 1.0.0.0 
 * @since 2019-12-28 12:10:43 
 * @package Barebone
 *
 */

namespace Barebone\Javascript;
 
class PHPClassToJSObject{
    
    private $php_class = null;
    
	/**
	* @var $php_class a valid php class
	*/
	public function __construct( $php_class ){
		$this->php_class = $php_class;
	}
	/**
	 * @var $filename the .js filename we save to
	 */
	public function SaveToDisk($filename){
	    
	    try {
	        file_put_contents($filename, $this);
	    } catch (\Exception $e) {
	    }
	}
	
	public function __toString(){
	    
        $classname = (new \ReflectionClass($this->php_class))->getShortName();
        $props   = (new \ReflectionClass($this->php_class))->getProperties();
        $jsobj = [];
        foreach($props as $prop){
            $jsobj[$prop->getName()] = $prop->getValue(new $this->php_class);
        }
        
        return 'var ' . $classname . ' = ' . json_encode($jsobj, JSON_PRETTY_PRINT);
	}
}
 