<?php

namespace Barebone;

/**
 * And an example usage:
 * $proxy = new Barebone\ProxyHandler('http://publicsite.example.com','http://privatesite.example.com');
 * $proxy->execute();
 */

class ProxyHandler{
    
    private $url;
    private $proxy_url;
    private $translated_url;
    private $curl_handler;

    function __construct($url, $proxy_url)
    {   
        $this->url = $url;
        $this->proxy_url = $proxy_url;

        // Parse all the parameters for the URL
        if (isset($_GET['route']))
        {   
            $proxy_url .= $_GET['route'];
        }
        else
        {   
            $proxy_url .= '/';
        }

        if ($_SERVER['QUERY_STRING'] !== '')
        {   
            $proxy_url .= "?{$_SERVER['QUERY_STRING']}";
        }

        $this->translated_url = $proxy_url;

        $this->curl_handler = curl_init($proxy_url);

        // Set various options
        $this->setCurlOption(CURLOPT_RETURNTRANSFER, true);
        $this->setCurlOption(CURLOPT_TIMEOUT, 10);
        $this->setCurlOption(CURLOPT_BINARYTRANSFER, true); // For images, etc.
        $this->setCurlOption(CURLOPT_USERAGENT,$_SERVER['HTTP_USER_AGENT']);
        $this->setCurlOption(CURLOPT_WRITEFUNCTION, array($this,'readResponse'));
        $this->setCurlOption(CURLOPT_HEADERFUNCTION, array($this,'readHeaders'));
        
        
        // Process post data.
        if (count($_POST))
        {   
            // Empty the post data
            $post=array();

            // Set the post data
            $this->setCurlOption(CURLOPT_POST, true);

            // Encode and form the post data
            foreach($_POST as $key=>$value)
            {   
                $post[] = urlencode($key)."=".urlencode($value);
            }

            $this->setCurlOption(CURLOPT_POSTFIELDS, implode('&',$post));

            unset($post);
        }
        elseif ($_SERVER['REQUEST_METHOD'] !== 'GET') // Default request method is 'get'
        {   
            // Set the request method
            $this->setCurlOption(CURLOPT_CUSTOMREQUEST, $_SERVER['REQUEST_METHOD']);
        }

    }
    
    public function apache_request_headers() {
        $arh = array();
        $rx_http = '/\AHTTP_/';
        foreach ($_SERVER as $key => $val) {
            if (preg_match($rx_http, $key)) {
                $arh_key = preg_replace($rx_http, '', $key);
                $rx_matches = array();
                // do some nasty string manipulations to restore the original letter case
                // this should work in most cases
                $rx_matches = explode('_', $arh_key);
                if (count($rx_matches) > 0 and strlen($arh_key) > 2) {
                    foreach ($rx_matches as $ak_key => $ak_val){
                        $rx_matches[$ak_key] = ucfirst($ak_val);
                    }
                    $arh_key = implode('-', $rx_matches);
                }
                $arh[$arh_key] = $val;
            }
        }
        return( $arh );
    }

    // Executes the proxy.
    public function execute()
    {
        return curl_exec($this->curl_handler);
    }

    // Get the information about the request.
    // Should not be called before exec.
    public function getCurlInfo()
    {
        return curl_getinfo($this->curl_handler);
    }

    // Sets a curl option.
    public function setCurlOption($option, $value)
    {
        curl_setopt($this->curl_handler, $option, $value);
    }

    protected function readHeaders(&$cu, $string)
    {
        $length = strlen($string);
        if (preg_match(',^Location:,', $string))
        {
            $string = str_replace($this->proxy_url, $this->url, $string);
        }
        header($string);
        return $length;
    }

    protected function readResponse(&$cu, $string)
    {
        $length = strlen($string);
        echo $string;
        return $length;
    }
}