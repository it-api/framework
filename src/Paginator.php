<?php

namespace Barebone;

use Barebone\HTML\TElement;
use Barebone\HTML\THtml;

class Paginator extends TElement {
	/**
	 * The page we are linking to
	 * @var string
	 */
	private string $base_url			= '';
	/**
	 * Total number of items (eg: database results)
	 * @var int
	 */
	private int $total_rows  		=  0;
	/**
	 * Max number of items you want shown per page
	 * @var int 
	 */
	private int $per_page	 		= 10;
	/**
	 * Number of "digit" links to show before/after the currently viewed page
	 * [10,20,30,40,50]
	 * @var int
	 */
	private int $num_links			=  5;
	/**
	 * The current page being viewed
	 * @var int 
	 */
	private int $current_page	 		=  0;
	/**
	 * The text on the first link
	 * @var string
	 */
	private string $first_link_txt		= 'First';
	/**
	 * The text on the next link
	 * @var string
	 */
	private string $next_link_txt		= 'Next';
	/**
	 * The text on the previous link
	 * @var string
	 */
	private string $prev_link_txt		= 'Previous';
	/**
	 * The text on the last link
	 * @var string
	 */
	private string $last_link_txt		= 'Last';
	/**
	 * Goto first page link [ << ]
	 * @var THtml
	 */
	private $first_link;
	/**
	 * Goto next page link [ > ]
	 * @var THtml
	 */
	private $next_link;
	/**
	 * Goto previous page link [ < ]
	 * @var THtml
	 */
	private $prev_link;
	/**
	 * Goto last page link [ >> ]
	 * @var THtml
	 */
	private $last_link;
	/**
	 * Goto a certain page link [1,2,4,5]
	 * @var THtml
	 */
	private $page_link;
	/**
	 * Paginator constructor
	 * @param int $cur_page The current page (use $_GET['start'])
	 * @param int $per_page The number of items per page
	 * @param int $total_rows The total number of items
	 * @param string $base_url eg: /this/url/
	 */
	function __construct(int $cur_page = 1, int $per_page = 10, int $total_rows = 0, string $base_url = '') {

		$this->tag = 'ul';
		$this->add_class('pagination');
		$this->total_rows = $total_rows;
		$this->per_page = $per_page;
		$this->current_page = $cur_page;
		$this->base_url = $base_url;
	}
	/**
	 * Returns the base_url
	 * @return string
	 */
	public function getBase_url(): string {
		return $this->base_url;
	}
	/**
	 *  Returns the total rows
	 * @return int
	 */
	public function getTotal_rows(): int {
		return $this->total_rows;
	}
	/**
	 * Returns the number of items per page
	 * @return int
	 */
	public function getPer_page(): int {
		return $this->per_page;
	}
	/**
	 * Resturn the number of links [10,20,30,40, etc]
	 * @return int
	 */
	public function getNum_links(): int {
		return $this->num_links;
	}
	/**
	 * Return the current page number
	 * @return int
	 */
	public function getCurrent_page(): int {
		return $this->current_page;
	}
	/**
	 * Returns the text of the first link
	 * @return string
	 */
	public function getFirst_link_txt(): string {
		return $this->first_link_txt;
	}
	/**
	 * Returns the text of the next link
	 * @return string
	 */
	public function getNext_link_txt(): string {
		return $this->next_link_txt;
	}
	/**
	 * Returns the text of the previous link
	 * @return string
	 */
	public function getPrev_link_txt(): string {
		return $this->prev_link_txt;
	}
	/**
	 * Returns the text of the last link
	 * @return string
	 */
	public function getLast_link_txt(): string {
		return $this->last_link_txt;
	}
	/**
	 * Set the base url
	 * @return string
	 */
	public function setBase_url(string $base_url): Paginator {
		$this->base_url = $base_url;
		return $this;
	}
	/**
	 * Set the total number of rows
	 * @param int $total_rows
	 * @return \Barebone\Paginator
	 */
	public function setTotal_rows(int $total_rows): Paginator {
		$this->total_rows = $total_rows;
		return $this;
	}
	/**
	 * Set the total number of items per page
	 * @param int $per_page
	 * @return \Barebone\Paginator
	 */
	public function setPer_page(int $per_page): Paginator {
		$this->per_page = $per_page;
		return $this;
	}
	/**
	 * Set the total number of in between links
	 * @param int $num_links
	 * @return \Barebone\Paginator
	 */
	public function setNum_links(int $num_links): Paginator {
		$this->num_links = $num_links;
		return $this;
	}
	/**
	 * Set the current page number
	 * @param int $current_page
	 * @return \Barebone\Paginator
	 */
	public function setCurrent_page(int $current_page): Paginator {
		$this->current_page = $current_page;
		return $this;
	}
	/**
	 * Set the text for first link [<<]
	 * @param string $first_link_txt
	 * @return \Barebone\Paginator
	 */
	public function setFirst_link_txt(string $first_link_txt): Paginator {
		$this->first_link_txt = $first_link_txt;
		return $this;
	}
	/**
	 * Set the text for next link [>]
	 * @param string $next_link_txt
	 * @return \Barebone\Paginator
	 */
	public function setNext_link_txt(string $next_link_txt): Paginator {
		$this->next_link_txt = $next_link_txt;
		return $this;
	}
	/**
	 * Set the text for previous link [<]
	 * @param string $prev_link_txt
	 * @return \Barebone\Paginator
	 */
	public function setPrev_link_txt(string $prev_link_txt): Paginator {
		$this->prev_link_txt = $prev_link_txt;
		return $this;
	}
	/**
	 * Set the text for last link [>>]
	 * @param string $last_link_txt
	 * @return \Barebone\Paginator
	 */
	public function setLast_link_txt(string $last_link_txt): Paginator {
		$this->last_link_txt = $last_link_txt;
		return $this;
	}
	/**
	 * Generate the pagination links
	 * @param bool $return
	 * @return mixed
	 */
	function generate_links(bool $return = false):mixed {

		// If our item count or per-page total is zero there is no need to continue.
		if ($this->total_rows === 0 or $this->per_page === 0) {
			return '';
		}
		// Calculate the total number of pages
		$num_pages = ceil($this->total_rows / $this->per_page);

		// Is there only one page? Hm... nothing more to do here then.
		if ($num_pages === 1) {
			return '';
		}

		$this->num_links = (int)$this->num_links;

		if ($this->num_links < 1) {

			die('Your number of links must be a positive number.');
		}

		if (!is_numeric($this->current_page)) {

			$this->current_page = 0;
		}

		// Is the page number beyond the result range?
		// If so we show the last page
		if ($this->current_page > $this->total_rows) {
			$this->current_page = ($num_pages - 1) * $this->per_page;
		}

		$uri_page_number = $this->current_page;
		$this->current_page = floor(($this->current_page / $this->per_page) + 1);

		// Calculate the start and end numbers. These determine
		// which number to start and end the digit links with
		$start = (($this->current_page - $this->num_links) > 0) ? $this->current_page - ($this->num_links - 1) : 1;
		$end   = (($this->current_page + $this->num_links) < $num_pages) ? $this->current_page + $this->num_links : $num_pages;

		// And here we go...

		// Render the "First" link
		if ($this->current_page > ($this->num_links + 1)) {

			$this->first_link	= THtml::A($this->first_link_txt, $this->base_url . '?start=0');
			$this->first_link->class = 'nav-buttons';
			$this->first_link->wrap('li');
			$this->append($this->first_link->wrap('li'));
		}

		// Render the "previous" link
		if ($this->current_page != 1) {

			$i = $uri_page_number - $this->per_page;
			if ($i === 0) $i = '0';
			$prevlink 	= THtml::A($this->prev_link_txt, $this->base_url . '?start=' . $i);
			$prevlink->class = 'nav-buttons';
			if (isset($this->prev_link->onclick)) {
				$prevlink->onclick = sprintf($this->prev_link->onclick, $i, $this->per_page);
			}
			$this->append($prevlink->wrap('li'));
		}

		// Write the pagina links
		for ($loop = $start - 1; $loop <= $end; $loop++) {

			$i = ($loop * $this->per_page) - $this->per_page;

			if ($i >= $this->per_page) {
				if ($this->current_page === $loop) {

					$current = new TElement('span');
					$current->innertext = $i;
					$this->append($current->wrap('li', ['class' => 'active'])); // Current page

				} else {

					$n = ($i === 0) ? $this->per_page : $i;
					$link = THtml::A($n, $this->base_url . '?start=' . $n);
					$link->class = 'nav-buttons';
					if (isset($this->page_link->onclick)) {
						$link->onclick = sprintf($this->page_link->onclick, $n, $this->per_page);
					}
					$this->append($link->wrap('li'));
				}
			}
		}

		// Render the "next" link
		if ($this->current_page < $num_pages) {

			$nextlink 	= THtml::A($this->next_link_txt, $this->base_url . '?start=' . ($this->current_page * $this->per_page));
			$nextlink->class = 'nav-buttons';
			if (isset($this->next_link->onclick)) {
				$nextlink->onclick = sprintf($this->next_link->onclick, ($this->current_page * $this->per_page), $this->per_page);
			}
			$this->append($nextlink->wrap('li'));
		}

		// Render the "Last" link
		if (($this->current_page + $this->num_links) < $num_pages) {

			$i = (($num_pages * $this->per_page) - $this->per_page);

			$lastlink 	= THtml::A($this->last_link_txt, $this->base_url . '?start=' . $i);
			$lastlink->class = 'nav-buttons';
			if (isset($this->last_link->onclick)) {
				$lastlink->onclick = sprintf($this->last_link->onclick, $i, $this->per_page);
			}
			$this->append($lastlink->wrap('li'));
		}

		if ($return) {
			return $this->__toString();
		} else {
			echo $this->__toString();
			return '';
		}
	}
}
