<?php

namespace Barebone;

/**
 * Heap sorts the array as you add items to it
 *
 * @author Frank
 */
class Heap extends \SplHeap implements \Iterator, \Countable {
    public function compare(mixed $value1, mixed $value2): int {
        if ($value1 > $value2) {
            return 1;
        } elseif ($value1 === $value2) {
            return -1;
        } else {
            return 0;
        }
    }
}
