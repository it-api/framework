<?php

namespace Barebone;

use Barebone\jQuery\Action;
use Barebone\jQuery\Element;

/**
 * jQuery
 *
 * @access   public
 * @package  Barebone
 * @version  1.0
 */

class jQuery
{
    /**
     * static var for realize singlton
     * @var jQuery
     */
    public static jQuery $jQuery;
    
    /**
     * response stack
     * @var array
     */
    public array $response = [
        // actions (addMessage, addError, eval etc.)
        'a' => [],
        // jqueries
        'q' => []
    ];
    /**
     * __construct
     *
     * @access  public
     */
    function __construct() 
    {
    	
    }
    
    /**
     * init
     * init singleton if needed
     *
     * @return bool
     */
    public static function init(): bool
    {
        if (empty(jQuery::$jQuery)) {
            jQuery::$jQuery = new jQuery();
        }
        return true;
    }


    /**
     * addData
     *
     * add any data to response
     *
     * @param string $key
     * @param mixed $value
     * @param string $callBack
     * @return jQuery
     */
    public static function addData (string $key, mixed $value, string $callBack = null)
    {
        jQuery::init();

        $jQuery_Action = new Action();
        $jQuery_Action ->add('k', $key);
        $jQuery_Action ->add('v', $value);
        
        // add call back func into response JSON obj
        if ($callBack) {
            $jQuery_Action ->add("callback", $callBack);
        }

        jQuery::addAction(__FUNCTION__, $jQuery_Action);

        return jQuery::$jQuery;
    }

    /**
     * addMessage
     * 
     * @param string $msg
     * @param string $callBack
     * @param array  $params
     * @return jQuery
     */
    public static function addMessage (string $msg, string $callBack = null, ?array $params = null)
    {
        jQuery::init();
        
        $jQuery_Action = new Action();        
        $jQuery_Action ->add("msg", $msg);
        
        
        // add call back func into response JSON obj
        if ($callBack) {
            $jQuery_Action ->add("callback", $callBack);
        }
        
        if ($params) {
            $jQuery_Action ->add("params",  $params);
        }
        
        jQuery::addAction(__FUNCTION__, $jQuery_Action);
        
        return jQuery::$jQuery;
    }
    
    /**
     * addError
     * 
     * @param string $msg
     * @param string $callBack
     * @param array  $params
     * @return jQuery
     */
    public static function addError (string $msg, string $callBack = null, ?array $params = null)
    {
        jQuery::init();
        
        $jQuery_Action = new Action();        
        $jQuery_Action ->add("msg", $msg);

        // add call back func into response JSON obj
        if ($callBack) {
            $jQuery_Action ->add("callback", $callBack);
        }
        
        if ($params) {
            $jQuery_Action ->add("params",  $params);
        }
        
        jQuery::addAction(__FUNCTION__, $jQuery_Action);
        
        return jQuery::$jQuery;
    }
    /**
     * evalScript
     *      
     * @param  string $foo
     * @return jQuery
     */
    public static function evalScript (string $foo)
    {
        jQuery::init();
        
        $jQuery_Action = new Action();        
        $jQuery_Action ->add("foo", $foo);

        jQuery::addAction(__FUNCTION__, $jQuery_Action);
        
        return jQuery::$jQuery;
    }
    
    /**
     * response
     * init singleton if needed
     *
     * @return string JSON
     */
    public static function getResponse(): string
    {
        jQuery::init();
        
        echo json_encode(jQuery::$jQuery->response);
        exit ();
    }
    
    /**
     * addQuery
     * add query to stack
     *
     * @return Element
     */
    public static function addQuery(string $selector)
    {
        jQuery::init();
        
        return new Element($selector);
    }
    
    /**
     * addQuery
     * add query to stack
     * 
     * @param  Element $jQuery_Element
     * @return void
     */
    public static function addElement(Element &$jQuery_Element)
    {
        jQuery::init();
        
        array_push(jQuery::$jQuery->response['q'], $jQuery_Element);
    }
    
        
    /**
     * addAction
     * add query to stack
     * 
     * @param  string $name
     * @param  Action $jQuery_Action
     * @return void
     */
    public static function addAction(string $name, Action &$jQuery_Action)
    {
        jQuery::init();
        
        jQuery::$jQuery->response['a'][$name][] = $jQuery_Action;
    }
}


/**
 * jQuery
 *
 * alias for jQuery::jQuery
 *
 * @access  public
 * @param   string   $selector
 * @return  Element
 */
function jQuery(string $selector) 
{
    return jQuery::addQuery($selector);
}
