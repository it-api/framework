<?php

namespace Barebone;

/**
 * AuthController extends a normal controller with authentication check
 * It will redirect to the login page if the user is not logged in
 *
 * @author Frank
 */
class AuthController extends Controller {

	function __construct() {

		parent::__construct();

		if ( ! isloggedin() ) {

			if ($this->request->is_ajax()) {
				$this->auto_render = false;
				\Barebone\jQuery::init();
				\Barebone\jQuery::evalScript('alert("Je bent niet meer ingelogd. Open een nieuw venster en log opnieuw in. Daarna kan je de actie nog een keer uitvoeren in dit venster.")');
				\Barebone\jQuery::getResponse();
			} else {

				header('Location: /auth/index/?referer=' . $_SERVER['REQUEST_URI'] ?? '/');
				exit();
			}
		}
	}
}
