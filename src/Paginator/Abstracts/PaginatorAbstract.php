<?php

/*
  PaginatorAbstract.php
  UTF-8
  29-apr-2018 18:20:51
  barebone-php7
  Creator Frank
 */

namespace Barebone\Paginator\Abstracts;
use Barebone\HTML\TElement;
use Barebone\Paginator\Interfaces\PaginatorInterface;
/**
 * Description of PaginatorAbstract
 *
 * @author Frank
 */
abstract class PaginatorAbstract extends TElement implements PaginatorInterface {
	
	/**
	 * The page we are linking to
	 * @var string
	 */
	private $base_url			= ''; 
	/**
	 * Total number of items (eg: database results)
	 * @var int
	 */
	private $total_rows  		=  0; 
	/**
	 * Max number of items you want shown per page
	 * @var int 
	 */
	private $per_page	 		= 10;
	/**
	 * Number of "digit" links to show before/after the currently viewed page
	 * [10,20,30,40,50]
	 * @var int
	 */
	private $num_links			=  5; 
	/**
	 * The current page being viewed
	 * @var int 
	 */
	private $current_page	 		=  0; 
	/**
	 * The text on the first link
	 * @var string
	 */
	private $first_link_txt		= 'Eerste';
	/**
	 * The text on the next link
	 * @var string
	 */
	private $next_link_txt		= 'Volgende';
	/**
	 * The text on the previous link
	 * @var string
	 */
	private $prev_link_txt		= 'Vorige';
	/**
	 * The text on the last link
	 * @var string
	 */
	private $last_link_txt		= 'Laatste';	
	/**
	 * Goto first page link [ << ]
	 * @var THtml
	 */
	private $first_link;
	/**
	 * Goto next page link [ > ]
	 * @var THtml
	 */
	private $next_link;
	/**
	 * Goto previous page link [ < ]
	 * @var THtml
	 */
	private $prev_link;
	/**
	 * Goto last page link [ >> ]
	 * @var THtml
	 */
	private $last_link;
	/**
	 * Goto a certain page link [1,2,4,5]
	 * @var THtml
	 */
	private $page_link;
	
	/**
	 * Returns the base_url
	 * @return string
	 */
	public function getBase_url() : string {
		return $this->base_url;
	}
	/**
	 *  Returns the total rows
	 * @return int
	 */
	public function getTotal_rows() : int {
		return $this->total_rows;
	}
	/**
	 * Returns the number of items per page
	 * @return int
	 */
	public function getPer_page() : int {
		return $this->per_page;
	}
	/**
	 * Resturn the number of links [10,20,30,40, etc]
	 * @return int
	 */
	public function getNum_links() : int {
		return $this->num_links;
	}
	/**
	 * Return the current page number
	 * @return int
	 */
	public function getCurrent_page() : int {
		return $this->current_page;
	}
	/**
	 * Returns the text of the first link
	 * @return string
	 */
	public function getFirst_link_txt() : string {
		return $this->first_link_txt;
	}
	/**
	 * Returns the text of the next link
	 * @return string
	 */
	public function getNext_link_txt() : string {
		return $this->next_link_txt;
	}
	/**
	 * Returns the text of the previous link
	 * @return string
	 */
	public function getPrev_link_txt() : string {
		return $this->prev_link_txt;
	}
	/**
	 * Returns the text of the last link
	 * @return string
	 */
	public function getLast_link_txt() : string {
		return $this->last_link_txt;
	}
	/**
	 * Set the base url
	 * @return string
	 */
	public function setBase_url(string $base_url) : \Barebone\Paginator\Abstracts\PaginatorAbstract {
		$this->base_url = $base_url;
		return $this;
	}
	/**
	 * Set the total number of rows
	 * @param int $total_rows
	 * @return \Barebone\Paginator
	 */
	public function setTotal_rows(int $total_rows) : \Barebone\Paginator\Abstracts\PaginatorAbstract {
		$this->total_rows = $total_rows;
		return $this;
	}
	/**
	 * Set the total number of items per page
	 * @param int $per_page
	 * @return \Barebone\Paginator
	 */
	public function setPer_page(int $per_page) : \Barebone\Paginator\Abstracts\PaginatorAbstract {
		$this->per_page = $per_page;
		return $this;
	}
	/**
	 * Set the total number of in between links
	 * @param int $num_links
	 * @return \Barebone\Paginator
	 */
	public function setNum_links(int $num_links) : \Barebone\Paginator\Abstracts\PaginatorAbstract {
		$this->num_links = $num_links;
		return $this;
	}
	/**
	 * Set the current page number
	 * @param int $current_page
	 * @return \Barebone\Paginator
	 */
	public function setCurrent_page(int $current_page) : \Barebone\Paginator\Abstracts\PaginatorAbstract {
		$this->current_page = $current_page;
		return $this;
	}
	/**
	 * Set the text for first link [<<]
	 * @param string $first_link_txt
	 * @return \Barebone\Paginator
	 */
	public function setFirst_link_txt(string $first_link_txt) : \Barebone\Paginator\Abstracts\PaginatorAbstract {
		$this->first_link_txt = $first_link_txt;
		return $this;
	}
	/**
	 * Set the text for next link [>]
	 * @param string $next_link_txt
	 * @return \Barebone\Paginator
	 */
	public function setNext_link_txt(string $next_link_txt) : \Barebone\Paginator\Abstracts\PaginatorAbstract {
		$this->next_link_txt = $next_link_txt;
		return $this;
	}
	/**
	 * Set the text for previous link [<]
	 * @param string $prev_link_txt
	 * @return \Barebone\Paginator
	 */
	public function setPrev_link_txt(string $prev_link_txt) : \Barebone\Paginator\Abstracts\PaginatorAbstract {
		$this->prev_link_txt = $prev_link_txt;
		return $this;
	}
	/**
	 * Set the text for last link [>>]
	 * @param string $last_link_txt
	 * @return \Barebone\Paginator
	 */
	public function setLast_link_txt(string $last_link_txt) : \Barebone\Paginator\Abstracts\PaginatorAbstract {
		$this->last_link_txt = $last_link_txt;
		return $this;
	}
}
