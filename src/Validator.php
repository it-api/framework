<?php

namespace Barebone;

class Validator
{
    private $data = array();
    private $errors = array();
    private $validators = array();

    public function __construct(array $data, array $validators)
    {
        $this->data = $data;
        foreach($validators as $validator){
            $this->addValidator( $validator );
        }
    }
    
    function addValidator(\Barebone\Validators\Base $validator){
        $this->validators[] = $validator;
    }

    public function execute(): bool{
        $result = true;

        foreach ($this->validators as $validator) {
            if (! $validator->execute($this->data)) {
                $this->addError($validator->getField(), $validator->getErrorMessage());
                $result = false;
            }
        }

        return $result;
    }

    public function addError(string $field, string $message): Validator{
        if (! isset($this->errors[$field])) {
            $this->errors[$field] = array();
        }

        $this->errors[$field][] = $message;
        return $this;
    }

    public function getErrors(){
        return $this->errors;
    }
    
    public function showErrors(){
        
        $errors = $this->getErrors();
        if(count($errors) > 0){
            
            $div = new \Barebone\HTML\TElement('div', ['class' => 'alert alert-danger']);
            $ul = new \Barebone\HTML\TElement('ul');
        
            foreach($errors as $field => $errormsgs){
                $li = new \Barebone\HTML\TElement('li');
                $li->innertext($errormsgs[0]);
                $ul->append($li);
            }
        
            $div->append($ul);
            echo $div;
        }
    }
    
    public function flashErrors(){
        
        $errors = $this->getErrors();
        if(count($errors) > 0){
            
            $div = new \Barebone\HTML\TElement('div', ['class' => 'alert alert-danger']);
            $ul = new \Barebone\HTML\TElement('ul');
        
            foreach($errors as $field => $errormsgs){
                $li = new \Barebone\HTML\TElement('li');
                $li->innertext($errormsgs[0]);
                $ul->append($li);
            }
        
            $div->append($ul);
            return $div;
        }
    }
    
    public function isValid(){
        return (count( $this->getErrors() ) === 0);
    }
}
