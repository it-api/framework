<?php

namespace Barebone;

/**
 * This creates a json file with all classes, methods and properties 
 * for the autocomplete feature in the Barebone framework
 */
class AutocompleteCreator {

    function __construct() {

        // if a database is configured
        if (Config()->getValue('database', 'dbname') !== '') {

            $this->create_json([
                'models' => FULL_BAREBONE_PATH . 'models/*',
                'activerecords' => FULL_BAREBONE_PATH . 'models/database/' . Config()->getValue('database', 'dbname') . '/*',
                'Barebone' => FULL_VENDOR_PATH . 'it-api/barebone-framework/src/*',
            ]);
        } else {

            $this->create_json([
                'models' => FULL_BAREBONE_PATH . 'models/*',
                'Barebone' => FULL_VENDOR_PATH . 'it-api/barebone-framework/src/*',
            ]);
        }
    }

    private function create_json(array $namespaces) {

        $data = [];

        foreach ($namespaces as $namespace => $path) {
            $files = glob($path);

            $data[$namespace] = [];
            foreach ($files as $file) {
                
                if(stristr($file, 'README')){continue;}
                
                if (is_dir($file)) {

                    $prefix = basename($file) . '\\';
                    $ffiles = glob($file . '/*');
                    $data = $this->add_files($ffiles, $data, $prefix, $namespace);
                } elseif (is_file($file)) {

                    $clsName = substr(basename($file), 0, strlen(basename($file)) - 4);
                    $data[$namespace][$clsName] = [];
                    (array) $data[$namespace][$clsName]['properties'] = (array) [];
                    (array) $data[$namespace][$clsName]['methods'] = (array) [];
                    try {
                        if (class_exists("\\{$namespace}\\{$clsName}")) {
                            $refl = new \ReflectionClass("\\{$namespace}\\{$clsName}");
                            foreach ($refl->getProperties(\ReflectionMethod::IS_FINAL | \ReflectionMethod::IS_PUBLIC) as $propertie) {
                                (array) $data[$namespace][$clsName]['properties'][] = $propertie->getName();
                            }
                            foreach ($refl->getMethods(\ReflectionMethod::IS_FINAL | \ReflectionMethod::IS_PUBLIC) as $method) {
                                $parameters = [];
                                foreach ($refl->getMethod($method->getName())->getParameters() as $parameter) {
                                    (array) $parameters[] = (array) ['name' => $parameter->getName(), 'type' => (string)$parameter->getType()];
                                }
                                (array) $data[$namespace][$clsName]['methods'][] = ['name' => $method->getName(), 'parameters' => $parameters];
                            }
                        } else {
                            require_once($file);
                            if (class_exists("\\{$clsName}")) {
                                $refl = new \ReflectionClass("\\{$clsName}");
                                foreach ($refl->getProperties(\ReflectionMethod::IS_FINAL | \ReflectionMethod::IS_PUBLIC) as $propertie) {
                                    (array) $data[$namespace][$clsName]['properties'][] = $propertie->getName();
                                }
                                foreach ($refl->getMethods(\ReflectionMethod::IS_FINAL | \ReflectionMethod::IS_PUBLIC) as $method) {
                                    $parameters = [];
                                    foreach ($refl->getMethod($method->getName())->getParameters() as $parameter) {
                                        (array) $parameters[] = (array) ['name' => $parameter->getName(), 'type' => (string)$parameter->getType()];
                                    }
                                    (array) $data[$namespace][$clsName]['methods'][] = ['name' => $method->getName(), 'parameters' => $parameters];
                                }
                            }
                        }
                    } catch (\Exception $e) {
                    }
                }
            }
        }

        file_put_contents(FULL_PUBLIC_PATH . 'assets/barebone/autocomplete.json', json_encode($data));
    }

    private function add_files($files, $data, $prefix, $namespace) {
        foreach ($files as $file) {

            if (is_dir($file)) {

                $ffiles = glob($file . '/*');
                $data = $this->add_files($ffiles, $data, $prefix . basename($file) . '\\', $namespace);
            } elseif (is_file($file)) {

                $clsBasename = substr(basename($file), 0, strlen(basename($file)) - 4);
                $clsName = '\\' . $namespace . '\\' . $prefix . $clsBasename;

                $data[$namespace][$prefix][$clsBasename] = [];
                (array) $data[$namespace][$prefix][$clsBasename]['properties'] = (array) [];
                (array) $data[$namespace][$prefix][$clsBasename]['methods'] = (array) [];

                try {
                    if (class_exists($clsName)) {

                        $refl = new \ReflectionClass($clsName);
                        foreach ($refl->getProperties(\ReflectionMethod::IS_FINAL | \ReflectionMethod::IS_PUBLIC) as $propertie) {
                            (array) $data[$namespace][$prefix][$clsBasename]['properties'][] = $propertie->getName();
                        }
                        foreach ($refl->getMethods(\ReflectionMethod::IS_FINAL | \ReflectionMethod::IS_PUBLIC) as $method) {
                            (array) $parameters = [];
                            foreach ($refl->getMethod($method->getName())->getParameters() as $parameter) {
                                (array) $parameters[] = (array) ['name' => $parameter->getName(), 'type' => (string)$parameter->getType()];
                            }
                            (array) $data[$namespace][$prefix][$clsBasename]['methods'][] = ['name' => $method->getName(), 'parameters' => $parameters];
                        }
                    }
                } catch (\Exception $e) {
                }
            }
        }

        return $data;
    }
}
