<?php

namespace Barebone;
/**
 * @example
 * 
 *  $log = new Barebone\Log();
    $log::alert('Test alert', 'More information');
    // write to error_log()
 * 
 *  $log = new Barebone\Log();
    $log::use_stack(true);
    $log::use_dbtable(new \log());
    $log::alert('Test alert', 'More information');
    $log::save_stack();
    // write stack to dbtable
 */
class Log extends Model{

	public static $EMERG   = 0;  // Emergency: system is unusable
	public static $ALERT   = 1;  // Alert: action must be taken immediately
	public static $CRIT    = 2;  // Critical: critical conditions
	public static $ERR     = 3;  // Error: error conditions
	public static $WARN    = 4;  // Warning: warning conditions
	public static $NOTICE  = 5;  // Notice: normal but significant condition
	public static $INFO    = 6;  // Informational: informational messages
	public static $DEBUG   = 7;  // Debug: debug messages
	public static $EMAIL   = 8;  // Email: Verzonden emails
	public static $SUCCES  = 9;  // on succes
	public static $AUTH		= 10; 	//on login/logout
    
    private static $use_stack = false;
    private static $stack = [];
    
    private static ?\Barebone\Activerecord $dbtable = null;
	
	public static function use_dbtable(\Barebone\Activerecord $dbtable){
	    self::$dbtable = $dbtable;
	}
	
	public static function use_stack($use_stack = true){
	    self::$use_stack = $use_stack;
	}
	
	public static function save_stack(){
	    if(count(self::$stack) > 0){
    	    foreach(self::$stack as $logmessage){
    	      
    	        if($logmessage instanceof \Barebone\Activerecord){
                    $logmessage->save();
                } else {
                    
                    echo "description: {$logmessage->Description}<br>";
                    echo "type: {$logmessage->Type}<br>";
                    echo "customerid: {$logmessage->CustomerID}<br>";
                    echo "extra info: {$logmessage->Query}<br><hr>";
                }
    	    }
	    }
	}
	
	public static function log($msg, $query = '', $type = 0){
        
        if(self::$dbtable instanceof \Barebone\Activerecord){
            $Logboek_actions = new self::$dbtable;
            $Logboek_actions->Description = $msg;
            $Logboek_actions->Query = ($query);
            $Logboek_actions->Timestamp = date('Y-m-d H:i:s');
            $Logboek_actions->Type = $type;
            if(self::$use_stack === true){
                self::$stack[] = $Logboek_actions;
            } else {
               $Logboek_actions->save(); 
            }
        } else {
            $Logboek_actions = new \stdClass;
            $Logboek_actions->Description = $msg;
            $Logboek_actions->Query = ($query);
            $Logboek_actions->Timestamp = date('Y-m-d H:i:s');
            $Logboek_actions->Type = $type;
            if(self::$use_stack === true){
                self::$stack[] = $Logboek_actions;
            } else {
                $type = self::get_type($type)[0];
                error_log("{$msg}; {$Logboek_actions->Timestamp}; {$type}");
            }
        }
	}
	/**
	 * This will also send a PANIC email
	 * @param string $msg the message to log
	 * @param integer $adresid the Adressen.Id
	 * @param string $query extra info
	 */
	public static function emerg($msg, $query = ''){
		self::log($msg, $query, self::$EMERG);
	}
	/**
	 * 
	 * @param string $msg the message to log
	 * @param integer $adresid the Adressen.Id
	 * @param string $query extra info
	 */
	public static function alert($msg, $query = ''){
		self::log($msg, $query, self::$ALERT);
	}
	/**
	 * 
	 * @param string $msg the message to log
	 * @param integer $adresid the Adressen.Id
	 * @param string $query extra info
	 */
	public static function crit($msg, $query = ''){
		self::log($msg, $query, self::$CRIT);
	}
	/**
	 * 
	 * @param string $msg the message to log
	 * @param integer $adresid the Adressen.Id
	 * @param string $query extra info
	 */
	public static function err($msg, $query = ''){
		self::log($msg, $query, self::$ERR);
	}
	/**
	 * 
	 * @param string $msg the message to log
	 * @param integer $adresid the Adressen.Id
	 * @param string $query extra info
	 */
	public static function warn($msg, $query = ''){
		self::log($msg, $query, self::$WARN);
	}
	/**
	 * 
	 * @param string $msg the message to log
	 * @param integer $adresid the Adressen.Id
	 * @param string $query extra info
	 */
	public static function notice($msg, $query = ''){
		self::log($msg, $query, self::$NOTICE);
	}
	/**
	 * 
	 * @param string $msg the message to log
	 * @param integer $adresid the Adressen.Id
	 * @param string $query extra info
	 */
	public static function info($msg, $query = ''){
		self::log($msg, $query, self::$INFO);
	}
	/**
	 * 
	 * @param string $msg the message to log
	 * @param integer $adresid the Adressen.Id
	 * @param string $query extra info
	 */
	public static function debug($msg, $query = ''){
		self::log($msg, $query, self::$DEBUG);
	}
	/**
	 * 
	 * @param string $msg the message to log
	 * @param integer $adresid the Adressen.Id
	 * @param string $query extra info
	 */
	public static function email($msg, $query = ''){
		self::log($msg, $query, self::$EMAIL);
	}
	/**
	 * 
	 * @param string $msg the message to log
	 * @param integer $adresid the Adressen.Id
	 * @param string $query extra info
	 */
	public static function succes($msg, $query = ''){
		self::log($msg, $query, self::$SUCCES);
	}
	/**
	 * 
	 * @param string $msg the message to log
	 * @param integer $adresid the Adressen.Id
	 * @param string $query extra info
	 */
	public static function auth($msg, $query = ''){
		self::log($msg, $query, self::$AUTH);
	}
	
	public static function get_type($id){
	
		switch ($id) {
			case 0:
				return array('Informational', 'information.png');
				break;
			case 1:
				return array('Alert', 'application_error.png');;
				break;
			case 2:
				return array('Critical', 'application_lightning.png');;
				break;
			case 3:
				return array('Error', 'error.png');;
				break;
			case 4:
				return array('Warning', 'user_red.png');;
				break;
			case 5:
				return array('Notice', 'note.png');;
				break;
			case 6:
				return array('Informational', 'information.png');;
				break;
			case 7:
				return array('Debug', 'application_xp_terminal.png');;
				break;
			case 8:
				return array('Email', 'email.png');;
				break;
			case 9:
				return array('Succes', 'tick.png');;
				break;
			case 10:
				return array('Authenticatie', 'key.png');;
				break;
			default:
				return array('Informational', 'information.png');
				break;
		}
	}

}
