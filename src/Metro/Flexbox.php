<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro;

use Barebone\Metro\BaseElement;
/**
 * Description of Flexbox
 *
 * @author Frank
 */
class Flexbox extends BaseElement{
    
    public function __construct() {
        
        parent::__construct();            
        $this->add_class('d-flex');        
    }
    
    function addItem(BaseElement $element){
        
        $this->append($element);
        return $this;
    }
    
    function inline($size = null){
        
        if($this->class_exists('d-flex')){
            $this->remove_class('d-flex');
        } 
        if(is_null($size)){
            $this->add_class('d-inline-flex');
        } else {
            $this->add_class("d-inline-flex-{$size}");
        }
        return $this;
    }
    /**
     * Choose from start (browser default), end, center, between, or around. 
     */
    function justify($justify){
        
        $classes = explode(' ', (string) $this->class->getValue());
       
        foreach($classes as $key => $class){
            if(strstr($class, 'flex-justify-')){
                unset($classes[$key]);
            }
        }

        $this->class->setValue((string)implode(' ', $classes));
        $this->add_class("flex-justify-{$justify}");
        return $this;
    }
}
