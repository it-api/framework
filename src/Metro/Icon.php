<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro;

use \Barebone\Metro\BaseElement;

/**
 * Description of Icon
 *
 * @author Frank
 */
class Icon extends BaseElement{
    
    public function __construct($mif_icon) {
        parent::__construct('span');
        $this->add_class($mif_icon);
    }
    
    function size($size){
        $this->add_class($size);
        return $this;
    }
    
    function color($color){
        $this->add_class($color);
        return $this;
    }
}
