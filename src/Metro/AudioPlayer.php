<?php

namespace Barebone\Metro;

class AudioPlayer extends BaseElement{
    
    /*
    <audio data-role="audio-player" 
        data-src="file.mp3"
        data-show-loop="false"
        data-show-stop="false"
        data-show-volume="false"
        data-show-info="false"></audio>
      */
    
    public function __construct(string $src, float $volume = .1) {
        parent::__construct('audio');
        $this->data('role', 'audio-player');
        
        $this->data('src', $src);
        $this->data('volume', $volume);
    }
    
    function autoplay(){
        $this->data('autoplay', 'true');
    }
    
    function dark(){
        $this->class = 'dark';
    }
    
    function light(){
        $this->class = 'light';
    }
    
    function showLoop($show = 'false'){
        $this->data('show-loop', $show);
    }
    
    function showStop($show = 'false'){
        $this->data('show-stop', $show);
    }
    
    function showVolume($show = 'false'){
        $this->data('show-volume', $show);
    }
    
    function showInfo($show = 'false'){
        $this->data('show-info', $show);
    }
    
}