<?php

namespace Barebone\Metro;

class Calendar extends BaseElement{
    
    public function __construct() {
        parent::__construct('div');
        $this->data('role', 'calendar');
    }
    // 	data-years-before 	100 	Years before today
    function yearsBefore(int $years){
        $this->data('years-before', $years);
        return $this;
    }
    // 	data-years-after 	100 	Years after today
    function yearsAfter(int $years){
        $this->data('years-after', $years);
        return $this;
    }
    // 	data-show 	null 	Show specific date
    function show($date = null){
        $this->data('show', $date);
        return $this;
    }
    // 	data-picker-mode 	false 	Set calendar to picker mode
    function pickerMode($bool = 'false'){
        $this->data('picker-mode', $bool);
        return $this;
    }
    // 	data-locale 	METRO_LOCALE (default nl-NL) 	Calendar language
    function locale($locale = 'nl-NL'){
        $this->data('locale', $locale);
        return $this;
    }
    // 	data-week-start 	0 	Start month from sunday or monday (0 - sunday, 1 - monday)
    function weekStart(int $start){
        $this->data('week-start', $start);
        return $this;
    }
    // 	data-outside 	true 	Show dates outside from month (prev or next month dates)
    function outside($bool = 'true'){
        $this->data('outside', $bool);
        return $this;
    }
    // 	data-buttons 	cancel, today, clear, done 	Buttons can be showing in calendar
    function buttons($buttons){
        $this->data('buttons', $buttons);
        return $this;
    }
    // 	data-ripple 	false 	Add ripple effect to click
    function ripple($bool = 'true'){
        $this->data('ripple', $bool);
        return $this;
    }
    // 	data-ripple-color 	#cccccc 	Ripple color
    function rippleColor(string $color){
        $this->data('ripple-color', $color);
        return $this;
    }
    // 	data-exclude 	null 	Dates can be excluded from selection
    function exclude(string $date){
        $this->data('exclude', $date);
        return $this;
    }
    // 	data-preset 	null 	Dates can be preselect
    function preset($preset = null){
        $this->data('preset', $preset);
        return $this;
    }
    // 	data-min-date 	null 	Min date
    function minDate(string $date){
        $this->data('min-date', $date);
        return $this;
    }
    // 	data-max-date 	null 	Max date
    function maxDate(string $date){
        $this->data('max-date', $date);
        return $this;
    }
    // 	data-week-day-click 	false 	If true, user can click on week day to select column of days, required multiSelect can be set to true
    function weekDayClick($bool = 'true'){
        $this->data('week-day-click', $bool);
        return $this;
    }
    // 	data-week-day-click 	false 	If true, user can select a few days
    function multiSelect($bool = 'true'){
        $this->data('week-day-click', $bool);
        return $this;
    }
    // 	data-show-header 	true 	If true, header is visible
    function showHeader($bool = 'true'){
        $this->data('show-header', $bool);
        return $this;
    }
    // 	data-show-footer 	true 	If true, footer is visible
    function showFooter($bool = 'true'){
        $this->data('show-footer', $bool);
        return $this;
    }
}