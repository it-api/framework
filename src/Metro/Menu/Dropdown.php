<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Menu;

use \Barebone\Metro\BaseElement;

/**
 * Description of Dropdown
 *
 * @author Frank
 */
class Dropdown extends BaseElement{
    
    public function __construct() {
        parent::__construct('ul');
        $this->add_class('d-menu');
        $this->data('role', 'dropdown');
    }
    
    function addItem($caption, $href = '#'){
        
        $li = new BaseElement('li');
        $a = new BaseElement('a');
        $a->href = $href;
        $a->innertext($caption);
        $li->append($a);
        
        $this->append($li);
        
        return $this;
    }

}
