<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Menu;

use Barebone\Metro\BaseElement;

/**
 * Description of AppBar
 *
 * @author Frank
 */
class AppBar extends BaseElement{
    
    public function __construct() {
        
        parent::__construct();
        $this->data('role', 'appbar');
    }
    
    function expandPoint($point = 'md'){
        
        $this->data('expand-point', $point);
        return $this;
    }
    
    function setBrand($href, $brandname){
        
        $a = new BaseElement('a');
        $a->href = $href;
        $a->add_class('brand no-hover');
        $a->innertext($brandname);
        
        $this->append($a);
        
        return $this;
    }
    
    function addMenu(){
        
        $menu = new AppBarMenu();
        $this->append($menu);
        
        return $menu;
    }
}
