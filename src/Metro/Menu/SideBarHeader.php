<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Menu;

use Barebone\Metro\BaseElement;

/**
 * Description of SideBarHeader
 *
 * @author Frank
 */
class SideBarHeader extends BaseElement{
    
    public function __construct() {
        parent::__construct();
        $this->add_class('sidebar-header');
    }
    /**
     * 
     * @return $this
     */
    function addCloseButton($btn = null){
        
        if(is_null($btn)){
            $a = new BaseElement('a');
            $a->href = '#';
            $a->add_class('fg-white sub-action');
            $a->on('click', "Metro.sidebar.close($(this).parent().parent());");
            $span = new BaseElement('span');
            $span->add_class('mif-arrow-left mif-2x');
            
            $a->append($span);
            $this->append($a);
        } else {
            $this->append($btn);
        }
        
        return $this;
    }
    /**
     * 
     * @param type $src
     * @return $this
     */
    function image($src){
        $div = new BaseElement('div');
        $div->add_class('avatar');
        $img = new BaseElement('img');
        $img->src = $src;
        $div->append($img);
        
        $this->append($div);
        return $this;
    }
    /**
     * 
     * @param type $email
     * @return $this
     */
    function gravatar($email){
        $div = new BaseElement('div');
        $div->add_class('avatar');
        $img = new BaseElement('img');
        $img->data('role', 'gravatar');
        $img->data('email', $email);

        $div->append($img);
        
        $this->append($div);
        return $this;
        
    }
    /**
     * 
     * @param type $title
     * @return $this
     */
    function title($title){
        
        $span = new BaseElement('span');
        $span->add_class('title');
        $span->innertext($title);
        $this->append($span);
        return $this;
        
    }   
    /**
     * 
     * @param type $subtitle
     * @return $this
     */
    function subtitle($subtitle){
        
        $span = new BaseElement('span');
        $span->add_class('subtitle');
        $span->innertext($subtitle);
        $this->append($span);
        return $this;
        
    }
    
    function backgroundImage($src){
        $this->data('image', $src);
        return $this;
    }

}
