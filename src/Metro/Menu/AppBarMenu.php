<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Menu;

use Barebone\Metro\BaseElement;

/**
 * Description of AppBar
 *
 * @author Frank
 */
class AppBarMenu extends BaseElement{
    
    public function __construct() {
        
        parent::__construct('ul');
        $this->add_class('app-bar-menu');
        
    }
    
    function addItem($caption, $href){
        
        $li = new BaseElement('li');
        $a = new BaseElement('a');
        $a->href = $href;
        $a->innertext($caption);
        $li->append($a);
        $this->append($li);
        
        return $this;
    }
    
    function addDropdown($caption){
        
        $li = new BaseElement('li');
        $a = new BaseElement('a');
        $a->add_class('dropdown-toggle');
        $a->innertext($caption);
        $dropdown = new Dropdown();
        $li->append($a);
        $li->append($dropdown);
        $this->append($li);
        
        return $dropdown;
    }
}
