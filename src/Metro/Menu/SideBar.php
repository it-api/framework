<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Menu;

use Barebone\Metro\BaseElement;

/**
 * Description of SideBar
 *
 * @author Frank
 */
class SideBar extends BaseElement{
    
    public function __construct() {
        
        parent::__construct();
        $this->add_class('sidebar');
        $this->data('role', 'sidebar');
        
    }
    
    function addHeader($title, $subtitle = '', $closeBtn = '', $bgcolor = ''){
        
        $header = new SideBarHeader();
        $header->title($title);
        $header->subtitle($subtitle);
        if(!empty($closeBtn)){
            $header->addCloseButton();
        }
        
        if(!empty($color)){
            $header->add_class("bg-{$bgcolor}");
        }
        
        $this->append($header);
        
        return $header;
    }
    
    function addMenu(){
        $menu = new SideBarMenu();
        
        $this->append($menu);
        
        return $menu;
    }
    
}
