<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Menu;

use Barebone\Metro\BaseElement;

/**
 * Description of Tools
 *
 * @author Frank
 */
class Tools extends BaseElement{

    public function __construct() {
        
        parent::__construct('ul');
        $this->add_class('t-menu open');
        
    }
    
    function addItem($href, $mif_icon){
        
        $li = new BaseElement('li');
        $a = new BaseElement('a');
        $span = new BaseElement('span');
        
        $a->href = $href;
        $span->add_class('icon');
        $span->add_class($mif_icon);
        
        $li->append($a);
        $a->append($span);
        
        $this->append($li);
        
        return $this;
    }
    
    function compact(){
        
        $this->add_class('compact');
        return $this;
        
    }
    
    function horizontal(){
        
        $this->add_class('horizontal');
        return $this;
        
    }
}
