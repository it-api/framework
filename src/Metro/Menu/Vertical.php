<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Menu;

use Barebone\Metro\BaseElement;

/**
 * Description of Vertical
 *
 * @author Frank
 */
class Vertical  extends BaseElement{
    
    public function __construct() {
        
        parent::__construct('ul');
        $this->add_class('v-menu');
        
    }
    
    function addItem($caption, $href, $mif_icon){
        
        $li = new BaseElement('li');
        $a = new BaseElement('a');
        $span = new BaseElement('span');
        $span->add_class('icon');
        $span->add_class($mif_icon);
        
        $a->href = $href;
        $a->innertext($caption);
        $a->append($span);
        $li->append($a);
        
        $this->append($li);
        
        return $this;
    }
    
    function addTitle($caption){
        
        $li = new BaseElement('li');
        $li->add_class('menu-title');
        $li->innertext($caption);
        
        $this->append($li);
        
        return $this;
    }
}
