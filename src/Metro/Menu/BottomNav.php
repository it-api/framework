<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Menu;

use \Barebone\Metro\BaseElement;

/**
 * Description of BottomNav
 *
 * @author Frank
 */
class BottomNav extends BaseElement{
    
    public function __construct() {
        
        parent::__construct();
        $this->add_class('bottom-nav pos-absolute');
        
    }
    
    function addButton($caption, $mif_icon){
        
        $button = new BaseElement('button');
        $span_icon = new BaseElement('span');
        $span_label = new BaseElement('span');
        $button->add_class('button');
        $span_icon->add_class("icon {$mif_icon}");
        $span_label->innertext($caption);
        $span_label->add_class('label');
        
        $button->append($span_icon);
        $button->append($span_label);
        $this->append($button);
        
        return $this;
        
    }

}
