<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Menu;

use Barebone\Metro\BaseElement;

/**
 * Description of SideBarMenu
 *
 * @author Frank
 */
class SideBarMenu extends BaseElement{
    
    public function __construct() {
        
        parent::__construct('ul');
        $this->add_class('sidebar-menu');
        
    }
    
    function addItem($caption, $href, $mif_icon){
        
        $li = new BaseElement('li');
        $a = new BaseElement('a');
        $span = new BaseElement('span');
        
        $span->add_class('icon');
        $span->add_class("mif-{$mif_icon}");
        
        $a->innertext($caption);
        $a->href = $href;
        $a->append($span);
        $li->append($a);
        
        $this->append($li);
        
        return $this;
        
    }
    
    function addGroupTitle($title){
        //<li class="group-title">Group one</li>
        $li = new BaseElement('li');
        $li->add_class('group-title');
        $li->innertext($title);
        $this->append($li);
        
        return $this;
    }
    
    function addDivider(){
        
        $li = new BaseElement('li');
        $li->add_class('divider');
        $this->append($li);
        
        return $this;
    }

}
