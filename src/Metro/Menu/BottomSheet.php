<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Menu;

use Barebone\Metro\BaseElement;

/**
 * Description of BottomSheet
 *
 * @author Frank
 */
class BottomSheet extends BaseElement{
    
    public function __construct() {
        
        parent::__construct('ul');
        $this->data('role', 'bottomsheet');
        
    }
    function addButton($caption, $mif_icon){
        
        $li = new BaseElement('li');
        $span = new BaseElement('span');
        $span->add_class('icon');
        $span->add_class($mif_icon);
        $li->innertext($caption);
        
        $li->append($span);
        $this->append($li);
        
        return $this;
    }
    
    function addDivider(){
        
        $li = new BaseElement('li');
        $li->add_class('divider');
        $this->append($li);
        
        return $this;
    }
}
