<?php

namespace Barebone\Metro;

class Donut extends BaseElement{
    
    /**
    <div id="donut4" data-role="donut" data-value="35"
        data-stroke="#F44336" data-fill="#FFC107" data-color="#FFFFFf"
        data-cap="" data-animate="10" data-background="#4fc3f7"
        data-font-size="32"></div>
    */
    public function __construct() {
        parent::__construct('div');
        $this->data('role', 'donut');
    }
    
    function setValue($value){
        $this->data('value', $value);
    }
    function setStroke($value){
        $this->data('stroke', $value);
    }
    function setFill($value){
        $this->data('fill', $value);
    }
    function setColor($value){
        $this->data('color', $value);
    }
    function setCap($value){
        $this->data('cap', $value);
    }
    function setAnimate($value){
        $this->data('animate', $value);
    }
    function setBackground($value){
        $this->data('background', $value);
    }
}
