<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro;

use Barebone\Metro\BaseElement;

/**
 * Description of Badge
 *
 * @author Frank
 */
class Badge extends BaseElement{
    
    public function __construct($value = null) {
        parent::__construct('span');
        $this->add_class('badge');
        if(!is_null($value)){
            $this->value($value);
        }
    }
    
    function value($text){
        $this->innertext($text);
        return $this;
    }
    
    function inline(){
        $this->add_class('inline');
        return $this;
    }
    
    function inside(){
        $this->add_class('inside');
        return $this;
    }
}
