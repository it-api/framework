<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro;

use Barebone\Metro\BaseElement;
/**
 * Description of FlexColumn
 *
 * @author Frank
 */
class FlexColumn extends BaseElement{
    
    public function __construct() {
        
        parent::__construct();            
        $this->add_class('d-flex');  
        $this->add_class('flex-column');
    }
    
    function addItem(BaseElement $element){
        
        $this->append($element);
        return $this;
    }
    
    /**
     * Choose from start (browser default), end, center, between, or around. 
     */
    function justify($justify){
        
        $classes = explode(' ', (string) $this->class->getValue());
       
        foreach($classes as $key => $class){
            if(strstr($class, 'flex-justify-')){
                unset($classes[$key]);
            }
        }

        $this->class->setValue((string)implode(' ', $classes));
        $this->add_class("flex-justify-{$justify}");
        return $this;
    }
}
