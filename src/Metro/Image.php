<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro;

/**
 * Description of Image
 *
 * @author Frank
 */
class Image extends BaseElement{
    
    public function __construct($src, $alt = '') {
        
        parent::__construct('div');
        $this->add_class('img-container');
        $img = new BaseElement('img');
        $img->src = $src;
        $img->alt = $alt;
        
        $this->append($img);
        
    }
    
    function selected(){
        $this->add_class('selected');
        return $this;
    }
    function thumbnail($title = null){
        $this->add_class('thumbnail');
        if(!is_null($title)){
            $span = new BaseElement('span');
            $span->add_class('title');
            $span->innertext($title);
            $this->append($span);
        }
        return $this;
    }
    function rounded(){
        $this->add_class('rounded');
        return $this;
    }
    function dropShadow(){
        $this->add_class('drop-shadow');
        return $this;
    }
    
    function overlay($title, $subtitle, $bgcolor = 'op-blue'){   
        
        $overlay = new BaseElement('div');
        $overlay->add_class('image-overlay');
        $overlay->add_class($bgcolor);
        
        $overlay_title = new BaseElement('div');
        $overlay_title->add_class('h2');
        $overlay_title->innertext($title);
        
        $overlay_subtitle = new BaseElement('p');
        $overlay_subtitle->innertext($subtitle);
        
        $overlay->append($overlay_title);
        $overlay->append($overlay_subtitle);
        $this->append($overlay);
        
        return $this;
    }
}
