<?php

namespace Barebone\Metro;

class Tabs extends BaseElement{
    /**
     * 
     * <ul data-role="tabs" data-expand="true">
            <li><a href="#">Home</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Links</a></li>
        </ul>
        <div class="border bd-default no-border-top p-2">
            <div id="_target_1">
                A falsis, calceus altus racana.
            </div>
        </div>*/
    private BaseElement $tabs;    
    private BaseElement $tabsContainers;
    
    public function __construct() {
        
        parent::__construct('div');
        
        $this->tabs = new BaseElement('ul');
        $this->tabs->data('role', 'tabs');
        
        $this->tabsContainers = new BaseElement('div');
        $this->tabsContainers->class = 'border bd-default no-border-top p-2';
        
    }
    
    function expand($value){
        $this->tabs->data('expand', $value);
        return $this;
    }
    
    function addTab(string $caption, string $content = ''){
        
        $id = uniqid();
        
        $a = new BaseElement('a');
        $a->innertext($caption);
        $a->href = "#{$id}";
        $li = new BaseElement('li');
        $li->append($a);
        $this->tabs->append($li);
        
        $div = new BaseElement('div');
        $div->innertext($content);
        $div->id = $id;
        $this->tabsContainers->append($div);
        return $this;
        
    }
    
    function __toString(){
        
        
        return $this->tabs . $this->tabsContainers;
        
    }
}