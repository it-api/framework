<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro;

use Barebone\Metro\BaseElement;

/**
 * Description of IconBox
 *
 * @author Frank
 */
class IconBox extends BaseElement{

    public function __construct() {
        parent::__construct();
        $this->add_class('icon-box');
    }
    
    function addIcon($mif_icon){
        $div = new BaseElement('div');
        $div->add_class('icon');
        $icon = new Icon($mif_icon);
        $div->append($icon);
        $this->append($div);
        return $this;
    }
    
    function addContent($content){
        $div = new BaseElement('div');
        $div->add_class('content');
        $div->innertext($content);
        $div->padding(4);
        $this->append($div);
        return $this;
    }
    
    function addProgress(){
        
    }
}
