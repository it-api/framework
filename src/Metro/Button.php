<?php

namespace Barebone\Metro;

class Button extends BaseElement{
    /**
     * 
     * @param string $tag
     * @param array $attributes
     *  <button class="button">Default</button>
        <button class="button primary">Primary</button>
        <button class="button secondary">Secondary</button>
        <button class="button success">Success</button>
        <button class="button alert">Alert</button>
        <button class="button warning">Warning</button>
        <button class="button yellow">Yellow</button>
        <button class="button info">Info</button>
        <button class="button dark">Dark</button>
        <button class="button light">Light</button>
        <button class="button link">Link</button>
     */
    public function __construct($caption, $className = null) {
        
        parent::__construct('button');
        
        $this->add_class('button');
        
        if(!is_null($className)){
            $this->add_class($className);
        }
        
        $this->innertext($caption);
    }
    
    function primary(){
        $this->add_class('primary');
        return $this;
    }
    function secondary(){
        $this->add_class('secondary');
        return $this;
    }
    function success(){
        $this->add_class('success');
        return $this;
    }
    function alert(){
        $this->add_class('alert');
        return $this;
    }
    function warning(){
        $this->add_class('warning');
        return $this;
    }
    function yellow(){
        $this->add_class('yellow');
        return $this;
    }
    function info(){
        $this->add_class('info');
        return $this;
    }
    function dark(){
        $this->add_class('dark');
        return $this;
    }
    function light(){
        $this->add_class('light');
        return $this;
    }
    function link(){
        $this->add_class('link');
        return $this;
    }
    function outline(){
        $this->add_class('outline');
        return $this;
    }
    function mini(){
        $this->remove_class('small');
        $this->remove_class('large');
        $this->add_class('mini');
        return $this;
    }
    function small(){
        $this->remove_class('mini');
        $this->remove_class('large');
        $this->add_class('small');
        return $this;
    }
    function large(){
        $this->remove_class('mini');
        $this->remove_class('small');
        $this->add_class('large');
        return $this;
    }
    function rounded(){
        $this->add_class('rounded');
        return $this;
    }
    function shadowed(){
        $this->add_class('shadowed');
        return $this;
    }
    function square(){
        $this->add_class('square');
        return $this;
    }
    function cycle(){
        $this->add_class('cycle');
        return $this;
    }
    function flat(){
        $this->add_class('flat-button');
        return $this;
    }
    function image($mif_icon, $position = 'left'){
        $this->class = 'image-button';
        //<span class="mif-share icon"></span>
        $mif = new BaseElement('span');
        $mif->add_class($mif_icon);
        $mif->add_class('icon');
        $this->append($mif);

        $caption = new BaseElement('span');
        $caption->add_class('caption');
        $caption->innertext($this->innertext());
        $this->innertext(' ');
        $this->append($caption);
        
        if($position == 'right') {
            
            $this->add_class('icon-right');
        }
        return $this;
    }
    /**
         * <button class="shortcut">
            <span class="badge">10</span>
            <span class="caption">Rocket</span>
            <span class="mif-rocket icon"></span>
        </button>
         */
    function shortcut($mif_icon, $badge = null){
        $this->class = 'shortcut';
        
        if(!is_null($badge)){
            $badgespan = new BaseElement('span');
            $badgespan->add_class('badge');
            $badgespan->innertext($badge);
            $this->append($badgespan);
        }
        $caption = new BaseElement('span');
        $caption->add_class('caption');
        $caption->innertext($this->innertext());
        $this->innertext(' ');
        $this->append($caption);
        
        $mif = new BaseElement('span');
        $mif->add_class($mif_icon);
        $mif->add_class('icon');
        $this->append($mif);
        
        return $this;
    }
    
}