<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Lists;

use Barebone\Metro\BaseElement;

/**
 * Description of StepList
 *
 * @author Frank
 */
class StepList extends BaseList{
    
    public function __construct() {
        $this->add_class('step-list');
    }
    
    function addItem($title, $text){
        
        $li = new BaseElement('li');
        $h4 = new BaseElement('h4');
        $p = new BaseElement('p');
        
        $h4->innertext($title);
        $p->innertext($text);
        
        $li->append($h4);
        $li->append($p);        
        
        $this->append($li);
        
        return $this;
    }
}
