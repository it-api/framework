<?php

namespace Barebone\Metro\Lists;

use Barebone\Metro\BaseElement;

class DraggableList extends BaseElement{
    
    function __construct(){
        $this->data('role', 'drag-items');
    }
    
    function addItem($caption){
        $item = new BaseElement('li');
        $item->innertext($caption);
        $this->append($item);
    }
}