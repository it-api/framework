<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Lists;

use Barebone\Metro\BaseElement;

/**
 * Description of FeedList
 *
 * @author Frank
 */
class FeedList extends BaseList{
    
    public function __construct($title = '') {
        
        $this->add_class('feed-list');
        $li = new BaseElement('li');
        $li->add_class('title');
        $li->innertext($title);
        $this->append($li);
        
    }
    
    function addItem($src, $label, $second_label){
        
        $li = new BaseElement('li');
        $img = new BaseElement('img');        
        $spanlabel = new BaseElement('span');
        $spansecondlabel = new BaseElement('span');
        
        $img->src = $src;
        $img->add_class('avatar');
        
        $spanlabel->innertext($label);
        $spanlabel->add_class('label');
        $spansecondlabel->innertext($second_label);
        $spansecondlabel->add_class('second-label');        
        
        $li->append($img);
        $li->append($spanlabel);
        $li->append($spansecondlabel);
        
        $this->append($li);
        
        return $this;
    }
}
