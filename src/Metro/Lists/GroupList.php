<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Lists;

use Barebone\Metro\BaseElement;

/**
 * Description of GroupList
 *
 * @author Frank
 */
class GroupList extends BaseList{
    
    public function __construct() {
        $this->add_class('group-list');
    }
    
    function addItem($caption, $href = null){
        
        $div = new BaseElement('div');        
        $div->add_class('item');
        
        if(!is_null($href)){
            $a = new BaseElement('a');
            $a->href = $href;
            $a->innertext($caption);
            $div->append($a);
        } else {
            $div->innertext($caption);
        }
        
        $this->append($div);
    }
    
    function horizontal(){
        $this->add_class('horizontal');
        return $this;
    }
}
