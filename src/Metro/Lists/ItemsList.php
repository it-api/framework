<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Lists;

use Barebone\Metro\BaseElement;

/**
 * Description of ItemsList
 *
 * @author Frank
 */
class ItemsList  extends BaseList{
    
    public function __construct() {
        $this->add_class('items-list');
    }
    
    function addItem($src, $label, $second_label, $action){
        
        $div = new BaseElement('div');
        $div->add_class('item');
        $img = new BaseElement('img');        
        $spanlabel = new BaseElement('span');
        $spansecondlabel = new BaseElement('span');
        $spansecondaction = new BaseElement('span');
        
        $img->src = $src;
        $img->add_class('avatar');
        
        $spanlabel->innertext($label);
        $spanlabel->add_class('label');
        $spansecondlabel->innertext($second_label);
        $spansecondlabel->add_class('second-label');        
        $spansecondaction->add_class($action);
        $spansecondaction->add_class('second-action');
        
        $div->append($img);
        $div->append($spanlabel);
        $div->append($spansecondlabel);
        $div->append($spansecondaction);
        
        $this->append($div);
        
        return $this;
    }
}
