<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Lists;

use Barebone\Metro\BaseElement;

/**
 * Description of CustomMarkerList
 *
 * @author Frank
 */
class CustomMarkerList extends BaseList{
    
    public function __construct() {
        $this->add_class('custom-list-marker');
    }
    
    function addItem($caption, $marker = null){
        
        $li = new BaseElement('li');
        $li->innertext($caption);
        if(!is_null($marker)){
            $li->data('marker', $marker);
        }
        $this->append($li);
        
        return $this;
    }
}
