<?php

namespace Barebone\Metro;

class CalendarPicker extends BaseElement{
    
    //<input type="text" data-role="calendarpicker">
    
    public function __construct() {
        parent::__construct('input');
        $this->data('role', 'calendarpicker');
    }
    // 	data-dialog-mode 	false 	Set picker into dialog mode
    function dialogMode($bool = 'false'){
        $this->data('dialog-mode', $bool);
        return $this;
    }
    // 	data-dialog-point 	360 	Set max screen width when picker switch into dialog mode
    function dialogPoint(int $max = 360){
        $this->data('dialog-point', $max);return $this;
    }
    // 	data-dialog-overlay 	true 	Create overlay for dialog mode
    function dialogOverlay($bool = 'true'){
        $this->data('dialog-overlay', $bool);
        return $this;
    }
    // 	data-overlay-color 	#000000 	Overlay color
    function overlayColor($color){
        $this->data('dialog-color', $color);
        return $this;
    }
    // 	data-overlay-alpha 	.5 	Overlay alpha color channel
    function overlayAlpha($alpha = '.5'){
        $this->data('overlay-alpha', $alpha);
        return $this;
    }
    // 	data-locale 	METRO_LOCALE {en-US} 	Component language
    function locale($locale = 'nl-NL'){
        $this->data('locale', $locale);
        return $this;
    }
    // 	data-size 	100% 	Component size
    function size($size = '100%'){
        $this->data('size', $size);
        return $this;
    }
    // 	data-format 	%Y/%m/%d 	Output date format
    function format($format = '%d%m%Y'){
        $this->data('format', $format);
        return $this;
    }
    // 	data-input-format 	null 	Input date format
    function inputFormat($format = null){
        $this->data('input-format', $format);
        return $this;
    }
    // 	data-clear-button 	false 	Show/hide clear button
    function clearButton($bool = 'false'){
        $this->data('clear-button', $bool);
        return $this;
    }
    // 	data-clear-button-icon 	<span class='mif-cross'></span> 	Icon for clear button
    function clearButtonIcon($icon){
        $this->data('clear-button-icon', $icon);
        return $this;
    }
    // 	data-calendar-button-icon 	<span class='mif-calendar'></span> 	Icon for show calendar button
    function calendarButtonIcon($icon){
        $this->data('calendar-button-icon', $icon);
        return $this;
    }
    // 	data-cls-picker 		Additional class for calendarpicker
    function clsPicker($cls = ''){
        $this->data('cls-picker', $cls);
        return $this;
    }
}