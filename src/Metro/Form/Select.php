<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Form;

use Barebone\Metro\Form\BaseInputElement;

/**
 * Description of Select
 *
 * @author Frank
 */
class Select  extends BaseInputElement{
    
    public function __construct() {
        
        parent::__construct('select');        
        $this->data('role', 'select');
    }
    
    function addItem($text, $value = ''){
        $option = new Option($text, $value);
        $this->append($option);
        return $this;
    }
    
    function addOptgroup($label){
        $optgroup = new OptGroup($label);
        $this->append($optgroup);
        return $optgroup;
    }
    
    function multiple(){
        $this->multiple = 'multiple';
        return $this;
    }
}
