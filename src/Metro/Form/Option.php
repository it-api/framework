<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Form;

use Barebone\Metro\Form\BaseInputElement;

/**
 * Description of Option
 *
 * @author Frank
 */
class Option extends BaseInputElement{
    
    public function __construct($text, $value = '') {        
        parent::__construct('option'); 
        if(!empty($value)){
            $this->value = $value;
        }
        $this->innertext($text);
    }
}
