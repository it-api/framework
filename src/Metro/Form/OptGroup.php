<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Form;

use Barebone\Metro\Form\BaseInputElement;

/**
 * Description of OptGroup
 *
 * @author Frank
 */
class OptGroup extends BaseInputElement{
    
    public function __construct($label) {        
        parent::__construct('optgroup'); 
        $this->label = $label;
    }
    
    function addItem($text, $value = ''){
        $option = new Option($text, $value);
        $this->append($option);
        return $this;
    }
}
