<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Form;

use Barebone\Metro\Form\BaseInputElement;

/**
 * Description of TagInput
 *
 * @author Frank
 */
class TagInput extends BaseInputElement{
    
    public function __construct() {
        parent::__construct();
        $this->data('role', 'taginput');
    }
    /**
     * By default this attribute have a next values Enter, Space, Comma - ⏎, , ,. You can set your own trigger with this attribute. 
     * @param type $trigger
     * @return $this
     */
    function trigger($trigger){
        $this->data('trigger', $trigger);
        return $this;
    }
    
    function max($max){
        $this->data('max-tags', $max);
        return $this;
    }
    
    function randomColors(){
        $this->data('random-color', 'true');
        return $this;
    }
}
