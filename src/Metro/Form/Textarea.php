<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Form;

use Barebone\Metro\Form\BaseInputElement;

/**
 * Description of Textarea
 *
 * @author Frank
 */
class Textarea extends BaseInputElement{
    
    public function __construct() {
        parent::__construct('textarea');
        $this->data('role', 'textarea');
    }
    
    function defaultValue($value){
        $this->data('default-value', $value);
        return $this;
    }
    
    function customClearButton($mif_icon){
        $this->data('clear-button-icon', "<span class='{$mif_icon}'></span>");
        return $this;
    }
    
    function hideClearButton(){
        $this->data('clear-button', 'false');
        return $this;
    }
    
    function showClearButton(){
        $this->data('clear-button', 'true');
        return $this;
    }
    
    function showCounter($counterElement, $template = 'You entered ($1) char(s)'){
        $this->data('chars-counter', $counterElement);
        $this->data('chars-counter-template', $template);
        return $this;
    }
}
