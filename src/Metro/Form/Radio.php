<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Form;

use Barebone\Metro\Form\BaseInputElement;

/**
 * Description of Checkbox
 *
 * @author Frank
 */
class Radio extends BaseInputElement{
    
    public function __construct() {
        parent::__construct();
        $this->type = 'radio';
        $this->data('role', 'radio');
    }
    
    function checked(){
        $this->checked = 'checked';
        return $this;
    }
    
    function disabled(){
        $this->disabled = 'disabled';
        return $this;
    }
    
    function style($stylenr){
        $this->data('style', $stylenr);
        return $this;
    }
    
    function label($caption, $position = ''){
        if($position === 'left'){
            $this->data('caption-position', 'left');
        }
        $this->data('caption', $caption);
        return $this;
    }
    
    function color($fgcolor, $bgcolor = ''){
        if($bgcolor === ''){
            $bgcolor = $fgcolor;
        }
        $this->data('cls-caption', "fg-{$fgcolor} text-bold");
        $this->data('cls-check', "bg-{$bgcolor}");
        return $this;
    }
}
