<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Form;

use \Barebone\Metro\BaseElement;

/**
 * Description of RadioGroup
 *
 * @author Frank
 */
class RadioGroup  extends BaseElement{
    
    private $name;
    
    public function __construct($name) {
        parent::__construct('div');
        $this->add_class('radio-group');
        $this->name = $name;
    }
    
    function setLabel($caption){
        
        $label = new BaseElement('div');
        $label->add_class('radio-group-label');
        $label->innertext($caption);
        $this->append($label);
        return $this;
       
    }
    
    function addItem($caption, $value, $id = null){
        
        if(is_null($id)){
            $id = uniqid('RADIO');
        }
        
        $input = new BaseElement('input');
        $input->name = $this->name;
        $input->id = $id;
        $input->type = 'radio';
        $input->value = $value;
        $input->add_class('radio-group-item');
        
        $label = new BaseElement('label');
        $label->for = $id;
        $label->innertext($caption);
        $this->append($input);
        $this->append($label);
        return $this;
    }
}
