<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Form;

use Barebone\Metro\Form\BaseInputElement;

/**
 * Description of Checkbox
 *
 * @author Frank
 */
class Checkbox extends BaseInputElement{
    
    public function __construct() {
        parent::__construct();
        $this->type = 'checkbox';
        $this->data('role', 'checkbox');
    }
    
    function checked(){
        $this->checked = 'checked';
        return $this;
    }
    
    function disabled(){
        $this->disabled = 'disabled';
        return $this;
    }
    
    function style($stylenr){
        $this->data('style', $stylenr);
        return $this;
    }
    
    function label($caption, $position = ''){
        if($position === 'left'){
            $this->data('caption-position', 'left');
        }
        $this->data('caption', $caption);
        return $this;
    }
    
    function indeterminate(){
        $this->data('indeterminate', 'true');
        return $this;
    }
    
    function color($fgcolor, $bgcolor = ''){
        if($bgcolor === ''){
            $bgcolor = $fgcolor;
        }
        $this->data('cls-caption', "fg-{$fgcolor} text-bold");
        $this->data('cls-check', "bg-{$bgcolor}");
        return $this;
    }
    
    function switch(){
        $this->data('role', 'switch');
        return $this;
    }
    
    function material(){
        $this->data('material', 'true');
        return $this;
    }
    
    function textOn($text = 'On'){
        $this->data('on', $text);
        return $this;
    }
    
    function textOff($text = 'Off'){
        $this->data('off', $text);
        return $this;
    }
}
