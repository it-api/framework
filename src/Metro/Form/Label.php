<?php

namespace Barebone\Metro\Form;

use Barebone\Metro\BaseElement;

class Label extends BaseElement{
    
    public function __construct($caption = null) {
        parent::__construct('label');
        if(!is_null($caption)){
            $this->innertext($caption);
        }
    }
    
}