<?php

namespace Barebone\Metro\Form;

use Barebone\Metro\BaseElement;

class Input extends BaseElement{
    
    public function __construct($type = 'text', $attributes = []) {
        parent::__construct('input', $attributes);
        $this->type = $type;
    }
    
    function prepend($prepend){
        $this->data('prepend', $prepend);
        return $this;
    }
    
    function prependMif($icon){
        $mif = new BaseElement('span');
        $mif->class = $icon;
        $this->data('prepend', str_replace('"', "'", (string)$mif));
        return $this;
    }
}