<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Form;

use Barebone\HTML\TElement;

/**
 * Description of BaseInputElement
 *
 * @author Frank
 */
class BaseInputElement  extends TElement{
    
    public function __construct(string $tag = 'input', array $attributes = []) {
        parent::__construct($tag, $attributes);
    }
    
    function prependData($data){
        $this->data('prepend', $data);
        return $this;
    }
    
    function appendData($data){
        $this->data('append', $data);
        return $this;
    }
    
    function placeholder($placeholder){
        $this->placeholder = $placeholder;
        return $this;
    }
    
    function normal(){
        $this->remove_class('input-small');
        $this->remove_class('input-large');
    }
    
    function small(){
        $this->add_class('input-small');
        return $this;
    }
    
    function large(){
        $this->add_class('input-large');
        return $this;
    }
    
    function cls($cls, $color){
        $this->data("cls-{$cls}", $color);
        return $this;
    }
}
