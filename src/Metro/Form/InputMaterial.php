<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Form;

use Barebone\Metro\Form\BaseInputElement;

/**
 * Description of InputMaterial
 *
 * @author Frank
 */
class InputMaterial extends BaseInputElement {
    
    public function __construct() {
        parent::__construct();
        $this->type = 'text';
        $this->data('role', 'materialinput');
    }
    
    function label($caption){
        $this->data('label', $caption);
        return $this;
    }
    
    function informer($informer){
        $this->data('informer', $informer);
        return $this;
    }
    
    function icon($mif_icon){
        $this->data('icon', "<span class='{$mif_icon}'></span>");
        return $this;
    }
}
