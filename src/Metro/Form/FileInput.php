<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Form;

use Barebone\Metro\Form\BaseInputElement;

/**
 * Description of Checkbox
 *
 * @author Frank
 */
class FileInput extends BaseInputElement{
    
    public function __construct() {
        parent::__construct();
        $this->type = 'file';
        $this->data('role', 'file');
    }
    
    function prependText($text){
        $this->data('prepend', $text);
        return $this;
    }
    
    function buttonTitle($btntitle){
        $this->data('button-title', $btntitle);
        return $this;
    }
    
    function dropMode(){
        $this->data('mode', 'drop');
        return $this;
    }
}
