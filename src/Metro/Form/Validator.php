<?php

namespace Barebone\Metro\Form;

use Barebone\Metro\BaseElement;

class Validator extends BaseElement{
    
    private $input = null;
    private $errorMessage = null;
    
    public function __construct(Input $input) {
        $this->input = $input;
    }
    
    function errorMessage($msg){
        $this->errorMessage = new BaseElement('span');
        $this->errorMessage->add_class('invalid_feedback');
        $this->errorMessage->innertext($msg);
    }
    //<input type="text" data-validate="required">
    function required(){
        $this->input->data('validate', $this->input->data('validate') . ' required');
    }
    
    function length($length){
        $this->input->data('validate', $this->input->data('validate') . ' length='.$length);
    }
    
    function minLength($length){
        $this->input->data('validate', $this->input->data('validate') . ' minlength='.$length);
    }
    
    function __toString(){
        return $this->input . $this->errorMessage;
    }
/*
    Length
    Min length
    Max length
    Min value
    Max value
    Email
    Domain
    Url
    Date
    Number
    Integer
    Float
    Digits
    Hex color
    Color
    Pattern
    Compare
    Not
    NotEquals
    Equals
    Custom
*/
}