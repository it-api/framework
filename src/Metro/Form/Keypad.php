<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Form;

use Barebone\Metro\Form\BaseInputElement;

/**
 * Description of Keypad
 *
 * @author Frank
 */
class Keypad  extends BaseInputElement {
    
    public function __construct() {
        parent::__construct();
        $this->type = 'text';
        $this->data('role', 'keypad');
    }
    
    function keySize($keysize = 32){
        $this->data('key-size', $keysize);
        return $this;
    }
    
    function keys($csvkeys = '1, 2, 3, 4, 5, 6, 7, 8, 9, 0'){
        $this->data('keys', $csvkeys);
        return $this;
    }
    
    function shuffle(){
        $this->data('shuffle', 'true');
        return $this;
    }
    /**
     * Keys position. Can be a: left, top-left, top, top-right, right, bottom-right, bottom, bottom-left.
     * @param type $position
     * @return $this
     */
    function position($position = 'bottom-left'){
        $this->data('position', $position);
        return $this;
    }
    
    function hideValue(){
        $this->data('showValue', 'false');
        return $this;
    }
    
    function target($target){
        $this->data('target', $target);
        return $this;
    }
    
    function keyLength($length){
        $this->data('key-length', $length);
        return $this;
    }
}
