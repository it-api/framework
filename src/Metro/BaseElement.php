<?php

namespace Barebone\Metro;

use Barebone\HTML\TElement;

class BaseElement extends TElement{
    /**
     * 
     * @param string $tag
     * @param array $attributes
     */
    public function __construct(string $tag = 'div', array $attributes = []) {
        parent::__construct($tag, $attributes);
    }
    
    function bg($color){
        $this->add_class("bg-{$color}");
        return $this;
    }
    
    function fg($color){
        $this->add_class("fg-{$color}");
        return $this;
    }
    /**
     * Floats element to left or right
     * @param type $pos [left, right]
     * @return $this
     */
    function place($pos = 'left'){
        $this->add_class("place-{$pos}");
        return $this;
    }
    /**
     * Add clear class to element
     * @return $this
     */
    function clearfix(){
        $this->add_class("clear");
        return $this;
    }
    
    function padding($p, $size = null){
        if(is_null($size)){
            $this->add_class("p-{$p}");
        } else {
            $this->add_class("d-{$p}-{$size}");
        }

        return $this;
    }
    function margin($m, $size = null){
        if(is_null($size)){
            $this->add_class("m-{$m}");
        } else {
            $this->add_class("m-{$m}-{$size}");
        }
        return $this;
    }
    function border($b){
        if(!$this->class_exists('border')){
            $this->add_class('border');
        }
        $this->add_class("border-size-{$b}");
        return $this;
    }
    function borderSides($sides){
        if($this->class_exists('border')){
            $this->remove_class('border');
        }
        $this->add_class($sides);
        return $this;
    }
    function borderColor($color){
        if(!$this->class_exists('border')){
            $this->add_class('border');
        }
        $this->add_class("bd-{$color}");
        return $this;
    }
    /**
     * solid, dashed, dotted, double, groove, inset, outset, ridge
     * @param type $style
     * @return $this
     */
    function borderStyle($style){
        $this->add_class("border-{$style}");
        return $this;
    }
    /**
     * 
        (default 4px)
        1 to 10 - set border radius from 10% to 100%

     * @param type $radius
     */
    function borderRadius($radius = null){
        if(!$this->class_exists('border')){
            $this->add_class('border');
        }
        if(is_null($radius)){
            $this->add_class("border-radius");
        } else {
            $this->add_class("border-radius-{$radius}");
        }
        return $this;
    }
    
    function cursor($cursor){
        $this->add_class("c-{$cursor}");
        return $this;
    }
    /**
     * 
     * @param type $display [none, block, inline, inline-block, table, table-row, table-cell, flex, inline-flex]
     * @param type $size [sm | md | lg | xl | xxl]
     * @return $this
     */
    function display($display, $size = null){
        if(is_null($size)){
            $this->add_class("d-{$display}");
        } else {
            $this->add_class("d-{$display}-{$size}");
        }
        return $this;
    }
    /**
     * 
     * @param type $size [sm | md | lg | xl | xxl]
     * @return $this
     */
    function show($size = null){
        if($this->class_exists('no-visible')){
            $this->remove_class('no-visible');
        }
        if(is_null($size)){
            $this->add_class("visible");
        } else {
            $this->add_class("visible-{$size}");
        }        
        return $this;
    }
    /**
     * 
     * @param type $size [sm | md | lg | xl | xxl]
     * @return $this
     */
    function hide($size = null){
        if(is_null($size)){
            $this->add_class("no-visible");
        } else {
            $this->add_class("no-visible-{$size}");
        }
        return $this;
    }
    
    function height($height){
        $this->css('height', $height);
        return $this;
    }
    
    function width($width){
        $this->css('width', $width);
        return $this;
    }
    
    /**
     * Used for flex items
     * @param type $align
     * @return $this
     */
    function selfAlign($align){
        $this->add_class("flex-self-{$align}");
        return $this;
    }
    /**
     * 
     * @param type $float [left, right, none]
     * @param type $size [sm | md | lg | xl | xxl]
     * @return $this
     */
    function float($float = 'none', $size = null){
       
        if(is_null($size)){
            $this->add_class("float-{$float}");
        } else {
            $this->add_class("float-{$float}-{$size}");
        }
        
        return $this;
    }
    
    function fixed(){
        $this->add_class("pos-fixed");
    }
    
    function fixedTop(){
        $this->add_class("fixed-top");
        return $this;
    }
    
    function fixedBottom(){
        $this->add_class("fixed-bottom");
        return $this;
    }
    
    /**
     * Metro 4 contains classes to place object on sides of rose of the wind. To place use classes: 
     * @param type $put [nw, n, .ne, sw, s, se, wn, w, ws, en, e, es, [left, right] dropdown ]
     */
    function put($put){
        $this->add_class("put-{$put}");
        return $this;
    }
    
    /**
     * You can use next classes to set element position inside parent: 
     * @param type $position [top-left, top-center, top-right, bottom-left, bottom-center, bottom-right, right-center, left-center and center. ]
     */
    function position($position){
        $this->add_class("pos-{$position}");
        return $this;
    }
    
    function material(){
        $this->data('material', 'true');
        return $this;
    }
    
    function draggable(){
        $this->data('role', 'draggable');
    }
}