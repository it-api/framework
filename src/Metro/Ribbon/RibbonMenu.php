<?php

namespace Barebone\Metro\Ribbon;

class RibbonMenu extends Element{
    
    private $tabsHolder = null;
    private $contentHolder = null;
    
    function __construct(){
        
        parent::__construct('nav');
        $this->data('role','ribbonmenu');
        
        // <ul class="tabs-holder"> 
        $this->tabsHolder = new Element('ul');
        $this->tabsHolder->add_class('tabs-holder');
        
        // <div class="content-holder">
        $this->contentHolder = new Element('div');
        $this->contentHolder->add_class('content-holder');
        
        $this->append($this->tabsHolder);
        $this->append($this->contentHolder);
    }
    
    function tabsHolder(){
        return $this->tabsHolder;
    }
    
    function contentHolder(){
        return $this->contentHolder;
    }
    
    function addTab($caption, $href, $className = null){
        // <li class="static"><a href="#">File</a></li>
        $li = new Element('li');
        if(!is_null($className)){
            $li->add_class($className);
        }
        $link = new Element('a');
        $link->href = $href;
        $link->innertext($caption);
        
        $sectionid = str_replace('#', '', $href);
        if($sectionid != ''){
            $section = new Element('div');
        
        
            $section->id = $sectionid;
            $section->add_class('section');
            $this->contentHolder()->append($section);
            
        }
        
        $li->append($link);
        
        $this->tabsHolder()->append($li);
        
        return $section ?? null;
        
    }
    
    function addGroup($section, $caption = null){
        // <div class="group">
        $group = new Group($caption);
        
        $section->append($group);
        
        return $group;
    }
    
    function addGroupDivider($section){
        $groupdivider = new GroupDivider();
        
        $section->append($groupdivider);
        
        return $this;
    }
    
    function addButton($group, $caption, $iconClass, $callback = null){
        
        //<button class="ribbon-button">
        //    <span class="icon">
        //        <span class="mif-share fg-cyan"></span>
        //    </span>
        //    <span class="caption">Share</span>
        //</button>
        $button = new Element('button');
        $button->add_class('ribbon-button');
        $button->on('click', $callback);
        
        $spanicon = new Element('span');
        $spanicon->add_class('icon');
        
        $spanmif = new Element('span');
        $spanmif->add_class($iconClass);
        
        $text = new Element('span');
        $text->add_class('caption');
        $text->innertext($caption);
        
        $spanicon->append($spanmif);
        $button->append($spanicon);
        $button->append($text);
        
        $group->append($button);
        
    }
    
    function addDropdown($group, Dropdown $dropdown){
        $group->append($dropdown);
    }
    
    function addFlexColumn($section){
        $flexcolumn = new FlexColumn();
        
        $section->append($flexcolumn);
        
        return $flexcolumn;
    }
    
    function loadFromJSON($json){
        
        $data = json_decode($json);
        
        
        if(!isset($data->RibbonMenu)){
            die('This is not a RibbonMenu');
        }
        
        if(!isset($data->RibbonMenu->tabs)){
            die('This is not a RibbonMenu');
        }
        
        if(!isset($data->RibbonMenu->groups)){
            die('This is not a RibbonMenu');
        }
        
        foreach($data->RibbonMenu->tabs as $key => $tab){
            //echo $data->RibbonMenu->groups[$key]->section;
            $section = $this->addTab($tab->caption, $tab->href, $tab->className ?? null);
            if(!is_null($section)){
                $group = $this->addGroup($section, $data->RibbonMenu->groups[$key]->section);
                
                foreach($data->RibbonMenu->groups[$key]->items as $item){
                    
                    if($item->type == 'button'){
                        
                        $this->addButton($group, $item->caption, $item->iconClass, $item->callback ?? null);
                                            
                    }elseif($item->type == 'divider'){
                        
                        $this->addGroupDivider($group);
                                            
                    }elseif($item->type == 'flexColumn'){
                        
                        $flexColumn = $this->addFlexColumn($group);
                        foreach($item->items as $flexitem){
                            if($flexitem->type == 'iconbutton'){
                                
                                $iconbutton = new IconButton($flexitem->caption, $flexitem->className ?? null, count($flexitem->items) > 0, $flexitem->callback ?? null); 
                                foreach($flexitem->items as $sbitem){
                                    if($sbitem->className == 'divider'){
                                        $iconbutton->addDivider();
                                    } else {
                                        $iconbutton->addItem($sbitem->caption, $sbitem->href, $sbitem->className ?? null);
                                    }
                                }
                                $flexColumn->append($iconbutton);  
                                
                            }elseif($flexitem->type == 'toolbutton'){
                                
                                $toolbutton = new ToolButton($flexitem->className ?? null);                              
                                $flexColumn->append($toolbutton);  
                                
                            }elseif($flexitem->type == 'dropdown'){
                                
                                $fcdropdown = new Dropdown(null, $flexitem->iconClass);
                                foreach($flexitem->items as $fcitem){
                                    if($fcitem->className == 'divider'){
                                        $fcdropdown->addDivider();
                                    } else {
                                        $fcdropdown->addItem($fcitem->caption, $fcitem->href, $fcitem->className ?? null, $fcitem->callback ?? null);
                                    }
                                }
                                $fcdropdown->asToolButton();
                                $flexColumn->append($fcdropdown);
                                
                            }
                        }
                        //$this->append($flexColumn);   
                                            
                    }elseif($item->type == 'dropdown'){
                        
                        $dropdown = new Dropdown($item->caption, $item->iconClass ?? null);
                        
                        foreach($item->items as $dditem){
                            if($dditem->className == 'divider'){
                                $dropdown->addDivider();
                            } else {
                                $dropdown->addItem($dditem->caption, $dditem->href, $dditem->className ?? null, $dditem->callback ?? null);
                            }
                        }
                        
                        $this->addDropdown($group, $dropdown);
                        
                    } elseif($item->type == 'splitbutton'){
                        
                        $splitButton = new SplitButton($item->caption, $item->className ?? null, $item->callback ?? null);
                        foreach($item->items as $sbitem){
                            $splitButton->addItem($sbitem->caption, $sbitem->href, $sbitem->className ?? null, $sbitem->callback ?? null);
                        }
                        
                        $group->append($splitButton);
                    
                        
                    } elseif($item->type == 'togglegroup'){
                        
                        $toggleGroup = new ToggleGroup();
                        
                        foreach($item->items as $tgitem){
                            $toggleGroup->addButton($tgitem->caption, $tgitem->className ?? null, $tgitem->callback ?? null);
                        }
                        
                        $group->append($toggleGroup);
                        
                    }
                }
            }            
        }       
        
    }
}
