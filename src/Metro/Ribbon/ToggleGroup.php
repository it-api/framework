<?php

namespace Barebone\Metro\Ribbon;

/**
 * Description of ToggleGroup
 *<div class="ribbon-toggle-group">
 * @author Frank
 */
class ToggleGroup extends Element{
    
    public function __construct() {
        
        parent::__construct('div');
        $this->add_class('ribbon-toggle-group');
        
    }
    
    function addButton($caption, $iconClass, $callback = null){
        
        $this->append(new IconButton($caption, $iconClass, false, $callback));
        
    }

}
