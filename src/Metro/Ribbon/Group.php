<?php

namespace Barebone\Metro\Ribbon;

/**
 * Description of Group
 *
 * @author Frank
 */
class Group extends Element{
    
    public function __construct($caption = null) {
        
        parent::__construct('div');
        $this->add_class('group');
        
        if(!is_null($caption)){
            //<span class="title">ribbon controls</span>
            $title = new Element('span');
            $title->add_class('title');
            $title->innertext($caption);
            $this->append($title);
        }
    }

}
