<?php

namespace Barebone\Metro\Ribbon;

/**
 * Description of ToolButton
 *
 * @author Frank
 */
class ToolButton extends Element{
    
    //<button class="ribbon-tool-button">
    //<span class="mif-location fg-red"></span>
    //</button>
    /**
     * 
     * @param type $iconClass
     */
    public function __construct($iconClass) {
        parent::__construct('button');
        $this->add_class('ribbon-tool-button');
        $span = new Element('span');
        $span->add_class($iconClass);
        $this->append($span);
    }

}
