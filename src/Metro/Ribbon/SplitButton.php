<?php

namespace Barebone\Metro\Ribbon;

/**
 * Description of SplitButton
 *  <div class="ribbon-split-button">
        <button class="ribbon-main">
            <span class="icon">
                <span class="mif-cogs"></span>
            </span>
        </button>
        <span class="ribbon-split dropdown-toggle">Options</span>
        <ul class="ribbon-dropdown" data-role="dropdown" data-duration="100">
            <li class="checked"><a href="#">Modification</a></li>
            <li class="checked"><a href="#">Type</a></li>
            <li class="checked"><a href="#">Size</a></li>
            <li><a href="#">Creating</a></li>
            <li><a href="#">Authors</a></li>
            <li class="checked-one"><a href="#">Tags</a></li>
            <li><a href="#">Names</a></li>
            <li class="divider"></li>
            <li><a href="#">Columns...</a></li>
        </ul>
    </div>
 * @author Frank
 */
class SplitButton extends Element{
    
    private $button = null;
    private $ddlist = [];
    
    public function __construct($caption, $iconClass, $callback = null) {
        
        parent::__construct('div');            
        $this->add_class('ribbon-split-button');
        
        $this->button = new Element('button');
        $this->button->add_class('ribbon-main');
        if(!is_null($callback)){
            $this->button->on('click', $callback);
        }
        
        $icon = new Element('span');
        $icon->add_class('icon');
                
        $mif = new Element('span');
        $mif->add_class($iconClass);
        
        $icon->append($mif);
        $this->button->append($icon);
        
        $this->append($this->button);
        
        $split = new Element('span');
        $split->add_class('ribbon-split dropdown-toggle');
        $split->innertext($caption);
        
        $this->append($split);
        
        $this->ddlist = new Element('ul');
        $this->ddlist->add_class('ribbon-dropdown');
        $this->ddlist->data('role','dropdown');
        $this->ddlist->data('duration','100');
        
        $this->append($this->ddlist);
        
    }
    
    function addItem($caption, $href, $className = null, $callback = null){
        // <li class="checked"><a href="#">Modification</a></li>
        $li = new Element('li');
        if(!is_null($className)){
            $li->add_class($className);
        }
        $link = new Element('a');
        $link->innertext($caption);
        $link->href = $href;
        if(!is_null($callback)){
            $link->on('click', $callback);
        }
        $li->append($link);
        
        $this->ddlist->append($li);
        
    }
    
    function addDivider(){
        
        //<li class="divider"></li>
        $li = new Element('li');
        $li->add_class('divider');
        $this->ddlist->append($li);
        
    }

}
