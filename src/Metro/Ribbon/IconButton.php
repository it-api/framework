<?php

namespace Barebone\Metro\Ribbon;

/**
 * Description of IconButton
 *<div>
    <button class="ribbon-icon-button dropdown-toggle">
        <span class="icon">
            <span class="mif-apps"></span>
        </span>
        <span class="caption">Apps</span>
    </button>
    <ul class="ribbon-dropdown" data-role="dropdown">
        <li class="checked"><a href="#">Modification</a></li>
        <li class="checked"><a href="#">Type</a></li>
        <li class="checked"><a href="#">Size</a></li>
        <li><a href="#">Creating</a></li>
        <li><a href="#">Authors</a></li>
        <li class="checked-one"><a href="#">Tags</a></li>
        <li><a href="#">Names</a></li>
        <li class="divider"></li>
        <li>
            <a href="#" class="dropdown-toggle">Columns...</a>
            <ul class="ribbon-dropdown" data-role="dropdown">
                <li><a href="#">Creating</a></li>
                <li><a href="#">Authors</a></li>
                <li><a href="#">Names</a></li>
            </ul>
        </li>
    </ul>
</div>
 * @author Frank
 */
class IconButton  extends Element{
    
    private $button = null;
    private $ddlist = [];
    
    public function __construct($caption, $iconClass, bool $dropdown = false, $callback = null) {
        
        parent::__construct('div');        
        
        $this->button = new Element('button');
        $this->button->add_class('ribbon-icon-button');
        if(!is_null($callback)){
            $this->button->on('click', $callback);
        }
        
        $icon = new Element('span');
        $icon->add_class('icon');
                
        $mif = new Element('span');
        $mif->add_class($iconClass);
        
        $icon->append($mif);
        $this->button->append($icon);
        
        $this->append($this->button);
        
        $text = new Element('span');
        $text->add_class('caption');
        $text->innertext($caption);
        
        $this->button->append($text);
        
        if($dropdown === true){
            
            $this->button->add_class('dropdown-toggle');
            
            $this->ddlist = new Element('ul');
            $this->ddlist->add_class('ribbon-dropdown');
            $this->ddlist->data('role','dropdown');
            $this->ddlist->data('duration','100');

            $this->append($this->ddlist);
        }
        
    }
    
    function addItem($caption, $href, $className = null){
        // <li class="checked"><a href="#">Modification</a></li>
        $li = new Element('li');
        if(!is_null($className)){
            $li->add_class($className);
        }
        $link = new Element('a');
        $link->innertext($caption);
        $link->href = $href;
        
        $li->append($link);
        
        $this->ddlist->append($li);
        
    }
    
    function addDivider(){
        
        //<li class="divider"></li>
        $li = new Element('li');
        $li->add_class('divider');
        $this->ddlist->append($li);
        
    }
    
}
