<?php

namespace Barebone\Metro\Ribbon;

use Barebone\HTML\TElement;

class Element extends TElement{
    /**
     * 
     * @param string $tag
     * @param array $attributes
     */
    public function __construct(string $tag = 'div', array $attributes = []) {
        parent::__construct($tag, $attributes);
    }

}