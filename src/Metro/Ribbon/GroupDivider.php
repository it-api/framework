<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro\Ribbon;

/**
 * Description of GroupDivider
 *
 * @author Frank
 */
class GroupDivider  extends Element{
    
    public function __construct() {
        
        //<div class="group-divider"></div>
        parent::__construct('div');
        $this->add_class('group-divider');
        
    }
    
}
