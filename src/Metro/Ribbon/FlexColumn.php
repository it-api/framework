<?php

namespace Barebone\Metro\Ribbon;

/**
 * Description of FlexColumn
 *
 * @author Frank
 */
class FlexColumn extends Element{
    //<div class="pl-1 pr-1 d-flex flex-column">
    public function __construct() {
        parent::__construct('div');
        $this->add_class('pl-1 pr-1 d-flex flex-column');
    }

}
