<?php

namespace Barebone\Metro\Ribbon;

class Dropdown extends Element{
    
    private $button = null;
    private $ddlist = [];
    /**
     * 
     * @param type $caption
     * @param type $className
     */
    function __construct($caption, $className){
        //<div>
        parent::__construct('div');
        
        //<button class="ribbon-button dropdown-toggle">
        $this->button = new Button($caption, $className);
        $this->button->add_class('dropdown-toggle');

        $this->append($this->button);
        
        $this->ddlist = new Element('ul');
        $this->ddlist->add_class('ribbon-dropdown');
        $this->ddlist->data('role','dropdown');
        $this->ddlist->data('duration','100');
        
        $this->append($this->ddlist);
        
    }
    /**
     * 
     * @param type $caption
     * @param type $href
     * @param type $className
     */
    function addItem($caption, $href, $className = null, $callback = null){
        // <li class="checked"><a href="#">Modification</a></li>
        $li = new Element('li');
        if(!is_null($className)){
            $li->add_class($className);
        }
        
        
        $link = new Element('a');
        $link->innertext($caption);
        $link->href = $href;
        if(!is_null($callback)){
            $link->on('click', $callback);
        }
        
        $li->append($link);
        
        $this->ddlist->append($li);
        
        
    }
    
    function addDivider(){
        
        //<li class="divider"></li>
        $li = new Element('li');
        $li->add_class('divider');
        $this->ddlist->append($li);
        
    }
    
    function asToolButton(){
        $this->button->remove_class('ribbon-button');
        $this->button->add_class('ribbon-tool-button');
    }
    
}

