<?php

namespace Barebone\Metro\Ribbon;

class Button extends Element{
    /**
     * 
     * @param string $tag
     * @param array $attributes
     */
    public function __construct($caption, $className) {
        
        parent::__construct('button');
        $this->add_class('ribbon-button');
        
        //<span class="icon">
        $icon = new Element('span');
        $icon->add_class('icon');
        //<span class="mif-file-zip fg-cyan">
        $mif = new Element('span');
        $mif->add_class($className);
        //<span class="caption">Compress</span>
        if(!is_null($caption)){
            $text = new Element('span');
            $text->add_class('caption');
            $text->innertext($caption);
        }
        $icon->append($mif);
        $this->append($icon);
        if(!is_null($caption)){
            $this->append($text);
        }
    }

}