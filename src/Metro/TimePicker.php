<?php

namespace Barebone\Metro;

class TimePicker extends BaseElement{
    
    // <input data-role="timepicker">
    
    public function __construct() {
        parent::__construct('input');
        $this->data('role', 'timepicker');
    }

}