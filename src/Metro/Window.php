<?php

namespace Barebone\Metro;

class Window extends BaseElement{
    
    public function __construct($title, $content) {
        
        parent::__construct('div');
        $this->data('role', 'window');
        $this->content($content);
    }
    
    // 	data-btn-close 	true 	Add close button 	
    function btnClose(string $bool){
        $this->data('btn-close', $bool);
        return $this;
    }
    // 	data-btn-min 	true 	Add min button 	
    function btnMin(string $bool){
        $this->data('btn-min', $bool);
        return $this;
    }
    //  data-btn-max 	true 	Add max button 	
    function btnMax(string $bool){
        $this->data('btn-max', $bool);
        return $this;
    }
    // 	data-cls-window 		Additional classes for window 	
    function clsWindow(string $cls){
        $this->data('cls-window', $cls);
        return $this;
    }
    // 	data-cls-caption 		Additional classes for caption 	
    function clsCaption(string $cls){
        $this->data('cls-caption', $cls);
        return $this;
    }
    // 	data-cls-content 		Additional classes for content 	
    function clsContent(string $cls){
        $this->data('cls-content', $cls);
        return $this;
    }
    // 	data-draggable 	true 	Add draggable to window 	
    function draggable(){
        $this->data('draggable', 'true');
        return $this;
    }
    // 	data-drag-element 	.window-caption 	Set the drag element 	
    function dragElement(string $element){
        $this->data('drag-element', $element);
        return $this;
    }
    // 	data-drag-area 	parent 	Set the area where element can be draggable 
    function dragArea(string $element){
        $this->data('drag-area', $element);
        return $this;
    }	
    // 	data-shadow 	false 	Add shadow to window 
    function shadow(string $bool){
        $this->data('shadow', $bool);
        return $this;
    }	
    // 	data-icon 		Window icon 	
    function icon(string $mif){
        $this->data('icon', $mif);
        return $this;
    }
    // 	data-resizable 	true 	Add resizable to window 	
    function resizable(string $bool){
        $this->data('resizable', $bool);
        return $this;
    }
    // 	data-top 	auto 	Set window top position 	
    function top(int $top){
        $this->data('top', $top);
        return $this;
    }
    // 	data-left 	auto 	Set window left position 	
    function left(int $left){
        $this->data('left', $left);
        return $this;
    }
    // 	data-place 	auto 	Set window place (can be: center, top-left, top-center, top-right, right-center, bottom-right, bottom-center, bottom-left, left-center) 	
    function place($pos = 'center'){
        $this->data('place', $pos);
        return $this;
    }
    // 	data-top 	auto 	Set window top position 	
    function content(string $content){
        $this->data('content', $content);
        return $this;
    }
}