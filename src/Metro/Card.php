<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro;

use Barebone\Metro\BaseElement;

/**
 * Description of Card
 *
 * @author Frank
 */
class Card extends BaseElement{
    
    public function __construct() {
        parent::__construct();
        $this->add_class('card');
    }
    
    function addHeader($text, $fg = '', $bg = ''){
        
        $div = new BaseElement('div');
        $div->add_class('card-header');
        if(!empty($fg)){
            $div->add_class("fg-{$fg}");
        }
        if(!empty($fg)){
            $div->add_class("bg-{$bg}");
        }
        $div->innertext($text);
        $this->append($div);
        return $this;
        
    }
    
    function addHeaderWithImage($text, $src, $fg = '', $bg = ''){
        
        $this->add_class('image-header');
        
        $div = new BaseElement('div');
        $div->add_class('card-header');
        $div->css("background-image","url({$src})");
        if(!empty($fg)){
            $div->add_class("fg-{$fg}");
        }
        if(!empty($fg)){
            $div->add_class("bg-{$bg}");
        }
        $div->innertext($text);
        $this->append($div);
        return $this;
        
    }
    
    function addContent($text, $fg = '', $bg = ''){
        
        $div = new BaseElement('div');
        $div->add_class('card-content');
        if(!empty($fg)){
            $div->add_class("fg-{$fg}");
        }
        if(!empty($fg)){
            $div->add_class("bg-{$bg}");
        }
        $div->padding(3);
        $div->innertext($text);
        $this->append($div);
        return $this;
        
    }
    
    function addFooter($text, $fg = '', $bg = ''){
        
        $div = new BaseElement('div');
        $div->add_class('card-footer');
        if(!empty($fg)){
            $div->add_class("fg-{$fg}");
        }
        if(!empty($fg)){
            $div->add_class("bg-{$bg}");
        }
        $div->innertext($text);
        $this->append($div);
        
        return $this;
        
    }
}
