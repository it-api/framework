<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro;

/**
 * Description of Dropdown
 *
 * @author Frank
 */
class Dropdown extends BaseElement {
    
    private $ul = null;
    
    public function __construct($caption) {
    
        parent::__construct('div');
        $this->add_class('dropdown-button');
        
        $button = new BaseElement('button');
        $button->add_class('button dropdown-toggle');
        $button->innertext($caption);
        
        $this->append($button);
        
        $this->ul = new BaseElement('ul');
        $this->ul->add_class('d-menu');
        $this->ul->data('role', 'dropdown');
        
        $this->append($this->ul);
        
        return $this;
    }
    
    function addItem($caption, $href = '#'){
        
        $li = new BaseElement('li');
        $a = new BaseElement('a');
        $a->href = $href;
        $a->innertext($caption);
        $li->append($a);
        
        $this->ul->append($li);
        
        return $this;
    }
    
    function placeRight(){
        
        $this->add_class('place-right');
        
        return $this;
    }
}
