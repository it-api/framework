<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro;

use Barebone\Metro\BaseElement;

/**
 * Description of ActivityIndicator
 *
 * @author Frank
 */
class ActivityIndicator extends BaseElement{
    
    private $autoHide = 3000;
    private $overlayClickClose = true;
    
    public function __construct() {
        
        parent::__construct();
        $this->data('role', 'activity');
        
    }
    
    function setAutoHide( int $autoHide){
        $this->autoHide = $autoHide;
    }
    
    function getAutoHide() : int{
        return $this->autoHide;
    }
    
    function setOverlayClickClose(bool $overlayClickClose){
        $this->overlayClickClose = $overlayClickClose;
    }
    
    function getOverlayClickClose() : bool{
        return $this->overlayClickClose;
    }
    
    function ring(){
        $this->data('type', 'ring');
        return $this;
    }
    function metro(){
        $this->data('type', 'metro');
        return $this;
    }
    function square(){
        $this->data('type', 'square');
        return $this;
    }
    function cycle(){
        $this->data('type', 'cycle');
        return $this;
    }
    function simple(){
        $this->data('type', 'simple');
        return $this;
    }
    function atom(){
        $this->data('type', 'atom');
        return $this;
    }
    function bars(){
        $this->data('type', 'bars');
        return $this;
    }
    
    function dark(){
        $this->data('style', 'dark');
        return $this;
    }
    
    function color(){
        $this->data('style', 'color');
        return $this;
    }
    
    function getOnClick(){
        return 'Metro.activity.open({autoHide: '.$this->getAutoHide().', type: \''.$this->data('type').'\', overlayClickClose: '.($this->getOverlayClickClose()?'true':'false').'})';
    }
    
    function activity($type = 'metro', $autoHide = 3000, $overlayClickClose = true){
        return 'Metro.activity.open({autoHide: '.$autoHide.', type: \''.$type.'\', overlayClickClose: '.($overlayClickClose?'true':'false').'})';
    }
}
