<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro;

/**
 * Description of Toolbar
 *
 * @author Frank
 */
class Toolbar extends BaseElement{
    
    public function __construct() {
        
        parent::__construct('div');        
        $this->add_class('toolbar');
    }
    
    function addTextButton($text){
        
        //<button class="tool-button">Open</button>
        $button = new Button($text);
        $button->class = ('tool-button text-button');
        $this->append($button);
        
        return $button;
        
    }
    
    function addIconButton($mif_icon){
        
        //<button class="tool-button"><span class="mif-floppy-disk"></span></button>
        $span = new BaseElement('span');
        $span->add_class($mif_icon);
        $button = new Button('');
        $button->class = ('tool-button');
        
        $button->append($span);
        $this->append($button);
        
        return $button;
        
    }
}
