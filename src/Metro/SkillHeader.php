<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro;

use Barebone\Metro\BaseElement;

/**
 * Description of SkillHeader
 *
 * @author Frank
 */
class SkillHeader extends BaseElement{
    
    public function __construct() {
        parent::__construct();
        $this->add_class('header');
    }
    
    function addImage($src){
        $img = new BaseElement('img');
        $img->src = $src;
        $img->add_class('avatar');
        $this->append($img);
        
        return $this;
    }
    function addTitle($title){
        $div = new BaseElement('div');
        $div->innertext($title);
        $div->add_class('title');
        $this->append($div);
        
        return $this;
    }
    function addSubtitle($subtitle){
        $div = new BaseElement('div');
        $div->innertext($subtitle);
        $div->add_class('subtitle');
        $this->append($div);
        
        return $this;
    }
}
