<?php

namespace Barebone\Metro;

class AudioButton extends BaseElement{
    /*
    <button id="b1" class="button cycle large bg-green fg-white"
        data-role="audio-button"
        data-audio-src="data/modem.mp3"
        data-audio-volume=".1">
        <span class="mif-play"></span>
    </button>
    */
    
    public function __construct(string $src, float $volume = .1, string $icon = 'mif-play') {
        parent::__construct('button');
        $this->data('role', 'audio-button');
        $this->add_class('button cycle');
        $this->data('audio-src', $src);
        $this->data('audio-volume', $volume);
        
        $span = new BaseElement('span');
        $span->add_class($icon);
        $this->append($span);
    }
}