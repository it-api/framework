<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro;

use Barebone\Metro\BaseElement;

/**
 * Description of Accordion
 *
 * @author Frank
 */
class Accordion extends BaseElement{
    
    public function __construct() {
        
        parent::__construct();
        $this->data('role', 'accordion');
        
    }
    
    function showPreview(){
        echo new Accordion();
    }

    function addFrame($title, $content){
        
        $frame = new BaseElement('div');
        $frame->add_class('frame');
        
        $heading = new BaseElement('div');
        $heading->add_class('heading');
        $heading->innertext($title);
        
        $cont = new BaseElement('div');
        $cont->add_class('content');
        $cont->innertext($content);
        
        $frame->append($heading);
        $frame->append($cont);
        $this->append($frame);        
        
        return $this;
        
    }
}
