<?php

namespace Barebone\Metro;

class Form extends BaseElement{
    
    public function __construct() {
        parent::__construct('form');
    }
    
    function addItem(baseElement $input){
        $this->append($input);
    }
    
    function addGroup(baseElement $label, baseElement $input, $helptext = null){
        
        $group = new BaseElement('div');
        $group->add_class('form-group');
        
        $group->append($label);
        
        $group->append($input);
        
        if(!is_null($helptext)){
            $help = new BaseElement('small');
            $help->innertext($helptext);
            $help->class = 'text-muted';
            $group->append($help);
        }
        
        $this->append($group);
        
        return $this;
    }
    
}