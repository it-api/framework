<?php

namespace Barebone\Metro;

class Dialog extends BaseElement{
    /**
     *<div class="dialog">
        <div class="dialog-title">Use Windows location service?</div>
        <div class="dialog-content">
            Bassus abactors ducunt ad triticum.
            A fraternal form of manifestation is the bliss.
        </div>
        <div class="dialog-actions">
            <button class="button">Disagree</button>
            <button class="button primary">Agree</button>
        </div>
    </div>*/
    public function __construct($title, $content, $buttons = [], $accent = 'primary') {
        
        parent::__construct('div');
        $this->class = 'dialog secondary';
        $this->data('role', 'dialog');
        $dialogtitle = new BaseElement('div');
        $dialogtitle->innertext($title);
        $dialogtitle->class = 'dialog-title';
        $this->append($dialogtitle);
        
        $dialogcontent = new BaseElement('div');
        $dialogcontent->innertext($content);
        $dialogcontent->class = 'dialog-content';
        $this->append($dialogcontent);
        
        $dialogactions = new BaseElement('div');
        $dialogactions->class = 'dialog-actions';
        $this->append($dialogactions);
        
        foreach($buttons as $button){
            $dialogactions->append($button);
        }
        
    }
    //  data-close-button 	false 	If true, close button will be added to dialog to top left corner
    function closeButton($btn = 'false' ){
        $this->data('close-button', $btn);
        return $this;
    }
    // 	data-actions-align 	right 	Set dialog actions align.
    function actionsAlign(string $align = 'right'){
        $this->data('actions-align', $align);
        return $this;
    }
    //  data-overlay 	true 	Add overlay when dialog is opened
    function overlay(bool $overlay = false ){
        $this->data('overlay', $overlay);
        return $this;
    }
    // 	data-overlay-color 	#000000 	Overlay color. Can be hex color value or transparent
    function overlayColor(string $color = '#000000' ){
        $this->data('overlay-color', $color);
        return $this;
    }
    //  data-overlay-alpha 	0.5 	Overlay color alpha channel value.
    function overlayAlpha(float $alpha = 0.5){
        $this->data('overlay-alpha', $alpha);
        return $this;
    } 	
    //  data-overlay-click-close 	false 	Close dialog when user click on overlay
    function overlayClickClose($close = 'false' ){
        $this->data('overlay-click-close', $close);
        return $this;
    }
    // 	data-close-action 	true 	If this options is true Metro UI add event to elements with class .js-dialog-close for close dialog
    function closeAction($close = 'false' ){
        $this->data('close-action', $close);
        return $this;
    }
    // 	data-cls-dialog 		Add additional classes to dialog
    function clsDialog(string $cls = ''){
        $this->data('cls-dialog', $cls);
        return $this;
    }
    // 	data-cls-title 		Add additional classes to dialog title
    function clsTitle(string $cls = ''){
        $this->data('cls-title', $cls);
        return $this;
    }
    // data-cls-content 		Add additional classes to dialog content
    function clsContent(string $cls = ''){
        $this->data('cls-content', $cls);
        return $this;
    }
    //  data-cls-actions 		Add additional classes to dialog actions block
    function clsAction(string $cls = ''){
        $this->data('cls-actions', $cls);
        return $this;
    }
    // 	data-cls-default-actions 		Add additional classes to dialog default action button
    function clsDefaultAction(string $cls = ''){
        $this->data('cls-default-actions', $cls);
        return $this;
    }
    // 	data-auto-hide 	0 	If this options > 0, dialog closed after this timeout in milliseconds
    function autoHide($autohide = 'false' ){
        $this->data('auto-hide', $autohide);
        return $this;
    }
    // 	data-remove-on-close 	false 	If this options is true dialog will be removed after closes
    function removeOnClose($remove = 'false' ){
        $this->data('remove-on-close ', $remove);
        return $this;
    }
    // 	data-show 	false 	If this options is true dialog will be showed immediate after initializing
    function show($show = 'false' ){
        $this->data('show', $show);
        return $this;
    }
    // 	data-on-dialog-create 	Metro.noop 	This callback executed after dialog is created
    function onDialogCreate(string $jscallback = '' ){
        $this->data('on-dialog-create ', $jscallback);
        return $this;
    }
    // 	data-on-open 	Metro.noop 	This callback executed after dialog is open
    function onOpen(string $jscallback = '' ){
        $this->data('on-open', $jscallback);
        return $this;
    }
    // 	data-on-close 	Metro.noop 	This callback executed after dialog is close
    function onClose(string $jscallback = '' ){
        $this->data('on-close', $jscallback);
        return $this;
    }
    // 	data-on-show 	Metro.noop 	This callback executed after dialog is showing
    function onShow(string $jscallback = '' ){
        $this->data('on-show', $jscallback);
        return $this;
    }
    // 	data-on-hide 	Metro.noop 	This callback executed before dialog is hiding
    function onHide(string $jscallback = '' ){
        $this->data('on-hide', $jscallback);
        return $this;
    }
}