<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro;

/**
 * Description of ActionButton
 *
 * @author Frank
 */
class ActionButton extends BaseElement {
    
    private $ul = null;
    
    public function __construct(string $mif_icon = 'mif-plus', string $fgcolor = 'white', string $bgcolor = 'red') {
        
        parent::__construct('div');        
        $this->add_class('multi-action');
        
        $button = new BaseElement('button');
        $button->add_class('action-button rotate-minus');
        $button->add_class("bg-{$bgcolor}");
        $button->add_class("fg-{$fgcolor}");
        $button->on('click', "$(this).toggleClass('active')");
        
        
        
        $icon = new BaseElement('span');
        $icon->add_class('icon');
        
        $iconmif = new BaseElement('span');
        $iconmif->add_class($mif_icon);
        $icon->append($iconmif);
        
        $this->ul = new BaseElement('ul');
        $this->ul->add_class('actions drop-right');
        
        $this->append($button);
        $button->append($icon);
        $this->append($this->ul);
        
        return $this->ul;
    }
    
    function showPreview(){
        $actionbutton = new ActionButton();
        $actionbutton->addAction();
        $actionbutton->addAction();
        $actionbutton->addAction();
        echo $actionbutton;
    }
    
    function addAction($href = '#', $bgcolor = 'blue', $fgcolor = 'white', $mif_icon = null){
        
        $li = new BaseElement('li');
        $li->add_class("bg-{$bgcolor}");
        $li->add_class("fg-{$fgcolor}");
        $a = new BaseElement('a');
        $a->href = $href;
        
        $span = new BaseElement('span');
        $span->add_class($mif_icon);
        
        $li->append($a);
        $a->append($span);
        
        $this->ul->append($li);
        
        return $this;
    }

}
