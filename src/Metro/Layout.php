<?php

namespace Barebone\Metro;

class Layout extends BaseElement{
    
    /*
    <div data-role="splitter" class="h-100">
        <div class="d-flex flex-justify-center flex-align-center">This is panel 1</div>
        <div data-role="splitter" data-split-mode="vertical">
            <div class="d-flex flex-justify-center flex-align-center">Panel 1</div>
            <div class="d-flex flex-justify-center flex-align-center">Panel 2</div>
        </div>
    </div>
    */
    
    private $north = null;
    private $east = null;
    private $south = null;
    private $west = null;
    private $center = null;
    private $centerWrapper = null;
    
    function __construct(){
        
        parent::__construct('div');
        $this->data('role', 'splitter');
        $this->add_class('h-100');
        $this->data('gutter-size', '8');
        $this->data('split-sizes', '20, 60, 20');
        
        $this->north = new BaseElement('div');
        $this->east = new BaseElement('div');
        $this->south = new BaseElement('div');
        $this->west = new BaseElement('div');
        $this->center = new BaseElement('div');
        $this->centerWrapper = new BaseElement('div');
        
        $this->centerWrapper->data('role', 'splitter');
        $this->centerWrapper->add_class('h-100');
        $this->centerWrapper->data('split-mode', 'vertical');
        $this->centerWrapper->data('gutter-size', '8');
        $this->centerWrapper->data('split-sizes', '10, 80, 10');
        $this->centerWrapper->append($this->north);
        $this->centerWrapper->append($this->center);
        $this->centerWrapper->append($this->south);
        
        $this->north->add_class('d-flex');
        $this->east->add_class('d-flex');
        $this->south->add_class('d-flex');
        $this->west->add_class('d-flex');
        $this->center->add_class('d-flex');
        
        $this->north->data('split-mode', 'vertical');
        $this->south->data('split-mode', 'vertical');
        
        $this->append($this->west);
        $this->append($this->centerWrapper);
        $this->append($this->east);
        
        
    }
    
    function north(){
        return $this->north;
    }
    
    function east(){
        return $this->east;
    }
    
    function west(){
        return $this->west;
    }
    
    function center(){
        return $this->center;
    }
    
    function south(){
        return $this->south;
    }
}