<?php

namespace Barebone\Metro;

class VideoPlayer extends BaseElement{
    
    //<video data-role="video-player" data-src="https://metroui.org.ua/res/oceans.mp4"></video>
    public function __construct(string $src, float $volume = .1) {
        parent::__construct('video');
        $this->data('role', 'video-player');
        
        $this->data('src', $src);
        $this->data('volume', $volume);
    }
    
    function poster($poster){
        $this->data('poster', $poster);
        return $this;
    }
    
    function logo(string $src, int $height, int $width, string $target){
        $this->data('logo', $src);
        $this->data('logo-height', $height);
        $this->data('logo-width', $width);
        $this->data('logo-target', $target);
        return $this;
    }
}