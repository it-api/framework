<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone\Metro;

use Barebone\Metro\BaseElement;

/**
 * Description of SkillBox
 *
 * @author Frank
 */
class SkillBox extends BaseElement{
    
    private $skills = [];
    
    public function __construct() {
        
        parent::__construct();
        $this->add_class('skill-box');
        
        $this->skills = new BaseElement('ul');
        $this->skills->add_class('skills');
    }
    
    function addHeader($title, $subtitle, $image_src, $bg=null, $fg=null){
        
        $header = new SkillHeader();
        
        if(!empty($image_src)){
            $header->addImage($image_src);
        }
        
        if(!empty($title)){
            $header->addTitle($title);
        }
        if(!empty($subtitle)){
            $header->addSubtitle($subtitle);
        }        
        if(!is_null($bg)){
            $header->bg($bg);
        }
        if(!is_null($fg)){
            $header->fg($fg);
        }
        $this->append($header);
        $this->append($this->skills);
        return $this;
    }
    
    function addSkill(){
        $html = '';
        $args = func_get_args();
        $li = new BaseElement('li');
        foreach($args as $arg){
            
            $html .= $arg;
            //print_r($arg);
        }
        $li->innertext($html);
        $this->skills->append($li);
        return $this;
    }

}
