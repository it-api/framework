<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Barebone;

/**
 * Description of SimpleImage
 *
 * @author Frank
 */
class SimpleImage {

	var $image;
	var $image_type;

	function load(string $filename) {

		$image_info = getimagesize($filename);
		$this->image_type = $image_info[2];
		if ($this->image_type == IMAGETYPE_JPEG) {

			$this->image = imagecreatefromjpeg($filename);
		} elseif ($this->image_type == IMAGETYPE_GIF) {

			$this->image = imagecreatefromgif($filename);
		} elseif ($this->image_type == IMAGETYPE_PNG) {

			$this->image = imagecreatefrompng($filename);
		}
	}

	function save(string $filename, int $image_type = IMAGETYPE_JPEG, int $compression = 75, ?string $permissions = null) {

		if ($image_type == IMAGETYPE_JPEG) {
			imagejpeg($this->image, $filename, $compression);
		} elseif ($image_type == IMAGETYPE_GIF) {

			imagegif($this->image, $filename);
		} elseif ($image_type == IMAGETYPE_PNG) {

			imagepng($this->image, $filename);
		}
		if ($permissions != null) {

			chmod($filename, $permissions);
		}
	}

	function output(int $image_type = IMAGETYPE_JPEG) {

		if ($image_type == IMAGETYPE_JPEG) {
			imagejpeg($this->image);
		} elseif ($image_type == IMAGETYPE_GIF) {

			imagegif($this->image);
		} elseif ($image_type == IMAGETYPE_PNG) {

			imagepng($this->image);
		}
	}

	function getWidth(): int {

		return imagesx($this->image);
	}

	function getHeight(): int {

		return imagesy($this->image);
	}

	function resizeToHeight(int $height) {

		$ratio = $height / $this->getHeight();
		$width = $this->getWidth() * $ratio;
		$this->resize($width, $height);
	}

	function resizeToWidth(int $width) {
		$ratio = $width / $this->getWidth();
		$height = $this->getheight() * $ratio;
		$this->resize($width, $height);
	}

	function scale(int $scale) {
		$width = $this->getWidth() * $scale / 100;
		$height = $this->getheight() * $scale / 100;
		$this->resize($width, $height);
	}

	function resize(int $width, int $height) {
		$new_image = imagecreatetruecolor($width, $height);
		imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
		$this->image = $new_image;
	}

}
