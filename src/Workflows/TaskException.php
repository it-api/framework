<?php

/*
  TaskException.php
  UTF-8
  9-mei-2019 16:33:56
  backend_vastelastenbond
  Creator Frank
 */

namespace Barebone\Workflows;

/**
 * Description of TaskException
 *
 * @author Frank
 */
class TaskException extends \Exception{
	
	function __construct(string $message = "", int $code = 0, \Throwable $previous = null) {
		parent::__construct($message, $code, $previous);
	}
}
