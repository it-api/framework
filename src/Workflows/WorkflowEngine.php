<?php

/*
  WorkflowEngine.php
  UTF-8
  20-apr-2019 11:13:19
  backend_vastelastenbond
  Creator Frank
 */

namespace Barebone\Workflows;

/**
 * A WorkflowEngine is a collection of workflows
 *
 * @author Frank
 */
class WorkflowEngine implements Interfaces\IWorkflowEngine {
	
	/**
	 * Workflows that this WorkflowEngine will run
	 * @var array of Barebone\Workflows\Workflow
	 */
	private $workflows = [];
	
	public function getWorkflows() : array{
		
		return $this->workflows;
		
	}

	public function addWorkflowObject(Interfaces\IWorkflow $workflow){
		
		if($workflow->name === ''){
			$workflow->name = \uniqid('WORKFLOW_');
		}
		
		if(isset($this->workflows[$workflow->name])){
			throw new \Exception(sprintf('Workflowname "%s" allready exists.', $workflow->name));
		}
		
		$this->workflows[$workflow->name] = $workflow;
		return $this;
		
	}
	
	public function removeWorkflowObject( Interfaces\IWorkflow $workflow ){
		
		foreach($this->getWorkflows() as $key => $value){
			if($key === $workflow->name && $value instanceof $workflow){
				unset($this->workflows[$key]);
			}
		}
		return $this;
		
	}

	public function run() {
		
		foreach($this->getWorkflows() as $workflow){
			/*@var $workflow WorkflowAbstract*/
			$workflow->run();
			
		}
		
	}

}
