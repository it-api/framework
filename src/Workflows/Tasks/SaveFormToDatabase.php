<?php

/*
  SaveFormToDatabase.php
  UTF-8
  20-apr-2019 11:38:11
  backend_vastelastenbond
  Creator Frank
 */

namespace Barebone\Workflows\Tasks;
use Barebone\Workflows\TaskAbstract;
/**
 * Description of SaveFormToDatabase
 *
 * @author Frank
 */
class SaveFormToDatabase extends TaskAbstract{

	public function execute() {

		// try send action
		try {
			
			echo 'Sending form to database...';


		} catch (\Throwable $throwable){

			$this->hasError(true);
			throw new \Barebone\Workflows\TaskException('Task failed', 0, $throwable);

		} catch (\Exception $exc) {

			$this->hasError(true);
			throw new \Barebone\Workflows\TaskException('Task failed');

		}
		
	}	

}
