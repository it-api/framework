<?php

/*
  Task.php
  UTF-8
  20-apr-2019 11:06:58
  backend_vastelastenbond
  Creator Frank
 */

namespace Barebone\Workflows;

/**
 * Description of Task
 *
 * @author Frank
 */
abstract class TaskContext implements \Barebone\Workflows\Interfaces\ITaskContext{
    
    private $data = [];
    
    function getData() : array{
        return $this->data;
    }
    
    function setData( array $data ){
        $this->data = $data;
        return $this;
    }
}
