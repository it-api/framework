<?php

/*
  ITask.php
  UTF-8
  20-apr-2019 11:06:18
  backend_vastelastenbond
  Creator Frank
 */

namespace Barebone\Workflows\Interfaces;

/**
 *
 * @author Frank
 */
interface ITaskContext {
	
	public function getData() : array;
	public function setData( array $data );
	
}
