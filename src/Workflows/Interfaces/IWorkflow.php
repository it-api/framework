<?php

/*
  IWorkflow.php
  UTF-8
  20-apr-2019 11:05:44
  backend_vastelastenbond
  Creator Frank
 */

namespace Barebone\Workflows\Interfaces;

/**
 *
 * @author Frank
 */
interface IWorkflow {
	
	public function addTask( ITask $task );
	public function removeTask( ITask $task );
	public function run();
	public function isStarted();
	public function isRunning();
	public function isFinished();
	
}
