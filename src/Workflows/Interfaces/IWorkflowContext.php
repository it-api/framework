<?php

/*
  ITask.php
  UTF-8
  20-apr-2019 11:06:18
  backend_vastelastenbond
  Creator Frank
 */

namespace Barebone\Workflows\Interfaces;

/**
 *
 * @author Frank
 */
interface IWorkflowContext {
	
	public function getData();
	public function setData();
	
}
