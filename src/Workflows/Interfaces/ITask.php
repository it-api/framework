<?php

/*
  ITask.php
  UTF-8
  20-apr-2019 11:06:18
  backend_vastelastenbond
  Creator Frank
 */

namespace Barebone\Workflows\Interfaces;

/**
 *
 * @author Frank
 */
interface ITask {
	
	public function __construct(ITaskContext $context);
	public function execute();
	public function isStarted();
	public function isRunning();
	public function isFinished();
	public function getName();
    public function setName($name);
}
