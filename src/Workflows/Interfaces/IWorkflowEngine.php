<?php

/*
  IWorkflowEngine.php
  UTF-8
  20-apr-2019 11:06:31
  backend_vastelastenbond
  Creator Frank
 */

namespace Barebone\Workflows\Interfaces;

/**
 *
 * @author Frank
 */
interface IWorkflowEngine {
	
	public function addWorkflowObject( IWorkflow $workflow );
	public function removeWorkflowObject( IWorkflow $workflow );
	public function run();
	
}