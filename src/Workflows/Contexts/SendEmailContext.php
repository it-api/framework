<?php

/*
  SendEmailContext.php
  UTF-8
  9 dec. 2019 22:36:46
  backend_vastelastenbond
  Creator Frank
 */
namespace Barebone\Workflows\Contexts;
use Barebone\Workflows\TaskContext;
/**
 * Description of SendEmailContext
 *
 * @author Frank
 */
class SendEmailContext extends TaskContext{
	
	public $FromName = '';
	public $FromEmail = '';
	public $ToName = '';
	public $ToEmail = '';
	public $Subject = '';
	public $Body;
	public $Attachements = [];
	// no cc or bcc
	// then create a subtask
}
