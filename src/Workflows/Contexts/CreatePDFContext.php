<?php

/*
  CreatePDFContext.php
  UTF-8
  9 dec. 2019 22:50:03
  backend_vastelastenbond
  Creator Frank
 */

namespace Barebone\Workflows\Contexts;
use Barebone\Workflows\TaskContext;
/**
 * Description of CreatePDFContext
 *
 * @author Frank
 */
class CreatePDFContext extends TaskContext {
	
	public $input;
	public $output;
    
    function __construct(string $input, string $output) {
        $this->input = $input;
        $this->output = $output;
    }

    function getInput() : string {
        return $this->input;
    }

    function getOutput(): string {
        return $this->output;
    }

}
