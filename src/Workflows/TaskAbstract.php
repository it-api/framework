<?php

/*
  Task.php
  UTF-8
  20-apr-2019 11:06:58
  backend_vastelastenbond
  Creator Frank
 */

namespace Barebone\Workflows;

/**
 * Description of Task
 *
 * @author Frank
 */
abstract class TaskAbstract implements \Barebone\Workflows\Interfaces\ITask{
	
	public $name = null;
	public $isStarted = false;
	public $isFinished = false;
	public $isRunning = false;
	public $hasError = false;
	public $context = null;
	/**
	 * 
	 * @param \Barebone\Workflows\Interfaces\ITaskContext $context
	 */
	public function __construct(\Barebone\Workflows\Interfaces\ITaskContext $context) {
		$this->setContext( $context );
		$this->name = uniqid('TASK');
	}
	
	public function getContext() {
		return $this->context;
	}

	public function setContext(\Barebone\Workflows\Interfaces\ITaskContext $context) {
		$this->context = $context;
		return $this;
	}
	
	public function isStarted($isStarted = null){
		if(is_null($isStarted)){
			return $this->isStarted;
		}else{
			$this->isStarted = $isStarted;
			$this->isRunning( $this->isStarted );
		}
	}
	public function isFinished($isFinished = null){
		if(is_null($isFinished)){
			return $this->isFinished;
		}else{
			$this->isFinished = $isFinished;
			$this->isStarted( false );
		}
	}

	public function isRunning($isRunning = null) {
		if(is_null($isRunning)){
			return $this->isRunning;
		}else{
			$this->isRunning = $isRunning;
		}
	}
	
	public function hasError($hasError = null) {
		if(is_null($hasError)){
			return $this->hasError;
		}else{
			$this->hasError = $hasError;
		}
	}
    
    public function getName(){
        return $this->name;
    }
    
    public function setName($name){
        $this->name = $name;
        return $this;
    }
	
	function toJSON($prettyprint = false){
		if($prettyprint){
			return '<pre>'.\json_encode($this, JSON_PRETTY_PRINT).'</pre>';
		} else {
			return json_encode($this);
		}
	}
}
