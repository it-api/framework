<?php

/*
  Workflow.php
  UTF-8
  20-apr-2019 11:10:15
  backend_vastelastenbond
  Creator Frank
 */

namespace Barebone\Workflows;

/**
 * A workflow is a collection of tasks
 *
 * @author Frank
 */
class WorkflowAbstract implements Interfaces\IWorkflow{
	
	public $name = '';
	public $isStarted = false;
	public $isFinished = false;
	public $isRunning = false;
	public $hasError = false;
	public $context = null;
	private $last_error = null;
	private $last_finished_task = null;	
	
	/**
	 * Tasks that this workflow will execute
	 * @var array of \Barebone\Workflows\TaskAbstract
	 */
	public $tasks = [];	
	
	public function getTasks() {		
		return $this->tasks;		
	}
	
	public function hasTasks(){
	    return count($this->tasks) > 0;
	}
	
	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
		return $this;
	}
		
	public function addTask( Interfaces\ITask $task ){		
		$this->tasks[$task->getName()] = $task;
		return $this;		
	}
	
	public function getTask( Interfaces\ITask $task ){
		return $this->tasks[$task->getName()];
	}
	
	public function removeTask( Interfaces\ITask $task ){
		unset($this->tasks[$task->getName()]);
		return $this;		
	}
	
	public function run() {
		
		if($this->hasTasks()){	
		    
		    // set the workflow to started
		    $this->isStarted(true);
		    
    		foreach($this->getTasks() as $task){
    			
    			/*@var $task TaskAbstract*/
    			try {
    				// prevent next task from executing if there is an error in the last task
    				if(!is_null($this->last_error)){
    					
    					echo 'Last task has an error cannot continue workflow';
    					
    				}elseif($task->isFinished() === false && $task->hasError() === false){
    					
    					// set the task to started
    					$task->isStarted(true);
    					// execute the task
    					$task->execute();
    					// action is finished
    					$task->isFinished(true);
    					
    					$this->last_finished_task = $task;
    					
    				} else {
    					
    					echo 'Task has finished allready';
    				}
    				
    			} catch (TaskException $exc) {
    				
    				// Loggin
    				// Terminate and persist state of the workflow
    				$this->last_error = $exc;
    				$this->hasError(true);
    				
    				return false;
    			}			
    			
    		}
    		
    		$this->isFinished(true);
    		
		} else {
		    
		    echo 'No tasks to prcocess';
		}
		
	}
	
	public function getContext() {
		return $this->context;
	}

	public function setContext(Interfaces\IWorkflowContext $context) {
		$this->context = $context;
		return $this;
	}
	
	public function isStarted($isStarted = null){
		if(is_null($isStarted)){
			return $this->isStarted;
		}else{
			$this->isStarted = $isStarted;
			$this->isRunning( $this->isStarted );
		}
	}
	public function isFinished($isFinished = null){
		if(is_null($isFinished)){
			return $this->isFinished;
		}else{
			$this->isFinished = $isFinished;
			$this->isStarted( false );
		}
	}

	public function isRunning($isRunning = null) {
		if(is_null($isRunning)){
			return $this->isRunning;
		}else{
			$this->isRunning = $isRunning;
		}
	}
	
	public function hasError($hasError = null) {
		if(is_null($hasError)){
			return $this->hasError;
		}else{
			$this->hasError = $hasError;
			$this->isRunning( $this->hasError === false );
		}
	}
	
	function toJSON($prettyprint = false){
		if($prettyprint){
			return '<pre>'.\json_encode($this, JSON_PRETTY_PRINT).'</pre>';
		} else {
			return json_encode($this);
		}
	}

}
