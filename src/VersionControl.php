<?php

namespace Barebone;

class VersionControl {
    
	/**
	 * The path where to store the versions of the files
	 */
	private string $save_path = '';
	
	/**
	 * 
	 * Constructor
	 * @var $save_path path to folder to save versioned file
	 */
	public function __construct( string $save_path ){
	    $this->save_path = $save_path;
	    if( ! is_dir( $this->save_path ) ){
	        mkdir( $this->save_path );
	    }
	}
	
	private function isEqualContents( string $current_version, string $new_version ){
	    return $current_version === $new_version;
	}
	
	/**
	 * 
	 * @var $filename the path of the file to version
	 */
	public function save( string $filename, string $code = '' ){
	    
	    $isDisabled = Config()->getValue('VCS', 'enabled') === false || Config()->getValue('VCS', 'enabled') === 'false';
	    if($isDisabled){
	        return;
	    }
	    $maxitems = (int) Config()->getValue('VCS', 'maxitems');
	    // name of the json file
	    $vcs_filename = $this->save_path . sha1( $filename ) . '.json';
	    // create data array
	    $data = [];
	    $data['filename'] = $filename;
	    if( file_exists($vcs_filename) ){
	        $json = file_get_contents($vcs_filename);
	        $data['versions'] = json_decode($json)->versions;
	    } else {
	        $data['versions'] = [];
	    }
	    
	    if(count($data['versions']) > 0 && $this->isEqualContents($data['versions'][count($data['versions'])-1]->contents, base64_encode($code))){
	        return;
	    }
	    
	    $vc = [];
	    $vc['timestamp'] = date('d-m-Y H:i:s');
	    $vc['contents'] = base64_encode(file_get_contents($filename));
	    $data['versions'][] = (object) $vc;
	    
        if (sizeof( $data['versions'] ) >= $maxitems+1) {
            array_shift($data['versions']);
        }
        return file_put_contents( $vcs_filename, json_encode($data, JSON_PRETTY_PRINT) );
	    
	}
}
