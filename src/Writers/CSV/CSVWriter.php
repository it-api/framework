<?php

/*
  Creator Frank
 */

namespace Barebone\Writers\CSV;

/**
 * Description of CSVWriter
 *
 * @author Frank
 */
class CSVWriter {
	/**
	 *
	 * @var \SplFileObject
	 */
	private $stream;
	private $delimiter = ',';
	private $enclosure = '"';
	private $escape = '\\';
	/**
	 * 
	 * @param string $filename the name of the csv file
	 * @param string $delimiter default , [,|;tab]
	 * @param string $enclosure '"'
	 * @param string $escape '\\'
	 */
	public function __construct($filename, $delimiter = ',', $enclosure = '"', $escape = '\\') {
		$this->delimiter = $delimiter;
		$this->enclosure = $enclosure;
		$this->escape = $escape;
		$this->stream = new \SplFileObject($filename, 'w');
	}
	/**
	 * Write an item to the csv
	 * @param array $fields array of fields
	 * @return $this
	 */
	function writeItem($fields = []){
		$this->stream->fputcsv($fields, $this->delimiter, $this->enclosure, $this->escape);
		return $this;
	}
	/**
	 * write an array of fields
	 * @param array $array
	 */
	function writeItems($array = []){
		foreach($array as $fields){
			$this->writeItem($fields);
		}
	}


}
