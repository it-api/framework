<?php

namespace Barebone\Assets\Types;

/**
 * Barebone Stylesheet Asset
 * @author Frank
 *
 */
class Stylesheet {

    /**
     * The stylesheet href attribute
     * @var string 
     */
    public string $href;
    public string $nonce = '';

    /**
     * The constructor for the stylesheet
     * @param string $href
     */
    function __construct(string $href, $nonce = '') {

        $this->href = $href;
        $this->nonce = $nonce;
    }

    function render() {
        ?>
    <link rel="stylesheet" type="text/css" href="<?= $this->href; ?>" nonce="<?=$this->nonce;?>">
<?php
    }

}
