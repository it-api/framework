<?php

namespace Barebone\Assets\Types;

/**
 * Barebone Javascript Asset
 * @author Frank
 * 
 */
class Javascript {
    
    const DEFER = true;
    const ASYNC = true;
    /**
     * The src attribute of the javascript
     * @var type 
     */
    public string $src;
    public bool $defer = false;
    public bool $async = false;
    public string $nonce = '';
    /**
     * Constructor
     * @param string $src
     */
    function __construct(string $src, bool $defer = false, bool $async = false, string $nonce = '') {
        
        $this->src = $src;
        $this->defer = $defer;
        $this->async = $async;
        $this->nonce = $nonce;
        
    }
    
    /**
     * Renders the javascript tag
     */
    function render() {
        ?>
    <script<?=$this->defer === true?' defer="true"':($this->async === true?' async="true"':'');?> type="text/javascript" src="<?=$this->src;?>" nonce="<?=$this->nonce;?>"></script>
<?php
    }

}
