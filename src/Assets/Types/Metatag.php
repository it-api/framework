<?php

namespace Barebone\Assets\Types;

/**
 * Barebone Metatag Asset
 * @author Frank
 *
 */
class Metatag {

    /**
     * The metatags attributes
     * @var array 
     */
    public array $attributes = [];

    /**
     * The metatag constructor
     * @param array $attributes
     */
    function __construct(array $attributes = []) {
        $this->attributes = $attributes;
    }

    /**
     * Renders the metatag
     */
    function render() {
        ?>
    <meta<?php foreach ($this->attributes as $attribute => $value) {
            echo " " . $attribute . '="' . $value . '"';
        } ?>>
<?php
    }

}
