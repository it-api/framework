<?php

namespace Barebone\Assets\Types;

class Link{
    
    /**
     * The link tag attributes
     * @var array 
     */
    public array $attributes = [];

    /**
     * The link tag constructor
     * @param array $attributes
     */
    function __construct(array $attributes = []) {
        $this->attributes = $attributes;
    }

    /**
     * Renders the link tag
     */
    function render() {
        ?>
    <link<?php foreach ($this->attributes as $attribute => $value) {
            echo " " . $attribute . '="' . $value . '"';
        } ?>>
<?php
    }
}
