<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Button
 *
 * @author Frank
 */
class Image extends Element{
	/**
	 * 
	 * @param string $src
	 * @param array $attributes
	 */
	public function __construct($src, $attributes = []) {
		parent::__construct('img', $attributes);
		$this->src = $src;
	}
	/**
	 * Make image responsive
	 * @return $this
	 */
	function responsive(){
		$this->add_class('img-responsive');
		return $this;
	}
	/**
	 * Make image rounded
	 * @return $this
	 */
	function rounded(){
		$this->add_class('img-rounded');
		return $this;
	}
	/**
	 * Make image a circle
	 * @return $this
	 */
	function circle(){
		$this->add_class('img-circle');
		return $this;
	}
	/**
	 * Make image a thumbnail
	 * @return $this
	 */
	function thumbnail(){
		$this->add_class('img-thumbnail');
		return $this;
	}

}