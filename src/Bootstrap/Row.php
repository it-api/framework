<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;
/**
 * Description of Row
 *
 * @author Frank
 */
class Row extends Element{
	
	private $columns = [];
	
	function __construct() {
		parent::__construct('div', ['class' => 'row']);
	}
	/**
	 * 
	 * @param type $text
	 * @param type $attributes
	 * @return $this
	 */
	function addColumn($text, $attributes = []){
		
		$column = new Column($text, $attributes);
		$this->columns[] = $column;
		$this->append($column);
		
		foreach($this->columns as $column){
			if(!isset($attributes['class'])){
				$available_columns = 12;
				$col = $available_columns / count($this->columns);
				$column->class = ("col-xs-12 col-md-$col col-lg-$col");
			}			
		}
			
		return $this;
	}
}
