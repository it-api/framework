<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Column
 *
 * @author Frank
 */
class Column extends Element{
	
	private $rows = [];
	/**
	 * 
	 * @param type $text
	 * @param type $attributes
	 */
	function __construct($text, $attributes = []) {
		parent::__construct('div', $attributes);
		$this->innertext($text);
	}

}
