<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Breadcrumbs
 *
 * @author Frank
 */
class Breadcrumbs  extends Element{
	
	/**
	 * Breadcrumbs Constructor
	 * @param array $crumbs array of crumbs [caption=>'#']
	 */
	public function __construct($crumbs = []) {
		parent::__construct('ul');
		$this->add_class('breadcrumb');
		if(!empty($crumbs)){
			foreach($crumbs as $caption => $href){
				$this->addCrumb($caption, $href);
			}
		}
	}
	/**
	 * Adds a crumb to the breadcrumbs
	 * @param string $caption
	 * @param string $href
	 */
	function addCrumb($caption, $href){
		$this->append(new Breadcrumb($caption, $href));
	}
}
