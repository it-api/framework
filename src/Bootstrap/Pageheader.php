<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Pageheader
 *
 * @author Frank
 */
class Pageheader extends Element{
	
	public function __construct($text, $element = 'h1') {
		parent::__construct('div');
		$h1 = new Element($element);
		$h1->innertext($text);
		$this->add_class('page-header');
		$this->append($h1);
	}

}
