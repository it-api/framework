<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Badge
 *
 * @author Frank
 */
class Badge extends Element{
	/**
	 * 
	 * @param string $text text on the badge
	 */
	public function __construct($text) {
		parent::__construct('span');
		$this->add_class('badge');
		$this->innertext($text);
	}

}
