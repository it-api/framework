<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;
use Barebone\HTML\TTable;
/**
 * Description of Table
 *
 * @author Frank
 */
class Table extends TTable{
	
	public function __construct($attributes = []) {
		parent::__construct($attributes);
		$this->class = 'table';
	}
	
	function stripped(){
		$this->add_class('table-stripped');
		return $this;
	}
	function condensed(){
		$this->add_class('table-condensed');
		return $this;
	}
	function bordered(){
		$this->add_class('table-bordered');
		return $this;
	}
	function hover(){
		$this->add_class('table-hover');
		return $this;
	}
}
