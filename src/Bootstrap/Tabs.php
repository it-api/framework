<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Tabs
 * 
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#home" data-toggle="tab">Home</a></li>
  <li><a href="#profile" data-toggle="tab">Profile</a></li>
  <li><a href="#messages" data-toggle="tab">Messages</a></li>
  <li><a href="#settings" data-toggle="tab">Settings</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="home">...</div>
  <div class="tab-pane" id="profile">...</div>
  <div class="tab-pane" id="messages">...</div>
  <div class="tab-pane" id="settings">...</div>
</div>

 *
 * @author Frank
 */
class Tabs extends Element{
	/**
	 *
	 * @var array of Tab
	 */
	private $tabs = [];
	private $panes = [];
	
	public function __construct() {
		// we create a wrapping element for the tabs
		parent::__construct('div');
		$this->class = 'wrapper-nav-tabs';
		$this->tabs = new Element('ul');
		$this->tabs->class = 'nav nav-tabs';
		$this->panes = new Element('div');
		$this->panes->class = 'tab-content';
		$this->append($this->tabs);
		$this->append($this->panes);
	}
	/**
	 * Add a tab to the tabs widget
	 * @param string $caption
	 * @param string $href
	 * @param bool $active
	 * @param string $content
	 * @return \Barebone\Bootstrap\Element the pane of the tab
	 */
	function addTab(string $caption = 'Untitled', string $href = null, bool $active = false, string $content = ''){
		// the tab
		$tab = new Element('li');	
		if($active === true){
			$tab->class = 'active';
		}
		// the link on the tab
		$a = new Element('a');
		$a->innertext($caption);
		$a->href = $href;
		$a->data('toggle', 'tab');
		// append the link to the tab
		$tab->append($a);
		// append the tab to the tabs array
		$this->tabs->append($tab);
		// create the tab pane
		$pane = new Element('div');
		$pane->class = 'tab-pane';
		$pane->id = substr($href, 1);
		$pane->innertext($content);
		// if active add class active to pane
		if($active === true){
			$pane->add_class( 'active' );
		}	
		// append the pane to the panes array
		$this->panes->append($pane);
		// return the pane
		return $pane;
	}
	
	function setContent(){}

}
