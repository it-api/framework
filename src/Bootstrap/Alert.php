<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Alert
 *
 * @author Frank
 */
class Alert extends Element{
	
	/**
	 * Constructor of the alert message
	 * @param string $message
	 * @param string $type [info,success,waring,danger]
	 */
	public function __construct($message = '', $type = 'info', $dismissable = false) {
		parent::__construct('div');
		if($message !== ''){
			
			if($dismissable === true){
				$this->dismissable();
			}
			$this->innertext($message);
			$this->class = "alert alert-$type";
		}
	}
	
	function dismissable(){
		//<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		$btn = new Element('button');
		$btn->class = 'close';
		$btn->data('dismiss', 'alert');
		$btn->{'aria-hidden'} = 'true';
		$btn->innertext('&times;');
		$this->append($btn);
		$this->add_class('alert-dismissable');
		return $this;
	}
}
