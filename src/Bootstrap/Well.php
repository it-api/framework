<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Well
 *
 * @author Frank
 */
class Well extends Element{
	
	public function __construct($content) {
		parent::__construct('div');
		$this->add_class('well');
		$this->innertext($content);
	}
	
	function large(){
		$this->add_class('well-lg');
	}
	
	function small(){
		$this->add_class('well-sm');
	}

}
