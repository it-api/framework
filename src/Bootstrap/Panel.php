<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;
/**
 * Description of Panel
 *
 * @author Frank
 */
class Panel extends Element{
	
	/**
	 *
	 * @var Barebone\Bootstrap\Element 
	 */
	private $header = null;
	/**
	 *
	 * @var Barebone\Bootstrap\Element 
	 */
	private $content = null;
	/**
	 *
	 * @var Barebone\Bootstrap\Element 
	 */
	private $footer = null;
	/**
	 * 
	 * @param string $contextual [info,succes,warning,danger]
	 */
	public function __construct($contextual = 'default') {
		parent::__construct('div');
		$this->add_class("panel panel-$contextual");
	}
	/**
	 * Set the header for the panel
	 * @param string $text
	 */
	function setHeader($text){
		$this->header = new Element('div');
		$this->header->add_class('panel-heading');
		$this->header->innertext($text);
		$this->append($this->header);
		return $this;
	}
	/**
	 * Set the content for the panel
	 * @param string $text
	 */
	function setContent($text){
		$this->content = new Element('div');
		$this->content->add_class('panel-body');
		$this->content->innertext($text);
		$this->append($this->content);
		return $this;
	}
	/**
	 * Set the footer for the panel
	 * @param string $text
	 */
	function setFooter($text){
		$this->footer = new Element('div');
		$this->footer->add_class('panel-footer');
		$this->footer->innertext($text);
		$this->append($this->footer);
		return $this;
	}
	
	function getHeader(): Element {
		return $this->header;
	}

	function getContent(): Element {
		return $this->content;
	}

	function getFooter(): Element {
		return $this->footer;
	}



}
