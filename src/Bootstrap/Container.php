<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Container
 *
 * @author Frank
 */
class Container extends Element{
	
	/**
	 * Container contructor
	 * @param bool $fluid
	 * @param array $attributes
	 */
	public function __construct($fluid = false, $attributes = []) {
		
		parent::__construct('div', $attributes);
		if($fluid === true){
			$this->add_class('container-fluid');
		}else{
			$this->add_class('container');
		}
	}	
	/**
	 * Make grid fluid
	 * @return $this
	 */
	function fluid(){
		$this->class = 'container-fluid';
		return $this;
	}
	/**
	 * Add a row to the grid
	 * @return \Barebone\Bootstrap\Row
	 */
	function addRow(){
		
		$row = new Row();
		$this->rows[] = $row;
		$this->append($row);
		return $row;
		
	}
	/**
	 * 
	 * @param array $array
	 */
	function appendArrayOfElements($array){
		foreach($array as $element){
			$this->append($element);
		}
	}

}
