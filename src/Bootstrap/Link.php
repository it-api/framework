<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Link
 *
 * @author Frank
 */
class Link extends Element{
	
	public function __construct($caption, $href, $attributes = []) {
		parent::__construct('a', $attributes);
		$this->innertext($caption);
		$this->href = $href;
	}
	
	function addBadge($text){
		$this->append(new Badge($text));
		return $this;
	}
}
