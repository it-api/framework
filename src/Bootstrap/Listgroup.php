<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Listgroup
 *
 * @author Frank
 */
class Listgroup extends Element{
	/**
	 * Listgroup constructor
	 */
	public function __construct() {
		parent::__construct('ul');
		$this->add_class('list-group');
	}
	
	/**
	 * 
	 * @param string $caption text on the list-item
	 * @param string $bagde bagde text
	 * @param string $href link
	 * @param string $contextual [null,default,primary,info,success,warning,danger]
	 * @return $this
	 */
	function addItem($caption, $bagde = null, $href = null, $contextual = null){
		
		// if a link is given in
		if(!is_null($href)){
			
			// transform the ul to div
			$this->tag = 'div';
			// create the link
			$listitem = new Element('a');
			$listitem->class = 'list-group-item';
			$listitem->innertext($caption);
			$listitem->href = $href;
			
		}else{
			
			// no link so we use standard li element
			$listitem = new Element('li');
			$listitem->class = 'list-group-item';
			$listitem->innertext($caption);
		}
		
		// set the contextual color
		if(!is_null($contextual)){
			$listitem->add_class("list-group-item-$contextual");
		}
		
		// add a badge to the list-group-item
		if(!is_null($bagde)){
			$b = new Badge($bagde);
			$listitem->append($b);
		}		
		// add the listitem to the parent
		$this->append($listitem);
		return $this;
	}

}
