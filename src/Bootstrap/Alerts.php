<?php

namespace Barebone\Bootstrap;

/**
 * Description of Alerts
 * 
    <div class="alert alert-success">
      <strong>Well done!</strong> You successfully read this important alert message.
    </div>
    <div class="alert alert-info">
      <strong>Heads up!</strong> This alert needs your attention, but it's not super important.
    </div>
    <div class="alert alert-warning">
      <strong>Warning!</strong> Better check yourself, you're not looking too good.
    </div>
    <div class="alert alert-danger">
      <strong>Oh snap!</strong> Change a few things up and try submitting again.
    </div>
 *
 * @author Frank
 */
class Alerts extends Element {

	/**
	 * Constructor of the alert message
	 * @param string $message
	 * @param string $type [info,success,waring,danger]
	 */
	public function __construct(string $message = '', string $type = 'info', bool $dismissable = false) {
		parent::__construct('div');
		if ($message !== '') {
			$this->append(new Alert($message, $type, $dismissable));
		}
	}
	/**
	 * Echo's out the alert message
	 * @param string $message
	 * @param string $type [info,success,waring,danger]
	 */
	function alert(string $message = '', string $type = 'info', bool $dismissable = false) {
		echo new Alert($message, $type, $dismissable);
		return $this;
	}
}
