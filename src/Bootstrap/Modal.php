<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Modal
	<div class="modal fade">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Modal title</h4>
		  </div>
		  <div class="modal-body">
			<p>One fine body&hellip;</p>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary">Save changes</button>
		  </div>
		</div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

 * @author Frank
 */
class Modal extends Element{
	/**
	 *
	 * @var string the id of the dialog
	 */
	private $_id;
	/**
	 *
	 * @var Element
	 */
	private $dialog;
	/**
	 *
	 * @var Element
	 */
	private $content;
	/**
	 *
	 * @var Element
	 */
	private $header;
	/**
	 *
	 * @var Element 
	 */
	private $title;
	/**
	 *
	 * @var Element
	 */
	private $body;
	/**
	 *
	 * @var Element
	 */
	private $footer;
	
	public function __construct($title = '', $content = '', $id = null) {
		
		parent::__construct('div');
		// if no id is given then create a unique one, else use the id passed into the function
		if(is_null($id)){
			$this->_id = uniqid('Modal');
		}else{
			$this->_id = $id;
		}
		// the actual modal <div id="$id">
		$this->id = $this->_id;
		$this->class = 'modal fade';
		$this->role = 'dialog';
		$this->tabindex = '-1';
		$this->aria('labelledby', "{$this->_id}Label"); // aria-labelledby="myModalLabel"
		$this->aria('hidden', 'true');
		// dialog div
		$this->dialog = new Element('div');
		$this->dialog->class = 'modal-dialog';
			// content div
			$this->content = new Element('div');
			$this->content->class = 'modal-content';
				// header div
				$this->header = new Element('div');
				$this->header->class = 'modal-header';
				// <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				$button = new Element('button');
				$button->type = 'button';
				$button->class = 'close';
				$button->data('dismiss', 'modal');
				$button->aria('hidden', 'true');
				$button->innertext('&times;');
				// title
				$this->title = new Element('h4');
				$this->title->class = 'modal-title';
				$this->title->innertext($title);
				$this->title->id = "{$this->_id}Label"; // aria-labelledby="myModalLabel"
				// body div
				$this->body = new Element('div');
				$this->body->class = 'modal-body';
				$this->body->innertext($content);
				// footer div
				$this->footer = new Element('div');
				$this->footer->class = 'modal-footer';
		
		// putting the modal togheter
		$this->header->append($button);
		$this->header->append($this->title);
		$this->content->append($this->header);
		$this->content->append($this->body);
		$this->content->append($this->footer);
		$this->dialog->append($this->content);
		$this->append($this->dialog);
	}
	/**
	 * Includes a modal-backdrop element. 
	 * Alternatively, specify static for a backdrop which doesn't close the modal on click.
	 * @param string $backdrop backdrop [true, false, static]
	 */
	function backdrop($backdrop = 'true'){
		$this->data('backdrop', $backdrop);
	}
	
	function closeOnEscape($keyboard){
		$this->data('keyboard', $keyboard === true ? 'true' : 'false');
	}
	
	/**
	 * Shows the modal inline in the page
	 */
	function debug(){
		$this->class = '';
	}
	
	/**
	 * Cancel dialog button dismisses the modal when clicked
	 * @param string $caption text on the button
	 * @param array $attributes attributes for the button
	 */
	function addCancelbutton($caption, $attributes = []){
		if(!isset($attributes['class'])){
			$attributes['class'] = 'btn btn-default';
		}
		$btn = new Element('button', $attributes);
		$btn->type = 'button';
		$btn->data('dismiss', 'modal');
		$btn->innertext($caption);
		
		$this->footer->append($btn);
		return $this;
	}
	/**
	 * 
	 * @param string $caption text on the button
	 * @param string $callback
	 * @param array $attributes
	 * @param array $data
	 */
	function addButton($caption, $callback = null, $attributes = [], $data = []){
		// <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		// add default class if no class is set
		if(!isset($attributes['class'])){
			$attributes['class'] = 'btn btn-default';
		}
		// create the button element
		$btn = new Element('button', $attributes);
		$btn->type = 'button';
		$btn->add_class('btn');
		// append all data from the array to the button
		foreach($data as $dkey=>$dval){
			$btn->data($dkey, $dval);
		}
		// set callback
		if(!is_null($callback)){
			$btn->on('click', $callback);
		}
		// set text button
		$btn->innertext($caption);
		// add the button to the footer
		$this->footer->append($btn);
		return $btn;
	}
	
	function large(){
		$this->dialog->add_class('modal-lg');
	}
	function small(){
		$this->dialog->add_class('modal-sm');
	}
	/**
	 * returns the dialog part of the modal
	 * @return \Barebone\Bootstrap\Element
	 */
	public function getDialog(): Element {
		return $this->dialog;
	}
	/**
	 * returns the content part of the modal
	 * @return \Barebone\Bootstrap\Element
	 */
	public function getContent(): Element {
		return $this->content;
	}
	/**
	 * returns the header part of the modal
	 * @return \Barebone\Bootstrap\Element
	 */
	public function getHeader(): Element {
		return $this->header;
	}
	/**
	 * returns the title part of the modal
	 * @return \Barebone\Bootstrap\Element
	 */
	public function getTitle(): Element {
		return $this->title;
	}
	/**
	 * returns the body part of the modal
	 * @return \Barebone\Bootstrap\Element
	 */
	public function getBody(): Element {
		return $this->body;
	}
	/**
	 * returns the footer part of the modal
	 * @return \Barebone\Bootstrap\Element
	 */
	public function getFooter(): Element {
		return $this->footer;
	}
	/**
	 * Pass in your own dialog element
	 * @param \Barebone\Bootstrap\Element $dialog
	 * @return $this
	 */
	public function setDialog(Element $dialog) {
		$this->dialog = $dialog;
		return $this;
	}
	/**
	 * Set your own content element
	 * @param \Barebone\Bootstrap\Element $content
	 * @return $this
	 */
	public function setContent(Element $content) {
		$this->content = $content;
		return $this;
	}
	/**
	 * Set your own header element
	 * @param \Barebone\Bootstrap\Element $header
	 * @return $this
	 */
	public function setHeader(Element $header) {
		$this->header = $header;
		return $this;
	}
	/**
	 * Set tour own title element
	 * @param \Barebone\Bootstrap\Element $title
	 * @return $this
	 */
	public function setTitle(Element $title) {
		$this->title = $title;
		return $this;
	}
	/**
	 * Set your own body element
	 * @param \Barebone\Bootstrap\Element $body
	 * @return $this
	 */
	public function setBody(Element $body) {
		$this->body = $body;
		return $this;
	}
	/**
	 * Set your own footer element
	 * @param \Barebone\Bootstrap\Element $footer
	 * @return $this
	 */
	public function setFooter(Element $footer) {
		$this->footer = $footer;
		return $this;
	}



}
