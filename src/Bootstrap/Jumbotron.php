<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Jumbotron
 *
 * @author Frank
 */
class Jumbotron extends Element{
	
	/**
	 *
	 * @var Element
	 */
	private $h1;
	/**
	 *
	 * @var Element
	 */
	private $content;
	/**
	 * 
	 * @param \Barebone\Bootstrap\Element $h1
	 * @param \Barebone\Bootstrap\Element $content
	 */
	public function __construct($h1 = '', $content = '') {
		parent::__construct('div');
		$this->add_class('jumbotron');
		$this->setH1( $h1 );
		$this->setContent( $content );
	}
	/**
	 * 
	 * @return \Barebone\Bootstrap\Element
	 */
	public function getH1(): Element {
		return $this->h1;
	}
	/**
	 * 
	 * @return \Barebone\Bootstrap\Element
	 */
	public function getContent(): Element {
		return $this->content;
	}
	/**
	 * 
	 * @param \Barebone\Bootstrap\Element $h1
	 * @return $this
	 */
	public function setH1($h1) {
		if($h1 instanceof Element){
			$this->h1 = $h1;
		}else{
			$this->h1 = new Element('h1');
			$this->h1->innertext($h1);
		}
		$this->append($this->h1);
		return $this;
	}
	/**
	 * 
	 * @param \Barebone\Bootstrap\Element $content
	 * @return $this
	 */
	public function setContent($content) {
		if($content instanceof Element){
			$this->content = $content;
		}else{
			$this->content = new Element('div');
			$this->content->innertext($content);
		}
		$this->append($this->content);
		return $this;
	}
	
}
