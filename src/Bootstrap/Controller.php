<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Page
 *
 * @author Frank
 */
class Controller extends \Barebone\Controller{
	
	public function __construct() {
		parent::__construct();
		$this->layout->set_layout('bootstrap');
	}

}
