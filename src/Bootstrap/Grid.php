<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Grid
 *
 * @author Frank
 */
class Grid extends Element {
	/**
	 * Array that will hold the rows
	 * @var array $rows
	 */
	private $rows = [];
	/**
	 * 
	 * @param array $attributes
	 */
	function __construct(array $attributes = []) {
		if (empty($attributes)) {
			$attributes = ['class' => 'container'];
		}
		parent::__construct('div', $attributes);
	}
	/**
	 * Make grid fluid
	 * @return $this
	 */
	function fluid() {
		$this->class = 'container-fluid';
		return $this;
	}
	/**
	 * Add a row to the grid
	 * @return \Barebone\Bootstrap\Row
	 */
	function addRow() {

		$row = new Row();
		$this->rows[] = $row;
		$this->append($row);
		return $row;
	}
}
