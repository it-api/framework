<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Button
 *
 * @author Frank
 */
class Button extends Element {
	/**
	 * 
	 * @param string $caption text for button
	 * @param string $contextual [default,info,success,warning,danger]
	 */
	public function __construct(string $caption, string $contextual = 'default') {

		parent::__construct('button');
		$this->type = 'button';
		$this->innertext($caption);
		$this->add_class("btn btn-{$contextual}");
	}

	function large() {
		$this->add_class('btn-lg');
		return $this;
	}

	function small() {
		$this->add_class('btn-sm');
		return $this;
	}

	function xsmall() {
		$this->add_class('btn-xs');
		return $this;
	}
}
