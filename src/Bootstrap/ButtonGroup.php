<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;
/**
 * Description of ButtonGroup
 *
<div class="btn-group">
  <button type="button" class="btn btn-default">Left</button>
  <button type="button" class="btn btn-default">Middle</button>
  <button type="button" class="btn btn-default">Right</button>
</div>

 * @author Frank
 */
class ButtonGroup extends Element{
	
	/**
	 * All the Buttongroup items
	 * @var array
	 */
	private $items = [];
	
	public function __construct(Element $element = null) {
		parent::__construct('div');
		$this->add_class('btn-group');
		if(!is_null($element)){
			$this->addItem($element);
		}
	}
	
	function addItem(Element $element){
		if($element instanceof Dropdown){
			$this->append($element->asBtnGroup());
		}else{
			$this->append($element);
		}
	}
	
	function addButton($caption, $attributes = []){
		$btn = new Element('button', $attributes);
		$btn->innertext($caption);
		$btn->type = 'button';
		if(!isset($attributes['class'])){
			$btn->add_class('btn btn-default');
		}else{
			$btn->add_class('btn');
		}

		$this->append($btn);
		return $this;
	}
	
	function large(){
		$this->add_class('btn-group-lg');
	}
	
	function small(){
		$this->add_class('btn-group-sm');
	}
	
	function extrasmall(){
		$this->add_class('btn-group-xs');
	}
	
	function justify(){
		$this->add_class('btn-group-justified');
		for($i = 0; $i < $this->count_children(); $i++){
			$this->replace_child($i, $this->get_child($i)->wrap('div', ['class' => 'btn-group']));
	
		}
	}
	/**
	 <div class="btn-toolbar" role="toolbar">
		<div class="btn-group">...</div>
		<div class="btn-group">...</div>
		<div class="btn-group">...</div>
	  </div>

	 */
	function toolbar(){
		$this->class = 'btn-toolbar';
		$this->role = 'toolbar';
		return $this;
	}
	
	function addGroup(){
		$buttongroup = new ButtonGroup();
		$this->append($buttongroup);
		return $buttongroup;
	}
	/**
	  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
		<span class="caret"></span>
		<span class="sr-only">Toggle Dropdown</span>
	  </button>
	 */
	function addSplitButton($caption, $attributes = []){
		
		$this->addButton($caption, $attributes);
		
		$button = new Element('button', $attributes);
		$button->type = 'button dropdown-toggle';
		if(!isset($attributes['class'])){
			$button->add_class('btn btn-default');
		}else{
			$button->add_class('btn');
			//$button->add_class($attributes['class']);
		}
		//$button->class = 'btn btn-default';
		$button->data('toggle', 'dropdown');
		
		$caret = new Element('span');
		$caret->class = 'caret';
		
		$toggle = new Element('span');
		$toggle->class = 'sr-only';
		$button->append($caret);
		$button->append($toggle);
		$this->append($button);
	}
}
