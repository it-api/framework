<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;
/**
 * Description of Progress
<div class="progress">
  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
    <span class="sr-only">60% Complete</span>
  </div>
</div>

 *
 * @author Frank
 */
class Progress extends Element{
	
	private $value = 0;
	private $valuemin = 0;
	private $valuemax = 100;
	/**
	 *
	 * @var Barebone\HTML\TElement
	 */
	private $complete;
	/**
	 * 
	 * @param int $value
	 * @param int $valuemin
	 * @param int $valuemax
	 * @param string $contextual [info,success,warning,danger]
	 */
	public function __construct($value, $valuemin = 0, $valuemax = 100, $contextual = null) {
		
		$this->value = $value;
		$this->valuemin = $valuemin;
		$this->valuemax = $valuemax;
		
		parent::__construct('div');
		$this->class = 'progress';
		
		$bar = new Element('div');
		$bar->class = 'progress-bar';
		$bar->css('width', $this->value.'%');
		$bar->role = 'progressbar';
		$bar->aria('valuenow', $this->value);
		$bar->aria('valuemin', $this->valuemin);
		$bar->aria('valuemax', $this->valuemax);
		
		if(!is_null($contextual)){
			$bar->add_class("progress-bar-$contextual");
		}
		
		$this->complete = new Element('span');
		$this->complete->class = 'sr-only';
		$this->complete->innertext($this->value . '% Complete');
		// building it togheter
		
		$bar->append($this->complete);
		$this->append($bar);
	}
	/**
	 * Show a label in the progress bar
	 * @return $this
	 */
	function show_label(){
		$this->complete->remove_class('sr-only');
		return $this;
	}
	/**
	 * Stripped progress bar
	 * @return $this
	 */
	function striped(){
		$this->add_class('progress-striped');
		return $this;
	}
	/**
	 * Animated progress bar
	 * @return $this
	 */
	function animated(){
		$this->striped();
		$this->add_class('active');
		return $this;
	}

}
