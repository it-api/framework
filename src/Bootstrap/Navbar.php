<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Navbar
 *
 * @author Frank
 */
class Navbar extends Element{
	
	private $_id;
	private $container;
	private $collapsable;
	
	public function __construct($brandname = '', $type = 'default', $fluid = true) {
		
		if($type !== 'default' && $type !== 'inverse'){
			throw new \InvalidArgumentException('parameter type is of the wrong type');
		}
		// create unique id
		$this->_id = uniqid('navbar');
		
		// <nav class="navbar navbar-default" role="navigation">
		parent::__construct('nav');
		$this->role = 'navigation';
		$this->add_class('navbar');
		$this->add_class("navbar-$type");
		
		$this->addContainer($fluid);
		$this->addNavbarHeaderToContainer($brandname);
		$this->addCollapsable();
	}
	/**
	 * Add a container to the navbar
	 * @param type $fluid
	 * @return \Barebone\Bootstrap\Element
	 */
	function addContainer($fluid = false){		
		if($fluid === true){
			$this->container = new Element('div', ['class' => "container-fluid"]);
		}else{
			$this->container = new Element('div', ['class' => "container"]);
		}
		$this->append($this->container);
		return $this->container;
	}
	/**
	 *	<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="#">Brand</a>
		</div>
	 */
	function addNavbarHeaderToContainer($brandname = 'Brand'){
		
		$header = new Element('div', ['class' => 'navbar-header']);
		
		$collapse = new Element('button');
		$collapse->type = 'button';
		$collapse->add_class('navbar-toggle');
		$collapse->data('toggle', 'collapse');
		$collapse->data('target', '#' . $this->_id);
		
		$toggle = new Element('span');
		$toggle->class = 'sr-only';
		$toggle->innertext('Toggle navigation');		
		$collapse->append($toggle);
		
		$iconbar = new Element('span');
		$iconbar->class = 'icon-bar';
		$collapse->append($iconbar);
		$collapse->append($iconbar);
		$collapse->append($iconbar);
		
		$brandelement = new Element('a');
		$brandelement->class = 'navbar-brand';
		$brandelement->href = '#';
		$brandelement->innertext($brandname);
		$header->append($brandelement);
		$header->append($collapse);
		$this->container->append($header);
	}
	
	function addCollapsable(){
		// <!-- Collect the nav links, forms, and other content for toggling -->
		// <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		$this->collapsable = new Element('div');
		$this->collapsable->add_class('collapse');
		$this->collapsable->add_class('navbar-collapse');
		$this->collapsable->id = $this->_id;
		$this->container->append($this->collapsable);
	}
	/**
	 * 
	 * @param \Barebone\Bootstrap\Nav $nav
	 * @return $this
	 */
	function addNav(Nav $nav){
		$nav->class = ('nav navbar-nav');
		$this->collapsable->append($nav);
		return $this;
	}
	
	function addText($text, $attributes = []){
		$p = new Element('p', $attributes);
		$p->innertext($text);
		$p->add_class('navbar-text');
		$this->collapsable->append($p);
	}
	/**
	 * 
	 * @param type $caption
	 * @param type $contextual
	 * @param type $callback
	 */
	function addButton($caption, $contextual = 'default', $callback = null){
		$button = new Button($caption, $contextual);
		$button->add_class('navbar-btn');
		$button->type = 'button';
		if(!is_null($callback)){
			$button->on('click', $callback);
		}
		$this->collapsable->append($button);
	}
	
	/**
	 * Add a form to the nav element
	 * @param \Barebone\Bootstrap\Form $form
	 * @return $this
	 */
	function addForm(Form $form){
		$form->add_class( 'navbar-form' );
		$this->collapsable->append($form);
		return $this;
	}
	/**
	 * 
	 * @param type $caption
	 * @param type $items
	 * @return $this
	 */
	function addDropDown($caption, $items = []){
		// <li class="dropdown">
        // <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
        // <ul class="dropdown-menu"></ul>
		// </li>
		$nav = new \Barebone\Bootstrap\Nav();
		$nav->addDropDown($caption, $items);
		$this->addNav($nav);
		return $this;
	}
	
	function fixedTo($position = 'top'){
		if($position === 'top'){
			$this->add_class('navbar-fixed-top');
		}elseif($position === 'bottom'){
			$this->add_class('navbar-fixed-bottom');
		}			
	}
	
	function staticTop(){
		$this->add_class('navbar-static-top');
	}

}
