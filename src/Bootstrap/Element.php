<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

use Barebone\HTML\TElement;

/**
 * Description of Element
 *
 * @author Frank
 */
class Element extends TElement {

	public function __construct(string $tag, array $attributtes = []) {
		parent::__construct($tag, $attributtes);
	}
	/**
	 * Muted context
	 * @param string $context [text, bg]
	 * @return $this
	 */
	function muted(string $context = 'text') {
		$this->add_class("{$context}-muted");
		return $this;
	}
	/**
	 * Primary context
	 * @param string $context [text, bg]
	 * @return $this
	 */
	function primary(string $context = 'text') {
		$this->add_class("{$context}-primary");
		return $this;
	}
	/**
	 * Success context
	 * @param string $context [text, bg]
	 * @return $this
	 */
	function success(string $context = 'text') {
		$this->add_class("{$context}-success");
		return $this;
	}
	/**
	 * Info context
	 * @param string $context [text, bg]
	 * @return $this
	 */
	function info(string $context = 'text') {
		$this->add_class("{$context}-info");
		return $this;
	}
	/**
	 * Warning context
	 * @param string $context [text, bg]
	 * @return $this
	 */
	function warning(string $context = 'text') {
		$this->add_class("{$context}-warning");
		return $this;
	}
	/**
	 * Danger context
	 * @param string $context [text, bg]
	 * @return $this
	 */
	function danger(string $context = 'text') {
		$this->add_class("{$context}-danger");
		return $this;
	}
	/**
	 * Align element
	 * @param string $alignement [left, top, right, bottom]
	 * @return $this
	 */
	function align(string $alignement = 'left') {
		$this->add_class("text-{$alignement}");
		return $this;
	}
	/**
	 * Furthermore, .invisible can be used to toggle only the visibility of an element, 
	 * meaning its display is not modified and the element can still affect the flow of the document.
	 * @return $this
	 */
	function invisible() {
		$this->add_class("invisible");
		return $this;
	}
	/**
	 * Hide element
	 * @return $this
	 */
	function hide() {
		$this->add_class("hidden");
		return $this;
	}
	/**
	 * Show element
	 * @return $this
	 */
	function show() {
		$this->add_class("show");
		return $this;
	}
	/**
	 * Add tooltip to the element
	 * @param string $tooltip
	 * @param string $placement [left,top,right,bottom]
	 */
	function tooltip(string $tooltip, string $placement = 'top') {
		// data-toggle="tooltip" data-placement="left" title="Tooltip on left">
		$this->data('toggle', 'tooltip');
		$this->data('placement', $placement);
		$this->title = $tooltip;
	}

	function popover(string $title = '', string $content = '', string $placement = 'top') {
		//data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus faucibus."
		$this->data('container', 'body');
		$this->data('toggle', 'popover');
		$this->data('placement', $placement);
		$this->data('title', $title);
		$this->data('content', $content);
	}
}
