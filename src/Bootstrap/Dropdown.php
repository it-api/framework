<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Dropdown
<div class="dropdown">
  <button class="btn dropdown-toggle sr-only" type="button" id="dropdownMenu1" data-toggle="dropdown">
    Dropdown
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
    <li role="presentation" class="divider"></li>
    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
  </ul>
</div>

 *
 * @author Frank
 */
class Dropdown extends Element{
	
	private $_id;
	private $button;
	private $menu;
	/**
	 * 
	 * @param string $caption
	 * @param string $contextual [info,primary,success,warning,danger]
	 */
	public function __construct($caption, $contextual = 'default') {
		
		// create unique id
		$this->_id = uniqid('ddbtn');
		
		parent::__construct('div');
		$this->add_class('dropdown');
		
		$this->button = new Element('button');
		$this->button->add_class("btn btn-$contextual dropdown-toggle");
		$this->button->type = 'button';
		$this->button->id = $this->_id;
		$this->button->data('toggle', 'dropdown');
		$this->button->innertext($caption);
		$this->button->append(new Element('span', ['class'=>'caret']));
		
		$this->append($this->button);
		
		$this->menu = new Element('ul');
		$this->menu->class = 'dropdown-menu';
		$this->menu->role = 'menu';
		$this->menu->aria('labelledby', $this->_id);
		
		$this->append($this->menu);
		
	}
	/**
	 * Add item to dropdown menu.
	 * @param string $caption text on the item
	 * @param type $href link for the item
	 */
	function addItem($caption, $href = '#'){
		
		$li = new Element('li');
		$li->role = 'presentation';
		
		$a = new Element('a');
		$li->role = 'menuitem';
		$li->tabindex = '-1';
		$a->innertext($caption);
		$a->href = $href;
		
		$li->append($a);
		
		$this->menu->append($li);
		return $this;
	}
	/**
	 * Add a header to label sections of actions in any dropdown menu.
	 * //<li role="presentation" class="dropdown-header">Dropdown header</li>
	 * @param string $caption
	 */
	function addHeader($caption){
		$li = new Element('li');
		$li->role = 'presentation';
		$li->class = 'dropdown-header';
		$li->innertext($caption);
		$this->menu->append($li);
		return $this;
	}
	/**
	 * 
	 * @return $this
	 */
	function large(){
		$this->button->add_class('btn-lg');
		return $this;
	}
	/**
	 * 
	 * @return $this
	 */
	function small(){
		$this->button->add_class('btn-sm');
		return $this;
	}
	/**
	 * 
	 * @return $this
	 */
	function extrasmall(){
		$this->button->add_class('btn-xs');
		return $this;
	}
	/**
	 * Return as btn-group
	 * @return $this
	 */
	function asBtnGroup(){
		$this->class = 'btn-group';
		return $this;
	}
	
	function dropup(){
		$this->add_class('dropup');
	}
	

}
