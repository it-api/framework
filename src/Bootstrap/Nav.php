<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Nav
<ul class="nav nav-tabs">
  <li class="active"><a href="#">Home</a></li>
  <li><a href="#">Profile</a></li>
  <li><a href="#">Messages</a></li>
</ul>

 * @author Frank
 */
class Nav extends Element{
	
	public function __construct() {
		parent::__construct('ul');		
	}
	/**
	 * Nav as tabs Add nav-tabs class
	 */
	function tabs(){
		$this->add_class('nav');
		$this->add_class('nav-tabs');
		return $this;
	}
	/**
	 * Nav as pills Add nav-pills class
	 */
	function pills(){
		$this->add_class('nav');
		$this->add_class('nav-pills');
		return $this;
	}
	/**
	 * Add new-stacked class
	 * @return $this
	 */
	function stacked(){
		$this->add_class('nav-stacked');
		return $this;
	}
	/**
	 * Add nav-justified class
	 * @return $this
	 */
	function justified(){
		$this->add_class('nav-justified');
		return $this;
	}
	/**
	 * Add a nav item
	 * @param string $caption text for the item
	 * @param string $href the link for the navitem
	 * @param bool $active is the item active
	 * @param string|Badge $badge string or a badge object
	 */
	function addItem($caption, $href = '#', $active = false, $badge = null){
		$li = new Element('li');
		if($active === true){
			$li->class = 'active';
		}		
		
		$a = new Element('a');
		$a->innertext($caption);
		$a->href = $href;
		
		if(!is_null($badge) && !$badge instanceof Badge){
			$b = new Badge($badge);
			$a->append($b);
		}elseif(!is_null($badge) && $badge instanceof Badge){
			$a->append($badge);
		}
		
		$li->append($a);
		$this->append($li);
		return $this;
	}
	/**
	 * Add a form to the nav element
	 * @param \Barebone\Bootstrap\Form $form
	 * @return $this
	 */
	function addForm(Form $form){
		$form->add_class( 'navbar-form' );
		$this->append($form);
		return $this;
	}
	
	function addDropDown($caption, $items = []){
		// <li class="dropdown">
        // <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
        // <ul class="dropdown-menu"></ul>
		// </li>
		$li = new Element('li');
		$li->class = 'dropdown';
		$a = new Element('a');
		$a->class ="dropdown-toggle";
		$a->data('toggle', 'dropdown');
		$a->innertext($caption);
		$a->href='#';
		$caret = new Element('b');
		$caret->class = 'caret';
		$a->append($caret);
		$li->append($a);
		// dropdown items
		$dropdown = new Element('ul');
		$dropdown->class = 'dropdown-menu';
		foreach($items as $txtlink => $href){
			$dditem = new Element('li');
			$link = new Element('a');
			$link->href = $href;
			$link->innertext($txtlink);
			$dditem->append($link);
			$dropdown->append($dditem);
		}
		$li->append($dropdown);
		
		$this->append($li);
	}

}
