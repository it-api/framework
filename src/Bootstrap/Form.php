<?php

namespace Barebone\Bootstrap;

/**
 * Description of Form
 *
 * @author Frank
 */
class Form extends Element {
	/**
	 * 
	 * @param array $attributes
	 */
	function __construct(array $attributes = []) {
		parent::__construct('form', $attributes);
		$this->role = 'form';
	}
	/**
	 * Set the form to inline
	 * @return Form
	 */
	function inline() {
		$this->add_class('form-inline');
		return $this;
	}
	/**
	 * Set the form to horizontal
	 * @return Form
	 */
	function horizontal() {
		$this->add_class('form-horizontal');
		return $this;
	}
	/**
	 * Add a formgroup
	 * @param \Barebone\Bootstrap\FormGroup $formgroup
	 * @return \Barebone\Bootstrap\FormGroup
	 */
	function addFormGroup(?\Barebone\Bootstrap\FormGroup $formgroup = null) {
		if (empty($formgroup)) {
			$formgroup = new FormGroup();
		}
		$this->append($formgroup);
		return $formgroup;
	}
}
