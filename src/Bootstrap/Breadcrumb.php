<?php

/*
  Creator Frank
 */

namespace Barebone\Bootstrap;

/**
 * Description of Breadcrumb
 *
 * @author Frank
 */
class Breadcrumb extends Element {
	/**
	 * 
	 * @param string $caption text for crumb
	 * @param string $href link for crumb
	 */
	public function __construct(string $caption, string $href = '#') {
		parent::__construct('li');
		$a = new Element('a');
		$a->href = $href;
		$a->innertext($caption);
		$this->append($a);
	}
}
