<?php

namespace Barebone\Classbuilder;

class ClassbuilderConstant{
	
	/**
	 * The name of the constant
	 * @var string
	 */
	private $name = '';
	/**
	 * The value of the constant
	 * @var string
	 */
	private $value = '';
	/**
	 * The phpdoc explaination
	 * @var string
	 */
	private $phpdoc = '';
	/**
	 * The type of constant eg: [string, int, constant]
	 * @var string
	 * @default string
	 */
	private $type = 'string';
	
	function __construct($name, $value, $phpdoc = ''){
		
		$this->name = $name;
		$this->value = $value;
		$this->phpdoc = $phpdoc;
		
	}
	
	function __toString(){
?>
<?php if($this->getPhpdoc() != ''):?>

	/**
	* <?php echo $this->getPhpdoc()?> 
	* @var <?php echo $this->getName()?> 
	*/
<?php endif;?>
	const <?php echo $this->getName()?><?php if($this->getType() === 'string'):?><?php echo $this->getValue() != '' ? " = '" . $this->getValue() . "'" : ''?><?php else:?><?php echo $this->getValue() != '' ? " = " . $this->getValue() : ''?><?php endif;?>;
<?php 
		$output = ob_get_contents();
		ob_clean();
		return $output;

	}

    /**
     * name
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * name
     * @param string $name
     * @return ClassbuilderConstant{
     */
    public function setName($name){
        $this->name = $name;
        return $this;
    }

    /**
     * value
     * @return string
     */
    public function getValue(){
        return $this->value;
    }

    /**
     * value
     * @param string $value
     * @return ClassbuilderConstant{
     */
    public function setValue($value){
        $this->value = $value;
        return $this;
    }

    /**
     * phpdoc
     * @return string
     */
    public function getPhpdoc(){
        return $this->phpdoc;
    }

    /**
     * phpdoc
     * @param string $phpdoc
     * @return ClassbuilderConstant{
     */
    public function setPhpdoc($phpdoc){
        $this->phpdoc = $phpdoc;
        return $this;
    }


    /**
     * type
     * @return string
     */
    public function getType(){
        return $this->type;
    }

    /**
     * type
     * @param string $type
     * @return ClassbuilderConstant{
     */
    public function setType($type){
        $this->type = $type;
        return $this;
    }

}
