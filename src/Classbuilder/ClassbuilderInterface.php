<?php

namespace Barebone\Classbuilder;


class ClassbuilderInterface{

	/**
	 * The phpdoc explaination before the class
	 * @var string
	 */
	private $phpdoc = '';
	/**
	 * the name of the interface
	 * @var string
	 */
	private $name = '';
	
	/**
	 * array of methods for this interface
	 * @var array
	 */
	private $methods = array();
	/**
	 * 
	 * @param string $name
	 */
	function __construct(string $name){
		
		$this->setName($name);
		
	}
	
	function __toString(){
		
		ob_start();
?>
<?php if($this->getPhpdoc() != ''):?>

/**
* <?php echo $this->getPhpdoc()?> 
* @var <?php echo $this->getName()?> 
*/
<?php endif;?>
interface <?php echo $this->getName()?>{
<?php foreach ($this->getMethods() as $method):?>
<?php echo $method;?>
<?php endforeach;?>
}

<?php 
		$output = ob_get_contents();
		ob_clean();
		return $output;
	}

    /**
     * name
     * @return string
     */
    public function getName(){
        return $this->name;
    }
	
	/**
     * name
     * @return string
     */
    public function getClassname(){
        return $this->name;
    }

    /**
     * name
     * @param string $name
     * @return ClassbuilderInterface{
     */
    public function setName(string $name){
        $this->name = $name;
        return $this;
    }
	/**
	 * 
	 * @param string $name
	 * @param string $visibility
	 * @param array $params
	 * @param string $phpdoc
	 * @return type
	 */
    function addMethod(string $name, string $visibility = 'public', array $params = array(), string $phpdoc = ''){
    	return $this->setMethod($name, $visibility, $params, $phpdoc);
    }
    
    /**
     * Set a method to the class
     * @return ClassbuilderMethod
     */
    public function setMethod(string $name, string $visibility = 'public', array $params = array(), string $phpdoc = ''){
    
    	if(isset($this->methods[$name])) throw new \Exception(sprintf('The method: %s allready exitsts.', $name));
    
    	$this->methods[$name] = new ClassbuilderInterfaceMethod($name, $visibility, $params, $phpdoc);
    	 
    	return $this;
    
    }

    /**
     * methods
     * @return array
     */
    public function getMethods(){
        return $this->methods;
    }

    /**
     * methods
     * @param array $methods
     * @return $this
     */
    public function setMethods(array $methods){
        $this->methods = $methods;
        return $this;
    }
    
    /**
     * phpdoc
     * @return string
     */
    public function getPhpdoc(){
    	return $this->phpdoc;
    }
    
    /**
     * phpdoc
     * @param string $phpdoc
     * @return ClassbuilderClass{
     */
    public function setPhpdoc(string $phpdoc){
    	$this->phpdoc = $phpdoc;
    	return $this;
    }

}
