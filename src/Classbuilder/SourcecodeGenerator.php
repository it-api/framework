<?php

namespace Barebone\Classbuilder;

class SourcecodeGenerator {
    
    private $klasse = null;
    
	function AddClass($classname){
	    $this->klasse = new ClassbuilderClass($classname);;
	    return $this->klasse;
	}

	
	function __toString(){
	    return $this->klasse->__toString();
	}
}
/*
$scg = new SourcecodeGenerator();
$scg
    ->AddClass('Speak')
        ->setPhpdoc('Speak class')
        ->addPropertie('_loaded', false, $phpdoc = 'Is it loaded', 'public', 'bool')
        ->addPropertie('_started', false, $phpdoc = 'Is it started', 'public', 'bool')
        ->addPropertie('_finished', false, $phpdoc = 'Is it finished', 'public', 'bool')
        ->addMethod(
            'helloWorld', 
            'public', 
            [
                new ClassbuilderMethodParam('int', 'Bar', 'Fu', 'int'), 
                new ClassbuilderMethodParam('string', 'Fu', 'Bar', 'string')
            ], 
            'Hello world example', 
            (new ClassbuilderMethodBody('echo "Hello world!";'))
                ->writeline('echo "I am a new line";')
                ->writeline('for($Bar as $Fu){')
                ->writeline('echo "I am a new line";')
                ->writeline('}')
                ->writeline('echo "I am a new line";')
        )
        ->addMethod(
            'helloUniverse', 
            'public', 
            [new ClassbuilderMethodParam('string', 'Fu', null, 'string')], 
            'Hello universe example', 
            'echo "Hello {$Fu} i am the universe!";')
            
        ->create_getter_setters();
    
echo $scg;

*/