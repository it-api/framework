<?php

/*
  ClassbuilderCollection.php
  UTF-8
  29-apr-2018 23:30:07
  barebone-php7
  Creator Frank
 */

namespace Barebone\Classbuilder;

/**
 * Description of ClassbuilderCollection
 *
 * @author Frank
 */
class ClassbuilderCollection {
	
	/**
	 * The path of the file
	 * @var string
	 */
	private $filename = null;
	/**
	 *
	 * @var array
	 */
	private $collection = [];
	
	public function getCollection() : array {
		return $this->collection;
	}

	public function setCollection(array $collection) {
		$this->collection = $collection;
		return $this;
	}
	
	function __toString() {
		$output = '';
		foreach($this->getCollection() as $item){
			$output .= ltrim($item,' ');
		}
		return ltrim($output,' ');
	}

	/**
	 * 
	 * @param string $filename
	 * @return $this
	 */
	public function setFilename(string $filename){
		$this->filename = $filename;
		return $this;
	}
	/**
	 * 
	 * @return string
	 */
	public function getFilename() : string{
		return $this->filename;
	}
	
	function LoadFromDisk(){
		
		if(!isset($this->filename)){
			throw new \Barebone\Exception('Filename is not set.');
		}
		$json = file_get_contents($this->filename);
		$data = json_decode($json);
		//print_r($data);
		if(isset($data->interfaces))
		foreach($data->interfaces as $interface){
			$classbuilderInterface = new ClassbuilderInterface($interface->name);
			$classbuilderInterface->setPhpdoc($interface->phpdoc);
			if(isset($interface->methods))
			foreach ($interface->methods as $key => $method){
				//print_r($method);
				$classbuilderInterface->addMethod($method->name, $method->visibility, $method->parameters, $method->phpdoc);
			}
			$this->collection[] = $classbuilderInterface;
						
		}
			
		
		if(isset($data->classes))
		foreach($data->classes as $class){
			$classbuilderClass = new ClassbuilderClass(
				$class->name, 
				$class->extends,
				$class->implements
			);
			
			if(isset($class->abstract)){
				$classbuilderClass->setIs_abstract($class->abstract);
			}
			
			if(isset($class->constants))
			foreach($class->constants as $constant){
				$classbuilderClass->addConstant($constant->name, $constant->value, $constant->phpdoc, $constant->visibility);
			}
			if(isset($class->properties))
			foreach($class->properties as $propertie){
				$classbuilderClass->addPropertie($propertie->name, $propertie->value, $propertie->phpdoc, $propertie->visibility);
			}
			//$this->setPhpdoc($class->phpdoc);
			//$this->setClsname($class->name);
			//$this->setExtends($class->extends);
			//$this->setImplements($class->implements);
			if(isset($class->methods))
			foreach ($class->methods as $key => $method){
				$classbuilderClass->addMethod($method->name, $method->visibility, $method->parameters, $method->phpdoc, $method->body);
			}
			
			$this->collection[] = $classbuilderClass;
		}
	}
	/**
	 * If a folder is given the a files will be save to the folder
	 * If no folder is given the collection gets saved to a single file
	 * @param string $folder
	 * @throws \Barebone\Exception
	 */
	function SaveToDisk($folder = null){
		
		if(is_null($folder)){
			
			if(!isset($this->filename)){
				throw new \Barebone\Exception('Filename is not set.');
			}
			file_put_contents($this->filename, '<?php'."\n\n".$this);
			
		}else{
			
			foreach($this->getCollection() as $item){
				file_put_contents($folder . $item->getClassname(). '.php', '<?php'."\n\n".$item);	
			}
			
		}
	}
}
