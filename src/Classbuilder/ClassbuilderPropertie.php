<?php

namespace Barebone\Classbuilder;

class ClassbuilderPropertie{
	
	/**
	 * The visibility of the propertie [private, public, protected,]
	 * @var string
	 */
	private string $visibility = 'public';
	/**
	 * The name of the propertie
	 * @var string
	 */
	private string $name = '';
	/**
	 * The value of the propertie
	 * @var string
	 */
	private string $value = '';
	/**
	 * The phpdoc explaination
	 * @var string
	 */
	private string $phpdoc = '';
	/**
	 * The type of parameter eg: [string, int, constant]
	 * @var string
	 */
	private string $type = 'string';
	
	function __construct(string $name, string $value, string $phpdoc = '', string $visibility = 'public', string $type = 'string'){
		
		$this->visibility = $visibility;
		$this->name = $name;
		$this->value = $value;
		$this->phpdoc = $phpdoc;
		$this->type = $type;
		
	}
	
	function __toString(){

		//ob_start();
		?>
<?php if($this->getPhpdoc() != ''):?>

	/**
	* <?php echo $this->getPhpdoc()?> 
	* @var <?php echo $this->getName()?> 
	*/
<?php endif;?>
	<?php echo $this->getVisibility()?> $<?php echo $this->getName()?><?php if($this->getType() === 'string'):?><?php echo $this->getValue() != '' ? " = '" . $this->getValue() . "'" : ''?><?php else:?><?php echo $this->getValue() != '' ? " = " . $this->getValue() : ''?><?php endif;?>;
	
<?php 
		$output = ob_get_contents();
		ob_clean();
		return $output;
				
	}
	
	/**
	 * visibilty
	 * @return string [public,private,static,protected]
	 */
	public function getVisibility(): string {
		return $this->visibility;
	}
	
	/**
	 * visibilty
	 * @param string $visibilty [public,private,static,protected]
	 * @return ClassbuilderPropertie{
	 */
	public function setVisibility(string $visibility){
		$this->visibility = $visibility;
		return $this;
	}

    /**
     * name
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * name
     * @param string $name
     * @return ClassbuilderPropertie{
     */
    public function setName(string $name){
        $this->name = $name;
        return $this;
    }

    /**
     * value
     * @return string
     */
    public function getValue(): string {
        return $this->value;
    }

    /**
     * value
     * @param string $value
     * @return ClassbuilderPropertie{
     */
    public function setValue(string $value){
        $this->value = $value;
        return $this;
    }

    /**
     * phpdoc
     * @return string
     */
    public function getPhpdoc(): string {
        return $this->phpdoc;
    }

    /**
     * phpdoc
     * @param string $phpdoc
     * @return ClassbuilderPropertie{
     */
    public function setPhpdoc(string $phpdoc){
        $this->phpdoc = $phpdoc;
        return $this;
    }


    /**
     * type
     * @return string
     */
    public function getType(){
        return $this->type;
    }

    /**
     * type
     * @param string $type
     * @return ClassbuilderPropertie{
     */
    public function setType(string $type){
        $this->type = $type;
        return $this;
    }

}
