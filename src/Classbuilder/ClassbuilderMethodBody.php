<?php

namespace Barebone\Classbuilder;

class ClassbuilderMethodBody{
    
    private $code = '';
    
    public function __construct($code = ''){
        $this->code = $code;
        return $this;
    }
    
    public function __toString(){
        return $this->code;
    }
    
    public function writeline( $code ){
        $this->code .= "\n\t\t$code";
        return $this;
    }
}