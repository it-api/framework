<?php

/*
  VisibiltyInterface.php
  UTF-8
  29-apr-2018 16:14:55
  barebone-php7
  Creator Frank
 */

namespace Barebone\Classbuilder\Interfaces;

/**
 *
 * @author Frank
 */
interface VisibiltyInterface {
	
	const VISIBLE_PRIVATE = 'private';
	const VISIBLE_PUBLIC = 'public';
	const VISIBLE_PROTECTED = 'protected';
	
}
