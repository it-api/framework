<?php

namespace Barebone\Classbuilder;

use Barebone\Exception;

class ClassbuilderInterfaceMethod{
	
	private $visibility = 'public';
	private $name = '';
	private $params = array();
	private $phpdoc = '';
	
	function __construct($name, $visibility = 'public', $params = array(), $phpdoc = ''){
	
		$this->setVisibility($visibility);
		$this->setName($name);
		$this->setPhpdoc($phpdoc);
	
		foreach($params as $param){
			
			if(is_object($param)) $param = \Barebone\Functions::object_to_array($param);
			
			$this->params[$param['name']] = new ClassbuilderMethodParam(isset($param['cast']) ? $param['cast'] : null, $param['name'], $param['value'], $param['type']);
		}
	}
	
	function __toString(){

?>

<?php if($this->getPhpdoc() != ''):?>
	/**
	* <?php echo $this->getPhpdoc()?><?php foreach($this->getParams() as $parameter):?>	
	* @var<?=$parameter?><?php endforeach;?>	
	*/
<?php endif;?>
	<?php echo ($this->getVisibility() != '' ? $this->getVisibility() . ' ' : '')?>function <?php echo $this->getName()?>(<?php echo $this->getParamsString()?>);
<?php 
			$output = ob_get_contents();
			ob_clean();
			return $output;
			
		}
		
		/**
		 * visibilty
		 * @return string [public,private,static,protected]
		 */
		public function getVisibility(){
			return $this->visibility;
		}
		
		/**
		 * visibilty
		 * @param string $visibilty [public,private,static,protected]
		 * @return ClassbuilderMethod{
		 */
		public function setVisibility($visibility){
			$allowed_visibilitys = array('public','private','static','protected');
			if(!in_array($visibility, $allowed_visibilitys)) throw new Exception('Invalid visibility, allowed are: ' .implode(',', $allowed_visibilitys));
			$this->visibility = $visibility;
			return $this;
		}
	
	    /**
	     * name
	     * @return string
	     */
	    public function getName(){
	        return $this->name;
	    }
	
	    /**
	     * name
	     * @param string $name
	     * @return ClassbuilderMethod{
	     */
	    public function setName($name){
	        $this->name = $name;
	        return $this;
	    }
	
	    function getParam($name){
	    	return $this->params[$name];
	    }
	    
	    function getParamsString(){
	    	$i = 0;
	    	foreach ($this->params as $param){
?>
 <?php echo $param;?><?php if($i != count($this->params)-1):?>, <?php endif;?>
<?php 
			$i++;
	    	}
			$output = ob_get_contents();
			ob_clean();
			return $output;
	    	
	    }
	    /**
	     * params
	     * @return array
	     */
	    public function getParams(){
	        return $this->params;
	    }
	
	    /**
	     * params
	     * @param array $params
	     * @return ClassbuilderMethod{
	     */
	    public function setParams($params){
	        $this->params = $params;
	        return $this;
	    }
	    
	    /**
	     * phpdoc
	     * @return string
	     */
	    public function getPhpdoc(){
	    	return $this->phpdoc;
	    }
	    
	    /**
	     * phpdoc
	     * @param string $phpdoc
	     * @return ClassbuilderMethod{
	     */
	    public function setPhpdoc($phpdoc){
	    	$this->phpdoc = $phpdoc;
	    	return $this;
	    }
}
