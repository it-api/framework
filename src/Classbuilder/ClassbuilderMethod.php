<?php

namespace Barebone\Classbuilder;

class ClassbuilderMethod{
	
	private $visibility = 'public';
	private $name = '';
	private $params = array();
	private $phpdoc = '';
	private $body = '';
	/**
	 * 
	 * @param string $name
	 * @param string $visibility
	 * @param array $params
	 * @param string $phpdoc
	 * @param string $body
	 */
	function __construct(string $name, string $visibility = 'public', array $params = array(), string $phpdoc = '', string $body = ''){
		
		$this->setVisibility($visibility);
		$this->setName($name);
		$this->setBody($body);
		$this->setPhpdoc($phpdoc);
		
		foreach($params as $key => $param){
			
			if(is_array($param) && !$param instanceof ClassbuilderMethodParam)
				$param = new ClassbuilderMethodParam($param['cast'], $param['name'], $param['value'], $param['type']);
			else if(is_object($param) && !$param instanceof ClassbuilderMethodParam){
				$param = new ClassbuilderMethodParam($param->cast, $param->name, $param->value, $param->type);
			}

			$this->params[$param->getName()] = new ClassbuilderMethodParam(
					$param->getCast(), 
					$param->getName(), 
					$param->getValue(), 
					$param->getType()
			);
		}
	}
	
	function __toString(){
		?>
<?php if($this->getPhpdoc() != ''):?>
	/**
	* <?php echo $this->getPhpdoc()?><?php foreach($this->getParams() as $parameter):?>	
	* @var<?=$parameter?><?php endforeach;?>
	
	*/
<?php endif;?>
	<?php echo ($this->getVisibility() != '' ? $this->getVisibility() . ' ' : '')?>function <?php echo $this->getName()?>(<?php echo $this->getParamsString()?>){

<?php if($this->getBody() != ''):?>
		<?php echo $this->getBody();?>
		
<?php endif;?>
	}
	
<?php 
		$output = ob_get_contents();
		ob_clean();
		return $output;
		
	}
	
	/**
	 * visibilty
	 * @return string [public,private,static,protected]
	 */
	public function getVisibility(){
		return $this->visibility;
	}
	
	/**
	 * visibilty
	 * @param string $visibility [public,private,static,protected]
	 * @return ClassbuilderMethod{
	 */
	public function setVisibility($visibility){
		$this->visibility = $visibility;
		return $this;
	}

    /**
     * name
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * name
     * @param string $name
     * @return ClassbuilderMethod{
     */
    public function setName(string $name){
        $this->name = $name;
        return $this;
    }

    function getParam(string $name){
    	return $this->params[$name];
    }
    
    function getParamsString() : string{
    	$i = 0;
    	foreach ($this->params as $param){
?>
 <?php echo $param;?> <?php if($i != count($this->params)-1):?>, <?php endif;?>
<?php 
$i++;
    	}
    	
		$output = ob_get_contents();
		$output = rtrim($output, "\t");
		ob_clean();
		return $output;
    	
    }
    /**
     * params
     * @return array
     */
    public function getParams() : array{
        return $this->params;
    }

    /**
     * params
     * @param array $params
     * @return ClassbuilderMethod{
     */
    public function setParams(array $params){
        $this->params = $params;
        return $this;
    }
    
    /**
     * phpdoc
     * @return string
     */
    public function getPhpdoc() : string {
    	return $this->phpdoc;
    }
    
    /**
     * phpdoc
     * @param string $phpdoc
     * @return ClassbuilderMethod{
     */
    public function setPhpdoc(string $phpdoc){
    	$this->phpdoc = $phpdoc;
    	return $this;
    }


    /**
     * body
     * @return string
     */
    public function getBody(){
        return $this->body;
    }

    /**
     * body
     * @param string $body
     * @return ClassbuilderMethod{
     */
    public function setBody(string $body){
        $this->body = $body;
        return $this;
    }

}
