<?php

namespace Barebone\Classbuilder;

class ClassbuilderClass {

	/**
	 * The phpdoc explaination before the class
	 * @var string
	 */
	private string $phpdoc = '';

	/**
	 * the name of the class
	 * @var string
	 */
	private string $clsname = '';

	/**
	 * the class extends
	 * @var string
	 */
	private string $extends = '';

	/**
	 * the class implements
	 * @var string
	 */
	private string $implements = '';

	/**
	 * array of constant objects
	 * @var array
	 */
	private array $constants = [];

	/**
	 * array of properties objects
	 * @var array
	 */
	private array $properties = [];

	/**
	 * array of methods objects
	 * @var array
	 */
	private array $methods = [];
	/**
	 * The path of the file
	 * @var string
	 */
	private ?string $filename = null;
	/**
	 * Is the class abstract
	 * @var bool
	 */
	private bool $is_abstract = false;
	/**
	 * 
	 * @param string $clsname
	 * @param string $extends
	 * @param string $implements
	 * @param array $constants
	 * @param array $properties
	 * @param array $methods
	 */
	function __construct(string $clsname, string $extends = '', string $implements = '', array $constants = [], array$properties = [], array $methods = []) {

		$this->setClsname($clsname);
		$this->setExtends($extends);
		$this->setImplements($implements);
		$this->setConstants($constants);
		$this->setProperties($properties);
		$this->setMethods($methods);
	}
	/**
	 * 
	 * @return string
	 */
	function __toString() {

		ob_start();
?>
<?php if ($this->getPhpdoc() != ''): ?>
	/**
	* <?php echo $this->getPhpdoc() ?> 
	* @var <?php echo $this->getClassname() ?> 
	*/
<?php endif; ?>
<?=$this->getIs_abstract() === true ? 'abstract ': ''?>class <?php echo $this->getClassname() ?><?php if ($this->getExtends() != ''): ?> extends <?php echo $this->getExtends(); ?><?php endif; ?><?php if ($this->getImplements() != ''): ?> implements <?php echo $this->getImplements(); ?><?php endif; ?>{
<?php foreach ($this->getConstants() as $constant): ?>
<?php echo $constant; ?>
<?php endforeach; ?>
<?php foreach ($this->getProperties() as $propertie): ?>
<?php echo $propertie; ?>
<?php endforeach; ?>
<?php foreach ($this->getMethods() as $method): ?>
<?php echo $method; ?>
<?php endforeach; ?>
}

<?php
		$output = ob_get_contents();
		ob_clean();
		return $output;
	}
	/**
	 * Add a constant to the class
	 * @param string $name
	 * @param string $value
	 * @param string $phpdoc
	 * @param string $visibility
	 * @return type
	 */
	function addConstant(string $name, string $value, string $phpdoc = '', string $visibility = 'public') {
		return $this->setConstant($name, $value, $phpdoc, $visibility);
	}
	/**
	 * Add a propertie to the class
	 * @param string $name
	 * @param string $value
	 * @param string $phpdoc
	 * @param string $visibility
	 * @param string $type
	 * @return type
	 */
	function addPropertie(string $name, string $value, string $phpdoc = '', string $visibility = 'public', string $type = 'string') {
		return $this->setPropertie($name, $value, $phpdoc, $visibility, $type);
	}
	/**
	 * Add a method to the class
	 * @param string $name
	 * @param string $visibility
	 * @param array $params
	 * @param string $phpdoc
	 * @param string $body
	 * @return ClassbuilderMethod
	 */
	function addMethod(string $name, string $visibility = 'public', array $params = [], string $phpdoc = '', string $body = '') {
		return $this->setMethod($name, $visibility, $params, $phpdoc, $body);
	}

	/**
	 * Get the Classname
	 * @return string
	 */
	public function getClassname() {
		return $this->clsname;
	}

	/**
	 * clsname
	 * @param string $clsname
	 * @return ClassbuilderClass{
	 */
	public function setClsname(string $clsname) {
		$this->clsname = $clsname;
		return $this;
	}

	/**
	 * extends
	 * @return string
	 */
	public function getExtends() {
		return $this->extends;
	}

	/**
	 * extends
	 * @param string $extends
	 * @return ClassbuilderClass{
	 */
	public function setExtends(string $extends) {
		$this->extends = $extends;
		return $this;
	}

	/**
	 * implements
	 * @return string
	 */
	public function getImplements() {
		return $this->implements;
	}

	/**
	 * implements
	 * @param string $implements
	 * @return ClassbuilderClass{
	 */
	public function setImplements(string $implements) {
		$this->implements = $implements;
		return $this;
	}

	/**
	 * Get a constant from the class
	 * @return ClassbuilderConstant
	 */
	public function getConstant(string $name) {

		if (!isset($this->constants[$name]))
			throw new \Exception(sprintf('The constant: %s Does not exitst', $name));

		return $this->constants[$name];
	}

	/**
	 * Set a constant to the class
	 * @param string $name
	 * @param string $value
	 * @param string $phpdoc
	 * @param string $visibility
	 * @return type
	 * @throws \Exception
	 */
	public function setConstant(string $name, string $value, string $phpdoc = '', string $visibility = 'public') {

		if (isset($this->constants[$name]))
			throw new \Exception(sprintf('The constant: %s allready exitsts.', $name));

		return $this->constants[$name] = new ClassbuilderConstant($name, $value, $phpdoc);
	}

	/**
	 * Constants of this class
	 * @return array of constants of this class
	 */
	public function getConstants() {
		return $this->constants;
	}

	/**
	 * Set the constants of this class with an array
	 * @param array $constants
	 * @return ClassbuilderClass{
	 */
	public function setConstants(array $constants = []) {

		foreach ($constants as $name => $value) {
			$this->setConstant($name, $value);
		}

		return $this;
	}

	/**
	 * Set a propertie to the class
	 * @param string $name
	 * @param string $value
	 * @param string $phpdoc
	 * @param string $visibility
	 * @param string $type
	 * @return $this
	 * @throws \Exception
	 */
	public function setPropertie(string $name, string $value, string $phpdoc = '', string $visibility = 'public', string $type = 'string') {

		if (isset($this->properties[$name]))
			throw new \Exception(sprintf('The propertie: %s allready exitsts.', $name));

		$this->properties[$name] = new ClassbuilderPropertie($name, $value, $phpdoc, $visibility, $type);
		return $this;
	}

	/**
	 * Get the class properties
	 * @return array
	 */
	public function getProperties() {
		return $this->properties;
	}

	/**
	 * Set the class properties
	 * @param array $properties
	 * @return ClassbuilderClass{
	 */
	public function setProperties(array $properties = []) {

		if (!is_array($properties))
			throw new \Exception('The parameter properties needs to be an array');

		if ($properties)
			foreach ($properties as $propertie) {

				$this->setPropertie($propertie->getName(), $propertie->getValue(), $propertie->getPhpdoc(), $propertie->getVisibility());
			}

		return $this;
	}

	/**
	 * Set a method to the class
	 * @param string $name
	 * @param string $visibility
	 * @param array $params
	 * @param string $phpdoc
	 * @param string $body
	 * @return $this
	 * @throws \Exception
	 */
	public function setMethod(string $name, string $visibility = 'public', array $params = array(), string $phpdoc = '', string $body = '') {

		if (isset($this->methods[$name]))
			throw new \Exception(sprintf('The method: %s allready exitsts.', $name));

		$this->methods[$name] = new ClassbuilderMethod($name, $visibility, $params, $phpdoc, $body);

		return $this;
	}
	/**
	 * 
	 * @param string $name
	 * @return type
	 */
	function getMethod(string $name) {
		return $this->methods[$name];
	}

	/**
	 * methods
	 * @return array
	 */
	public function getMethods() {
		return $this->methods;
	}

	/**
	 * methods
	 * @param array $methods
	 * @return ClassbuilderClass{
	 */
	public function setMethods(array $methods) {
		$this->methods = $methods;
		return $this;
	}

	/**
	 * phpdoc
	 * @return string
	 */
	public function getPhpdoc() : string  {
		return $this->phpdoc;
	}

	/**
	 * phpdoc
	 * @param string $phpdoc
	 * @return ClassbuilderClass{
	 */
	public function setPhpdoc(string$phpdoc) {
		$this->phpdoc = $phpdoc;
		return $this;
	}
	/**
	 * 
	 * @param string $filename
	 * @return $this
	 */
	public function setFilename(string $filename){
		$this->filename = $filename;
		return $this;
	}
	/**
	 * 
	 * @return string
	 */
	public function getFilename() : string{
		return $this->filename;
	}
	/**
	 * Load file fro disk. You need to set the filename first
	 * @throws \Barebone\Exception
	 */
	function LoadFromDisk(){
		if(!isset($this->filename)){
			throw new \Barebone\Exception('Filename is not set.');
		}
		$json = file_get_contents($this->filename);
		$data = json_decode($json);
		//print_r($data);
		foreach($data->classes as $class){
			$this->setPhpdoc($class->phpdoc);
			$this->setClsname($class->name);
			$this->setExtends($class->extends);
			$this->setImplements($class->implements);
			if(isset($class->methods))
			foreach ($class->methods as $key => $method){
				$this->addMethod($method->name, $method->visibility, $method->parameters, $method->phpdoc, $method);
			}
		}

	}
	/**
	 * Save file file to disk. You need to set the filename first
	 * @throws \Barebone\Exception
	 */
	function SaveToDisk(){
		if(!isset($this->filename)){
			throw new \Barebone\Exception('Filename is not set.');
		}
		file_put_contents($this->filename, '<?php'."\n\n".$this);
	}
	/**
	 * Get the class to be abstract
	 * @return bool
	 */
	public function getIs_abstract() :bool {
		return $this->is_abstract;
	}
	/**
	 * Set the class to be abstract
	 * @param bool $is_abstract
	 * @return $this
	 */
	public function setIs_abstract(bool $is_abstract) {
		$this->is_abstract = $is_abstract;
		return $this;
	}
	
	/**
	 * Create a get an set function for the propertie
	 * @param string $propertie
	 */
	public function create_getter_setter($propertie) {

		$this->addMethod('get' . ucfirst($propertie), 'public', [], 'get $this->' . $propertie, 'return $this->' . $propertie . ';');

		$ClassbuilderMethodParam = new ClassbuilderMethodParam('', $propertie, '');

		$this->addMethod('set' . ucfirst($propertie), 'public', [$ClassbuilderMethodParam], 'set $this->' . $propertie, '$this->' . $propertie . ' = $' . $propertie . ";\n\t\t" . 'return $this; ');
	}
	/**
	 * Create a get an set function for all the properties
	 */
	public function create_getter_setters() {

		foreach ($this->properties as $propertie) {

			$this->create_getter_setter($propertie->getName());
		}
	}

}
