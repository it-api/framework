<?php

namespace Barebone\Classbuilder;

/**
 * Build a parameter for a method in a class
 * @author Frank
 * @example 
 * $param = new ClassbuilderMethodParam();
 *
 */
class ClassbuilderMethodParam{
	
	/**
	 * The cast of the parameter eg stdClass
	 * @var string
	 */
	private $cast = null;
	/**
	 * The name of the parameter
	 * @var string
	 */
	private $name = '';
	/**
	 * The value of the parameter
	 * @var string
	 */
	private $value = '';
	/**
	 * The type of parameter eg: [string, int, constant]
	 * @var string
	 */
	private $type = 'string';
	/**
	 * 
	 * @param string $typehint
	 * @param string $name
	 * @param string $value
	 * @param string $type
	 * @return $this
	 */
	function __construct(string $typehint = '', string $name = '', string $value = '', string $type = 'string'){
		
		$this->setCast($typehint);
		$this->setName($name);
		$this->setValue($value);
		$this->setType($type);
		return $this;
	}
	
	function __toString(){
?><?php echo ($this->getCast() != null ? ' '. $this->getCast().' ' : '')?>$<?php echo $this->getName()?><?php if($this->getType() === 'string'):?><?php echo ($this->getValue() != '' ? " = '" . $this->getValue() . "'" : '')?><?php else:?><?php echo ($this->getValue() != '' ? " = " . $this->getValue() : '')?><?php endif;?>
<?php 
		$output = ob_get_contents();
		$output = rtrim($output, "\t");
		ob_clean();
		return $output;		
	}

    /**
     * name
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * name
     * @param string $name
     * @return ClassbuilderMethodParam{
     */
    public function setName($name){
        $this->name = $name;
        return $this;
    }

    /**
     * value
     * @return string
     */
    public function getValue(){
        return $this->value;
    }

    /**
     * value
     * @param string $value
     * @return ClassbuilderMethodParam{
     */
    public function setValue($value){
        $this->value = $value;
        return $this;
    }


    /**
     * cast
     * @return string
     */
    public function getCast(){
        return $this->cast;
    }

    /**
     * cast
     * @param string $cast
     * @return ClassbuilderMethodParam{
     */
    public function setCast($cast){
        $this->cast = $cast;
        return $this;
    }


    /**
     * type
     * @return string
     */
    public function getType(){
        return $this->type;
    }

    /**
     * type
     * @param string $type
     * @return ClassbuilderMethodParam{
     */
    public function setType($type){
        $this->type = $type;
        return $this;
    }

}
