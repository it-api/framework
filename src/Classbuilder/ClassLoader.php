<?php

namespace Barebone\Classbuilder;
use Barebone\Classbuilder\Interfaces\IClassLoader;
class ClassLoader implements IClassLoader {
    /**
     * Load class
     *
     * @param string $class
     * @return object
     * @throws \Barebone\Exception
     */
    public function loadClass(string $class){
        
        if(class_exists($class) === false) {
            throw new \Barebone\Exception(sprintf('The class: "%s" does not exist.', $class));
        }
        
        return new $class();
    }
    
    /**
     * Called when loading class method
     * @param object $class
     * @param string $method
     * @param array $parameters
     * @return string
     */
    public function loadClassMethod(object $class, string $method, array $parameters): string
    {
        return (string) $class->{$method}($parameters);
    }

    /**
     * Load closure
     *
     * @param Callable $closure
     * @param array $parameters
     * @return string
     */
    public function loadClosure(callable $closure, array $parameters): string
    {
        return (string) call_user_func_array($closure, array_values($parameters));
    }
}
