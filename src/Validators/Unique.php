<?php

namespace Barebone\Validators;

use PDO;

class Unique extends Base
{
    private PDO $pdo;
    private string $primary_key;
    private string $table;

    public function __construct(string $field, string $error_message, PDO $pdo, string $table, string $primary_key = 'id') {
        parent::__construct($field, $error_message);

        $this->pdo = $pdo;
        $this->primary_key = $primary_key;
        $this->table = $table;
    }

    public function execute(array $data): bool {
        if ($this->isFieldNotEmpty($data)) {
            if (! isset($data[$this->primary_key])) {
                $rq = $this->pdo->prepare('SELECT 1 FROM '.$this->table.' WHERE '.$this->field.'=?');
                $rq->execute(array($data[$this->field]));
            } else {

                $rq = $this->pdo->prepare(
                    'SELECT 1 FROM '.$this->table.'
                    WHERE '.$this->field.'=? AND '.$this->primary_key.' != ?'
                );

                $rq->execute(array($data[$this->field], $data[$this->primary_key]));
            }

            $result = $rq->fetchColumn();

            if ((int) $result === 1) {
                return false;
            }
        }

        return true;
    }
}