<?php

namespace Barebone\Validators;

class Required extends Base
{
    public function execute(array $data): bool {
        return $this->isFieldNotEmpty($data);
    }
}