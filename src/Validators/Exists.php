<?php

namespace Barebone\Validators;

use PDO;

class Exists extends Base
{
    private PDO $pdo;
    private string $key;
    private string $table;

    public function __construct(string $field, string $error_message, PDO $pdo, string $table, string $key = '') {
        parent::__construct($field, $error_message);

        $this->pdo = $pdo;
        $this->table = $table;
        $this->key = $key;
    }


    public function execute(array $data): bool {
        if (! $this->isFieldNotEmpty($data)) {
            return true;
        }

        if ($this->key === '') {
            $this->key = $this->field;
        }

        $rq = $this->pdo->prepare('SELECT 1 FROM '.$this->table.' WHERE '.$this->key.'=?');
        $rq->execute(array($data[$this->field]));

        return $rq->fetchColumn() == 1;
    }
}