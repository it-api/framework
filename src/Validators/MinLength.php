<?php

namespace Barebone\Validators;

class MinLength extends Base
{
    private int $min;

    public function __construct(string $field, string $error_message, int $min) {
        parent::__construct($field, $error_message);
        $this->min = $min;
    }

    public function execute(array $data): bool {
        if ($this->isFieldNotEmpty($data)) {
            $length = mb_strlen($data[$this->field], 'UTF-8');
            return $length >= $this->min;
        }

        return true;
    }
}