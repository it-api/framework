<?php

namespace Barebone\Validators;

class Length extends Base
{
    private int $min;
    private int $max;

    public function __construct(string $field, string $error_message, int $min, int $max) {
        parent::__construct($field, $error_message);
        $this->min = $min;
        $this->max = $max;
    }

    public function execute(array $data): bool {
        if ($this->isFieldNotEmpty($data)) {
            $length = mb_strlen($data[$this->field], 'UTF-8');
            return $length >= $this->min && $length <= $this->max;
        }

        return true;
    }
}