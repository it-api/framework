<?php

/*
  Creator Frank
 */

namespace Barebone\Validators;

/**
* Description of String
*
* @author Frank
*/
class IsString extends Base {

    public function execute(array $data): bool {
        if ($this->isFieldNotEmpty($data)) {
            return is_string($data[$this->field]);
        }

        return true;
    }
}