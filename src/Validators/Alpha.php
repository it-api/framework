<?php

namespace Barebone\Validators;

class Alpha extends Base
{
    public function execute(array $data): bool {
        if ($this->isFieldNotEmpty($data)) {
            return ctype_alpha($data[$this->field]);
        }

        return true;
    }
}