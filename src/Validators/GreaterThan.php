<?php

namespace Barebone\Validators;

class GreaterThan extends Base
{
    private int $min;

    public function __construct(string $field, string $error_message, int $min) {
        parent::__construct($field, $error_message);
        $this->min = $min;
    }

    public function execute(array $data): bool {
        if ($this->isFieldNotEmpty($data)) {
            return $data[$this->field] > $this->min;
        }

        return true;
    }
}