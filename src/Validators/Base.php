<?php

namespace Barebone\Validators;

abstract class Base
{
    protected string $field = '';
    protected string $error_message = '';
    protected array $data = [];

    abstract public function execute(array $data);

    public function __construct(string $field, string $error_message) {
        $this->field = $field;
        $this->error_message = $error_message;
    }

    public function getErrorMessage(): string {
        return $this->error_message;
    }

    public function getField(): string {
        if (is_array($this->field)) {
            return $this->field[0];
        }

        return $this->field;
    }

    public function isFieldNotEmpty(array $data): bool {
        return isset($data[$this->field]) && $data[$this->field] !== '';
    }
}