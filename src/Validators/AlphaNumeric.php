<?php

namespace Barebone\Validators;

class AlphaNumeric extends Base
{
    public function execute(array $data): bool {
        if ($this->isFieldNotEmpty($data)) {
            return ctype_alnum($data[$this->field]);
        }

        return true;
    }
}