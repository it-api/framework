<?php

/*
  Creator Frank
 */

namespace Barebone\Validators;

/**
* Description of Punctuation
*
* @author Frank
*/
class Punctuation extends Base {

    public function execute(array $data): bool {
        if ($this->isFieldNotEmpty($data)) {
            return ctype_print($data[$this->field]);
        }

        return true;
    }
}