<?php

namespace Barebone\Validators;

class Range extends Base
{
    private int $min;
    private int $max;

    public function __construct(string $field, string $error_message, int $min, int $max) {
        parent::__construct($field, $error_message);

        $this->min = $min;
        $this->max = $max;
    }

    public function execute(array $data): bool {
        if ($this->isFieldNotEmpty($data)) {

            if (! is_numeric($data[$this->field])) {
                return false;
            }

            if ($data[$this->field] < $this->min || $data[$this->field] > $this->max) {
                return false;
            }
        }

        return true;
    }
}