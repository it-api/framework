<?php

namespace Barebone\Validators;

class Numeric extends Base
{
    public function execute(array $data): bool {
        if ($this->isFieldNotEmpty($data)) {
            return is_numeric($data[$this->field]);
        }

        return true;
    }
}