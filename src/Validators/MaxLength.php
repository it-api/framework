<?php

namespace Barebone\Validators;

class MaxLength extends Base
{
    private int $max;

    public function __construct(string $field, string $error_message, int $max) {
        parent::__construct($field, $error_message);
        $this->max = $max;
    }

    public function execute(array $data): bool {
        if ($this->isFieldNotEmpty($data)) {
            $length = mb_strlen($data[$this->field], 'UTF-8');
            return $length <= $this->max;
        }

        return true;
    }
}