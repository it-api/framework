<?php

namespace Barebone\Validators;

class Equals extends Base
{
    private string $field2;

    public function __construct(string $field1, string $field2, string $error_message) {
        parent::__construct($field1, $error_message);
        $this->field2 = $field2;
    }

    public function execute(array $data): bool {
        if ($this->isFieldNotEmpty($data)) {
            if (! isset($data[$this->field2])) {
                return false;
            }

            return $data[$this->field] === $data[$this->field2];
        }

        return true;
    }
}