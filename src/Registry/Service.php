<?php

namespace Barebone\Registry;


class Service {

	private string $name = '';
	private mixed $object = null;
	/**
	 * name
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}

	/**
	 * name
	 * @param string $name
	 * @return Service{
	 */
	public function setName(string $name) {
		$this->name = $name;
		return $this;
	}

	/**
	 * object
	 * @return string
	 */
	public function getObject(): mixed {
		return $this->object;
	}

	/**
	 * object
	 * @param string $object
	 * @return Service{
	 */
	public function setObject($object) {
		$this->object = $object;
		return $this;
	}
}
