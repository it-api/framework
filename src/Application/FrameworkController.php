<?php

namespace Barebone\Application;

use Barebone\Controller;
use Barebone\Application\Framework;
use Barebone\Router;
/**
 * Description of ApplicationFrameworkController
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource ApplicationFrameworkController.php
 * @since 20-jan-2017 22:41:49
 * 
 */
class FrameworkController extends Controller {
    
    /**
     * The applications framework
     * @var Framework
     */
    public $framework;
    
    /**
     * @overrides controller __construct()
     * Constructor
     */
    public function __construct() {        
        parent::__construct();   
        $router = Router::getInstance();
        $controller = $router->controller;
        $filename = FULL_APPS_PATH . $router->application . "/config/{$controller}_application_config.json";
        if(file_exists($filename)){
            $this->layout->app_json = json_decode(file_get_contents($filename));
            $this->framework = new Framework(); 
            $this->framework->setTheme($this->layout->app_json->application->theme);
        }
    }

    /**
     * @overrides controller __destruct()
     * On destruction all change to the framework are saved to the {controller}_application_config.json
     */
    function __destruct() {
        $this->layout->set_layout( $this->framework->getTheme() );
        parent::__destruct();
        $router = Router::getInstance();
        $controller = $router->controller;
        $filename = FULL_APPS_PATH . $router->application . "/config/{$controller}_application_config.json";
        if(file_exists($filename)){
            file_put_contents($filename, json_encode($this->layout->app_json));
        }
    }
}
