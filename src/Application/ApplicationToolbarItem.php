<?php

namespace Barebone\Application;

/**
 * Description of ApplicationToolbarItem
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource ApplicationToolbarItem.php
 * @since 20-jan-2017 20:18:40
 * 
 */
class ApplicationToolbarItem {
    
    /**
     * The text on the button
     * @var string
     */
    public $caption = '';
    /**
     * Type of button eg: primary default success
     * @var string 
     */
    public $type = '';
    /**
     * Icon on the button
     * @var string 
     */
    public $glyphicon = '';
    
    public $callback;
    /**
     * Child elements of this item
     * @var array 
     */
    public $children = [];
    
    public function __construct($caption, $type = 'default', $glyphicon = '', $callback = '') {
        $this->caption = $caption;
        $this->type = $type;
        $this->glyphicon = $glyphicon;
        $this->callback = $callback;
        return $this;
    }
    
    function addSubItem($caption, $callback){
        
        $item = new ApplicationToolbarItemChild($caption, $callback);
        $this->children[] = $item;
        return $item;
    }

}
