<?php

namespace Barebone\Application;

/**
 * Description of ApplicationContent
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource ApplicationContent.php
 * @since 21-jan-2017 0:56:07
 * 
 */
class ApplicationContent {
    
    /**
     * The title of the content blok
     * @var string 
     */
    public string $title = '';
    /**
     * A view object
     * @var \Barebone\View
     */
    public \Barebone\View $view;
    
    public function __construct(string $title, \Barebone\View $view, bool $use_panel = true) {
        
        $this->title = $title;
        $this->view = $view;
        
        if($use_panel){
            
            $wrapper    = new \Barebone\Form\Element('div', ['class'=>'panel panel-default']);
            $title      = new \Barebone\Form\Element('div', ['class'=>'panel-heading','innertext'=>$title]);
            $content    = new \Barebone\Form\Element('div', ['class'=>'panel-body', 'innertext'=>$view->get_html()]);
            
            $wrapper->addChild($title);
            
            $wrapper->addChild($content);

            $view->append($wrapper->get_html());
            
        }
    }

}
