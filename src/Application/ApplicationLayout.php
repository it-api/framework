<?php

/**
 * ApplicationLayout is a layout with north, east, west, south panels
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource ApplicationLayout.php
 * @since 20-jan-2017 20:15:11
 * 
 */

namespace Barebone\Application;

use Barebone\Application\ApplicationLayoutSection;
use Barebone\Application\ApplicationPanels;
use Barebone\Application\ApplicationListgroup;
use Barebone\Application\ApplicationToolbar;

class ApplicationLayout {
    
    /**
     * North panel holds the toolbar
     * @var ApplicationLayoutSection
     */
    public ApplicationLayoutSection $north;
    /**
     * East panel holds more panels
     * @var ApplicationPanels 
     */
    public ApplicationPanels $east;
    /**
     * West panel is a LayoutSection with a listgroup
     * @var ApplicationLayoutSection 
     * @access $this->west->listgroup
     */
    public ApplicationLayoutSection $west;
    /**
     * South panel 
     * @var ApplicationLayoutSection 
     */
    public ApplicationLayoutSection $south;
    
    /**
     * The constructor will create the four panels
     */
    public function __construct() {
        
        $this->north = new ApplicationLayoutSection();
        $this->north->toolbar = new ApplicationToolbar();
        
        $this->east = new ApplicationPanels();
        //$this->east->sidebar = new ApplicationPanels(); 
        
        $this->west = new ApplicationLayoutSection();
        $this->west->listgroup = new ApplicationListgroup();
        
        $this->south = new ApplicationLayoutSection();
    }

    /**
     * Get the north panel
     * @return ApplicationLayoutSection
     */
    public function getNorth(): ApplicationLayoutSection {
        return $this->north;
    }
    
    /**
     * Get the east panel
     * @return ApplicationPanels
     */
    public function getEast(): ApplicationPanels {
        return $this->east;
    }

    /**
     * Get the west panel
     * @return ApplicationLayoutSection
     */
    public function getWest(): ApplicationLayoutSection {
        return $this->west;
    }

    /**
     * Get the south panel
     * @return ApplicationLayoutSection
     */
    public function getSouth(): ApplicationLayoutSection {
        return $this->south;
    }

    public function setNorth(ApplicationLayoutSection $north) {
        $this->north = $north;
        return $this;
    }

    public function setEast(ApplicationPanels $east) {
        $this->east = $east;
        return $this;
    }

    public function setWest(ApplicationLayoutSection $west) {
        $this->west = $west;
        return $this;
    }

    public function setSouth(ApplicationLayoutSection $south) {
        $this->south = $south;
        return $this;
    }

}
