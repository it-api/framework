<?php

namespace Barebone\Application;

/**
 * Description of ApplicationListgroupItem
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource ApplicationListgroupItem.php
 * @since 20-jan-2017 21:47:08
 * 
 */
class ApplicationListgroupItem {
    
    public $glyphicon;
    public $badge;
    public $active;
    public $caption;
    public $callback;
    public $type;
    public $data;
    
    public function __construct($glyphicon, $badge, $active, $caption, $callback, $type) {
        $this->glyphicon = $glyphicon;
        $this->badge = $badge;
        $this->active = $active;
        $this->caption = $caption;
        $this->callback = $callback;
        $this->type = $type;
        return $this;
    }

    function setData($name, $value){
        $this->data[$name] = $value;
        return $this;
    }
}
