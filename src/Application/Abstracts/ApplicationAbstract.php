<?php

namespace Barebone\Application\Abstracts;

use Barebone\Application\Interfaces\IApplication;

abstract class ApplicationAbstract implements IApplication{
    
    protected string $name = '';
    
    public function getName(): string {
        return $this->name;
    }
    
    public function setName(string $name): void {
        $this->name = $name;
    }
}
