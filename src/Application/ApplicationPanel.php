<?php

namespace Barebone\Application;

/**
 * Description of ApplicationPanel
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource ApplicationPanel.php
 * @since 20-jan-2017 21:36:40
 * 
 */
class ApplicationPanel {
    
    public $title;//": "Panel settings",
    public $body;//": "This panel could hold a form for settings in you application",
    public $glyphicon;//": "envelope",
    public $type;//": "info"
    
    public function __construct($title, $body, $glyphicon, $type) {
        $this->title = $title;
        $this->body = $body;
        $this->glyphicon = $glyphicon;
        $this->type = $type;
    }

}
