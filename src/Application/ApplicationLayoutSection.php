<?php

namespace Barebone\Application;

/**
 * Description of ApplicationLayoutSection
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource ApplicationLayoutSection.php
 * @since 20-jan-2017 20:43:24
 * 
 */
class ApplicationLayoutSection {
    
    public $toolbar;
    public $listgroup;
}
