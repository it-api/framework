<?php

namespace Barebone\Application\Interfaces;

interface IApplication{
    
    function getName(): string;
    function setName(string $name);
    
}