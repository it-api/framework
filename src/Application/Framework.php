<?php

namespace Barebone\Application;

use Barebone\Application\FrameworkApplication;
/**
 * Description of Framework
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource Framework.php
 * @since 20-jan-2017 20:04:28
 * 
 */
class Framework {
    
    /**
     *
     * @var FrameworkApplication
     */
    public $application;
    
    public $theme = 'dark';
    
    public function __construct() {        
        $this->application = new FrameworkApplication();
    }

    function getToolbar(){
        return $this->application->getLayout()->getNorth()->toolbar;
    }
    
    function getPanels(){
        //debug($this->application->getLayout()->getEast());
        return $this->application->getLayout()->getEast();
    }
    
    function getListgroup(){
        return $this->application->getLayout()->getWest()->listgroup;
    }
    
    function addPanel($title, $body, $glyphicon, $type){
        return $this->getPanels()->addPanel($title, $body, $glyphicon, $type);
    }
    
    function addListItem($glyphicon, $badge, $active, $caption, $callback, $type){
        return $this->getListgroup()->addListItem($glyphicon, $badge, $active, $caption, $callback, $type);
    }
    
    function setApplicationName($name){
        $this->application->setName($name);
        return $this;
    }
    
    function setTheme($theme){
        $this->theme = $theme;                
    }
    
    function getTheme(){
        if($this->theme == 'dark'){
            return 'appframework';
        }elseif($this->theme == 'light'){
            return 'app-fw-light';
        }
    }
    
    function __toString(){        
        return json_encode($this, JSON_PRETTY_PRINT);
    }
}
