<?php

namespace Barebone\Application;

/**
 * Description of ApplicationListgroup
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource ApplicationListgroup.php
 * @since 20-jan-2017 21:46:53
 * 
 */
class ApplicationListgroup {
    
    public $items = [];
 
    function addListItem($glyphicon, $badge, $active, $caption, $callback, $type){
        $item = new ApplicationListgroupItem($glyphicon, $badge, $active, $caption, $callback, $type);
        $this->items[] = $item;
        return $item;
    }
}
