<?php

namespace Barebone\Application;

/**
 * Description of ApplicationToolbar
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource ApplicationToolbar.php
 * @since 20-jan-2017 20:17:14
 * 
 */
class ApplicationToolbar {
    
    /**
     * Will hold the toolbar items
     * @var array
     */
    public $items = [];
    
    function addItem($caption, $type = 'default', $glyphicon = '', $callback = ''){
        $item = new ApplicationToolbarItem($caption, $type, $glyphicon, $callback);
        $this->items[] = $item;
        return $item;
    }
    
    public function getItems() {
        return $this->items;
    }

    public function setItems($items) {
        $this->items = $items;
        return $this;
    }


}
