<?php

namespace Barebone\Application;

/**
 * Description of ApplicationPanels
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource ApplicationPanels.php
 * @since 20-jan-2017 21:35:42
 * 
 */
class ApplicationPanels {
    
    public $panels = [];
    
    function addPanel($title, $body, $glyphicon, $type){
        $this->panels[] = new ApplicationPanel($title, $body, $glyphicon, $type);
    }
    
    function getPanelByIndex($index){
        return $this->panels[$index];
    }
}
