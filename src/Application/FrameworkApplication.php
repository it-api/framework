<?php

namespace Barebone\Application;

use Barebone\Application\ApplicationLayout;
/**
 * Description of FrameworkApplication
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource FrameworkApplication.php
 * @since 20-jan-2017 20:25:53
 * 
 */
class FrameworkApplication {
    
    /**
     * The name of the application
     * @var string 
     */
    public $name;
    /**
     * 
     * @var ApplicationLayout
     */
    public $layout;
    
    public function __construct() {
        
        $this->setLayout( new ApplicationLayout() );
        
    }
    
    public function getLayout(): ApplicationLayout {
        return $this->layout;
    }

    public function setLayout(ApplicationLayout $layout) {
        $this->layout = $layout;
        return $this;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    
}
