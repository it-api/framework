<?php

namespace Barebone\Application;

/**
 * Description of ApplicationToolbarItemChild
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource ApplicationToolbarItemChild.php
 * @since 20-jan-2017 20:57:20
 * 
 */
class ApplicationToolbarItemChild {
    
    public $caption;
    
    public $callback;
    
    public $data;
    
    public function __construct($caption, $callback) {
        $this->caption = $caption;
        $this->callback = $callback;
        return $this;
    }
    
    function setData($name, $value){
        $this->data[$name] = $value;
        return $this;
    }

}
