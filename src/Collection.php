<?php

namespace Barebone;

use Barebone\Collection\Exceptions\KeyInvalidException;

/**
 * Description of Collection
 * MYFW 
 * UTF-8
 *
 * @author Frank
 * @filesource Collection.php
 * @since 15-jan-2017 20:23:26
 * 
 */
class Collection implements \JsonSerializable {

    private array $items = array();
    const SORT_ASC = 'ASC';
    const SORT_DESC = 'DESC';


    /**
     * Add an item to the items array
     * @param mixed $obj
     * @param string $key
     * @throws KeyInvalidException
     */
    public function addItem(mixed $obj, string $key = null) {

        if ($key === null) {

            $this->items[count($this->items)] = $obj;
        } else {

            if (isset($this->items[$key])) {
                throw new KeyInvalidException("Key $key already in use.");
            } else {
                $this->items[$key] = $obj;
            }
        }
    }
    /**
     * Remove item from the items array
     * @param string $key
     * @throws KeyInvalidException
     */
    public function deleteItem(string $key) {
        if (isset($this->items[$key])) {
            unset($this->items[$key]);
        } else {
            throw new KeyInvalidException("Invalid key $key.");
        }
    }
    /**
     * Get item from the items array
     * @param string $key
     * @throws KeyInvalidException
     */
    public function getItem(string $key) {
        if (isset($this->items[$key])) {
            return $this->items[$key];
        } else {
            throw new KeyInvalidException("Invalid key $key.");
        }
    }
    /**
     * Get all items in the collection
     * @return array
     */
    function getItems(): array {
        return $this->items;
    }
    /**
     * Set all items in the collection
     * @param array $items
     * @return $this
     */
    function setItems(array $items = array()) {
        $this->items = $items;
        return $this;
    }
    /**
     * Clear all items in the collection
     * @return $this
     */
    function clear() {
        unset($this->items);
        return $this;
    }
    /**
     * Return a list of all key of the items
     * @return array
     */
    public function keys(): array {
        return array_keys($this->items);
    }
    /**
     * Return the number of items in the collection
     * @return int
     */
    public function length(): int {
        return count($this->items);
    }
    /**
     * Check if a key exists
     * @param type $key
     * @return type
     */
    public function keyExists($key): bool {
        return isset($this->items[$key]);
    }
    /**
     * Sort the items in the collection
     * @param string $direction ASC|DESC [Collection::SORT_ASC | Collection::SORT_DESC]
     * @return $this
     */
    public function sort($direction = self::SORT_ASC) {
        // check if sortorder is valid ASC / DESC allowed
        if ($direction !== self::SORT_ASC && $direction !== self::SORT_DESC) {
            throw new Exception('Invalid sort direction');
        }

        if ($direction === self::SORT_ASC) {
            arsort($this->items);
        } elseif ($direction === self::SORT_DESC) {
            asort($this->items);
        }
        return $this;
    }
    /**
     * Reverse the order of the items in the collection
     * @param bool $preserve_keys
     * @return $this
     */
    function reverse($preserve_keys = true) {
        $this->items = array_reverse($this->items, $preserve_keys);
        return $this;
    }
    function jsonSerialize(): mixed {
        return $this->toJSONObject();
    }
    /**
     * Associative array always output as object
     * @return JSON string
     * @throws Exception
     */
    function toJSONArray() {
        $json = json_encode($this->items);
        if (is_null($json)) {
            throw new Exception('Error converting array to jsonArray');
        }
        return $json;
    }
    /**
     * Associative array always output as object
     * @return type
     * @throws Exception
     */
    function toJSONObject() {
        $json = json_encode($this->items, JSON_FORCE_OBJECT);
        if (is_null($json)) {
            throw new Exception('Error converting array to jsonObject. NULL');
        }
        return $json;
    }
}
