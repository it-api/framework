<?php

namespace Barebone;

/**
 * FixedArray is an array of fixed size
 *
 * @author Frank
 */
class FixedArray extends \SplFixedArray implements \IteratorAggregate, \ArrayAccess, \Countable, \JsonSerializable{
    
    public function getIterator(): \Iterator {
        return $this->toArray();
    }

    public function jsonSerialize(): array {
        return $this->toArray();
    }

}