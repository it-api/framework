<?php

namespace Barebone\Database;

class Columntype{
	
	public string $column_type = '';
	public array $columntypes = array(
			
			'string' => 'VARCHAR(128)',
			'text' => 'TEXT',
			'int' => 'INT(11)',
			'float' => 'FLOAT(10,2)',
			'decimal' => 'DECIMAL(10,2)',
			'bool' => 'TINYINT(1)',
			'datetime' => 'DATETIME',
			'blob' => 'BLOB',
			'set' => "SET('%s')",
			'enum' => "ENUM('%s')",
			'reference' => ''			
	);
	
	function set_options( string $type, array $options = [] ){

		$this->column_type = sprintf($this->columntypes[$type], implode("','", $options));
		return($this->column_type);
	}
	
	function __construct( string $type, array $options = [] ){

		if(array_key_exists($type, $this->columntypes)){
			
			if($type === 'set' || $type === 'enum'){
				
				$this->column_type = $this->set_options( $type, $options );
				
			}else{
				
				$this->column_type = $this->columntypes[$type];
				
			}
			
		}else{ 
			
			$this->column_type = $type;
		}
		
		return $this->column_type;
	}
	
	function __toString(){
		
		return (string) $this->column_type;
	}
}
