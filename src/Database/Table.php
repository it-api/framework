<?php

namespace Barebone\Database;

use Barebone\Database\Column;
use Barebone\Exception;
use Barebone\Database;
use Barebone\Database\Connectors\PDO;

class Table {

    public $db;

    /**
     * The name of the table
     * @var string
     */
    private $tablename = '';

    /**
     * The description of the table
     * @var string
     */
    private $description = '';

    /**
     * Valid JSON string representing the table columns
     * @var mixed
     */
    private $schema = NULL;

    /**
     * Database engine type
     * INNODB, MyISAM, MEMORY, ARCHIVE
     * @var string
     */
    private $engine = 'INNODB';

    /**
     * Database encoding type (Collatie)
     * @var string
     */
    private $charset = 'utf8';

    /**
     * array for all the indexes in this table
     * @var array
     */
    private $indexes = array();

    /**
     * array with the columns in this table
     * @var array
     */
    private $columns = array();

    /**
     * default columns that get created
     * @var array
     */
    private $default_columns = array();

    function __construct($options) {

        $this->db = new Database();
        $columns = array();
        $options = \Barebone\Functions::array_to_object($options);
        // when the option file is given then it will lookup the file and load from that
        if (isset($options->file)) {
            if (!file_exists($options->file)) {
                throw new Exception('File "' . $options->file . '" not found');
            }
            $file_schema = file_get_contents($options->file);
            $options = json_decode($file_schema);
        }


        if (empty($options->name)) {
            throw new Exception('A tablename is mandatory');
        }

        $this->tablename = $options->name; // the name for the table
        $this->description = isset($options->description) ? $options->description : $this->description; // the comment for the table
        $this->schema = is_string($options->schema) ? json_decode($options->schema) : $options->schema; // the column definitions for the table
        $this->engine = isset($options->engine) ? $options->engine : $this->engine;
        $this->indexes = isset($options->indexes) ? $options->indexes : array();
        $this->default_columns = array(
            new Column('id', 'INT(11)', [], 'Unique id for this table', true, false, null, true),
            new Column('created_at', 'TIMESTAMP', [], 'Timestamp on creation', false, false, "DEFAULT '0000-00-00 00:00:00'"),
            new Column('updated_at', 'TIMESTAMP', [], 'Timestamp on update', false, false, "DEFAULT '0000-00-00 00:00:00'"),
            new Column('revision', 'TIMESTAMP', [], 'Timestamp on revision', false, false, "DEFAULT '0000-00-00 00:00:00'"),
            new Column('links', 'TEXT', [])
        );
        foreach ($this->schema as $column) {

            foreach ($this->default_columns as $key => $object) {

                foreach ($object as $propertie => $value) {

                    if (isset($column->name) && $column->name === $object->name) {

                        throw new Exception('Cannot use this columnname "' . $column->name . '", it is reserverd');
                    }
                }
            }

            $columns[] = new Column(
                $column->name,
                $column->type,
                $column->options,
                $column->comment,
                $column->null,
                $column->default,
                $column->auto_increment
            );
        }

        $this->columns = array_merge($this->default_columns, $columns);
    }

    /**
     * Get the JSON representation of the schema
     * @return string
     */
    function get_json() {

        $schema = array(
            'name' => $this->tablename,
            'description' => $this->description,
            'engine' => $this->engine,
            'charset' => $this->charset,
            'schema' => $this->columns
        );

        return json_encode($schema, JSON_PRETTY_PRINT);
    }

    /**
     * Create this table object in the database
     * @param string $database
     * @throws Exception
     */
    function create_in_database(string $database = '') {

        $columns_text = implode(",\n", $this->columns);
        $database_text = strlen($database) > 0 ? "`$database`." : '';
        $sql = "\nCREATE DATABASE IF NOT EXISTS `$database`;";
        $sql .= "\nCREATE TABLE IF NOT EXISTS {$database_text}`{$this->tablename}` (
		$columns_text,
		PRIMARY KEY ( `id` )
		) ENGINE = INNODB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1 COMMENT='{$this->description}';\n";


        if (count($this->indexes) > 0) {
            foreach ($this->indexes as $index) {
                $sql .= "ALTER TABLE `{$this->tablename}` ADD INDEX ( `{$index}` ) ;\n";
            }
        }
        try {
            echo $sql;
            $this->db->pdo->exec($sql);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Merge this table into an axisting table in the database
     * @param string $database
     * @throws Exception
     */
    function merge_in_database($database = '') {

        // create empty string for SQL
        $sql = '';
        // if the database is passes in we will use it to build the query
        $database_text = strlen($database) > 0 ? "`$database`." : '';
        // go over the schema columns
        foreach ($this->schema as $column) {
            // if the columns has an attribute options pass it to the Columntype constructor
            if (isset($column->options)) {
                $type = new Columntype($column->type, $column->options);
            } else {
                $type = new Columntype($column->type);
            }
            // build the query
            $count_sql = "SELECT COUNT( * ) AS Total_cols
				FROM INFORMATION_SCHEMA.COLUMNS
				WHERE TABLE_NAME = '{$this->tablename}'
				AND COLUMN_NAME = '{$column->name}' ;";
            // put the count query in a try catch block                    
            try {
                // prepare the statement
                $stmt2 = $this->db->pdo->prepare($count_sql);
                // execute it
                $stmt2->execute();
                // get result as an object
                $nrrows = $stmt2->fetchAll(PDO::FETCH_OBJ);
                // close the cursor
                $stmt2->closeCursor();
            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }

            // alter the table and add the missing data
            if ($nrrows[0]->Total_cols === 0) {
                $sql = "ALTER TABLE {$database_text}`{$this->tablename}` ADD `{$column->name}` {$type} {$column->null} {$column->comment};";

                try {
                    $stmt3 = $this->db->pdo->prepare($sql);
                    $stmt3->execute();
                    $nrrows = $stmt2->fetchAll(PDO::FETCH_ASSOC);
                    $stmt3->closeCursor();
                } catch (Exception $e) {
                    throw new Exception($e->getMessage());
                }
            }
        }
        // add indexes to the table
        if (count($this->indexes) > 0) {
            foreach ($this->indexes as $index) {
                $sql = "ALTER TABLE {$database_text}`{$this->tablename}` ADD INDEX ( `$index` ) ;\n";
                try {

                    $stmt4 = $this->db->pdo->prepare($sql);
                    $stmt4->execute();
                    $nrrows = $stmt2->fetchAll(PDO::FETCH_ASSOC);
                    $stmt4->closeCursor();
                } catch (Exception $e) {
                    throw new Exception($e->getMessage());
                }
            }
        }
    }
}
