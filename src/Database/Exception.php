<?php

namespace Barebone\Database;

/**
 * Description of Exception
 *
 * @author Frank
 */
class Exception extends \Barebone\Exception{
	
	public function __construct($message) {

		parent::__construct($message);
		
	}

}
