<?php

namespace Barebone\Database\Connectors;

use Barebone\Config;

/**
 * Barebone PDO Database connector
 * @author Frank
 *
 */
class PDO extends \PDO {

    function __construct($host, $user, $pass, $name) {

        $dsn = "mysql:host={$host};dbname={$name};charset=utf8";

        $options = null;

        try {
            parent::__construct($dsn, $user, $pass, $options);
            $this->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);

            $config = new Config();
            if ($config->getValue('application', 'debug') === true) {
                $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
                $this->exec("SET NAMES 'utf8'");
                $this->exec("SET CHARACTER SET utf8");
                $this->exec("SET CHARACTER_SET_CONNECTION=utf8");
            }
        } catch (\PDOException $exc) {
            echo $exc->getTraceAsString();
        }

    }

}
