<?php

namespace Barebone\Database;

/**
 * Description of Queryresult
 *
 * @author Frank
 */
class Queryresult {
	
	public array $rows = [];
	public ?object $row = null;
	public int $num_rows = 0;
	
	public function __construct(array $rows, \PDOStatement $query) {
		
		$this->setRow( isset($rows[0]) ? $rows[0] : null );
        $this->setRows( $rows );
        $this->setNum_rows( $query->rowCount() );
		
	}
	
	public function getRows(): array {
		return $this->rows;
	}

	public function getRow() {
		return $this->row;
	}

	public function getNum_rows(): int {
		return $this->num_rows;
	}

	public function setRows(array $rows) {
		$this->rows = $rows;
		return $this;
	}

	public function setRow($row) {
		$this->row = $row;
		return $this;
	}

	public function setNum_rows(int $num_rows) {
		$this->num_rows = $num_rows;
		return $this;
	}



}
