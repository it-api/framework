<?php

namespace Barebone\Database;
/**
 * INSERT INTO users SET name = ''
 * SELECT * FROM Users WHERE Id = 1
 * UPDATE Users SET name = '' WHERE id = 1;
 * DELETE FROM Users WHERE id = 1
 */
 
/**
 * Example code
 */
/*
    $qb = new Querybuilder();
    // INSERT
    $sql = $qb->insert()
              ->into('Users')
              ->set(['name' => 'Tester']);
    echo $sql . "\n";
    
    // SELECT
    $sql = $qb->select('Users.id')
              ->from('Users')
              ->join('Orders')
              ->on('Users.id', 'Orders.Userid')
              ->where('Users.id', 1)
              ->or_where('Users.id', 2)
              ->and_where('Users.id','<', 3)
              ->orderby('Users.id DESC')
              ->limit(0,10)
              ;
    echo $sql. "\n";
    
    // UPDATE
    $sql = $qb->update('Users')
              ->set(['name' => 'Tester'])
              ->where('Users.id', 1);
    echo $sql . "\n";
    
    // DELETE
    $sql = $qb->delete()
              ->from('Users')
              ->where('Users.id', '>', 1);
    echo $sql . "\n";
*/

class Querybuilder {
	
	/**
	 * This will hold the build statement
	 */
	private string $statement = '';
	
	public function __toString(): string{
	    return $this->statement;
	}
	
	private function get_type($value){
	    if(is_numeric($value)){
	        return $value;
	    }elseif(is_string($value)){
	        return "'$value'";
	    }
	}
	
	public function insert(){
	    $this->statement = 'INSERT';
	    return $this;
	}
	
	public function into( string $table ){
	    $this->statement .= ' INTO ' . $table;
	    return $this;
	}
	
	public function set($values){
	    $this->statement .= ' SET ';
	    foreach ($values as $column => $value) {
	         $this->statement .=  $column . '=' . $this->get_type($value) . ',';
	    }
	    // take the last comma off the statment
	    $this->statement = rtrim($this->statement,',');
	    return $this;
	}
	
	public function select(string $columns = '*'){
	    $this->statement = 'SELECT ' . $columns;
	    return $this;
	}
	
	public function from( string $table ){
	    $this->statement .= ' FROM ' . $table;
	    return $this;
	}
	
	public function where($column, $value){
	    if(func_num_args() === 3){
	        $c = $column;
	        $d = $value;
	        $v = func_get_arg(2);
	        $this->statement .= ' WHERE ' . $c . $d . $this->get_type($v);
	    }elseif(func_num_args() === 2){
	        $this->statement .= ' WHERE ' . $column . '=' . $this->get_type($value);
	    }
	    return $this;
	}
	
	public function or_where($column, $value){
	    if(func_num_args() === 3){
	        $c = $column;
	        $d = $value;
	        $v = func_get_arg(2);
	        $this->statement .= ' OR ' . $c . $d . $this->get_type($v);
	    }elseif(func_num_args() === 2){
	        $this->statement .= ' OR ' . $column . '=' . $this->get_type($value);
	    }
	    return $this;
	}
	
	public function and_where($column, $value){
	    if(func_num_args() === 3){
	        $c = $column;
	        $d = $value;
	        $v = func_get_arg(2);
	        $this->statement .= ' AND ' . $c . $d . $this->get_type($v);
	    }elseif(func_num_args() === 2){
	        $this->statement .= ' AND ' . $column . '=' . $this->get_type($value);
	    }
	    
	    return $this;
	}
	
	public function update(string $table){
	    $this->statement = 'UPDATE ' . $table;
	    return $this;
	}
	
	public function delete(){
	    $this->statement = 'DELETE';
	    return $this;
	}
	
	public function orderby(string $order){
	    $this->statement .= ' ORDER BY ' . $order;
	    return $this;
	}
	
	public function groupby(string $groupby){
	    $this->statement .= ' GROUP BY ' . $groupby;
	    return $this;
	}
	
	public function having(string $having){
	    $this->statement .= ' HAVING ' . $having;
	    return $this;
	}
	
	public function limit($start, $offset = null){
	    if(is_null($offset)){
	        $this->statement .= ' LIMIT ' . $start;
	    }else {
	        $this->statement .= ' LIMIT ' . $start . ', ' . $offset;
	    }
	    return $this;
	}
	
	public function join(string $table, string $type = ''){
	    if(!is_null($type)){
	        $this->statement .= ' ' . $type . ' JOIN ' . $table;
	    } else {
	        $this->statement .= ' JOIN ' . $table;
	    }
	    return $this;
	}
	
	public function on(string $local, string $foreign){
	    $this->statement .= ' ON ' . $local .'='.  $foreign;
	    return $this;
	}
}

