<?php

namespace Barebone\Database;

use Barebone\Database\Columntype;

class Column{
	
	public string $name = '';
	public string $type = 'VARCHAR(128)';
	public bool $primary = false;
	public string $null = 'NOT NULL';
	public ?string $default = '';
	public bool $auto_increment = false;
	public string $comment = '';
	public $validation = null;
	
	function __construct(
	    string $name, 
	    string $type = '', 
	    array $options = [], 
	    string $comment = '', 
	    bool $primary = false, 
	    bool $null = false, 
	    ?string $default = '', 
	    bool $auto_increment = false, 
	    $validation = ''
	){
	
		$this->name = $name;
		$this->type = (string)new Columntype( $type, $options );
		$this->comment = strlen($comment) > 0 ? "COMMENT '$comment'" : '';
		$this->primary = $primary;
		$this->null = $null === true ? '' : 'NOT NULL';
		$this->default = strlen((string) $default) > 0 ? "$default" : "";
		$this->auto_increment = $auto_increment === true ? 'AUTO_INCREMENT' : '';
		// if a validation is given we use this  but it does not belong in a db column ...
		if($validation != ''){
			$this->validation = $validation;
        }
		return $this;
	}
	
	function __toString(){
		
		if($this->primary){
			return "`{$this->name}` {$this->type} {$this->null} {$this->default} {$this->auto_increment} {$this->comment}";
        } else {
			return "`{$this->name}` {$this->type} {$this->null} {$this->default} {$this->comment}";
        }
	}
}
