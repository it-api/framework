<?php

namespace Barebone\Database\Schema;

//use Barebone\Database\Column;
use Barebone\Exception;
use Barebone\Database\Table;
use Barebone\Database;
use Barebone\Functions;

class Schema {

    public Database $db;

    /**
     * The name of the table
     * @var string
     */
    private string $database = '';

    /**
     * Valid JSON string representing the table columns
     * @var string
     */
    private $schema = NULL;

    /**
     * Database encoding type (Collatie)
     * @var string
     */
    private string $charset = 'utf8';
    
    private array $default_tables = array();
    private array $tables = array('fubar' => '');

    /**
     * Schema constructor
     * @param type $options
     * @throws Exception
     */
    function __construct(array $options) {

        $this->db = new Database();

        $options = Functions::array_to_object($options);
        // when the option file is given then it will lookup the file and load from that
        if (isset($options->file)) {
            if (!file_exists($options->file)) {
                throw new Exception('File "' . $options->file . '" not found');
            }
            $file_schema = file_get_contents($options->file);
            $options = json_decode($file_schema);
        }


        if (empty($options->name)) {
            throw new Exception('A database name is mandatory');
        }

        $this->database = $options->name; // the name for the table
        $this->schema = is_string($options->schema) ? json_decode($options->schema) : $options->schema; // the column definitions for the table
        $this->charset = $options->charset;
        foreach ($this->schema as $table) {

            foreach ($this->default_tables as $key => $object) {

                foreach ($object as $propertie => $value) {

                    if (isset($table->name) && $table->name === $object->name) {

                        throw new Exception('Cannot use this tablename "' . $table->name . '", it is reserverd');
                    }
                }
            }

            $tables[] = new Table($table);
        }

        $this->tables = array_merge($this->default_tables, $tables);
    }

    /**
     * Create this object in the database
     * @throws Exception
     */
    function create_in_database() {

        $sql = "\nCREATE DATABASE IF NOT EXISTS `{$this->database}`;";
        try {
            $this->db->pdo->exec($sql);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
        /** @var $table Table */
        foreach ($this->tables as $table) {
            $table->create_in_database($this->database);
        }
    }

    /**
     * Merge the table in this schema into the database
     */
    function merge_in_database() {
        /** @var $table Table */
        foreach ($this->tables as $table) {
            $table->merge_in_database($this->database);
        }
    }

}
