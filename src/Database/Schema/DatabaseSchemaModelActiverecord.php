<?php

namespace Barebone\Database\Schema;
use Barebone\Config;

class DatabaseSchemaModelActiverecord{
	
	private string $schema_path;
	private string $schema_file;
	private string $schema_json;
	private object $schema_data;
	
	function __construct( string $activerecord ){
		
		$dbname = Config::getInstance()->getValue('database', 'dbname');
		$this->schema_path = FULL_BAREBONE_PATH . "models/schemas/{$dbname}/";
		$this->schema_file = $this->schema_path . $activerecord . '.json';
		$this->schema_json = file_get_contents($this->schema_file);
		$this->schema_data = json_decode($this->schema_json);
		
	}
}
