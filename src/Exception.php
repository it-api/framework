<?php

namespace Barebone;

/**
 * Basic Barebone exception
 * @author Frank
 */
class Exception extends \Exception {

	function __construct($message, $code = 0, \Exception $previous = null, $path = null) {

		$config = Config::getInstance();
		$request = new Request();
		if ($config->getValue('application', 'production') === "false") {

			if ($request->is_ajax()) {
				\Barebone\jQuery::init();
				\Barebone\jQuery::evalScript("Show_Dialog('Oeps!', 'Er is een error opgetreden: " . addslashes($message) . ". De technische dienst is op de hoogte gesteld.', 'error', ['Ok'])");
				\Barebone\jQuery::getResponse();
			} else {
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Exception</title>

	<!-- the meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- the styles -->
	<link href="/css/bootstrap/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css">
	<link href="/css/styles.css" media="screen" rel="stylesheet" type="text/css">
	<link href="/css/bootstrap/css/bootstrap-responsive.min.css" media="screen" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="/js/google-code-prettify/prettify.css">
	<?php if (stristr($config->getValue('barebone-gui', 'skin'), 'dark') || stristr($config->getValue('barebone-gui', 'skin'), 'black')) : ?>
		<link rel="stylesheet" type="text/css" href="/js/google-code-prettify/theme-black.css">
		<style>
			body {
				background-color: gray
			}
		</style>
	<?php endif; ?>
	<!-- the scripts -->
	<script type="text/javascript" src="/js/jquery/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="/js/bootstrap/bootstrap.min.js"></script>

	<style>
		.trace {
			font-size: 14px;
		}
	</style>
</head>

<body>
	<div class="container">
		<div class="row">
			<?php echo '<pre class="prettyprint lang-php">' . $message . '</pre>'; ?>
			<pre class="trace prettyprint"><?= $this->getTraceAsString() ?></pre>
		</div>
	</div>
	<script src="/js/google-code-prettify/prettify.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {

			// make code pretty
			window.prettyPrint && prettyPrint()

		});
	</script>
</body>

</html>
<?php
			}
			exit();
		} else {

			// inform somebody of the error
			// TODO: CREATE A LOGBOOK FOR THIS
		}
	}
}
