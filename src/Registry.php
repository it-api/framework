<?php

namespace Barebone;

use Barebone\Registry\Service;

class Registry {

    /**
     * Registred services
     * @var array
     */
    private static array $services = [];

    /**
     * Registred values
     * @var array
     */
    private static array $values = [];

    /**
     * The registry filename
     * @var string
     */
    private static string $filename = '';

    /**
     * The registry instance
     * @var self
     */
    protected static $instance = null;

    protected function __construct() {
        //Thou shalt not construct that which is unconstructable!
    }

    protected function __clone() {
        //Me not like clones! Me smash clones!
    }

    public static function getInstance(): self {

        if (!isset(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public static function addService(string $name, mixed $object, $overwrite = false): self {

        if (self::hasService($name) && $overwrite === false){
            throw new \Barebone\Registry\ServiceException(sprintf('The object "%s" is allready in the registry.', $name));
        }
        
        $service = new Service();
        $service->setName($name);
        $service->setObject($object);

        self::$services[$service->getName()] = $service->getObject();

        return self::getInstance();
    }

    /**
     * Check if service exists
     * $param string $name
     * @return bool
     */
    public static function hasService(string $name): bool{
        return isset(self::$services[$name]);
    }
    /**
     * Get the service
     * $param string $name
     * @return bool
     */
    public static function getService(string $name): mixed {

        if (!self::hasService($name)){
            throw new \Barebone\Registry\ServiceException(sprintf('The object "%s" doesn\'t exist in the registry.', $name));
        }
        return self::$services[$name];
    }

    /**
     * services
     * @return array
     */
    public static function getServices(): array {
        return self::$services;
    }

    /**
     * services
     * @param array $services
     * @return Service{
     */
    public static function setServices(array $services): self {
        self::$services = $services;
        return self::getInstance();
    }

    /**
     * values
     * @return array
     */
    public static function getValues(): array {
        return self::$values;
    }

    /**
     * values
     * @param array $values
     * @return Service{
     */
    public static function setValues(array $values) {
        self::$values = $values;
        return self::getInstance();
    }

    /**
     * filename
     * @return string
     */
    public static function getFilename(): string {
        return self::$filename;
    }

    /**
     * filename
     * @param string $filename
     * @return Registry{
     */
    public static function setFilename(string $filename) {
        self::$filename = $filename;
        return self::getInstance();
    }
    
    public static function saveToDisk(string $filename = ''): bool{
        
        if(self::getFilename() === '' && $filename === ''){
            throw new \Exception('No filename set. Set Filename with ::setFilename() first.');
        }
        
        if($filename !== ''){
            self::setFilename($filename);
        }
        
        $data = [];
        $data['services'] = [];
        foreach(self::getServices() as $name => $object){
            try{
                $data['services'][$name] = get_class($object);
            } catch(\Exception $e){
                debug($e);
            }
        }
        if(file_put_contents(self::getFilename(), json_encode($data))!==false){
            return true;
        } else {
            return false;
        }

    }
    
    public static function loadFromDisk(string $filename = ''): bool{
        
        if(self::getFilename() === '' && $filename === ''){
            throw new \Exception('No filename set. Set Filename with ::setFilename() first.');
        }
        
        if($filename !== ''){
            self::setFilename($filename);
        }
        
        $json = file_get_contents(self::getFilename());
        $data = json_decode($json);
        foreach($data->services as $name => $object){
            self::addService(name: $name, object: (new $object), overwrite: true);
        }
        
        return true;
    }

}
