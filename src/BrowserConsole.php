<?php

namespace Barebone;

class BrowserConsole{
    
    private static function write(mixed $data, $where = 'log'){
        if(is_array($data)){
            echo '<script>console.'.$where.'('.(new Javascript\PHPArrayToJSArray($data)).');</script>';
        } elseif(is_object($data)){
            $classname = get_class($data);
            echo '<script>'.(new Javascript\PHPClassToJSObject($classname)).';console.'.$where.'('.$classname.');</script>';
        } elseif(is_string($data)) {
            echo '<script>console.'.$where.'("'.$data.'");</script>';
        }
    }
    
    public static function log(mixed $data){
        self::write($data);
    }
    
    // They are almost identical as log - the only difference is that debug messages 
    //are hidden by default in recent versions of Chrome (you have to set the log level to Verbose in the Devtools topbar while in console to see debug messages; log messages are visible by default).
    public static function debug(mixed $data){
        self::write($data, 'debug');
    }
    
    public static function error(mixed $data){
        self::write($data, 'error');
    }
    
    public static function info(mixed $data){
        self::write($data, 'info');
    }
    
    public static function exception(mixed $data){
        self::write($data, 'exception');
    }
    
    public static function table(mixed $data){
        self::write($data, 'table');
    }
    
    public static function time(){
        echo '<script>console.time();</script>';
    }
    
    public static function timeEnd(){
        echo '<script>console.timeEnd();</script>';
    }
    
    public static function clear(){
        echo '<script>console.clear();</script>';
    }
    
    public static function alert(string $data){
        echo '<script>alert("'.$data.'");</script>';
    }
}