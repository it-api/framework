<?php

namespace Barebone;

use Barebone\Assets\Types\Stylesheet;
use Barebone\Assets\Types\Javascript;
use Barebone\Assets\Types\Metatag;
use Barebone\Assets\Types\Link;

/**
 * Barebone Assets manager helper to load css, js and metatags
 * @author frank
 *
 */
class Assets_manager {

    /**
     * Assets collection
     * @var array
     */
    public $assets = [];
    /**
     * Assets collection for before end body tag
     * @var array
     */
    public $assets_body = [];
    /**
     * Compress all assets (minify)
     */
    private $compressed = false;

    function __construct() {
        
    }

    function __destruct() {
        
    }
    /**
     * Compress all assets
     * @param bool $compress
     */
    function compress(bool $compress) {
        $this->compressed = $compress;
    }
    /**
     * Load an asset by its URI
     * @param string $filename
     * @example ->load_asset('/jquery.js'); // add javascript
     */
    function load_asset(string $filename) {

        if (substr($filename, strlen($filename) - 3, 3) == '.js') {

            $this->add_javascript($filename);
        }

        if (substr($filename, strlen($filename) - 4, 4) == '.css') {

            $this->add_stylesheet($filename);
        }
    }
    /**
     * Load all files from a certain path
     * @param string $path
     * @example ->load_from_path('/app/assets/'); // loads al files from the path
     */
    function load_from_path(string $path) {

        $handle = opendir(FULL_PUBLIC_PATH . $path);
        if ($handle) {

            while (false !== ($filename = readdir($handle))) {

                if ($filename != '.' && $filename != '..') {

                    $this->load_asset($path . $filename);
                }
            }

            closedir($handle);
        }
    }

    /**
     * Add a metatag to the assets array
     * @param array $attributes
     */
    function add_meta_tag($attributes = array()) {

        $this->assets[] = new Metatag($attributes);
    }

    function add_javascript($filename, $position = 'head', $defer = false, $async = false) {
        
        if($position !== 'head' && $position !=='body'){
            $position = 'head';
        }
        
        if($position === 'head'){
            $this->assets[] = new Javascript($filename, $defer, $async);
        } elseif($position === 'body'){
            $this->assets_body[] = new Javascript($filename, $defer, $async);
        }
    }

    function add_stylesheet($filename, $position = 'head') {
        
        if($position !== 'head' && $position !=='body'){
            $position = 'head';
        }
        
        if($position === 'head'){
            $this->assets[] = new Stylesheet($filename);
        } elseif($position === 'body'){
            $this->assets_body[] = new Stylesheet($filename);
        }
    
    }
    
    function add_link($attributes = array()){
        $this->assets[] = new Link($attributes);
    }

    function render($position = 'head') {
        
        if($position === 'head'){
            
            if (count($this->assets) > 0) {
                foreach ($this->assets as $asset) {
    
                    $asset->render();
                }
            }
            
        } elseif($position === 'body'){
            
            if (count($this->assets_body) > 0) {
                foreach ($this->assets_body as $asset) {
    
                    $asset->render();
                }
            }
            
        }
    }
    
    // CSS Minifier => http://ideone.com/Q5USEF + improvement(s)
    function minify_css($input) {
        if(trim($input) === "") return $input;
        return preg_replace(
            array(
                // Remove comment(s)
                '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')|\/\*(?!\!)(?>.*?\*\/)|^\s*|\s*$#s',
                // Remove unused white-space(s)
                '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/))|\s*+;\s*+(})\s*+|\s*+([*$~^|]?+=|[{};,>~]|\s(?![0-9\.])|!important\b)\s*+|([[(:])\s++|\s++([])])|\s++(:)\s*+(?!(?>[^{}"\']++|"(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')*+{)|^\s++|\s++\z|(\s)\s+#si',
                // Replace `0(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)` with `0`
                '#(?<=[\s:])(0)(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)#si',
                // Replace `:0 0 0 0` with `:0`
                '#:(0\s+0|0\s+0\s+0\s+0)(?=[;\}]|\!important)#i',
                // Replace `background-position:0` with `background-position:0 0`
                '#(background-position):0(?=[;\}])#si',
                // Replace `0.6` with `.6`, but only when preceded by `:`, `,`, `-` or a white-space
                '#(?<=[\s:,\-])0+\.(\d+)#s',
                // Minify string value
                '#(\/\*(?>.*?\*\/))|(?<!content\:)([\'"])([a-z_][a-z0-9\-_]*?)\2(?=[\s\{\}\];,])#si',
                '#(\/\*(?>.*?\*\/))|(\burl\()([\'"])([^\s]+?)\3(\))#si',
                // Minify HEX color code
                '#(?<=[\s:,\-]\#)([a-f0-6]+)\1([a-f0-6]+)\2([a-f0-6]+)\3#i',
                // Replace `(border|outline):none` with `(border|outline):0`
                '#(?<=[\{;])(border|outline):none(?=[;\}\!])#',
                // Remove empty selector(s)
                '#(\/\*(?>.*?\*\/))|(^|[\{\}])(?:[^\s\{\}]+)\{\}#s'
            ),
            array(
                '$1',
                '$1$2$3$4$5$6$7',
                '$1',
                ':0',
                '$1:0 0',
                '.$1',
                '$1$3',
                '$1$2$4$5',
                '$1$2$3',
                '$1:0',
                '$1$2'
            ),
        $input);
    }
    
    // JavaScript Minifier
    function minify_js($input) {
        if(trim($input) === "") return $input;
        return preg_replace(
            array(
                // Remove comment(s)
                '#\s*("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')\s*|\s*\/\*(?!\!|@cc_on)(?>[\s\S]*?\*\/)\s*|\s*(?<![\:\=])\/\/.*(?=[\n\r]|$)|^\s*|\s*$#',
                // Remove white-space(s) outside the string and regex
                '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/)|\/(?!\/)[^\n\r]*?\/(?=[\s.,;]|[gimuy]|$))|\s*([!%&*\(\)\-=+\[\]\{\}|;:,.<>?\/])\s*#s',
                // Remove the last semicolon
                '#;+\}#',
                // Minify object attribute(s) except JSON attribute(s). From `{'foo':'bar'}` to `{foo:'bar'}`
                '#([\{,])([\'])(\d+|[a-z_][a-z0-9_]*)\2(?=\:)#i',
                // --ibid. From `foo['bar']` to `foo.bar`
                '#([a-z0-9_\)\]])\[([\'"])([a-z_][a-z0-9_]*)\2\]#i'
            ),
            array(
                '$1',
                '$1$2',
                '}',
                '$1$3',
                '$1.$3'
            ),
        $input);
    }
    

}
