<?php

namespace Barebone;

/**
 * A pair is used by Map keys with values.
 * MYFW 
 * UTF-8
 *
 * @author Frank
 * @filesource Pair.php
 * @since 15-jan-2017 22:57:40
 * 
 */
class Pair implements \JsonSerializable {
    
    private $name;
    private $value;
    /**
     * 
     * @param string $name
     * @param mixed $value
     */
    public function __construct($name, $value) {
        $this->name = $name;
        $this->value = $value;
    }
    
    public function getName() {
        return $this->name;
    }

    public function getValue() {
        return $this->value;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setValue($value) {
        $this->value = $value;
        return $this;
    }

    public function jsonSerialize():mixed {
        return \json_encode($this->toArray(), JSON_FORCE_OBJECT);
    }
    
    public function toArray(){
        return ['name' => $this->getName(), 'value' => $this->getValue()];
    }

}
