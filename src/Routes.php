<?php

namespace Barebone;

use Barebone\Router\RouterExeption;
use Barebone\JSON;

class Routes{

    private array $routes = [];

    function __construct(){

    }

    function addRoute(string $uri, $callback): void {
        $this->routes[$uri] = $callback;
    }

    function routeExists(string $uri): bool {
        return isset($this->routes[$uri]);// || preg_match('/^\/[a-z0-9-_]+/', $uri, $output_array) === 1;
    }

    function getRoute(string $uri) {
        return $this->routes[$uri];
    }

    function getDispatchRoute(string $uri){
        $route = $this->getRoute($uri);
        return isset($route)
            ? str_replace('/$1', $uri, $route->destination)
            : str_replace('/$1', $uri, $this->getRoute('/^/[a-z0-9-_]+/')->destination);
    }

    function load_from_disk(){
        $filename = FULL_BAREBONE_PATH . 'config/routes.json';
        if(!file_exists($filename)){
            throw new RouterExeption('config/routes.json does not exist.');
        }
        $json = new JSON();
        $json->load_from_disk($filename);

        foreach($json->getData()->routes as $route){
            $this->addRoute($route->route, $route);
        }
    }
}
