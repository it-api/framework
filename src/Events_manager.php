<?php

namespace Barebone;
/*
//use Barebone\Filesystem\Exception\FileNotFoundException;
class CheckAccessRights{
    function authenticated(AuthUser $user){
        if($user->id != 1){
            die('Geen toegang');
        }
    }
}
class AuthUser{
    public $id = 1;
}
class BareboneEvent{
    function hello(){echo 'hello';}
}

class Event_listener_class{
    private $identifier;
    private $classname;
    private $action;
}

class Event_listener{
    private $event;
    private $command;
    public function __construct($event, $command) {
        $this->event = $event;
        $this->command = $command;
    }
    function getEvent(){
        return $this->event;
    }
    function getCommand(){
        return $this->command;
    }
}
class Event_dispatcher{
    function dispatch($args = array()){ echo 'something'; print_r($args); }
}
*/
class Events_manager{
	
        /**
        * @var array $registered_events The registered events
        */
        private $registered_events = array();
        /**
        * @var array $handled_events The handled events during execution
        */
        private $handled_events = array();        
        /**
        * @var array $unregistered_events The unregistered events during execution
        */
        private $unregistered_events = array();
        /**
         *
         * @var string The last executed event
         */
	private $last_event;
	/**
        * @var array $observers The registered observers
        */
	private $observers = array();
        
        function __construct() {
            //$router = Router::getInstance();
            //$this->listen($router->getRouteDotted(), array(new Event_dispatcher(), 'dispatch'));
        }
        
        function __destruct() {
            //$router = Router::getInstance();
            //$authuser = new AuthUser;
            //$this->dispatch($router->getRouteDotted(), array(new Component()));
            //debug($this);
        }
	
	/** 
	 * create event listener
	 * @example
	 * 	use a listener and dispatch to a method in a class
	 * 	$controller->events_manager->listen('switch_service_switch', array(new Switch_service(), 'SwitchClient'));
	 * 	create a workflow
	 * 	$controller->events_manager->listen('register_user', 'UserSignUp');
	 * 	$controller->events_manager->listen('register_user', 'UserCreateAccount');
	 * 	$controller->events_manager->listen('register_user', 'UserSendWelcomeMail');
	 * 	$controller->events_manager->listen('register_user', 'IncreaseStatsUsers');
	 * 
	 * @param string $event the event that we listen for
	 * @param mixed array|string $callback what happens when we have the event
	 */
	public function listen($event, $callback){
            
            $this->observers[$event][] = new Event_listener($event,$callback);
            $this->registered_events[] = $event;
            
            return $this;
                
	}
        /**
         * Remove event listener
         * @param type $event
         * @return $this
         */
        public function detach($event){
            
            if(isset($this->observers[$event])){
                unset($this->observers[$event]);
            }
            
            $this->unregistered_events[] = $event;
            
            return $this;
        }
	
	/**
	 * @example
	 * 
	 *  $client = new stdClass;
	 *  $client->Username = 'Rabuf';
	 *  $client->Password = 'Fubar';  
	 *  $controller->events_manager->dispatch('register_user', $client);
	 *  
	 * @param string $event the event to dispatch
	 * @param mixed array|string|object $param
	 * @return boolean
	 */
	public function dispatch($event, $param){

                if(!isset($this->observers[$event])) {
                    return false;                    
                }
                
                $this->handled_events[] = $event;
                $this->last_event = $event;
		
		foreach ($this->observers[$event] as $listener){

			if(is_array($listener)){
				// first item is an object?
				if(is_object($listener[0])){

					$class_name = get_class($listener[0]);
					$method_name = $listener[1];
					if(class_exists($class_name)){

						$object = new $class_name;

						if(method_exists($object, $method_name)){
							$object->$method_name($param) ;
						}
					}
                                        
				}
				
			}elseif(is_array($param)){
				
				call_user_func_array($listener->getCommand(), $param);
				
			}else{
				
				call_user_func_array($listener->getCommand(), array($param));
			}
		}
	}
	
	function load_from_disk( $filename ){
		
                if(!file_exists($filename)) {
                    throw new Exception( sprintf('File: "%s" not found!', $filename) );                
                }
		
		$json = file_get_contents($filename);
		$object = json_decode($json);
		foreach($object->listeners as $listener){
			$this->listen($listener->event, $listener->callback);
		}
	}

}
