<?php

namespace Barebone;

use Barebone\Router\Route;
use Barebone\Routes;
use Barebone\Router\RouterExeption;

//use Barebone\ACL;

/**
 * This class routes the request to the apropriate controller
 * @author Frank
 *
 */
class Router {

    /**
     * Singleton of the router
     * @var self Router
     */
    public static $router;

    /**
     * $_SERVER['BASE_PATH'] exploded into an array by /
     * @var array $_SERVER['BASE_PATH']
     */
    public $request;

    /**
     * Request method
     * @var string $request_method posible values (GET / POST / OPTIONS / HEAD / DELETE)
     */
    public string $request_method = 'GET';

    /**
     * Requested application
     * This is the first part of a uri
     * @link /test 
     * @var string $application
     */
    public string $application;

    /**
     * Requested controller 
     * This is the second part of a uri
     * @link /test/index
     * @var string $controller
     */
    public string $controller;

    /**
     * Requested action
     * This is the third part of a uri
     * @link /test/index/action
     * @var string $action
     */
    public string $action;

    /**
     * Requested parameters
     * These are every other after the third part of a uri
     * @link /test/index/action/param1/param2/param[n]
     * @var array $params
     */
    public array $params = array();

    /**
     * The actual route that we follow
     * @var Route
     */
    public $route;

    /**
     * The route before a reroute
     * @var Route
     */
    public $requested_route;

    /**
     * The default application if none is found in the request<br>
     * You can configure this in the config/application.json file<br>
     * In the section [application] > default_application
     * @var string
     */
    private string $default_application;

    /**
     * The default controller if none is found in the request<br>
     * You can configure this in the config/application.json file<br>
     * In the section [application] > default_controller
     * @var string
     */
    private string $default_controller;

    /**
     * The default action if none is found in the request<br>
     * You can configure this in the config/application.json file<br>
     * In the section [application] > default_action
     * @var string
     */
    private string $default_action;

    /**
     * This points to the location of the current application
     * if you run /installer/index/index/ this points to FULL_APPS_PATH/installer/
     * @var string
     */
    private string $_current_application_path;

    /**
     * This points to the location of the current application
     * if you run /installer/index/index/ this points to FULL_APPS_PATH/installer/controllers
     * @var string
     */
    private string $_current_application_controllers_path;

    /**
     * This points to the location of the current application
     * if you run /installer/index/index/ this points to FULL_APPS_PATH/installer/views
     * @var string
     */
    private string $_current_application_views_path;

    /**
     * This points to the location of the current application
     * if you run /installer/classbuilder/index/ this points to FULL_APPS_PATH/installer/views/classbuilder
     * @var string
     */
    private string $_current_application_action_view_path;

    private string $applications_path;
    
    public Routes $defined_routes;

    /**
     * Get the singleton Router
     * @return \Barebone\Router
     */
    public static function getInstance() {
        if (!isset(self::$router)) {
            self::$router = new self();
        }
        return self::$router;
    }

    /**
     * Router contructor
     */
    function __construct() {

        // get the config object
        $config = Config::getInstance();

        //set the default application
        $this->default_application = $config->getValue('application', 'default_application');
        //set the default controller
        $this->default_controller = $config->getValue('application', 'default_controller');
        //set the default action
        $this->default_action = $config->getValue('application', 'default_action');
        //store the $_SERVER['REQUEST_METHOD'] in the request_method propertie of the router class
        $this->request_method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';
        
        $routes_filename = FULL_BAREBONE_PATH . 'config/routes.json';
        if(file_exists($routes_filename)){
            $this->defined_routes = new Routes();
            $this->defined_routes->load_from_disk();
        }
        
        if (isset($_SERVER['PATH_INFO'])) {

            // use the PATH_INFO as the route
            $this->route = new Route($_SERVER['PATH_INFO']);
            $_GET['route'] = $_SERVER['PATH_INFO'];
        } elseif (isset($_GET['route'])) {

            // use the $_GET['route'] parameter as the route
            $this->route = new Route($_GET['route']);
        } else {

            // use the default route from application.json
            $this->route = new Route('/' . $this->default_application . '/' . $this->default_controller . '/' . $this->default_action);
        }

        //remove last trailing slash
        $this->route->setPath(rtrim($this->route->getPath(), '/'));
        //store the PATH_INFO in the request propertie of the router class
        $this->request = isset($this->route) ? explode("/", substr($this->route->getPath(), 1)) : array(0 => '', 1 => '', 2 => '');
        //the first item in the array is the application name. If none is found we use the default application
        $this->application = $this->request[0] ?? $this->default_application;
        // the second item in the array is the controller name else default controller
        $this->controller = $this->request[1] ?? $this->default_controller;
        //the third item is the action to take else default action
        $this->action = $this->request[2] ?? $this->default_action;
        // set the current application path
        $this->_current_application_path = FULL_APPS_PATH . $this->application . '/';
        // set the current application controllers path
        $this->_current_application_controllers_path = $this->_current_application_path . 'controllers/';
        // set the current application views path
        $this->_current_application_views_path = $this->_current_application_path . 'views/';
        // set the current application view action path
        $this->_current_application_action_view_path = $this->_current_application_views_path . $this->controller . '/';
        //the rest is parameters that are passed to the action
        for ($x = 3; $x < count($this->request); $x++) {
            $this->params[] = $this->request[$x];
        }

    }

    function get_application_action_view_path() {
        return $this->_current_application_action_view_path;
    }

    /**
     * function to check if a route is possible<br>
     * In the application.json file you can change the [application] debug option<br>
     * When debug is enabled it will show you as developer what is wrong.<br>
     * If debug is disabled it will return false and the application will show a 404 page
     * @throws RouterExeption
     * @return bool
     */
    function can_resolve_route() {

        // get config instance
        $config = Config::getInstance();
        $filename = FULL_BAREBONE_PATH . 'config/routes.json';
        if(file_exists($filename)){
            return true;
        }
        // find applications folder
        $this->applications_path = FULL_BAREBONE_PATH . $config->getValue('application', 'applications_path');
        if (!is_dir($this->applications_path)) {

            // if debugging is enabled we throw an exeption
            if ($config->getValue('application', 'debug') === true) {
                throw new RouterExeption(sprintf('The applications directory does not exist.'));
            }
            return false;
        }

        // find the application
        $application = $this->applications_path . $this->application . '/';
        if (!is_dir($application)) {

            // if debugging is enabled we throw an exeption
            if ($config->getValue('application', 'debug') === true) {
                throw new RouterExeption(sprintf('The application "%s" does not exists in the applications directory.', $this->application));
            }
            return false;
        }

        // find the controllers directory of this application
        $controllers = $application . 'controllers/';
        if (!file_exists($controllers)) {

            // if debugging is enabled we throw an exeption
            if ($config->getValue('application', 'debug') === true) {
                throw new RouterExeption(sprintf('The controllers directory does not exist in the application "%s".', $this->application));
            }
            return false;
        }

        // find the controller
        $controller = $controllers . $this->controller . 'Controller.php';
        if (!file_exists($controller)) {

            // if debugging is enabled we throw an exeption
            if ($config->getValue('application', 'debug') === true) {
                throw new RouterExeption(sprintf('The controller "%sController" does not exist in the directory of the application "%s".', $this->controller, $application));
            }
            return false;
        }

        
        require_once($controller);
        $classname = $this->controller . 'Controller';
        $action = $this->action . 'Action';
        $ajax = $this->action . 'AJAX';
        $json = $this->action . 'JSON';
        $jquery = $this->action . 'JQUERY';
        $xml = $this->action . 'XML';

        // does the method exists in the controller
        if (
            !method_exists($classname, $action)
            && !method_exists($classname, $ajax)
            && !method_exists($classname, $json)
            && !method_exists($classname, $jquery)
            && !method_exists($classname, $xml)
        ) {

            // if debugging is enabled we throw an exeption
            if ($config->getValue('application', 'debug') === true) {
                throw new RouterExeption(
                    sprintf(
                        'The %s does not exist in the class: %s or is not callable',
                        $action,
                        $classname
                    )
                );
            }
            return false;
        }

        // return true if al the above is oke
        return true;
    }

    function getRouteDotted() {
        return \str_replace('/', '.', \ltrim($this->route->getPath(), '/'));
    }
}
