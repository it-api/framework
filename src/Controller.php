<?php

namespace Barebone;

/**
 * If your controller extends from this controller it has all kinds of stuff loaded automagicly<br>
 * You have access to the config, router, layout, view and database from your controller
 * @author Frank
 *
 */

use Barebone\jQuery;

class Controller {

    /**
     * Configuration object
     * @var Config $config
     */
    public Config $config;

    /**
     * Router object exposed to the active controller
     * @var Router $router
     */
    public Router $router;

    /**
     * This is your layout object
     * @var Layout $layout
     */
    public Layout $layout;

    /**
     * This is the current view object
     * @var View $view
     */
    public View $view;

    /**
     * This is your database object
     * @var Database $db
     */
    public Database $db;
    
    /**
     * The current request from the client
     * @var Request $request
     */
    public Request $request;

    /**
     * Automagicly render the view.
     * Set to false if you render the view yourself from within your controller.
     * @var bool $auto_render
     * @example
     * $this->set_auto_render(false);<br/>
     * // do some important stuff here
     * $this->layout->render(); // will render the layout with the view<br/>
     * $this->view->render(); // will render only the view<br/>
     */
    public bool $auto_render = true;

    /**
     * This variable will hold your appmodel in either your application/models diectory
     * or in the global/models directory
     * @example create a new application > add model > application model
     * @var mixed object
     */
    public mixed $appmodel = null;

    /**
     * This variable will hold your controller model in either your application/models diectory
     * or in the global/models directory
     * @example create a new application > add model > controller model
     * @var mixed object
     */
    public mixed $model = null;
    /**
     * Adds Application, Controller Models if exist
     * this will load a application/model with the same name as the application into the appmodel propertie
     * this will give all controllers in an app access to this model
     */
    private function applyModels(): void{
        
        $app_name = $this->router->application;
        $appmodel_name = $app_name . 'Model';
        $controller_name = $this->router->controller;
        $model_name = $controller_name . 'Model';
        $app_model_name = FULL_APPS_PATH . $app_name . '/models/' . $app_name . 'Model.php';
        if (file_exists($app_model_name)) {
            $this->appmodel = new $appmodel_name($this);
        }

        // this will load a application/model with the same name as the controller into the model propertie
        // this will give all controllers in an app access to this model
        $app_model_name = FULL_APPS_PATH . $app_name . '/models/' . $controller_name . 'Model.php';
        if (file_exists($app_model_name)) {
            $this->model = new $model_name($this);
        }

        // this will load a global/model with the same name as the application into the appmodel propertie
        // BUT ONLY IF THERE IS NO APPLICATION MODEL		
        $controller_model_name = FULL_BAREBONE_PATH . 'models/' . $app_name . 'Model.php';
        if (file_exists($controller_model_name) && !isset($this->appmodel)) {
            $this->appmodel = new $appmodel_name($this);
        }

        // this will load a global/model with the same name as the controller into the model propertie
        // BUT ONLY IF THERE IS NO APPLICATION MODEL
        $controller_model_name = FULL_BAREBONE_PATH . 'models/' . $controller_name . 'Model.php';
        if (file_exists($controller_model_name) && !isset($this->model)) {
            $this->model = new $model_name($this);
        }
    }
    /**
     * Controller contructor.
     * This class is for you to extend from with your controllers.
     * Here you put your application wide functions that you want your controllers to have
     */
    function __construct() {

        $this->config = Config::getInstance();
        $this->router = Router::getInstance();
        $this->view = new View();
        $this->layout = new Layout($this->view);
        $this->db = new Database();
        $this->request = new Request();
        
        Registry::addService('db', $this->db, true);
        
        if($this->config->getValue('CSP', 'enabled') === 'true'){
            header(
                sprintf("Content-Security-Policy: %s", 
                    str_replace('{nonce}',$this->layout->nonce,
                    $this->config->getValue('CSP', 'header')))
                /*  
                "Content-Security-Policy: "
                //."default-src 'none'; " // By default we will deny everything
                ."object-src 'none'; "
                ."base-uri 'none'; "
                ."require-trusted-types-for 'script'; "
                ."script-src 'unsafe-eval'; script-src 'nonce-".$this->layout->nonce."'; 'unsafe-inline'; "
                */
            );
        }
        
        $this->applyModels();

        // this executes the init function in the controller if it exists. 
        if (method_exists($this, 'init')) {
            $this->init();
        }
    }

    /**
     * De destuctor of the controller will render the layout or view, depending on the precence of a layout.
     */
    function __destruct() {

        // if auto rendering is enabled
        if ($this->auto_render === true && ob_get_length() === 0) {

            // and we have a valid layout path
            if ($this->config->getValue('layout', 'layouts_path') != '') {

                if ($this->config->getValue('cache', 'enabled') == 'true') {
                    echo Cache::read($this->layout->get_html());
                } else {

                    /**
                     * A canonical tag, also known as a "rel=canonical tag," 
                     * is an HTML element used to indicate to search engines 
                     * where a page's unique and original content is located, 
                     * especially when there is similar or identical content 
                     * appears on multiple pages of a website.
                     */

                    // automagicly add canonical metatag to layout for /{app}/index/index/
                    if ($this->router->controller === 'index' && $this->router->action === 'index') {
                        $this->layout->add_link([
                            'rel' => 'canonical',
                            'href' => DOMAIN_NAME . "/{$this->router->application}/"
                        ]);
                        // automagicly add canonical metatag to layout for /{app}/{method}/index/
                    } elseif ($this->router->action === 'index') {
                        $this->layout->add_link([
                            'rel' => 'canonical',
                            'href' => DOMAIN_NAME . "/{$this->router->application}/{$this->router->controller}/"
                        ]);
                    }
                    
                    try{
                        
                        $this->layout->render();
                        
                    } catch(\Barebone\Layout\Exceptions\LayoutNotFoundException $e){}
                }
            } else {

                if ($this->config->getValue('cache', 'enabled') === 'true') {
                    echo Cache::read($this->view->get_html());
                } else {
                    $this->view->render();
                }
            }
        }

        /**
         * This might be in the Controller to clean up
         */
        if (method_exists($this, 'terminate')) {
            $this->terminate();
        }
    }

    /**
     * Turn autorender on or off
     * @param bool $bool
     */
    function set_auto_render(bool $bool) {
        $this->auto_render = $bool;
    }

    /**
     * Returns the auto_rendering status
     * @return boolean true if $this->auto_render is enabled
     */
    function get_auto_render(): bool {
        return $this->auto_render;
    }

    /**
     * Return a segment from a relative URL
     * @param integer $index
     * @param string $default_value
     */
    function segment(int $index, string $default_value = null) {

        return $this->request->getSegment($index, $default_value);
    }

    /**
     * This function calls a controller’s action from any location and returns data from the action.
     * The $url passed is a relative URL (/applicationname/controllername/actionname/params).
     * To pass extra data to the receiving controller action add to the $options array.
     * @param string $route relative URL
     * @param array $options
     * @return mixed|boolean
     */
    function requestAction(string $route, array $options = []) {

        $route_array = explode('/', $route);

        $application = $route_array[0];
        $controller = ($route_array[1] . 'Controller');
        $action = $route_array[2];

        $file = FULL_BAREBONE_PATH . "applications/{$application}/controllers/{$controller}.php";

        if(!file_exists($file)){
            throw new \Exception(sprintf('Controller %s does not exist.', $controller));
        }
        
        require($file);
        $class = new $controller();
        if (method_exists($class, $action . 'Action')) {
            return (call_user_func_array(array($class, $action . 'Action'), $options));
        } elseif (method_exists($class, $action)) {
            return (call_user_func_array(array($class, $action), $options));
        } else {
            throw new \Exception(sprintf(
                'Cannot dispatch to requestAction %s. Method %s does not exist in controller %s.', 
                $route, 
                $action, 
                $controller
            ));
        }
    }

    /**
     * Print out human friendly debugging information
     * @param mixed $data array|object
     * @param boolean $vardump use vardump instead of print_r
     */
    function debug(mixed $data, bool $vardump = false) {
        $this->auto_render = false;
        echo '<h2>Debugging is enabled further proccessing stopped</h2>'
            . '<p>You will not see the output of your code if you have this function enabled.'
            . '<pre class="prettyprint linenumbers">';
        if ($vardump) {
            var_dump($data);
        } else {
            print_r($data);
        }
        if (is_object($data)) {
            echo 'Functions in the class: "' . get_class($data) . '"<br/>';
            print_r(get_class_methods(get_class($data)));
        }
        echo '</pre>';
    }

    /**
     * Output your data as json.
     * Will output the proper headers and then echo the json code.
     * @param mixed $data data to encode as json
     * @param integer $options json options
     */
    function response_json(mixed $data, mixed $options = null) {

        $this->set_auto_render(false);
        $response = new Response();
        $response->json($data, $options);
    }

    /**
     * Output your data as download file.
     * Will output the proper headers and then offer the download.
     * @param string $path the name of the file
     */
    function response_file(string $path) {

        $this->set_auto_render(false);
        $response = new Response();
        $response->file($path);
    }

    function ShowNotification(string $msg, int $autoClose = 5000) {
        \Barebone\jQuery::init();
        \Barebone\jQuery::evalScript('Show_Notification("' . $msg . '", "' . $autoClose . '")');
        \Barebone\jQuery::getResponse();
    }

    function showZebraDialog(string $title, string $message, string $type) {
        \Barebone\jQuery::init();
        \Barebone\jQuery::evalScript('Show_Dialog("' . $title . '", "' . $message . '", "' . $type . '")');
        \Barebone\jQuery::getResponse();
    }

    function showEasyUIDialog(string $title, string $message, string $msgtype) {
        \Barebone\jQuery::init();
        $dialog_id = uniqid('dialog-');
        \Barebone\jQuery('body')->append(
            '<div style="padding:20px !important;max-width:800px !important;" title="' . $title . '" id="' . $dialog_id . '" class="alert alert-' . $msgtype . '">' . $message . '</div>'
        );
        \Barebone\jQuery('#' . $dialog_id)->dialog();
        \Barebone\jQuery('#' . $dialog_id)->dialog('open');
        \Barebone\jQuery::getResponse();
    }
    
    function showEasyUIMessagerAlert(string $title, string $message, string $icon = 'info', $width = 300){
        // icon : error,question,info,warning.
        \Barebone\jQuery::init();
        \Barebone\jQuery::evalScript('$.messager.alert({title: "'.$title.'", msg: "'.$message.'", icon: "'.$icon.'", width: '.$width.'})');
        \Barebone\jQuery::getResponse();
    }

    /**
     * Show a jquery dialog
     * This is for use in a $.php ajax response
     * @dependancie jqueryui needs to be in your page
     * @param string $title The title for the dialog
     * @param string $message The message in the dialog
     * @param string $msgtype The type of dialog
     */
    function showDialog(string $title, string $message, string $msgtype = 'warning') {

        \Barebone\jQuery::init();
        $dialog_id = uniqid('dialog-');
        //jQuery("#dialog_form_helper")->html('<div title="'.$title.'" id="dialog" class="alert alert-'.$msgtype.'"><strong>'.$title.'</strong> '.$message.'</div>');
        \Barebone\jQuery('body')->append('<div title="' . $title . '" id="' . $dialog_id . '" class="alert alert-' . $msgtype . '">' . $message . '</div>');
        \Barebone\jQuery('div#' . $dialog_id . '')->dialog(array('modal' => true));
        \Barebone\jQuery::evalScript('$( "#' . $dialog_id . '" ).dialog( "option", "buttons", 
  [
    {
      text: "Ok",
      icon: "ui-icon-check",
      click: function() {
        $( this ).dialog( "close" );
      }
 
      // Uncommenting the following line would hide the text,
      // resulting in the label being used as a tooltip
      //showText: false
    }
  ]
);');
        jQuery::getResponse();

        exit();
    }

    /**
     * $buttons = array();
     * $buttons['Ok'] = array(
     * 		'callback' => 'ok', 
     * 		'class' => 'btn-default'
     * );
     * $buttons['Sluiten'] = array(
     * 		'callback' => 'sluiten', 
     * 		'class' => 'btn-default'
     * );
     * @param string $title
     * @param string $message
     * @param array $buttons see above
     */
    function showModal(string $title, string $message, array $buttons = [], string $type = 'success') {

        \Barebone\jQuery::init();

        $dialog_id = uniqid('dialog-');

        $btns = '';

        if ($buttons) {

            foreach ($buttons as $button => $settings) {
                $btns .= '<button onclick="' . $settings['callback'] . '" type="button" class="btn ' . $settings['class'] . '" ' . ($button == 'Sluiten' || $button == 'Close' ? 'data-dismiss="modal"' : '') . '>' . $button . '</button>';
            }
        } else {
            $btns .= '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
        }


        $footer = '<div class="modal-footer">' . $btns . '</div>';

        \Barebone\jQuery('body')->append('<div id="' . $dialog_id . '" class="modal fade"><div class="modal-dialog modal-' . $type . '"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title">' . $title . '</h4></div><div class="modal-body"><p>' . $message . '</p></div>' . $footer . '</div><!-- /.modal-content --></div><!-- /.modal-dialog --></div><!-- /.modal -->');
        \Barebone\jQuery('div#' . $dialog_id . '')->modal();



        jQuery::getResponse();
    }
    /**
     * This helps creating the filters on your activercord
     * @param \Barebone\Activerecord $form_target
     * @example
     * First create a form from activerecord
     * Post th form to the controller and catch the post request
     * In your controller
     * if($this->request->is_post()){
     *     $this->create_form_filters(new BackboneConfig);
     *     // this will show a dialog where you van cconfigure your filter and validators
     *     // you get code to replace this piece of code
     * }
     */
    function create_form_filters(\Barebone\Activerecord $form_target) {

        // open output buffer
        ob_start();
        // create the dialog with the settings
        if ($this->request->is_post()) :
?>
            <div id="dialog-form-helper" title="Form 2 Activerecord connector">
                <p>
                    This is a helper tool to create form filters. 
                    Instead of <a target="_blank" href="https://www.php.net/manual/en/filter.filters.validate.php">reading the docs</a>
                    everytime you need to figure out how filter was called. You can select you filters here.
                </p>
                <h3>Posted data</h3>
                <table class="ui-table" style="width:100%">
                    <tr>
                        <td>Field name</td>
                        <td>Field value</td>
                    </tr>

                    <?php foreach ($_POST as $key => $value) : ?>
                        <tr>
                            <!-- file deepcode ignore XSS: <Is feature not a bug> -->
                            <td><?= $key ?></td>
                            <td>
                                <?php if (is_object($value) || is_array($value)) : ?>

                                    <table class="ui-table" style="width:100%">
                                        <tr>
                                            <td>Field name</td>
                                            <td>Field value</td>
                                        </tr>

                                        <?php foreach ($value as $k => $v) : ?>
                                            <tr>
                                                <td><?= $k ?></td>
                                                <td><?= $v ?></td>
                                            </tr>
                                        <?php endforeach; ?>

                                    </table>

                                <?php else : ?>
                                    <?= $value ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                </table>
                <div id="jquery-tabs">
                    <ul>
                        <li>
                            <a href="#tabs-1">Sanitize filters</a>
                        </li>

                        <li>
                            <a href="#tabs-2">Validate filters</a>
                        </li>

                        <li>
                            <a href="#tabs-3">Target</a>
                        </li>

                        <li>
                            <a href="#tabs-4">Code</a>
                        </li>
                    </ul>

                    <div id="tabs-1">
                        <h3>Assign sanitizers</h3>
                        <table class="ui-table" style="width:100%">
                            <tr>
                                <td>Post field</td>
                                <td>Filter</td>
                                <td>Flags</td>

                            </tr>
                            <?php foreach ($_POST as $key => $value) : ?>
                                <tr>
                                    <td><?= $key ?></td>
                                    <td>
                                        <select id="FILTER_<?= $key ?>" onchange="
                                        $('#FLAGS_<?= $key ?> option:selected').removeAttr('selected');
                                        $('#FLAGS_<?= $key ?> option.flags').hide();
                                        $('#FLAGS_<?= $key ?> option.' + $('#FILTER_<?= $key ?> :selected').text()).show();
                                        if(this.value == 'FILTER_CALLBACK'){
                                            $('#FILTER_CALLBACK_<?= $key ?>').show();
                                        } else {
                                            //$('.filter-options').hide();
                                            $('#FILTER_CALLBACK_<?= $key ?>').hide();
                                            
                                        }
                                        updatecode();">
                                            <option value="FILTER_DEFAULT">default</option>
                                            <option title="Remove all characters except letters, digits and !#$%&'*+-=?^_`{|}~@.[]." value="FILTER_SANITIZE_EMAIL">email</option>
                                            <option title="URL-encode string, optionally strip or encode special characters." value="FILTER_SANITIZE_ENCODED">encoded</option>
                                            <option title="Apply addslashes()." value="FILTER_SANITIZE_MAGIC_QUOTES">magic_quotes</option>
                                            <option title="Remove all characters except digits, +- and optionally .,eE. " value="FILTER_SANITIZE_NUMBER_FLOAT">number_float</option>
                                            <option title="Remove all characters except digits, plus and minus sign. " value="FILTER_SANITIZE_NUMBER_INT">number_int</option>
                                            <option title="HTML-escape ' ''<>& and characters with ASCII value less than 32, optionally strip or encode other special characters." value="FILTER_SANITIZE_SPECIAL_CHARS">special_chars</option>
                                            <option title="Equivalent to calling htmlspecialchars() with ENT_QUOTES set." value="FILTER_SANITIZE_FULL_SPECIAL_CHARS">full_special_chars</option>
                                            <option title="Alias of 'string' filter." value="FILTER_SANITIZE_STRIPPED">stripped</option>
                                            <option title="Remove all characters except letters, digits and $-_.+!*'(),{}|\\^~[]`<>#%&quot;;/?:@&=. " value="FILTER_SANITIZE_URL">url</option>
                                            <option title="Do nothing, optionally strip or encode special characters. This filter is also aliased to FILTER_DEFAULT." value="FILTER_UNSAFE_RAW">unsafe_raw</option>
                                            <option title="Call user-defined function to filter data." value="FILTER_CALLBACK">callback</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select multiple="multiple" id="FLAGS_<?= $key ?>" onchange="updatecode();">
                                            <option title="Strips characters that have a numerical value &lt; 32." class="flags encoded special_chars string unsafe_raw">FILTER_FLAG_STRIP_LOW</option>
                                            <option title="Strips characters that have a numerical value &gt; 127." class="flags encoded special_chars string unsafe_raw">FILTER_FLAG_STRIP_HIGH</option>
                                            <option title="Strips backtick ` characters." class="flags encoded special_chars string unsafe_raw">FILTER_FLAG_STRIP_BACKTICK</option>
                                            <option title="Encodes all characters with a numerical value &lt; 32. " class="flags encoded string unsafe_raw">FILTER_FLAG_ENCODE_LOW</option>
                                            <option title="Encodes all characters that have a numerical value &gt; 127." class="flags encoded special_chars string">FILTER_FLAG_ENCODE_HIGH</option>
                                            <option title="Allows a period (.) as a fractional separator in numbers. " class="flags number_float">FILTER_FLAG_ALLOW_FRACTION</option>
                                            <option title="Allows a comma (,) as a thousands separator in numbers." class="flags number_float">FILTER_FLAG_ALLOW_THOUSAND</option>
                                            <option title="Allows an e or E for scientific notation in numbers. " class="flags number_float">FILTER_FLAG_ALLOW_SCIENTIFIC</option>
                                            <option title="If this flag is present, single (') and double (&quot;) quotes will not be encoded." class="flags string">FILTER_FLAG_NO_ENCODE_QUOTES</option>
                                            <option title="Encodes ampersands (&)." class="flags string unsafe_raw">FILTER_FLAG_ENCODE_AMP</option>
                                        </select>
                                    </td>

                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                    <div id="tabs-2">
                        <h3>Assign validators</h3>
                        <table class="ui-table" style="width:100%">
                            <tr>
                                <td>Post field</td>
                                <td>Validator</td>
                                <td>Flags</td>
                                <td>Options</td>
                                <td>REGEXP</td>
                            </tr>
                            <?php foreach ($_POST as $key => $value) : ?>
                                <tr>
                                    <td><?= $key ?></td>
                                    <td>
                                        <select id="VALIDATE_<?= $key ?>" onchange="
                                        $('#VALIDATE_FLAGS_<?= $key ?> option:selected').removeAttr('selected');
                                        $('#VALIDATE_FLAGS_<?= $key ?> option.validate-flags').hide();
                                        $('#VALIDATE_FLAGS_<?= $key ?> option.' + $('#VALIDATE_<?= $key ?> :selected').text()).show();
                                        if(this.value == 'FILTER_VALIDATE_INT'){
                                            $('#FILTER_INT<?= $key ?>').show();
                                        } else {
                                            $('#FILTER_INT<?= $key ?>').hide();
                                        }
                                        updatecode();">
                                            <option value="">none</option>
                                            <option title="Returns TRUE for 1, true, on and yes. Returns FALSE otherwise. If FILTER_NULL_ON_FAILURE is set, FALSE is returned only for 0, false, off, no, and '', and NULL is returned for all non-boolean values. " value="FILTER_VALIDATE_BOOLEAN">boolean</option>
                                            <option title="Validates whether the value is a valid e-mail address. In general, this validates e-mail addresses against the syntax in RFC 822, with the exceptions that comments and whitespace folding are not supported. " value="FILTER_VALIDATE_EMAIL">validate_email</option>
                                            <option title="Validates value as float, and converts to float on success." value="FILTER_VALIDATE_FLOAT">float</option>
                                            <option title="Validates value as integer, optionally from the specified range, and converts to int on success." value="FILTER_VALIDATE_INT">int</option>
                                            <option title="Validates value as IP address, optionally only IPv4 or IPv6 or not from private or reserved ranges." value="FILTER_VALIDATE_IP">validate_ip</option>
                                            <option title="Validates value as MAC address." value="FILTER_VALIDATE_MAC">validate_mac_address</option>
                                            <option title="Validates value against regexp, a Perl-compatible regular expression." value="FILTER_VALIDATE_REGEXP">validate_regexp</option>
                                            <option title="Validates value as URL, optionally with required components. Beware a valid URL may not specify the HTTP protocol http:// so further validation may be required to determine the URL uses an expected protocol, e.g. ssh:// or mailto:. Note that the function will only find ASCII URLs to be valid; " value="FILTER_VALIDATE_URL">validate_url</option>
                                            <option title="Validates whether the domain name label lengths are valid. Validates domain names against RFC 1034, RFC 1035, RFC 952, RFC 1123, RFC 2732, RFC 2181, and RFC 1123.">validate_domain</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select multiple="multiple" id="VALIDATE_FLAGS_<?= $key ?>" onchange="updatecode();">
                                            <option class="validate-flags boolean" value="FILTER_NULL_ON_FAILURE">FILTER_NULL_ON_FAILURE</option>
                                            <option class="validate-flags float" value=" FILTER_FLAG_ALLOW_THOUSAND "> FILTER_FLAG_ALLOW_THOUSAND </option>
                                            <option class="validate-flags int" value="FILTER_FLAG_ALLOW_OCTAL">FILTER_FLAG_ALLOW_OCTAL</option>
                                            <option class="validate-flags int" value="FILTER_FLAG_ALLOW_HEX">FILTER_FLAG_ALLOW_HEX</option>
                                            <option class="validate-flags validate_regexp" value="FILTER_VALIDATE_REGEXP">FILTER_VALIDATE_REGEXP</option>
                                            <option class="validate-flags validate_ip" value="FILTER_FLAG_IPV4">FILTER_FLAG_IPV4</option>
                                            <option class="validate-flags validate_ip" value="FILTER_FLAG_IPV6">FILTER_FLAG_IPV6</option>
                                            <option class="validate-flags validate_ip" value="FILTER_FLAG_NO_PRIV_RANGE">FILTER_FLAG_NO_PRIV_RANGE</option>
                                            <option class="validate-flags validate_ip" value="FILTER_FLAG_NO_RES_RANGE">FILTER_FLAG_NO_RES_RANGE</option>
                                            <option class="validate-flags validate_url" value="FILTER_FLAG_SCHEME_REQUIRED">FILTER_FLAG_SCHEME_REQUIRED</option>
                                            <option class="validate-flags validate_url" value="FILTER_FLAG_HOST_REQUIRED">FILTER_FLAG_HOST_REQUIRED</option>
                                            <option class="validate-flags validate_url" value="FILTER_FLAG_PATH_REQUIRED">FILTER_FLAG_PATH_REQUIRED</option>
                                            <option class="validate-flags validate_url" value="FILTER_FLAG_QUERY_REQUIRED">FILTER_FLAG_QUERY_REQUIRED</option>
                                            <option class="validate-flags validate_domain" value="FILTER_FLAG_HOSTNAME">FILTER_FLAG_HOSTNAME</option>
                                            <option class="validate-flags validate_email" value="FILTER_FLAG_EMAIL_UNICODE">FILTER_FLAG_EMAIL_UNICODE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="FILTER_CALLBACK_<?= $key ?>" type="text" class="filter-options callback" title="callable function or method" value="['object', 'method']" onkeyup="updatecode();">
                                        <input id="FILTER_INT<?= $key ?>" type="text" class="filter-options int" value="['min_range' => 0, 'max_range' => 100]" onkeyup="updatecode();">
                                    </td>
                                    <td>
                                        <input placeholder="Regexp" onkeyup="updatecode();" id="REGEXP_<?= $key ?>" type="text" class="validate-flags validate_regexp">
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                    <div id="tabs-3">
                        <h3>Target</h3>
                        <?php if (isset($form_target)) : ?>
                            <div><?= get_class($form_target); ?></div>
                            <div>
                                <?php foreach ($_POST as $key => $value) : ?>
                                    <?php if (is_object($value) || is_array($value)) : ?>
                                        <?php foreach ($value as $k => $v) : ?>
                                            <div>
                                                |__<select id="POST_<?= $key ?>" onchange="updatecode()">
                                                    <?php
                                                    $reflection = new \ReflectionClass(get_class($form_target));
                                                    $properties = $reflection->getProperties();
                                                    foreach ($properties as $propertie) {
                                                        echo '<option ' . ($k == $propertie->getName() ? 'selected="selected"' : '') . ' value="' . $propertie->getName() . '">' . $propertie->getName() . '</option>';
                                                    }
                                                    ?>

                                                </select>
                                                = <b style="color: maroon">$_POST['<?= $key; ?>']</b>['<?= $k ?>']
                                            </div>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        <div>
                                            |__<select id="POST_<?= $key ?>" onchange="updatecode()">
                                                <?php
                                                $reflection = new \ReflectionClass(get_class($form_target));
                                                $properties = $reflection->getProperties();
                                                foreach ($properties as $propertie) {
                                                    echo '<option ' . ($key == $propertie->getName() ? 'selected="selected"' : '') . ' value="' . $propertie->getName() . '">' . $propertie->getName() . '</option>';
                                                }
                                                ?>

                                            </select>
                                            = <b style="color: maroon">$_POST</b>['<?= $key ?>']
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>

                        <?php else : ?>
                            <div>No target found. Set the target in your controller</div>
                        <?php endif; ?>
                    </div>
                    <div id="tabs-4">
                        <h3>PHP Code for the controller</h3>
                        <div><button onclick="updatecode()">Get code</button></div>
                        <textarea id="phpcode" rows="8" cols="80"></textarea>
                    </div>
                </div>
            </div>

            <script type="text/javascript">
                $(function() {
                    $("#jquery-tabs").tabs();
                    $('.filter-options').hide();
                    uitable();
                });
                updatecode = function() {

                    ar = '<?= get_class($form_target); ?>';

                    code = '';
                    code += 'if($this->request->is_post()){\n';
                    code += '\n';
                    code += '\t\t\t// create the filters\n';

                    <?php foreach ($_POST as $key => $value) : ?>

                        flags<?= $key ?> = $('#FLAGS_<?= $key ?>').val();

                        if ($('#FILTER_<?= $key ?>').val() == 'FILTER_CALLBACK') {
                            code += '\t\t\t$optionsFilter<?= $key ?> = [\n';
                            code += '\t\t\t\t\'options\' => ' + ($('#FILTER_CALLBACK_<?= $key ?>').val()) + '\n';
                            code += '\t\t\t];\n';
                            code += '\t\t\t$' + $('#POST_<?= $key ?>').val() + ' = filter_input(INPUT_POST, \'<?= $key ?>\', ' + $('#FILTER_<?= $key ?>').val() + (flags<?= $key ?> !== null ? ', ' + flags<?= $key ?>.join('|') : '') + ', $optionsFilter<?= $key ?>);\n';
                        } else if ($('#FILTER_<?= $key ?>').val() == 'FILTER_SANITIZE_NUMBER_INT') {
                            code += '\t\t\t$optionsFilter<?= $key ?> = [\n';
                            code += '\t\t\t\t\'options\' => ' + ($('#FILTER_INT<?= $key ?>').val()) + '\n';
                            code += '\t\t\t];\n';
                            code += '\t\t\t$' + $('#POST_<?= $key ?>').val() + ' = filter_input(INPUT_POST, \'<?= $key ?>\', ' + $('#FILTER_<?= $key ?>').val() + (flags<?= $key ?> !== null ? ', ' + flags<?= $key ?>.join('|') : '') + ', $optionsFilter<?= $key ?>);\n';
                        } else {
                            code += '\t\t\t$' + $('#POST_<?= $key ?>').val() + ' = filter_input(INPUT_POST, \'<?= $key ?>\', ' + $('#FILTER_<?= $key ?>').val() + (flags<?= $key ?> !== null ? ', ' + flags<?= $key ?>.join('|') : '') + ');\n';
                        }

                    <?php endforeach; ?>

                    code += '\n\t\t\t// create the validators\n';
                    <?php foreach ($_POST as $key => $value) : ?>
                        validateflags<?= $key ?> = $('#VALIDATE_FLAGS_<?= $key ?>').val();
                        if ($('#VALIDATE_<?= $key ?>').val() !== '') {
                            code += '\t\t\t$optionsValidator<?= $key ?> = [\n';

                            if (validateflags<?= $key ?> && validateflags<?= $key ?>.join(',') == 'FILTER_VALIDATE_REGEXP') {
                                code += '\t\t\t\t\'optionsValidator\' => [\n';
                                code += '\t\t\t\t\t\'regexp\' => \'' + $('#REGEXP_<?= $key ?>').val() + '\'\n'
                                code += '\t\t\t\t\t],\n';
                            } else if (validateflags<?= $key ?> && validateflags<?= $key ?>.join(',') == 'FILTER_VALIDATE_INT') {
                                code += '\t\t\t\t\'options\' => [\n';
                                code += '\t\t\t\t\t\'default\' => 3, // value to return if the filter fails\n';
                                code += '\t\t\t\t\t// other options here\n';
                                code += '\t\t\t\t\t\'min_range\' => 0\n';
                                code += '\t\t\t\t\t],\n';
                            } else if (validateflags<?= $key ?>) {
                                code += '\t\t\t\t\'flags\' => ' + validateflags<?= $key ?>.join(',') + '\n';
                            }
                            code += '\t\t\t];\n';
                            code += '\t\t\t$' + $('#POST_<?= $key ?>').val() + ' =  filter_var($' + $('#POST_<?= $key ?>').val() + ', ' + $('#VALIDATE_<?= $key ?>').val() + ', $optionsValidator<?= $key ?>);\n';
                        }
                    <?php endforeach; ?>

                    code += '\n\t\t\t// create the assignements\n';
                    code += '\t\t\t$' + ar + ' = new ' + ar + '();\n';
                    <?php foreach ($_POST as $key => $value) : ?>
                        //flags<?= $key ?> = $('#FLAGS_<?= $key ?>').val();
                        //console.log(flags<?= $key ?>);
                        code += '\t\t\t$' + ar + '->' + $('#POST_<?= $key ?>').val() + ' = $' + $('#POST_<?= $key ?>').val() + ';\n';
                    <?php endforeach; ?>
                    code += '\t\t\t//$' + ar + '->save();\n';
                    code += '\n';
                    code += '\t\t}\n';

                    $('#phpcode').text(code);
                };

                $(function() {
                    // hide the validation flags initial
                    $('option.validate-flags').hide();
                    // hide the filter flags initial
                    $('option.flags').hide();
                    // create the dialog
                    $('#dialog-form-helper').dialog({

                        resizable: false,
                        autoOpen: false,
                        width: 800,
                        height: 700,
                        modal: true,
                        buttons: {
                            Cancel: function() {
                                $(this).dialog("close");
                            }
                        },
                        close: function() {
                            $(this).dialog("destroy");
                            $('#dialog-form-helper').remove();
                        }
                    });
                    $('#dialog-form-helper').dialog('open');
                });
            </script>
<?php
        endif;

        // get the output buffer contents
        $html = ob_get_contents();
        // clean the buffer
        ob_clean();
        // append the html to the view
        $this->view->append($html);
    }
    
    /**
     * Add http authorisation
     * @param string $name
     * @return static
     */
    public function auth(string $name = '') {
        
        if(!isset ($_SERVER['PHP_AUTH_USER']) ){
		
            header('WWW-Authenticate: Basic realm="' . $name . '"');
            header('HTTP/1.0 401 Unauthorized');
            $this->auto_render = false;
            die('Not Unauthorized.');
        }
        
        return $this;
    }
    
    public function setAppmodel(mixed $appmodel){
        $this->appmodel = $appmodel;
        return $this;
    }
    
    public function getAppmodel(){
        if(is_object($this->appmodel)){
            return $this->appmodel;
        }
    }
    public function setControllermodel(mixed $model){
        $this->model = $model;
        return $this;
    }
    public function getControllermodel(){
        if(is_object($this->model)){
            return $this->model;
        }
        
    }

    function setConfig(Config $config){
        $this->config = $config;
        return $this;
    }

    function getConfig(): Config{
        return $this->config; 
    }

    function setDb(Database $db){
        $this->db = $db;
        return $this;
    }

    function getDb(): Database{
        return $this->db; 
    }

    function setLayout(Layout $layout){
        $this->layout = $layout;
        return $this;
    }

    function getLayout(): Layout{
        return $this->layout; 
    }

    function setRequest(Request $request){
        $this->request = $request;
        return $this;
    }

    function getRequest(): Request{
        return $this->request; 
    }

    function setRouter(Router $router){
        $this->router = $router;
        return $this;
    }

    function getRouter(): Router{
        return $this->router; 
    }

    function setView(View $view){
        $this->view = $view;
        return $this;
    }

    function getView(): View{
        return $this->view; 
    }
}
