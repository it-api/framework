<?php

namespace Barebone\Loggers;

use Barebone\Loggers\Interfaces\LoggerInterface;
use Barebone\Database\Connectors\PDO;

class DatabaseLogger extends BaseLogger implements LoggerInterface{
    
    private PDO $pdo;
    private array $dbconfig = [];
    
    function __construct(PDO $pdo, array $dbconfig = []){
        $this->pdo = $pdo;
        if(!isset($dbconfig['database'])){
            throw new \Exception('$dbconfig["database"] not set!');
        }
        
        if(!isset($dbconfig['tablename'])){
            throw new \Exception('$dbconfig["tablename"] not set!');
        }
        
        if(!isset($dbconfig['column_message'])){
            throw new \Exception('$dbconfig["column_message"] not set!');
        }
        
        if(!isset($dbconfig['column_timestamp'])){
            throw new \Exception('$dbconfig["column_timestamp"] not set!');
        }
        
        if(!isset($dbconfig['column_logtype'])){
            throw new \Exception('$dbconfig["column_logtype"] not set!');
        }
        
        $this->dbconfig = $dbconfig;
    }
    
    final public function log(string $level, string $message, array $context = []){
        
        $message = safehtml($message);
        $sql = "INSERT INTO `{$this->dbconfig['database']}`.`{$this->dbconfig['tablename']}` 
                SET {$this->dbconfig['column_message']} = '{$message}',
                {$this->dbconfig['column_timestamp']} = CURRENT_TIMESTAMP(),
                {$this->dbconfig['column_logtype']} = '{$level}'";
                
        return $this->pdo->exec($sql);
    }
}