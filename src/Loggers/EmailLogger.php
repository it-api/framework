<?php

namespace Barebone\Loggers;

use Barebone\Loggers\Interfaces\LoggerInterface;

class EmailLogger extends BaseLogger implements LoggerInterface{
    
    private string $email;
    private string $subject;
    private string $body;
    
    function __construct(string $email, string $subject){
        $this->email = $email;
        $this->subject = $subject;
    }
    
    public function log(string $level, string $message, array $context = []){
        $this->subject = "[{$level}] " . $this->subject;
        $this->body = $message;
        $this->body .= '<pre>'. print_r($context, true).'</pre>';
        debug($this);
    }
}