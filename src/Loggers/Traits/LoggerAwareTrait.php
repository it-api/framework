<?php

namespace Barebone\Loggers\Traits;

use Barebone\Loggers\Interfaces\LoggerInterface;

trait LoggerAwareTrait {
    
    /** @var Barebone\Loggers\Interfaces\LoggerInterface */
    protected $logger;

    /**
     * Sets a logger.
     * 
     * @param Barebone\Loggers\Interfaces\LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Gets a logger.
     * 
     * @return Barebone\Loggers\Interfaces\LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }
}