<?php

namespace Barebone\Loggers;

use Barebone\Loggers\Interfaces\LoggerInterface;

class FileLogger extends BaseLogger implements LoggerInterface{
    
    private string $filename;
    
    function __construct(string $filename){
        $this->filename = $filename;
    }
    
    final public function log(string $level, string $message, array $context = []){
        file_put_contents($this->filename, "[{$level}] - {$message}\n", FILE_APPEND);
    }
}