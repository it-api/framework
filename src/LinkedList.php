<?php
namespace Barebone;

class LinkedList {
    
    /**
     * 
     */
    public $previous;
    
    /**
     * 
     */
    public $next;
    
    /**
     * 
     */
    public $data;
    
    function __construct($data, LinkedList $next = NULL){
        $this->data = $data;
        $this->next = $next;
    }
    
    function getNext(){
        return $this->next;
    }
    
    function getPrevious(){
        return $this->previous;
    }
    
    function setNext(LinkedList $next){
        $this->next = $next;
    }
    
    function setPrevious(LinkedList $previous){
        $this->previous = $previous;
    }
}
/*
$data = array_rand([0,1,2,3,4,5,6,100]);
$linklist = new LinkedList($data, NULL);


$data = array_rand([0,1,2,3,4,5,6,100]);
$nextlinklist1 = new LinkedList($data, $linklist);
$linklist->setPrevious($nextlinklist1);
$data = array_rand([0,1,2,3,4,5,6,100]);
$nextlinklist2 = new LinkedList($data, $nextlinklist1);
$data = array_rand([0,1,2,3,4,5,6,100]);
$nextlinklist3 = new LinkedList($data, $nextlinklist2);
$data = array_rand([0,1,2,3,4,5,6,100]);
$nextlinklist4 = new LinkedList($data, $nextlinklist3);
$data = array_rand([0,1,2,3,4,5,6,100]);
$nextlinklist5 = new LinkedList($data, $nextlinklist4);
$data = array_rand([0,1,2,3,4,5,6,100]);
$nextlinklist6 = new LinkedList($data, $nextlinklist5);
$data = array_rand([0,1,2,3,4,5,6,100]);
$nextlinklist7 = new LinkedList($data, $nextlinklist6);
$data = array_rand([0,1,2,3,4,5,6,100]);
$nextlinklist8 = new LinkedList($data, $nextlinklist7);

print_r($nextlinklist6->next);
print_r($nextlinklist8);
*/