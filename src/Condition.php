<?php

namespace Barebone;

/**
    
    class cls_doeiets{
        function doeiets(){
            echo '<p>Done executing class method</p>';
        }    
    }
    function doeiets(){
        echo '<p>Done</p>';
    }
    
    $a = 1;
    $b = 2;
    $Condition = new Condition();
    $Condition->When($a === $b)->Call( 'doeiets' ); // When is FALSE
    $Condition->When($a !== $b)->Call( 'doeiets' ); // Done
    $Condition->When($a === $b)->Unless($a !== $b)->Call( 'doeiets' ); // Done
    $Condition->When($a !== $b)->Unless($b !== $a)->Call( 'doeiets' ); // Done
    $Condition->When($a === $b)->Unless(10 === 1)->Unless($a !== $b)->Call( new cls_doeiets(), 'doeiets' );
 */

class Condition {

    private $when = false;
    private $unless = false;
    /**
     * @var $args a comparison $a === $b
     * @return \Barebone\Condition
     */
    function When(bool $args): Condition {
        if ($this->when === false) {
            $this->when = $args;
        }
        return $this;
    }
    /**
     * Summary of Also
     * @param bool $args
     * @return \Barebone\Condition
     */
    function Also(bool $args) {
        $this->when = $args;
        return $this;
    }
    /**
     * Summary of Or
     * @param bool $args
     * @return \Barebone\Condition
     */
    function Or(bool $args) {

        $this->when = $args;

        return $this;
    }
    /**
     * @var $args a comparison $a === $b
     * @return \Barebone\Condition
     */
    function Unless(bool $args): Condition {

        if ($this->unless === false) {
            $this->unless = $args;
        }
        return $this;
    }
    /**
     * Summary of Otherwise
     * @param array $ func_get_args()
     * @return void
     */
    function Otherwise() {
        $this->Execute(func_get_args());
    }
    /**
     * Execute a method on a class or function
     * @return \Barebone\Condition
     */
    function Call() {
        if ($this->when === true && $this->unless === true) {
            //echo '<p>Unless is TRUE, When is TRUE';
            $this->Execute(func_get_args());
        } elseif ($this->when === false && $this->unless === true) {
            //echo '<p>Unless is TRUE, When is FALSE';
            $this->Execute(func_get_args());
        } elseif ($this->when === true) {
            //echo '<p>When is TRUE';
            $this->Execute(func_get_args());
        } elseif ($this->when === false) {
            //echo '<p>When is FALSE';
        }
        return $this;
    }
    /**
     * Execute call_user_func
     * @var $args mixed array|object|anomious function
     */
    private function Execute($args = []) {
        // anomious function is passed in
        if (is_callable($args[0])) {
            $args[0]();
            // an object is passed in with a method to execute
        } elseif (is_object($args[0])) {
            call_user_func([$args[0], $args[1]]);
            // just call the function
        } else {
            $args[0]();
        }
    }
}
