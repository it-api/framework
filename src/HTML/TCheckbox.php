<?php

namespace Barebone\HTML;
/**
 * @example <label>[left]<input type="checkbox">[default]</label>
 */
class TCheckbox extends TElement {
    /**
     * 
     * @var TElement 
     */
    private $input;
    function __construct($text, $attributes = array('label'=>'','input'=>''), $label_position = 'default') {

        parent::__construct('label', $attributes['label']);
        $this->input = new TElement('input', $attributes['input']);
        $this->input->type = 'checkbox';
        // if the $label_position is left we put the text before the checkbox
        if ($label_position === 'left') {
            $this->innertext($text);
            $this->append($this->input);
        } else {
            $this->append($this->input);
            $this->input->innertext($text);
        }              
        
    }

    function setChecked() {
        $this->input->checked = 'checked';
    }

    function setUnChecked() {
        unset($this->input->checked);
    }

    function isChecked() {
        return isset($this->input->checked);
    }

    function autoPostback($url) {
        $this->on('click', "$.php('{$url}', {});");
    }

}
