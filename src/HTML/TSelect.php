<?php

namespace Barebone\HTML;

/**
 * Description of TSelect
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource TSelect.php
 * @since 2-feb-2017 20:37:51
 * 
 */
class TSelect extends TElement {
    /**
     * Constructor TSelect
     * @param array $attributes attributes for the select element
     */
    public function __construct(array $attributes = []) {
        parent::__construct('select', $attributes);
    }

    /**
     * Add an option to the select element
     * @param string $key the value shown
     * @param string $value the value attribute of the option
     * @param array $attributes the attributes for the option
     */
    function add_option(string $key, string $value, array $attributes = []) {

        $attributes['value'] = $value;
        $option = new TElement('option', $attributes);
        $option->innertext($key);
        $this->append($option);
    }
}
