<?php

namespace Barebone\HTML;

class TLink extends TElement {
	/**
	 * 
	 * @param string $text text on the link
	 * @param string $href attr href
	 * @param array $attributes
	 */
    function __construct($text, $href = '', $attributes = array()) {

        $this->innertext($text);
        $this->href = $href;
        parent::__construct('a', $attributes);
    }

}
