<?php

/*
  Creator Frank
 */

namespace Barebone\HTML;

/**
 * TImage represents a <img> tag
 *
 * @author Frank
 */
class TImage extends TElement{
	
	function __construct($src, $alt = '', $attributes = []) {
		parent::__construct('img', $attributes);
		$this->src = $src;
		$this->alt = $alt;
	}

}
