<?php

/*
  Creator Frank
 */

namespace Barebone\HTML;

/**
 * Description of TTableRow
 *
 * @author Frank
 */
class TTableRow  extends TElement {

	function __construct() {
		parent::__construct('tr');
	}

	function addHeader(?string $value = '', array $attributes = []) {
		return $this->append(new TTableHeader($value, $attributes));
	}

	function addCell(?string $value = '', array $attributes = []) {
		return $this->append(new TTableCell($value, $attributes));
	}
}
