<?php

namespace Barebone\HTML;

/**
 * TVerticalBoxModel is a floating <div> element with wereby the width is divided by the columns
 */
class TVerticalbox extends TElement {

	function __construct(array $attributes = []) {
		parent::__construct('div', $attributes);
	}
	/**
	 * append a new child element
	 * @param \Barebone\HTML\TElement $child
	 * @return TElement
	 */
	function append(TElement $child) {

		// append the child element
		parent::append($child);
		return $child;
	}
}
