<?php

namespace Barebone\HTML;

class TLabel extends TElement {

    function __construct($text = '', $attributes = array()) {
        $this->innertext($text);
        parent::__construct('label', $attributes);
    }

}
