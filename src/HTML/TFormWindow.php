<?php

namespace Barebone\HTML;

class TFormWindow extends TElement {

	private $windowid = null;

	function __construct($title = 'Untitled', $contents = '', $buttons = []) {

		$this->createWindowID();

		$attributes = [];
		$attributes['id'] = $this->getWindowID();
		$attributes['title'] = $title;

		parent::__construct('div', $attributes);
		$div = new TElement('div');
		$this->append($div);
		$div->innertext((string)$contents);

		$script = new TElement('script');
		$this->append($script);

		$bstring = '';

		foreach ($buttons as $button) {
			foreach ($button as $key => $value) {
				if ($key === 'click') {
					$bstring .= 'click: ' . $value . ',';
				} else {
					$bstring .= $key . ': "' . $value . '",';
				}
			}
			$bstring = rtrim($bstring, ',');
		}
		$btns = 'buttons:[{' . $bstring . '}]';
		$script->innertext("$(document).ready(
	        function(){ 
	            $('#" . $this->getWindowID() . "').dialog({
	                autoOpen:false, 
	                modal:true,
	                $btns
	            });
	        })");
	}

	function createWindowID() {
		$this->setWindowID(uniqid('window-'));
	}

	function setWindowID($windowid) {
		$this->windowid = $windowid;
	}

	function getWindowID() {
		return $this->windowid;
	}

	function open() {
		return "$('#" . $this->getWindowID() . "').dialog('open');return false;";
	}

	function close() {
		return "$('#" . $this->getWindowID() . "').dialog('close');return false;";
	}
}
