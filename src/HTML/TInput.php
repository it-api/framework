<?php

namespace Barebone\HTML;

/**
 * Description of TInput
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource TInput.php
 * @since 2-feb-2017 22:33:45
 * 
 */
class TInput extends TElement{
    
    public function __construct($name= '', $attributes = []) {
        parent::__construct('input', $attributes);
        $this->name = $name;
    }

}
