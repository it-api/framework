<?php

namespace Barebone\HTML;

/**
 * THorizontalBoxModel is a single <div> element with 100% width
 */
class THorizontalbox extends TElement {

    function __construct($attributes = []) {
        parent::__construct('div',$attributes);
    }

    function append(TElement $child) {

        // append the child element
        parent::append($child);
        return $child;
    }

    function __toString(){
	    
	    if($this->count_children() > 0){
	        $width = 12 / $this->count_children();
	        foreach($this->get_children() as $column){
                $column->class = 'col-xs-12 col-sm-12 col-lg-' . $width ;
	        }
	    }else{
	        $this->css('width', '100%');
	        $this->css('float', 'left');
	    }
	    return parent::__toString();
	}
}
