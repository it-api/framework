<?php

namespace Barebone\HTML;

class THtml{
	
	public static function A($text, $href){
		$a = new TElement('a');
		$a->innertext($text);
		$a->href = $href;
		return $a;
	}
}
