<?php

namespace Barebone\HTML;

/**
 * Description of TTable
 *
 * @author Frank
 */
class TTable extends TElement {

	private TElement $thead;
	private TElement $tbody;
	private array $rows = [];

	function __construct(array $attributes = []) {
		parent::__construct('table', $attributes);
		$this->thead = new TElement('thead');
		$this->tbody = new TElement('tbody');
		$this->append($this->thead);
		$this->append($this->tbody);
	}

	function createFromObject($data) {

		if (is_object($data) || is_array($data)) {
			foreach ($data as $k => $v) {

				$tr = $this->addRow();
				$tr->addCell($k);
				if (is_object($v) || is_array($v)) {

					$tr->addCell((string)$this->createFromObject($v));
				} else {

					$tr->addCell($v);
				}
			}
		}
		return $this;
	}

	function addRow() {

		$tr = new TTableRow();

		if (count($this->rows) === 0) {
			$this->thead->append($tr);
		} else {
			$this->tbody->append($tr);
		}
		$this->rows[] = $tr;

		return $tr;
	}
}
/*
$tbl = new TTable();
$table = $tbl->createFromObject((object) [
    'Lowercase'=>'Uppercase',
    'aaaa'=>'AAAA', 
    'bbbb'=>'BBBB',
    'object' => [
        'firstName'=>'Test',
        'lastName'=>'Tester'
    ]
]);
echo $table;
*/
