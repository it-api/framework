<?php

/*
  Creator Frank
 */

namespace Barebone\HTML;

/**
 * Description of TRadiogroup
 *
 * @author Frank
 */
class TRadiogroup extends TElement {

	/**
	 * the name of the radiogroup
	 * @var string $name
	 */
	private string $name;
	/**
	 * the selected value of the radiogroup
	 * @var string $value
	 */
	private string $value;
	/**
	 * the radiogroup options
	 * @var array $options
	 */
	private array $options;
	/**
	 *	the wrapper for the radiogroup
	 * @var \Barebone\HTML\TElement $wrapper
	 */
	private \Barebone\HTML\TElement $wrapper;
	/**
	 * Constructor radiogroup
	 * <div></div>
	 * @param string $name the name of the radiogroup
	 * @param string $value the selected value of the radiogroup
	 * @param array $options the radiogroup options
	 * @param array $attributes the radiogroup wrapper attributes
	 */
	function __construct(string $name, string $value, array $options = [], array $attributes = []) {

		parent::__construct('div', $attributes);

		$this->name = $name;
		$this->value = $value;
		if ($options) {
			foreach ($options as $key => $val) {
				$this->add_option($key, $val);
			}
		}
	}
	/**
	 * <label><input name="{$this->name}" value="{$value}">{$key}</label>
	 * @param string $key
	 * @param string $value
	 * @param array $attributes
	 */
	function add_option(string $key, string $value, array $attributes = []) {

		$attributes['type'] = 'radio';
		$attributes['value'] = $value;
		$attributes['name'] = $this->name;
		if ($value === $this->value) {
			$attributes['selected'] = 'selected';
		}
		// create a label
		$label = new TElement('label');
		$radio = new TElement('input', $attributes);
		$label->append($radio);
		$radio->innertext("&nbsp;$key");
		// add the label to the wrapper
		$this->append($label);
	}
}
