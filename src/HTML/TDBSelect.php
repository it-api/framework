<?php

/*
  Creator Frank
 */

namespace Barebone\HTML;

/**
 * Description of TDBSelect
 *
 * @author Frank
 */
class TDBSelect extends TSelect{
	
	/**
	 *
	 * @var \Barebone\Database  
	 */
	private $db;
	/**
	 * 
	 * @param string $tablename the name of the table to get the data
	 * @param string $value the attribute value
	 * @param string $key the option value shown
	 * @param array $attributes attributes for the select element
	 */
	function __construct($name, $tablename, $value, $key, $attributes = []) {
		
		$attr = array_merge($attributes , ['name' => $name]);
		//contruct the parent select element
		parent::__construct($attr);
		// get the database from the registry
		$this->db = \Barebone\Registry::getService('db');
		// create options array for the query
		$options = [];
		// select the value and key
		$options['select'] = "$value, $key";
		// get the options from the database
		$select_options = $this->db->table($tablename)->fetch_all($options);
		if($select_options){
			foreach ($select_options as $select_option){
				$this->add_option($select_option->{$key}, $select_option->{$value});
			}
		}
		
	}

}
