<?php

namespace Barebone\HTML;

/**
 * Description of TTableCell
 *
 * @author Frank
 */
class TTableCell extends TElement {

	function __construct(?string $value, array $attributes = []) {
		parent::__construct('td', $attributes);
		$this->innertext($value);
	}
}
