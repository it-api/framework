<?php

namespace Barebone\HTML;

class TElement {

    protected string $tag = 'div';
    protected string $innertext = '';
    protected array $children = [];
    protected array $attributes = [];
    private static $non_closing_tags = array(
        'HR', 'INPUT', 'IMG', 'BR', 'LINK', 'META'
    );
    /**
     * Constructor
     * @param string $tag the name of the tag
     * @param array $attributes
     * @return TElement
     */
    function __construct(string $tag, array $attributes = []) {

        $this->tag = $tag;

        if (count($attributes) > 0) {
            foreach ($attributes as $name => $value) {
                if ($name === 'innertext') {
                    $this->innertext($value);
                } else {
                    $this->attributes[$name] = new TAttribute($name, $value);
                }
            }
        }
    }
    /**
     * Append a element to this element
     * @param \Barebone\HTML\TElement $element
     * @return TElement
     */
    function append(TElement $element) {
        $this->children[] = $element;
        return $this;
    }
    /**
     * Append a element to this element
     * @param \Barebone\HTML\TElement $element
     * @return TElement
     */
    function prepend(TElement $element) {
        array_unshift($this->children, $element);
        return $this;
    }
    /**
     * check if a class exists on this element
     * @param string $cssclass The css classname to add
     * @return bool
     */
    function class_exists($cssclass) {
        if ($this->class instanceof \Barebone\HTML\TAttribute) {
            $cls_string = $this->class->getValue();
            $cls_array = explode(' ', $cls_string);
        } else {
            $cls_array = explode(' ', $this->class ?? '');
        }
        foreach ($cls_array as $cls) {
            if ($cls === $cssclass) {
                return true;
            }
        }
        return false;
    }
    /**
     * add a class to this element
     * @param string $cssclass The css classname to add
     * @return TElement
     */
    function add_class($cssclass) {
        //var_dump($this->class);
        if ($this->class instanceof \Barebone\HTML\TAttribute) {
            $this->class->setValue($this->class->getValue() . ' ' . $cssclass);
        } else {
            $this->class = ($cssclass);
        }
        return $this;
    }
    /**
     * add a class to this element
     * @param string $cssclass The css classname to remove
     * @return TElement
     */
    function remove_class($cssclass) {
        $new_cls_string = '';
        if ($this->class instanceof \Barebone\HTML\TAttribute) {
            $cls_string = $this->class->getValue();
            $cls_array = explode(' ', $cls_string);
        } else {
            $cls_array = explode(' ', $this->class);
        }

        foreach ($cls_array as $cls) {
            if ($cls !== $cssclass) {
                $new_cls_string .= $cls . ' ';
            }
        }
        if ($new_cls_string !== '') {
            $this->class = $new_cls_string;
        } else {
            unset($this->class);
        }

        return $this;
    }
    /**
     * Set innertext value
     * @param string $value
     * @return string
     */
    function innertext(?string $value = null) {
        if (!is_null($value)) {
            $this->innertext = $value;
            return $this->innertext;
        } else {
            return $this->innertext;
        }
    }

    /**
     * Read / write [data-attribute="value"]
     * @param string $name
     * @param string $value
     * @return TElement
     */
    function data(string $name, string $value = '') {
        if ($value === '') {
            return $this->attributes['data-' . $name]->value;
        } else {
            $this->attributes['data-' . $name] = new TAttribute('data-' . $name, $value);
        }
        return $this;
    }
    /**
     * Read / write [aria-attribute="value"]
     * @param string $name
     * @param string $value
     * @return TElement
     */
    function aria(string $name, string $value = '') {
        if ($value === '') {
            return $this->attributes['aria-' . $name]->value;
        } else {
            $this->attributes['aria-' . $name] = new TAttribute('aria-' . $name, $value);
        }
        return $this;
    }

    /**
     * Add event handler
     * @param string $event [click,mouseover,mouseout etc...]
     * @param string $callback callback function in javascript
     * @return TElement
     */
    function on(string $event, string $callback) {
        $eventattr = new TEventAttribute($event, $callback);
        $this->__set($eventattr->name, $eventattr->value);
        return $this;
    }
    /**
     * Add inline styling to the element
     * @param string $identifier
     * @param string $rule
     * @return TElement
     */
    function css(string $identifier, string $rule) {

        if (isset($this->style)) {
            $this->style = $this->style->value . ";{$identifier}:{$rule}";
        } else {
            $this->style = "{$identifier}:{$rule}";
        }
        return $this;
    }

    /**
     * Wrap this tag into another tag
     * @param $tagname
     * @param $attributes
     * @return TElement
     */
    function wrap(string $tagname = 'div', array $attributes = []) {
        $wrapper = new TElement($tagname, $attributes);
        $wrapper->append($this);
        return $wrapper;
    }
    /**
     * Enable the element
     * @return TElement
     */
    function enable() {
        unset($this->disabled);
        return $this;
    }
    /**
     * Disable the element
     * @return TElement
     */
    function disable() {
        $this->disabled = 'disabled';
        return $this;
    }

    private function has_closing_tag() {
        return !in_array(strtoupper($this->tag), self::$non_closing_tags);
    }

    function has_children() {
        return count($this->children) > 0;
    }

    function count_children() {
        return count($this->children);
    }

    function get_children() {
        return $this->children;
    }

    function replace_child($key, $new_child) {
        $this->children[$key] = $new_child;
        return $this;
    }

    function get_child($key) {
        return $this->children[$key];
    }

    function __unset($name) {
        unset($this->attributes[$name]);
        return isset($this->attributes[$name]);
    }

    function __isset($name) {
        return isset($this->attributes[$name]);
    }

    function __set($name, $value) {
        $this->attributes[$name] = new TAttribute($name, $value);
        return $this;
    }

    function __get($name) {
        return $this->attributes[$name] ?? null;
    }

    function render() {
        echo $this->__toString();
    }

    function __toString() {

        $result = '';
        $result .= '<' . $this->tag;
        if (count($this->attributes) > 0) {
            foreach ($this->attributes as $attribute) {
                $result .= $attribute;
            }
        }
        $result .= '>';

        if (count($this->children) > 0) {
            $result .= "\n";
        }

        if ($this->innertext !== '') {
            $result .= $this->innertext;
        }
        if (count($this->children) > 0) {
            foreach ($this->children as $child) {
                $result .= $child;
            }
        }

        if ($this->has_closing_tag()) {
            $result .= '</' . $this->tag . '>' . "\n";
        }
        return $result;
    }

    function editable() {
        $this->contenteditable = 'true';
    }
}
