<?php

/*
  Creator Frank
 */

namespace Barebone\HTML;

/**
 * Description of TDBRadio
 *
 * @author Frank
 */
class TDBRadiogroup extends TRadiogroup{
	
	/**
	 *
	 * @var \Barebone\Database  
	 */
	private $db;
	/**
	 * 
	 * @param string $tablename the name of the table to get the data
	 * @param string $value the attribute value
	 * @param string $key the option value shown
	 * @param array $attributes attributes for the select element
	 */
	function __construct($name, $tablename, $value, $key, $attributes = []) {

		// get the database from the registry
		$this->db = \Barebone\Registry::getService('db');
		// create options array for the query
		$options = [];
		// select the value and key
		$options['select'] = "$value, $key";
		// get the options from the database
		$radiogroup_options = $this->db->table($tablename)->fetch_all($options);
		if($radiogroup_options){
			foreach($radiogroup_options as $radiogroup_option){
				$opts[$radiogroup_option->{$key}] = $radiogroup_option->{$value};
			}
		}
		//contruct the parent select element
		parent::__construct($name, $value, $opts, $attributes);
	}

}
