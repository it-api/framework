<?php

namespace Barebone\HTML;

/**
 * Description of TForm
 * Barebone 
 * UTF-8
 *
 * @author Frank
 * @filesource TForm.php
 * @since 2-feb-2017 20:47:44
 * 
 */
class TForm extends TElement{
    
    function __construct(array $attributes = []){
        parent::__construct('form', $attributes);
    }
    
    function add_element(TElement $element){
        $this->append($element);
        return $element;
    }
}
