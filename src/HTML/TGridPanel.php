<?php

/*
  Creator Frank
 */

namespace Barebone\HTML;

/**
 * Description of TGridPanel
 *
 * @author Frank
 */
class TGridPanel extends TElement{
	
	function __construct($width, $height) {
		parent::__construct('div');
		$this->css('width', $width.'px');
		$this->css('height', $height.'px');
		$this->css('position', 'relative');
	}

	function place(TElement $element, $left, $top){
		
		$div = new TElement('div');
		$div->css('left', $left.'px');
		$div->css('top', $top.'px');
		$div->css('position','absolute');
		$div->append($element);
		$this->append($div);
	}
}
