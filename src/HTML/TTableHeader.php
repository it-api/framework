<?php

/*
  Creator Frank
 */

namespace Barebone\HTML;

/**
 * Description of TTableHeader
 *
 * @author Frank
 */
class TTableHeader extends TElement {

	function __construct(string $value, array $attributes = []) {
		parent::__construct('th', $attributes);
		$this->innertext($value);
	}
}
