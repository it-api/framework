<?php

namespace Barebone\HTML;
/**
 * <button role="api-button" data-target="#target-div" data-route="/delphi/events/stop/">Stop</button>
 * <div id="target-div"></div>
 */
class TAPIButton extends TButton{
	
	/**
	 * TAPIButton constuctor
	 * @param string $caption text on the button
	 * @param string $target the div wich will be updated by the api call
	 * @param string $route the route to a controller that will handle the ajax call
	 * @param array $attributes extra attributes for the button
	 */
	function __construct($caption, $target = null, $route = null, $attributes = []){
	    parent::__construct($caption, $attributes);
		$this->role = 'api-button';
		if(!is_null($target)){
			$this->data('target', $target);
		}
		if(!is_null($route)){
			$this->data('route', $route);
		}		
	}
	/**
	 * Sets the target element thar recieves the result of the ajax call
	 * @param string $target any css identifier like '#id' OR '.class' 'input[type="button"]'
	 */
	function setTarget($target){
		$this->data('target', $target);
	}
	
	function setRoute($route){
		$this->data('route', $route);
	}
	/**
	 * 
	 * @return string
	 */
	function __toString(){
	    ob_start();
	    $s = parent::__toString();
		if(!defined('APIBUTTONS_SET')){
			$s .= '<script>$(document).ready(function(){$(\'button[role="api-button"]\').click(function(){ $.php( $(this).data("route"), $(this).data() ); return false; });});</script>';
			define('APIBUTTONS_SET', true);
		}
	    $s .= ob_get_clean();
	    return $s;
	}
}

