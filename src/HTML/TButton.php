<?php

namespace Barebone\HTML;

class TButton extends TElement{
    
	function __construct($text, $attributes = []){
	    $this->innertext($text);
	    parent::__construct('button', $attributes);
	}
	
	function attachEventHandler($event, $callback){
	    $this->on($event, $callback);
	}
}
