<?php

namespace Barebone\HTML;

class TDocument {

    private $doctype = 'html';
    private $html;
    private $head;
    private $body;

    public function get_doctype() {
        // return value of doctype
        return $this->doctype;
    }

    public function set_doctype($value) {
        // set value to doctype
        $this->doctype = $value;
    }

    public function getHead() {
        return $this->head;
    }

    public function getBody() {
        return $this->body;
    }

    function append(TElement $element) {
        $this->body->append($element);
    }

    function __construct() {

        $this->html = new TElement('html');
        $this->head = new TElement('head');
        $this->body = new TElement('body');

        $this->html->append($this->head);
        $this->html->append($this->body);
    }

    function __toString() {

        $result = '';
        $result .= '<!DOCTYPE ' . $this->doctype . '>';
        $result .= $this->html;
        return $result;
    }

}
