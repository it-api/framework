<?php

namespace Barebone\HTML;

class TEventAttribute extends TAttribute {

    function __construct($event, $callback) {
        parent::__construct('on' . $event, $callback);
    }

}
