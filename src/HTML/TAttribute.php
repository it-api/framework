<?php

namespace Barebone\HTML;

class TAttribute {

    /**
     * The name of the attribute
     */
    public $name;

    /**
     * The value of the attribute
     */
    public $value;

    /**
     * The contructor for the attribute
     */
    public function __construct($name, $value = '') {
        $this->name = $name;
        $this->value = $value;
    }

    function __toString() {
        return ' ' . $this->name . '="' . $this->value . '"'; // name="value"
    }
	
	public function getName() {
		return $this->name;
	}

	public function getValue() {
		return $this->value;
	}

	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	public function setValue($value) {
		$this->value = $value;
		return $this;
	}



}
