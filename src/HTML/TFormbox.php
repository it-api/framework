<?php

namespace Barebone\HTML;

/**
 * TFormBox is a div with 2 columns. One for label and one for input field
 * @example
 * $usernamefb = new Barebone\HTML\TFormbox(
        new Barebone\HTML\TLabel('Username'), 
        new Barebone\HTML\TInput('Username', ['type' => 'text'])
    );
    
    $form->add_element($usernamefb);
	    
 * <div class="form-box">
 *      <div class="form-label"><label>Username</label></div>
 *      <div class="form-input"><input name="Username" type="text"></div>
 * </div>
 */
class TFormbox extends TElement {

    /**
     * @var Barebone\HTML\TLabel
     */
    private $label;

    /**
     * @var Barebone\HTML\TElement
     */
    private $input;

    function __construct(TLabel $label, TElement $input) {

        parent::__construct('div', ['class' => 'form-box']);
        $this->label = $label;
        $this->input = $input;

        $form_label_wrap = new TElement('div', ['class' => 'form-label']);
        $form_label_wrap->append($label);
        $form_input_wrap = new TElement('div', ['class' => 'form-input']);
        $form_input_wrap->append($input);

        $this->append($form_label_wrap);
        $this->append($form_input_wrap);
    }

    function getLabel() {
        return $this->label;
    }

    function getInput() {
        return $this->input;
    }

}
