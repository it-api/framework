<?php

namespace Barebone;
use Barebone\Application\Framework;
/**
 * Description of ApplicationFrameworkController
 * Barebone 
 * UTF-8
 *
 * @author Frank Teklenburg <frank@it-api.nl>
 * @filesource ApplicationFrameworkController.php
 * @since 20-jan-2017 22:41:49
 * 
 */
class ApplicationFrameworkController extends Controller {
    
    /**
     * The applications framework
     * @var Framework
     */
    public $framework;
    
    /**
     * @overrides controller __construct()
     * Constructor
     */
    public function __construct() {        
        parent::__construct();        
        $this->framework = new Framework(); 
        $this->layout->app_json = $this->framework;        
    }

    /**
     * @overrides controller __destruct()
     * On destruction all change to the framework are saved to the application_config.json
     */
    function __destruct() {
        $this->layout->set_layout( $this->framework->getTheme() );
        parent::__destruct();
        $router = Router::getInstance();
        $filename = FULL_APPS_PATH . $router->application . '/config/application_config.json';
        file_put_contents($filename, $this->layout->app_json);
    }
}
