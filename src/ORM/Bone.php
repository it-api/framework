<?php

namespace Barebone\ORM;

/**
 * Description of Bone
 * ComposerFW 
 * UTF-8
 *
 * @author Frank
 * @filesource Bone.php
 * @since 19-feb-2017 1:14:22
 * 
 */
class Bone {
    
    /**
     * The unique id of the bone
     * @var integer
     */
    private $id = null;
    /**
     * The name of the bone
     * @var string The name of the table
     */
    private $name;
    
    /**
     * The columns and values of the table
     * @var array ['Username' => 'MyUsername']
     */
    private $properties;
    
    /**
     * The bone constructor
     * @param string $name
     * @param array $properties
     */
    public function __construct($name, $properties, $id = null) {
        $this->setName($name);
        $this->setProperties($properties);
        if(!is_null($id)){
            $this->setId($id);
        }
    }

    /**
     * Gets the name of the bone
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Gets the properties of the bone
     * @return array
     */
    public function getProperties() {
        return $this->properties;
    }

    /**
     * Sets the name of the bone
     * @param string $name
     * @return $this
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * Sets the properties of the bone
     * @param array $properties
     * @return $this
     */
    public function setProperties($properties) {
        $this->properties = $properties;
        return $this;
    }

    /**
     * Returns the id of the bone
     * @return integer
     */
    public function getId() {
        return $this->id;
    }
    
    /**
     * Sets the id of the bone
     * When the id is set it is used to update the bone
     * @param integer $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }


}
