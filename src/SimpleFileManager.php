<?php

namespace Barebone;

class SimpleFileManager{
    
    private $path;
    private $adresid;
    
    function __construct( string $path, int $adresid ){
        $this->path = $path;
        $this->adresid = $adresid;
    }
    
    function create_table(): HTML\TElement{
        
        $table = new \Barebone\HTML\TElement('table', [ 
            'style' => 'width: 100%',
            'class' => 'ui-table'
        ]);
        $header_row = new \Barebone\HTML\TElement('tr');
        $header_row->append(new \Barebone\HTML\TElement('td', ['innertext' => '']));
        $header_row->append(new \Barebone\HTML\TElement('td', ['innertext' => 'Name']));
        $header_row->append(new \Barebone\HTML\TElement('td', ['innertext' => 'Size']));
        $header_row->append(new \Barebone\HTML\TElement('td', ['innertext' => 'Created']));
        $header_row->append(new \Barebone\HTML\TElement('td', ['innertext' => 'Rights']));
        $header_row->append(new \Barebone\HTML\TElement('td', ['innertext' => '']));
        $table->append($header_row);
        
        return $table;
    }
    
    function create_js_script(){
        
        echo '<script>
        $(".ui-buttons").button();
        delete_from_dossier = function( filename ){
            Show_Dialog(
                "Weet je het zeker?", 
                "Weet je zeker dat je dit bestand wilt verwijderen?", 
                "question", 
                ["Ja", "Nee"], 
                function(btn){
                    if(btn === "Ja"){
                        $.php("/backend/klantenkaart/delete_from_dossier/",{
                            adresid: '.$this->adresid.', 
                            filename: filename });
                    }
                })
        };
        delete_folder_from_dossier = function( folder ){
            Show_Dialog(
                "Weet je het zeker?", 
                "Weet je zeker dat je de gehele folder wilt verwijderen? Dit verwijderd ook alle bestanden in deze folder en het is onherstelbaar!", 
                "question", 
                ["Ja", "Nee"], 
                function(btn){
                    if(btn === "Ja"){
                        $.php("/backend/klantenkaart/delete_folder_from_dossier/",{
                            adresid: '.$this->adresid.', 
                            folder: folder });
                    }
                })
        };
        </script>';
        
    }
    
    function render(){
        
        $folders = glob($this->path . '*', GLOB_ONLYDIR);
        $files = array_filter(glob($this->path . '*'), 'is_file');
        
        $table = $this->create_table();
        
        asort($folders);
        asort($files);
        
        foreach($folders as $folder){
            
            $row = new \Barebone\HTML\TElement('tr', ['id'=>'row-folder-'.basename($folder)]);
            $row->append(new \Barebone\HTML\TElement('td', [
                'innertext' => new \Barebone\HTML\TElement(
                    'img', [
                        'style'=>'cursor:pointer', 
                        'src' => '/images/folder.png', 
                        'onclick'=>"$('#folder-".basename($folder)."').toggle()"
                    ]) 
                ]
            ));
            $row->append(new \Barebone\HTML\TElement('td', [
                'innertext' => basename($folder), 
                'style'=>'cursor:pointer', 
                'onclick'=>"$('#folder-".basename($folder)."').toggle()"
            ]));
            $row->append(new \Barebone\HTML\TElement('td', [
                'innertext' => filesize($folder)
            ]));
            $row->append(new \Barebone\HTML\TElement('td', [
                'innertext' => date ("F d Y H:i:s.", filemtime($folder))
            ]));
            
            $row->append(new \Barebone\HTML\TElement('td', ['innertext' => posix_getpwuid(fileowner($folder))['name'].':'.posix_getgrgid(filegroup($folder))['name'].' - '.substr(sprintf('%o', fileperms($folder)), -4)]));
            $row->append(new \Barebone\HTML\TElement('td', ['innertext' => '
            <a href="#" onclick="delete_folder_from_dossier(\''.($folder).'/\');return false"><span class="ui-button-icon ui-icon ui-icon-trash" title="Verwijderen"></span></a>
            ']));
            $table->append($row);
            
            $folderfiles = glob($folder . '/*');
            
            if($folderfiles){
                
                $irow = new \Barebone\HTML\TElement('tr',[
                    'id'=>'folder-'.basename($folder), 
                    'style' => 'display:none'
                ]);
                // indent
                $irow->append(new \Barebone\HTML\TElement('td'));
                // create subtable
                $subtable = $this->create_table();
                $subtable->css('width', '100%');
                // add it to the row of the
                $irow->append(new \Barebone\HTML\TElement('td', ['colspan'=>'5', 'innertext' => $subtable]));
                
                foreach($folderfiles as $file){
                    
                    $subrow = new \Barebone\HTML\TElement('tr',['id'=> str_replace('/','',str_replace('.','',basename($folder).'/'.basename($file)))]);
                    $subtable->append($subrow);
                    
                    $subrow->append(new \Barebone\HTML\TElement('td', ['innertext' => new \Barebone\HTML\TElement('img', ['src' => '/images/'.(new \Barebone\Filesystem())->getFileIconClass($file).'.png'])  ]));
                    $subrow->append(new \Barebone\HTML\TElement('td', ['innertext' => basename($file)]));
                    $subrow->append(new \Barebone\HTML\TElement('td', ['innertext' => filesize($file).' bytes']));
                    $subrow->append(new \Barebone\HTML\TElement('td', ['innertext' => date ("F d Y H:i:s.", filemtime($file))]));
                    $subrow->append(new \Barebone\HTML\TElement('td', ['innertext' => posix_getpwuid(fileowner($file))['name'].':'.posix_getgrgid(filegroup($file))['name'].' - '.substr(sprintf('%o', fileperms($folder)), -4)]));
                    $subrow->append(new \Barebone\HTML\TElement('td', ['innertext' => '
                    <a href="#" onclick="delete_from_dossier(\''.basename($folder).'/'.basename($file).'\');return false"><span class="ui-button-icon ui-icon ui-icon-trash" title="Verwijderen"></span></a>
                    <a target="_blank" href="/backend/klantenkaart/view_from_dossier/?adresid='.$this->adresid.'&filename='.urlencode(basename($folder).'/'.basename($file)).'"><span class="ui-button-icon ui-icon ui-icon-search" title="Bekijken"></span></a>
                    <a target="_blank" href="/backend/klantenkaart/download_from_dossier/?adresid='.$this->adresid.'&filename='.urlencode(basename($folder).'/'.basename($file)).'"><span class="ui-button-icon ui-icon ui-icon-disk" title="Download"></span></a>
                    ']));
                    
                    //$subtable->append($subrow);
                }
                $table->append($irow);
            }
        }
        
        foreach($files as $file){
            
            $row = new \Barebone\HTML\TElement('tr',['id'=> str_replace('/','',str_replace('.','',basename($file)))]);
            $row->append(new \Barebone\HTML\TElement('td', ['innertext' => new \Barebone\HTML\TElement('img', ['src' => '/images/'.(new \Barebone\Filesystem())->getFileIconClass($file).'.png'])  ]));
            $row->append(new \Barebone\HTML\TElement('td', ['innertext' => basename($file)]));
            $row->append(new \Barebone\HTML\TElement('td', ['innertext' => filesize($file).' bytes']));
            $row->append(new \Barebone\HTML\TElement('td', ['innertext' => date ("F d Y H:i:s.", filemtime($file))]));
            $row->append(new \Barebone\HTML\TElement('td', ['innertext' => posix_getpwuid(fileowner($file))['name'].':'.posix_getgrgid(filegroup($file))['name'].' - '.substr(sprintf('%o', fileperms($file)), -4)]));
            $row->append(new \Barebone\HTML\TElement('td', ['innertext' => '
            <a class="ui-buttons" href="#" onclick="delete_from_dossier(\''.basename($file).'\');return false"><span class="ui-button-icon ui-icon ui-icon-trash" title="Verwijderen"></span></a>
            <a class="ui-buttons" target="_blank" href="/backend/klantenkaart/view_from_dossier/?adresid='.$this->adresid.'&filename='.urlencode(basename($file)).'"><span class="ui-button-icon ui-icon ui-icon-search" title="Bekijken"></span></a>
            <a class="ui-buttons" target="_blank" href="/backend/klantenkaart/download_from_dossier/?adresid='.$this->adresid.'&filename='.urlencode(basename($file)).'"><span class="ui-button-icon ui-icon ui-icon-disk" title="Download"></span></a>
            ']));
            $table->append($row);
        }
        echo '<style>.ui-icon{width: 16px;height:16px}</style>';
        echo $table;
        
        $this->create_js_script();
    }
}
