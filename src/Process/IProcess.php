<?php

namespace Barebone\Process;

/**
 *
 * @author Frank
 */
interface IProcess {
    
    function start();
    function update();
    function finish();
    function failed();
    
}
