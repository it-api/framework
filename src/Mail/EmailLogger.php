<?php

namespace Barebone\Mail;

/**
 * Description of EmailLogger
 * vlb2016 
 * UTF-8
 *
 * @author Frank
 * @filesource EmailLogger.php
 * @since 21-dec-2016 22:56:15
 * 
 */
class EmailLogger {
    
    private $email;
    function __construct(Email $email) {
        $this->email = $email;
    }
    
    function log($event){
         // log the email;
        debug($this->email);
    }
}
