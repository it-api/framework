<?php

namespace Barebone\Mail;

/**
 * Description of Reciever
 * vlb2016 
 * UTF-8
 *
 * @author Frank
 * @filesource Reciever.php
 * @since 22-dec-2016 1:06:53
 * 
 */
class Reciever {

    private string $name;
    private string $emailadres;

    function __construct(string $emailadres, string $name) {
        $this->emailadres = $emailadres;
        $this->name = $name;
    }

    public function getName():string {
        return $this->name;
    }

    public function getEmailadres():string {
        return $this->emailadres;
    }

    public function setName(string $name) {
        $this->name = $name;
        return $this;
    }

    public function setEmailadres(string $emailadres) {
        $this->emailadres = $emailadres;
        return $this;
    }

}
