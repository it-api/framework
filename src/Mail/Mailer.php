<?php

namespace Barebone\Mail;

use \Mailgun\Mailgun;

/**
 * Description of Mailer
 * vlb2016 
 * UTF-8
 *
 * @author Frank
 * @filesource Mailer.php
 * @since 21-dec-2016 21:38:36
 * 
 */
class Mailer {

    private $mgClient;
    private $domain = 'mailgun.domain';

    function __construct() {
        $config = \Barebone\Config::getInstance();
        $mg_apikey = $config->getValue('mailgun', 'apikey');
        $this->mgClient = Mailgun::create($mg_apikey);
    }

    function send_batch(Batch $batch) {
        foreach ($batch->getMails() as $email) {
            $this->send_email($email);
        }
    }

    function send_email(Email $email) {

        try {

            $bijlages = $email->getAttachements();
            $headers = $email->getHeaders();

            $msgBldr = new \Mailgun\Message\MessageBuilder();
            foreach ($bijlages as $bijlage) {
                $msgBldr->addAttachment($bijlage->getPath(), $bijlage->getName());
            }
            foreach ($headers as $header) {
                $msgBldr->addCustomheader($header['name'], $header['value']);
            }
            $msgBldr->setFromAddress($email->getFrom()->getName() . ' <' . $email->getFrom()->getEmailadres() . '>');
            $msgBldr->setSubject($email->getSubject());
            
            $msgBldr->setHtmlBody($email->getBody());
            $msgBldr->addToRecipient($email->getTo()->getName() . ' <' . $email->getTo()->getEmailadres() . '>');
            $msgBldr->setReplyToAddress(Config()->getValue('author', 'email'));
            
            $result = $this->mgClient->messages->send($this->domain, $msgBldr->getMessage(), $msgBldr->getFiles());

            // if response is bounced then let the bouncehandler take care of the mail
            if (! method_exists($result, 'getId')) {

                $bounceHandler = new BounceHandler();
                $bounceHandler->handle($email);
                
            } else {

                return $result->getId();

            }
            
        } catch (\Exception $exc) {
            
        }

    }

}
