<?php

namespace Barebone\Mail;

/**
 * Description of Email
 * ComposerFW 
 * UTF-8
 *
 * @author Frank
 * @filesource Email.php
 * @since 6-mrt-2017 1:44:08
 * 
 */
class Email {

	/**
	 * Id of the mail after it is send
	 * @var type 
	 */
	protected $_id;

	/**
	 * Timestamp of sending
	 * @var type 
	 */
	protected $_timestamp;

	/**
	 *
	 * @var string subject of the email
	 */
	private $Subject;

	/**
	 *
	 * @var string body of the email
	 */
	private $Body;

	/**
	 *
	 * @var array [email, name]
	 */
	private $From = [];

	/**
	 *
	 * @var array [email, name]
	 */
	private $To = [];

	/**
	 *
	 * @var array [email, name]
	 */
	private $CC = [];

	/**
	 *
	 * @var array [email, name]
	 */
	private $BCC = [];

	/**
	 *
	 * @var array [name, path]
	 */
	private $Attachements = [];

	/**
	 *
	 * @var array [name, value]
	 */
	private $Headers = [];

	/**
	 * 
	 */
	function __construct() {

	}
	
	/**
	 * Returns the subject of the email
	 * @return string
	 */
	public function getSubject() {
		return $this->Subject;
	}

	/**
	 * Returns the body of the email
	 * @return string
	 */
	public function getBody() {
		return $this->Body;
	}

	/**
	 * 
	 * @return Sender
	 */
	public function getFrom() {
		return $this->From;
	}

	/**
	 * 
	 * @return Sender
	 */
	function getTo() {
		return $this->To;
	}

	/**
	 * 
	 * @return Sender
	 */
	public function getCC() {
		return $this->CC;
	}

	/**
	 * 
	 * @return Sender
	 */
	public function getBCC() {
		return $this->BCC;
	}

	/**
	 * 
	 * @return array of type Attachment
	 */
	public function getAttachements() {
		return $this->Attachements;
	}

	/**
	 * 
	 * @return array of type Header
	 */
	public function getHeaders() {
		return $this->Headers;
	}

	/**
	 * Set the subject of the mail
	 * @param string $Subject
	 * @return $this
	 */
	public function setSubject($Subject) {
		$this->Subject = $Subject;
		return $this;
	}

	/**
	 * Set he content of the mail
	 * @param type $Body
	 * @return $this
	 */
	public function setBody($Body) {
		$this->Body = $Body;
		return $this;
	}

	/**
	 * 
	 * @param string $email
	 * @param string $name
	 * @return $this
	 */
	public function setFrom($email, $name = '') {
		$this->From = new Sender($email, $name);
		return $this;
	}

	/**
	 * 
	 * @param string $email
	 * @param string $name
	 * @return $this
	 */
	public function setCC($email, $name = '') {
		$this->CC = new Reciever($email, $name);
		return $this;
	}

	/**
	 * 
	 * @param string $email
	 * @param string $name
	 * @return $this
	 */
	public function setBCC($email, $name = '') {
		$this->BCC = new Reciever($email, $name);
		return $this;
	}

	/**
	 * Set an array of attachments for this email
	 * @param array $Attachements
	 * @return $this
	 */
	public function setAttachements($Attachements) {
		$this->Attachements = $Attachements;
		return $this;
	}

	/**
	 * 
	 * @param string $email
	 * @param string $name
	 * @return $this
	 */
	public function setTo($email, $name = '') {
		$this->To = new Reciever($email, $name);
		return $this;
	}

	/**
	 * Add an attachement tothis email
	 * @param type $name
	 * @param type $path
	 * @return $this
	 */
	public function addAttachement($name, $path) {
		$this->Attachements[] = new Attachement($name, $path);
		return $this;
	}

	/**
	 * Add custom header
	 * @param type $name
	 * @param type $value
	 * @return $this
	 */
	public function addHeader($name, $value) {
		$this->Headers[] = ['name' => $name, 'value' => $value];
		return $this;
	}

	/**
	 * Send the emeail
	 * @return message_id van Mailgun
	 */
	function send() {
		$mailer = new Mailer();
		return $mailer->send_email($this);
	}

}
