<?php

namespace Barebone\Mail;

/**
 * Description of Attachement
 * vlb2016 
 * UTF-8
 *
 * @author Frank
 * @filesource Attachement.php
 * @since 21-dec-2016 22:46:06
 * 
 */
class Attachement {
    
    private string $name;
    private string $path;
    
    function __construct(string $name, string $path) {
        $this->name = $name;
        $this->path = $path;
    }
    
    public function getName():string {
        return $this->name;
    }

    public function getPath():string {
        return $this->path;
    }

    public function setName(string $name) {
        $this->name = $name;
        return $this;
    }

    public function setPath(string $path) {
        $this->path = $path;
        return $this;
    }

}
