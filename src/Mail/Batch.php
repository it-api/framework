<?php

namespace Barebone\Mail;

/**
 * Description of Batch
 * vlb2016 
 * UTF-8
 *
 * @author Frank
 * @filesource Batch.php
 * @since 21-dec-2016 22:33:46
 * 
 */
class Batch {
    
    private $emails = [];
    
    function addEmail(Email $email){
        $this->emails[] = $email;
    }
    
    function getMails():array {
        return $this->emails;
    }
    
    function send():void {
        $mailer = new Mailer();
        foreach ($this->getMails() as $email){
            $mailer->send_email($email);
        }
    }
}
