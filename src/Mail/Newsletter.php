<?php

namespace Barebone\Mail;

/**
 * Description of Newsletter
 * vlb2016 
 * UTF-8
 *
 * @author Frank
 * @filesource Newsletter.php
 * @since 21-dec-2016 21:42:47
 * 
 */
class Newsletter {
    
    function subscribe(){}
    
    function unsubscribe(){}
    
    function send(){}
}
