<?php

namespace Barebone;

use Barebone\Database\Connectors\PDO;
use Barebone\Database\Table;
use Barebone\Database\Schema\Schema;
use Barebone\Database\Column;
use Barebone\Functions;

/**
 * Barebone Database model
 * @author Frank
 *
 */
class Database {

    /**
     *
     * @var PDO
     */
    public PDO $pdo;

    /**
     * Barebone\Database constructor
     * Gets the database information like username, password, database from the config object
     * If you have set the connection information it will create a PDO object
     * It will then set the connection to UTF8
     */
    function __construct() {

        $config = Config::getInstance();
        $host = $config->getValue('database', 'dbhost');
        $user = $config->getValue('database', 'dbuser');
        $pass = $config->getValue('database', 'dbpass');
        $name = $config->getValue('database', 'dbname');

        if (!empty($host) && !empty($user) && !empty($pass) && !empty($name)) {
            try {

                $this->pdo = new PDO($host, $user, $pass, $name);
                $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                
            } catch (\PDOException $e) {

                //echo 'Unable to create database connection.';
                die();
            }
        }
    }
    
    function can_connect(): bool{
        
        $config = Config::getInstance();
        $host = $config->getValue('database', 'dbhost');
        $user = $config->getValue('database', 'dbuser');
        $pass = $config->getValue('database', 'dbpass');
        $name = $config->getValue('database', 'dbname');

        if (!empty($host) && !empty($user) && !empty($pass) && !empty($name)) {
            try {

                $this->pdo = new PDO($host, $user, $pass, $name);
                $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return true;
            } catch (\PDOException $e) {

                return false;
            }
        }
        
        return false;
    }
    
    function load_sql_from_disk(string $filename){
        if(file_exists($filename)){
            $sqls = explode(";\n", file_get_contents($filename));
            try{
                $this->batch_execute($sqls);
                $this->create_activerecords(true, false, false);
            } catch(\Exception $e){
                debug($e);
            }
        }
    }
    
    /**
     * Returns the name of the database in use
     */
    function get_database() {
        return $this->pdo->query('select database()')->fetchColumn();
    }
    
    /**
     * Returns array [host,user,pass,name,tables[]]
     */
    function get_database_info(string $dbname = '') {

        if ($dbname === '') {
            $dbname = $this->get_database();
        }

        $config = Config::getInstance();
        $host = $config->getValue('database', 'dbhost');
        $user = $config->getValue('database', 'dbuser');
        $pass = $config->getValue('database', 'dbpass');
        $name = $config->getValue('database', 'dbname');

        return [
            'host' => $host,
            'user' => $user,
            'pass' => $pass,
            'name' => $name,
            'tables' => $this->get_table_names($dbname)
        ];
    }
    
    /**
     * Change database at runtime
     */
    function change_database(string $dbhost, string $dbuser, string $dbpass, string $dbname) {

        try {

            $this->pdo = new PDO($dbhost, $dbuser, $dbpass, $dbname);
            $this->pdo->exec("SET NAMES 'utf8';SET CHARACTER SET utf8;SET CHARACTER_SET_CONNECTION=utf8;SET GLOBAL sql_mode = '';use {$dbname}");
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return true;
        } catch (\PDOException $e) {

            echo 'Unable to create database connection.';
            //echo $e->getTraceAsString();
            die();
        }
    }

    /**
     * Create a new schema in the database
     * @param array $options
     */
    function create_schema(array $options) {

        $schema = new Schema($options);
        $schema->create_in_database();
    }
    /**
     * Merge a schema that is allready in the database with your schema
     * @param array $options
     */
    function merge_schema(array $options) {

        $schema = new Schema($options);
        $schema->merge_in_database();
    }
    /**
     * Create a table in the database
     * @param array $options
     * @param string $database name of the database
     */
    function create_table(array $options, string $database = '') {

        $table = new Table($options);
        $table->create_in_database($database);
    }

    /**
     * Merge a table into the database
     * @param array $options
     * @param string $database name of the database
     */
    function merge_table(array $options, string $database = '') {

        $table = new Table($options);
        $table->merge_in_database($database);
    }
    
    function get_schema_json($tablename, $dbname): string{
        $filename = FULL_BAREBONE_PATH . "models/schemas/{$dbname}/{$tablename}.json";
        if(!file_exists($filename)){
            throw new Exception(sprintf('The schema file %s does not exist', $filename));
        }
        return file_get_contents($filename);
    }
    
    function get_schema_data($tablename, $dbname): object{
        $filename = FULL_BAREBONE_PATH . "models/schemas/{$dbname}/{$tablename}.json";
        if(!file_exists($filename)){
            throw new Exception(sprintf('The schema file %s does not exist', $filename));
        }
        return json_decode( file_get_contents($filename) );
    }

    /**
     * Drop all created database models or pass in an array with the models you want to drop
     */
    function drop_activerecords(array $models = []) {
        
        $dbname = Config::getInstance()->getValue('database', 'dbname');
        $models_db_path = FULL_BAREBONE_PATH . 'models/database/';
        $models_schema_path = FULL_BAREBONE_PATH . 'models/schemas/';
        $save_path = $models_db_path . $dbname . '/';
        $schema_path = $models_schema_path . $dbname . '/';
        if (count($models) === 0) {

            $fs = new Filesystem();
            $fsmodels = $fs->get_Files_list($save_path);
            $fsschemas = $fs->get_Files_list($schema_path);

            foreach ($fsmodels as $model) {
                unlink($save_path . $model);
            }

            foreach ($fsschemas as $schema) {
                unlink($schema_path . $schema);
            }
        } else {

            foreach ($models as $model) {
                unlink($save_path . $model . '.php');
                unlink($schema_path . $model . '.json');
            }
        }
        
        try{
            rmdir($save_path);
            rmdir($schema_path);
        } catch(Exception $e){}
    }

    /**
     * Create actual php database activerecord objects
     * that reflect your database tables
     */
    function create_activerecords(
        bool $create_activerecords, 
		bool $create_activerecords_service_classes, 
		bool $overwrite_activerecords_service_classes
	) {

        $dbname = Config::getInstance()->getValue('database', 'dbname');
        $models_path = FULL_BAREBONE_PATH . 'models/';
        $models_db_path = FULL_BAREBONE_PATH . 'models/database/';
        $models_schema_path = FULL_BAREBONE_PATH . 'models/schemas/';
        $save_path = $models_db_path . $dbname . '/';
        $schema_path = $models_schema_path . $dbname . '/';
        if (!is_dir($models_path)) {
            mkdir($models_path, 0755);
        }
        if (!is_dir($models_db_path)) {
            mkdir($models_db_path, 0755);
        }
        if (!is_dir($models_schema_path)) {
            mkdir($models_schema_path, 0755);
        }
        if (!is_dir($save_path)) {
            mkdir($save_path, 0755);
        }
        if (!is_dir($schema_path)) {
            mkdir($schema_path, 0755);
        }

        if (is_dir(TEMP_PATH . 'activerecords')) {
            //mkdir(TEMP_PATH. 'activerecords');
            $fs = new Filesystem();
            $fs->remove_directory(TEMP_PATH . 'activerecords');
        }

        $stmt = $this->pdo->prepare("SHOW TABLES");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $classcode = '';
        
        $author_name = Config()->getValue('author', 'name');
        $author_email = Config()->getValue('author', 'email');
        $author_copyright = Config()->getValue('author', 'copyright');
        
        foreach ($result as $key2 => $val) {

            foreach ($val as $key => $table) {

                if (stristr($table, '-')) {
                    continue;
                }
                
                // create a service class for activerecord
                $serviceCode = "<?php\n\n";
                $serviceCode .= "class {$table}Service {\n\n";
                $serviceCode .= "\tpublic function __construct(private readonly {$table} \${$table}){\n\n\t}\n\n";
                
                $serviceCode .= "\tpublic function getById(int \$id){\n\n";
                $serviceCode .= "\t\treturn \$this->{$table}->get_by_id(\$id);\n\n";
                $serviceCode .= "\t}\n\n";
                
                $serviceCode .= "}\n\n";
                $service_filename = $save_path . $table . 'Service.php';
                if($create_activerecords_service_classes === true){
                    if(file_exists($service_filename) && $overwrite_activerecords_service_classes === true){
                        file_put_contents($service_filename, $serviceCode);
                    } elseif(!file_exists($service_filename)) {
                        file_put_contents($service_filename, $serviceCode);
                    }
                }
                $filename = $save_path . $table . '.php';
                $cols = array();
                $classcode = "<?php\n\n";
                //$classcode .= "#namespace Barebone\Activerecord; \n\n";
                $classcode .= "use Barebone\Activerecord; \n\n";
                $classcode .= "/**\n";
                $classcode .= " * Activerecord reflects the database table {$table} \n";
                $classcode .= " * This class inherits getters and setters from the Activerecord \n";
                $classcode .= " * This file is automaticly genarated by Barebone. Do not edit this file.\n";
                $classcode .= " * Use the service files or extend your own classes from this Activerecord.\n";
                $classcode .= " * @since " . date('d-m-Y') . "\n";
                $classcode .= " * @author {$author_name}\n";
                $classcode .= " * @email {$author_email}\n";
                $classcode .= " * @copyright {$author_copyright} \n";
                $classcode .= " */\n";

                $classcode .= "class {$table}  extends Activerecord{ \n\n";
                $stmt2 = $this->pdo->prepare("SHOW FULL COLUMNS  FROM `{$table}`;");
                $stmt2->execute();
                $result2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
                //print_r($result2);
                foreach ($result2 as $column) {

                    // create automatic validitors
                    $validation = new \stdClass;
                    $validation->rules = array();
                    $validation->messages = array();

                    if (stristr($column['Type'], 'varchar')) {

                        $validation->rules['required'] = true;
                        $validation->messages['required'] = 'This field cannot be empty';
                    } elseif (stristr($column['Type'], 'text')) {

                        $validation->rules['required'] = true;
                        $validation->messages['required'] = 'This field cannot be empty';
                    } elseif (stristr($column['Type'], 'int')) {

                        $validation->rules['digits'] = true;
                        $validation->messages['digits'] = 'This field can only contain numers';
                    } elseif (stristr($column['Type'], 'datetime')) {

                        $validation->rules['datetime'] = true;
                        $validation->messages['datetime'] = 'Please enter a valid date and time.';
                    } elseif (stristr($column['Type'], 'set')) {

                        $set = $column['Type'];
                        $set = substr($set, 5, strlen($set) - 7);
                        // Split into an array.
                        $setarray = preg_split("/','/", $set);
                        $validation->rules['set'] = true;
                        $validation->messages['set'] = 'Enter 1 of the following values: ' . implode(',', $setarray) . '.';
                    } elseif (stristr($column['Type'], 'enum')) {

                        $enum = $column['Type'];
                        $enum = substr($enum, 5, strlen($enum) - 7);
                        // Split into an array.
                        $enumarray = preg_split("/','/", $enum);
                        $validation->rules['enum'] = true;
                        $validation->messages['enum'] = 'Enter 1 of the following values: ' . implode(',', $enumarray) . '.';
                    }


                    $cols[] = new Column(
                        $column['Field'],
                        $column['Type'],
                        [],
                        $column['Comment'],
                        $column['Key'] === 'PRI',
                        $column['Key'] === 'NO',
                        $column['Default'],
                        stristr($column['Extra'], 'auto_increment'),
                        $column['Validation'] = $validation
                    );



                    //echo json_encode($validation);
                }

                /**
                 * "validation": {
                  "rules": [
                  {
                  "required": true
                  }
                  ],
                  "messages": [
                  {
                  "required": "This field is required"
                  }
                  ]
                  }
                  },
                 * @var array $options
                 */
                $opts = array(
                    'name' => $table,
                    'encoding' => 'utf8',
                    'type' => $this->get_engine($table),
                    'schema' => $cols
                );
                $options = Functions::array_to_object($opts);

                file_put_contents($schema_path . $options->name . '.json', json_encode($options, JSON_PRETTY_PRINT));

                foreach ($result2 as $key => $col) {

                    $classcode .= "\t /**\n";
                    $classcode .= "\t  * " . ((strlen($col['Comment']) > 0 && strstr($col['Comment'], '/*') === false) ? $col['Comment'] : '@columnname '.$col['Field']) . "\n";
                    $classcode .= "\t  * @columntype " . (strstr($col['Type'], '/*') === false ? $col['Type'] : '') . "\n";
                    $classcode .= "\t  * @param ".$this->get_php_type_from_column_type($col['Type'])." \${$col['Field']} \n";
                    $classcode .= "\t  */ \n";
                    if($this->get_php_type_from_column_type($col['Type']) === 'int'){
                        $classcode .= "\t public "
                        .'?'.$this->get_php_type_from_column_type($col['Type'])
                        ." \${$col['Field']}" 
                        . ($col['Default'] === null ? ' = NULL' : " = "
                        . $col['Default'] 
                        ) . ";\n\n ";
                    } else {
                        $classcode .= "\t public "
                        .'?'.$this->get_php_type_from_column_type($col['Type'])
                        ." \${$col['Field']}" 
                        . ($col['Default'] === null ? ' = NULL' : " = "
                        . "'"
                        . $col['Default'] 
                        . "'") . ";\n\n ";
                    }
                    
                }
                $classcode .= "\n}";
                
                if($create_activerecords){
                    file_put_contents($filename, $classcode);
                }
                
                foreach ($result2 as $key => $col) {
                    //echo $this->get_php_type_from_column_type($col['Type'])."\n";
                    $gets = $this->get_set($table, $col['Field']);
                    if(count($gets) > 0){
                        $enumName = "{$table}".ucfirst($col['Field']).'Enum';
                        
                        $enumcode = "<?php\n\n";
                        $enumcode .= "/**\n";
                        $enumcode .= " * Enum $enumName \n";
                        $enumcode .= " * This file is automaticly genarated by Barebone. Do not edit this file.\n";
                        $enumcode .= " * @since " . date('d-m-Y') . "\n";
                        $enumcode .= " * @author {$author_name}\n";
                        $enumcode .= " * @email {$author_email}\n";
                        $enumcode .= " * @copyright {$author_copyright} \n";
                        $enumcode .= " */\n";
                        $enumcode .= "enum {$enumName}: string {\n\n";
                        $gets = $this->get_set($table, $col['Field']);
                        foreach($gets as $g => $v){
                            $enumcode .= "\tcase ".stripped(strtoupper($v))." = '{$v}';\n";
                        }
                        $enumcode .= "\n}";
                        $enumfilename = $save_path.$enumName.'.php';
                        file_put_contents($enumfilename, $enumcode);
                        //echo $enumfilename."\n";
                    }
                    
                }
                
            }
        }

        return true;
    }

    /**
     * @return array of databases
     */
    function get_databases(string $host, string $user, string $pass): array {

        $dbh = new PDO($host, $user, $pass, null);
        $dbs = $dbh->query('SHOW DATABASES');
        $databases = array();
        while (($db = $dbs->fetchColumn(0)) !== false) {
            $databases[] = $db;
        }

        return $databases;
    }

    /**
     * Get an Activerecord object
     * @param string $activerecord
     * @return \Barebone\Activerecord
     */
    function getActiveRecord(string $activerecord): \Barebone\Activerecord {

        try {
            // check if the class exists else throw an exception
            if (!class_exists($activerecord)) {
                throw new Exception('The class "' . $activerecord . '" does not exist.');
            }
            // create the class, since we now we know it exists
            $ar = new $activerecord();

            // if the class is not an istance of activerecord then throw exception
            if (!$ar instanceof Activerecord) {
                throw new Exception('The object "' . get_class($ar) . '" is not an activerecord object.');
            }
            // return the activerecord
            return $ar;
        } catch (Exception $e) {

            throw new Exception('This active record could not be found.' . $e->getMessage());
        }
    }

    /**
     * Shorthand for Activerecord
     * @param string $tablename
     * @return \Barebone\Activerecord
     */
    function table(string $tablename): \Barebone\Activerecord {
        return $this->getActiveRecord($tablename);
    }
    
    /**
     * Clean string with htmlentities
     * @param string $data
     * @return string
     */
    function clean(string $data): string {
        return htmlentities($data, ENT_QUOTES, 'UTF-8');
    }

    /**
     * @return array of tablenames
     */
    function get_table_names(string $dbname = ''): array {
        // if database is not set then get the database from the config
        if ($dbname === '') {
            $config = Config::getInstance();
            $dbname = $config->getValue('database', 'dbname');
        }
        // if database is still not set then return an empty array
        if ($dbname === '') {
            return [];
        }
        // execute sql to use the database
        $dbname = $this->clean($dbname);
        $this->pdo->exec("USE `$dbname`");

        if ($dbname === '') {
            return [];
        }
        $tablenames = array();
        $sql = "SHOW FULL TABLES WHERE Table_type='BASE TABLE'";
        $array_name = 'Tables_in_' . $dbname;

        foreach ($this->pdo->query($sql) as $row) {
            $tablenames[] = ($row[$array_name]);
        }

        return $tablenames;
    }

    /**
     * Gets an array of all columns in the table
     * @param string $tablename The name of the table
     * @param string $dbname The name of the database
     * @param bool $full SHOW FULL COLUMNS
     * @return array Columns in the table
     */
    function get_columns(string $tablename, string $dbname = null, bool $full = true): array {

        $columnnames = array();
        $config = Config::getInstance();
        $dbname === null ? $dbname = $config->getValue('database', 'dbname') : $dbname;
        $fullstring = ($full === true ? 'FULL' : '');
        $sql = "SHOW $fullstring COLUMNS  FROM `$dbname`.`$tablename`";

        $query = $this->pdo->prepare($sql);
        $query->setFetchMode(PDO::FETCH_OBJ);
        $query->execute();

        $columnnames = array();

        while ($row = $query->fetchObject()) {
            $columnnames[] = $row;
        }

        return $columnnames;
    }

    /**
     * Return only the column names as opistite to full
     * @param string $tablename The name of the table
     * @param string $dbname The name of the database
     * @param bool $full SHOW FULL COLUMNS
     * @return array Column names in the table
     */
    function get_column_names(string $tablename, string $dbname = '', bool $full = true): array {
        $columnsfull = $this->get_columns($tablename, $dbname, $full);
        $result = [];
        foreach ($columnsfull as $columnfull) {
            $result[] = $columnfull->Field;
        }

        return $result;
    }

    /**
     * Get the status of a table
     * @uses SHOW TABLE STATUS WHERE Name = :tablename
     * @param string $tablename
     * @return object
     */
    function get_table_status(string $tablename) {

        $sql = "SHOW TABLE STATUS WHERE Name = :tablename";
        $query = $this->pdo->prepare($sql);
        $query->bindParam('tablename', $tablename);
        $query->execute();
        $row = $query->fetch(PDO::FETCH_OBJ);
        return $row;
    }

    /**
     * Get the engine of a certain table
     * @param string $tablename
     * @return string
     */
    function get_engine(string $tablename) {

        $row = $this->get_table_status($tablename);
        return $row->Engine;
    }

    /**
     * Get the encoding of a certain table
     * @param string $tablename
     * @return string
     */
    function get_encoding(string $tablename) {

        $row = $this->get_table_status($tablename);
        return $row->Collation;
    }

    /**
     * Get a result from the database
     * @param string $tablename
     * @param array $where
     * @param string $orderby
     * @param string $limit
     * @param string $class_name
     * @return array
     */
    function get(string $tablename, array $where = [], string $orderby = '', string $limit = '', string $class_name = 'stdClass') {
        return $this->pdo->get($tablename, $where, $orderby, $limit, $class_name);
    }
    /**
     * Get a row from the table
     * @param string $tablename
     * @param array $where
     * @param string $orderby
     * @param string $limit
     * @param string $class_name
     * @return mixed the selected row OR false
     */
    function getrow(string $tablename, array $where = [], string $orderby = '', string $limit = '', string $class_name = 'stdClass') {

        $rows = $this->get($tablename, $where, $orderby, $limit, $class_name);
        return isset($rows[0]) ? $rows[0] : false;
    }

    /**
     * Prepare and execute a statement
     * @param string $statement the query with :placeholders
     * @param array $input_parameters array of :placeholders => value
     * @param string $clsname the name of the object returned
     * @return \Barebone\Database\Queryresult
     */
    function doquery(string $statement, array $input_parameters = [], string $clsname = 'stdClass'): \Barebone\Database\Queryresult {

        try {
            $query = $this->pdo->prepare($statement);
            $query->setFetchMode(PDO::FETCH_OBJ);
            $query->execute($input_parameters);
        } catch (\PDOException $e) {
            throw new \Barebone\Database\Exception($e->getMessage());
        }

        $rows = array();

        while ($row = $query->fetchObject($clsname)) {
            $rows[] = $row;
        }

        $result = new \Barebone\Database\Queryresult($rows, $query);

        return $result;
    }

    /**
     * Get the primary key of a table. 
     * Returns the name of the primary key else NULL
     * @param string $tablename
     * @return string|null
     */
    function getprimary(string $tablename): ?string {

        $columns = $this->get_columns($tablename);

        if (is_array($columns)) {
            foreach ($columns as $column) {
                if ($column->Key === 'PRI') {
                    return $column->Field;
                }
            }
        } else {
            return null;
        }
    }

    /**
     * Shorthand to save data to a table
     * @param string $tablename
     * @param array|object $data
     * @return mixed
     */
    function set(string $tablename, array|object $data) {
        //debug($data);
        $primarykey = $this->getprimary($tablename);

        if (!is_array($data)) {
            $data = Functions::object_to_array($data);
        }

        if (isset($data[$primarykey])) {
            return $this->update($tablename, $primarykey, $data);
        } else {
            return $this->insert($tablename, $data);
        }
    }

    function insert(string $tablename, array|object $data) {

        if (!is_array($data)) {
            $data = Functions::object_to_array($data);
        }
        // INSERT INTO db_fruit (id, type, colour) VALUES (:id, :name, :color)" 
        $column_names = array();
        $placeholders = array();
        foreach ($data as $key => $value) {
            $column_names[] = '`' . $key . '`';
            $placeholders[] = ':' . $key;
        }
        $cols_string = implode(',', $column_names);
        $placeholder_string = implode(',', $placeholders);
        $query = "INSERT INTO `$tablename` ($cols_string) VALUES ($placeholder_string)";

        try {
            $sql = $this->pdo->prepare($query);
            $sql->execute($data);
            return $this->pdo->lastInsertId();
        } catch (\PDOException $e) {
            throw new \Barebone\Database\Exception($e->getMessage());
        }
    }

    function update(string $tablename, string $primarykey, $data) {

        if (is_object($data)) {
            $id = $data->$primarykey;
        } else {
            $id = $data[$primarykey];
        }
        $values = '';
        foreach ($data as $key => $val) {
            if ($key != $primarykey) {
                $values .= "`$key` = :$key,";
            }
        }
        $values = rtrim($values, ',');
        $sql = "UPDATE `$tablename` SET $values WHERE `$primarykey` = '$id'";

        $result = $this->pdo->prepare($sql);
        foreach ($data as $key => &$val) {
            if ($key != $primarykey) {
                $result->bindParam(':' . $key, $val);
            }
        }

        try {

            $result->execute();
            $result->closeCursor();

            return $this->get_by_primary($tablename, $primarykey, $id);
        } catch (\PDOException $e) {
            throw new \Barebone\Database\Exception($e->getMessage());
        }
    }

    /**
     * return object of type $class_name or else stdClass
     * @param string $tablename name of the table
     * @param string $primary primary key van de tabel
     * @param string $id id van primary
     * @param string $class_name  name of the class of the returned object
     * @return object of type $class_name or else stdClass
     */
    function get_by_primary(string $tablename, string $primary, string $id, string $class_name = 'stdClass') {

        $sql = "SELECT * FROM `{$tablename}` WHERE `{$primary}` = '{$id}'";
        $result = $this->pdo->prepare($sql);
        $result->execute();
        $return = $result->fetchAll(PDO::FETCH_CLASS, $class_name);
        $result->closeCursor();
        return isset($return[0]) ? $return[0] : false;
    }

    /**
     * executes an SQL statement in a single function call, 
     * returning the number of rows affected by the statement. 
     * @param string $sql
     * @return int
     * @throws \Barebone\Database\Exception
     */
    function execute(string $sql) {
        try {
            return $this->pdo->exec($sql);
        } catch (\Exception $e) {
            throw new \Exception($this->pdo->errorInfo()[2]);
        }
    }

    /**
     * executes an SQL statement in a single function call, 
     * returning the number of rows affected by the statement. 
     * @param string $sql
     * @return int
     * @throws \Barebone\Database\Exception
     */
    function delete(string $sql) {
        return $this->execute($sql);
    }

    /**
     * Remove a row from a table
     * @param string $tablename the name of the table
     * @param int $id the primary key id
     */
    function removerow(string $tablename, $id) {
        $primarykey = $this->getprimary($tablename);
        $sql = "DELETE FROM `{$tablename}` WHERE `{$primarykey}` = :id";
        try {
            $result = $this->pdo->prepare($sql);
            $result->bindParam(':id', $id);
            return $result->execute();
        } catch (\PDOException $e) {
            throw new \Barebone\Database\Exception($e->getMessage());
        }
    }

    function get_date_from_interval(string $interval, string $interval_type) {
        if ($interval < 1) {
            $sql = "SELECT DATE_SUB(CURRENT_DATE(), INTERVAL {$interval} {$interval_type}) AS Datum";
        } else {
            $sql = "SELECT DATE_ADD(CURRENT_DATE(), INTERVAL {$interval} {$interval_type}) AS Datum";
        }
        //echo $sql;
        $result = $this->doquery($sql)->getRows();

        return $result[0]->Datum;
    }

    function get_set(string $table, string $field) {
        $sql = "SHOW COLUMNS FROM `{$table}` LIKE '{$field}'";
        $result = $this->pdo->query($sql);
        $row = $result->fetch(PDO::FETCH_OBJ);
        preg_match_all("/'(.*?)'/", $row->Type, $categories);
        $fields = $categories[1];
        return $fields;
    }

    /**
     * Execute an array with queries
     * @param array $sqls
     */
    function batch_execute(array $sqls): void {
        foreach ($sqls as $sql) {
            $this->execute($sql);
        }
    }
    /**
     * Guess the columntype based on the var
     * @param mixed $var
     */
    function guess_column_type(mixed $var): string {
        if (is_bool($var)) {
            return "TINYINT(1)";
        } elseif (is_numeric($var)) {
            return "INT(11)";
        } elseif (is_string($var) && strlen($var) <= 240) {
            return "VARCHAR(240)";
        } elseif (is_string($var) && strlen($var) > 240) {
            return "TEXT";
        } elseif (is_array($var)) {
            return "ENUM('".implode("','", $var)."')";
        } else {
            return "VARCHAR(240)";
        }
    }
    /**
     * Guess the openapi columntype based on the fieldType
     * @param string $fieldType
     */
    function guess_openapi_column_type(string $fieldType) {
        
        if (strstr($fieldType, 'tinyint(1)')) {
            return "boolean";
        } elseif (strstr($fieldType, 'int')) {
            return 'integer';
        } elseif (strstr($fieldType, 'varchar')) {
            return "string";
        } else {
            return 'string';
        }
    }
    /**
     * Guess the php type based on the fieldType
     * @param string $fieldType
     */
    function get_php_type_from_column_type(string $fieldType) {
        
        if (strstr($fieldType, 'tinyint')) {
            return "int";
        } elseif (strstr($fieldType, 'int')) {
            return 'int';
        } elseif (strstr($fieldType, 'varchar')) {
            return "string";
        } elseif (strstr($fieldType, 'text')) {
            return "string";
        } elseif (strstr($fieldType, 'float')) {
            return "float";
        } else {
            return 'string';
        }
    }
    /**
     * Removes - and spaces from the columnnames
     * @param array $columns
     * @param string $name
     */
    private function format_csv_columns_name(array $columns, string $name) {
        $name = str_replace('-', '_', $name);
        $name = str_replace(' ', '_', $name);
        return $columns[$name];
    }
    /**
     * Create a new table
     * 
     * @param string $tablename name for the new table
     * @param array $columns array with columns
     * @param bool $return return the create statement
     */
    function create_new_table(string $tablename, array $columns = [], bool $return = false) {

        $newcolumns = array();
        $sql = "CREATE TABLE IF NOT EXISTS `{$tablename}` (";
        foreach ($columns as $column) {

            if ($column['primary'] === true) {
                $primary_key = $column['name'];
            }
            $newcolumns[] = "\n\t`" . $column['name'] . "` " . $column['type'] . (is_numeric($column['length']) ? "(" . $column['length'] . ")" : '') . " " . $column['null'] . " " . (isset($column['default']) ? 'DEFAULT ' . $column['default'] . '' : '') . " " . ($column['primary'] === true ? ' AUTO_INCREMENT' : '');
        }
        $sql .= implode(',', $newcolumns);
        if (isset($primary_key)) {
            $sql .= "\n\t,PRIMARY KEY (`" . $primary_key . "`)";
        }
        $sql .= "\n)";

        if ($return)
            return $sql;
        else
            return $this->execute($sql);
        /*
         * SAMPLE !!
         * $db->create_new_table('tbl_blog_posts' , array(
         * 		array(
         * 			'name'=>'PostID',
         * 			'type'=>'int',
         * 			'length'=>'11',
         * 			'null'=>'NOT NULL',
         * 			'default'=>'',
         * 			'primary'=>'true',
         * 		),
         * 		array(
         * 			'name'=>'Post_title',
         * 			'type'=>'varchar',
         * 			'length'=>'250',
         * 			'null'=>'NOT NULL'
         * 		)
         * 		[etc....]
         *
         * ))
          CREATE TABLE `tbl_blog_posts` (
          `PostID` int(11) NOT NULL   AUTO_INCREMENT,
          `Post_title` varchar(250) NOT NULL  ,
          `Post_body` text NOT NULL  ,
          `Post_date` int(11) NOT NULL  ,
          `Post_author` int(11) NULL
          ,PRIMARY KEY (`PostID`)
          )
         */
    }
    /**
     * Create a new table based on csv
     * 
     * @param string $tablename name for the new table
     * @param array $csv_data array with csv data
     */
    function csv_to_table(string $tablename, array $csv_data = []) {

        $columns = [];
        //get the first row of the csv_data
        $first_row = $csv_data[0];
        //create the unique key
        $columns[] = array(
            'name' => 'UID',
            'type' => 'int',
            'length' => '11',
            'null' => 'NOT NULL',
            'primary' => 'true'
        );
        //go over the first_row array with the keys and values
        foreach ($first_row as $key => $val) {
            //kolomnamen toevoegen aan de columns array
            $name = str_replace('-', '_', $val);
            $name = str_replace(' ', '_', $name);
            //echo $name;
            $columns[] = array(
                'name' => $name,
                'type' => $this->guess_column_type($csv_data[1][$key])
            );
        }

        //create the table
        $this->create_new_table($tablename, $columns);
        // initilalize some variables
        $rowi = 0;
        $row = array();
        //go over each row
        foreach ($csv_data as $data) {
            if ($rowi > 0) { //skip first row
                //go over the columns and construct a row to insert
                foreach ($data as $key => $val) {
                    $row[$this->format_csv_columns_name($first_row, $key)] = $val;
                }
                //now we can insert the rows from the csv
                $this->insert($tablename, $row);
            }
            $rowi++;
        }
    }
    /**
     * Begin transaction
     */
    function beginTransaction(){
        try {
            $this->pdo->beginTransaction();
        } catch(\PDOException $e){
            throw new \Barebone\Database\Exception('Database does not support transactions');
        }
    }
    /**
     * Commit transaction
     */
    function commitTransaction(){
        $this->pdo->commit();
    }
    /**
     * Rollback transaction
     */
    function rollbackTransaction(){
        $this->pdo->rollBack();
    }

}
