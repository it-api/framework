<?php

namespace Barebone;

use Barebone\Classbuilder\ClassbuilderClass;
use Barebone\Classbuilder\ClassbuilderInterface;
use Barebone\Classbuilder\ClassbuilderInterfaceMethod;
use Barebone\Classbuilder\ClassbuilderMethod;
use Barebone\Classbuilder\ClassbuilderMethodParam;

class Classbuilder {

	/**
	 * Holds all interfaces
	 * @var array
	 */
	private array $interfaces = [];
	/**
	 * Holds all classes
	 * @var array
	 */
	private array $classes = [];

	/**
	 * Load the classbuilder from json file
	 * @param string $filename
	 * @throws Exception if file does not exist
	 */
	function loadFromJSONFile(string $filename) {

		if (!file_exists($filename)) {
			throw new Exception(sprintf('The file %s does not exist.', $filename));
		}
		$json = file_get_contents($filename);
		$data = json_decode($json);

		if ($data->interfaces) {
			foreach ($data->interfaces as $interface) {

				$this->addInterface($interface->name);
				if ($interface->methods) {
					foreach ($interface->methods as $imethod) {

						$this->getInterface($interface->name)->addMethod($imethod->name, $imethod->visibility, $imethod->parameters, $imethod->phpdoc);
					}
				}
			}
		}
		if ($data->classes) {
			foreach ($data->classes as $class) {

				$this->addClass($class->name, $class->extends, $class->implements);

				if ($class->properties) {
					foreach ($class->properties as $key => $cpropertie) {

						$this->getClass($class->name)->addPropertie($cpropertie->name, $cpropertie->value, $cpropertie->phpdoc, $cpropertie->visibility, $cpropertie->type);
					}
				}
				if ($class->methods) {
					foreach ($class->methods as $key => $cmethod) {

						$this->getClass($class->name)->addMethod($cmethod->name, $cmethod->visibility, $cmethod->parameters, $cmethod->phpdoc, $cmethod->body);
					}
				}
			}
		}
	}

	/**
	 * Save the classbuilder to json file
	 * @param string $filename
	 */
	function saveToJSONFile(string $filename) {

		// this will hold the json data
		$data = new \stdClass();

		/** @var ClassbuilderInterface $interface */
		foreach ($this->interfaces as $interface) {

			// the json interface is a object
			$iface = new \stdClass();
			$iface->phpdoc = $interface->getPhpdoc();
			// the name of the interface
			$iface->name = (string) $interface->getName();
			// the methods on the interface
			$iface->methods = array();

			/** @var ClassbuilderInterfaceMethod $method */
			foreach ($interface->getMethods() as $method) {

				// interface method object
				$ifacemethod = new \stdClass();
				$ifacemethod->phpdoc = $method->getPhpdoc();
				$ifacemethod->name = (string) $method->getName();
				$ifacemethod->visibility = (string) $method->getVisibility();
				$ifacemethod->params = array();

				/** @var ClassbuilderMethodParam $param */
				foreach ($method->getParams() as $param) {

					$ifacemethodparam = new \stdClass();
					$ifacemethodparam->cast = (string) $param->getCast();
					$ifacemethodparam->name = (string) $param->getName();
					$ifacemethodparam->value = (string) $param->getValue();
					$ifacemethodparam->type = (string) $param->getType();

					$ifacemethod->params[$ifacemethodparam->name] = $ifacemethodparam;
				}

				$iface->methods[$ifacemethod->name] = $ifacemethod;
			}

			$data->interfaces[$iface->name] = $iface;
		}

		/** @var ClassbuilderClass $class */
		foreach ($this->classes as $class) {

			$cls = new \stdClass();
			$cls->phpdoc = $class->getPhpdoc();
			$cls->name = $class->getClassname();
			$cls->extends = $class->getExtends();
			$cls->implements = $class->getImplements();
			$cls->methods = array();

			/** @var ClassbuilderMethod $method */
			foreach ($class->getMethods() as $method) {

				// get the implemented function from the interface
				$ifaceMethods = $data->interfaces[$cls->implements]->methods;

				// if the method not comes from the interface
				if (is_array($ifaceMethods) && !array_key_exists((string)$method->getName(), $ifaceMethods)) {

					$clsmethod = new \stdClass();
					$clsmethod->phpdoc = $method->getPhpdoc();
					$clsmethod->name = (string) $method->getName();
					$clsmethod->visibility = (string) $method->getVisibility();
					$clsmethod->params = array();

					/** @var ClassbuilderMethodParam $param */
					foreach ($method->getParams() as $param) {

						$clsmethodparam = new \stdClass();
						$clsmethodparam->cast = (string) $param->getCast();
						$clsmethodparam->name = (string) $param->getName();
						$clsmethodparam->value = (string) $param->getValue();
						$clsmethodparam->type = (string) $param->getType();

						$clsmethod->params[$clsmethodparam->name] = $clsmethodparam;
					}

					$cls->methods[$method->getName()] = $clsmethod;
				}
			}
			$data->classes[$cls->name] = $cls;
		}
	}

	function getCreateClassStatement(string $clsname) {

		if (!isset($this->classes[$clsname])) {
			throw new \Exception(\sprintf('The class %s does not exist and cannot be created.', $clsname));
		}

		$cls = $this->classes[$clsname];
		$s = "\$" . $cls->getClassname() . " = new " . $cls->getClassname() . '();';
		return $s;
	}

	/**
	 * 
	 * @param string $clsname
	 * @param string $extends
	 * @param string $implements
	 * @param array $constants
	 * @param array $properties
	 * @param array $methods
	 * @return \Barebone\Classbuilder\ClassbuilderClass
	 */
	function addClass(string $clsname, string $extends = '', string $implements = '', array $constants = [], array $properties = [], array $methods = []) {

		$cls = new ClassbuilderClass($clsname, $extends, $implements, $constants, $properties, $methods);
		$this->classes[$clsname] = $cls;

		if (isset($this->interfaces[$implements])) {

			// get the implemented function from the interface
			$ifaceMethods = $this->getInterface($implements)->getMethods();

			// force them onto the class that implements the interface
			foreach ($ifaceMethods as $key => $method) {

				$this->getClass($clsname)->addMethod($method->getName(), 'public', $method->getParams(), $method->getPhpdoc());
			}
		}
		return $cls;
	}

	function addInterface(string $name) {

		$interface = new ClassbuilderInterface($name);
		$this->interfaces[$name] = $interface;
		return $interface;
	}

	/**
	 * 
	 * @param string $name
	 * @return \Barebone\Classbuilder\ClassbuilderClass
	 */
	function getClass(string $name) {
		return $this->classes[$name];
	}

	/**
	 * 
	 * @param unknown $name
	 * @return \Barebone\Classbuilder\ClassbuilderInterface
	 */
	function getInterface(string $name) {
		return $this->interfaces[$name];
	}

	function __toString(): string {

		$s = '';

		foreach ($this->interfaces as $interface) {
			$s .= $interface;
		}

		foreach ($this->classes as $cls) {
			$s .= $cls;
		}

		return $s;
	}

	public static function file_get_php_classes(string $filepath) {
		$php_code = file_get_contents($filepath);
		$classes = self::get_php_classes($php_code);
		return $classes;
	}

	public static function file_get_php_interfaces(string $filepath) {
		$php_code = file_get_contents($filepath);
		$interfaces = self::get_php_interfaces($php_code);
		return $interfaces;
	}

	public static function get_php_classes(string $php_code) {

		$classes = [];
		$tokens = token_get_all($php_code);
		$count = count($tokens);
		for ($i = 2; $i < $count; $i++) {
			if (
				$tokens[$i - 2][0] === T_CLASS
				&& $tokens[$i - 1][0] === T_WHITESPACE
				&& $tokens[$i][0] === T_STRING
			) {

				$class_name = $tokens[$i][1];
				$classes[] = $class_name;
			}
		}
		return $classes;
	}

	public static function get_php_interfaces(string $php_code) {

		$interfaces = [];
		$tokens = token_get_all($php_code);
		$count = count($tokens);
		for ($i = 2; $i < $count; $i++) {
			if (
				$tokens[$i - 2][0] === T_INTERFACE
				&& $tokens[$i - 1][0] === T_WHITESPACE
				&& $tokens[$i][0] === T_STRING
			) {

				$interface_name = $tokens[$i][1];
				$interfaces[] = $interface_name;
			}
		}
		return $interfaces;
	}
	
	public static function get_php_traits(string $php_code) {

		$traits = [];
		$tokens = token_get_all($php_code);
		$count = count($tokens);
		for ($i = 2; $i < $count; $i++) {
			if (
				$tokens[$i - 2][0] === T_TRAIT
				&& $tokens[$i - 1][0] === T_WHITESPACE
				&& $tokens[$i][0] === T_STRING
			) {

				$trait_name = $tokens[$i][1];
				$traits[] = $trait_name;
			}
		}
		return $traits;
	}

	public static function extract_namespace(string $filepath) {
		$ns = NULL;
		$handle = fopen($filepath, "r");
		if ($handle) {
			while (($line = fgets($handle)) !== false) {
				if (strpos($line, 'namespace') === 0) {
					$parts = explode(' ', $line);
					$ns = rtrim(trim($parts[1]), ';');
					return $ns;
					break;
				}
			}
			fclose($handle);
		}
		return $ns;
	}

	public static function create_class_from_array(string $classname, array $array) {
		return self::create_class_from_json($classname, json_encode($array));
	}

	public static function create_class_from_json(string $classname, string $json) {

		$class = [];
		$class[$classname] = json_decode($json);
		$json = json_encode(['data' => $class]);
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$headers = array();
		$headers[] = "Username: ";
		$headers[] = "Password: ";
		$headers[] = "Content-Type: application/x-www-form-urlencoded";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close($ch);

		//debug(json_decode($result)->data);
		echo tab2nbsp(nl2br(json_decode($result)->data));
		return $result;
	}

	public static function file_get_php_functions(string $filename) {

		$contents = file_get_contents($filename);
		$functions = explode('function ', $contents);
		$result = [];
		foreach ($functions as $function) {

			$functionName = explode('(', $function)[0];

			if (function_exists($functionName)) {
				$result[] = $functionName;
			}
		}
		return $result;
	}
	
	public static function file_get_php_traits(string $filename){
	    
	    $php_code = file_get_contents($filename);
		$traits = self::get_php_traits($php_code);
		return $traits;
	}
}
