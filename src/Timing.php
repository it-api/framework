<?php

namespace Barebone;

/**
 * Description of Timer
 * MYFW 
 * UTF-8
 *
 * @author Frank
 * @filesource Timer.php
 * @since 8-jan-2017 13:14:36
 * 
 * @example 
 * // Create new Timing class with \n break
    $timing = new Timing("\n");

    // Start timing
    $timing->start();

    // Loop ten rounds and sleep one second per round
    for ($i=1;$i<=10;$i++) { 
            echo $i . "\t"; sleep(1);
            // Print elapsed time every 2 rounds
            if ($i%2==0) {
                    $timing->printElapsedTime();
            }
    }

    // Stop/end timing
    $timing->stop();

    // Print only total execution time
    $timing->printTotalExecutionTime();

    // Print full stats
    $timing->printFullStats();
 * 
 */
class Timing {

    private $_break;
    private $_start_time;
    private $_stop_time;

    // Constructor for Timing class
    public function __construct($break = "") {
        $this->_break = $break;
        // Set timezone
        date_default_timezone_set('Europe/Amsterdam');
    }

    // Set start time
    public function start() {
        $this->_start_time = microtime(true);
    }

    // Set stop/end time
    public function stop() {
        $this->_stop_time = microtime(true);
    }

    // Returns time elapsed from start
    public function getElapsedTime() {
        return $this->getExecutionTime(microtime(true));
    }

    // Returns total execution time
    public function getTotalExecutionTime() {
        if (!$this->_stop_time) {
            return false;
        }
        return $this->getExecutionTime($this->_stop_time);
    }

    // Returns start time, stop time and total execution time
    public function getFullStats() {
        if (!$this->_stop_time) {
            return false;
        }

        $stats = array();
        $stats['start_time'] = $this->getDateTime($this->_start_time);
        $stats['stop_time'] = $this->getDateTime($this->_stop_time);
        $stats['total_execution_time'] = $this->getExecutionTime($this->_stop_time);

        return $stats;
    }

    // Prints time elapsed from start
    public function printElapsedTime() {
        echo $this->_break . $this->_break;
        echo "Elapsed time: " . $this->getExecutionTime(microtime(true));
        echo $this->_break . $this->_break;
    }

    // Prints total execution time
    public function printTotalExecutionTime() {
        if (!$this->_stop_time) {
            return false;
        }

        echo $this->_break . $this->_break;
        echo "Total execution time: " . $this->getExecutionTime($this->_stop_time);
        echo $this->_break . $this->_break;
    }

    // Prints start time, stop time and total execution time
    public function printFullStats() {
        if (!$this->_stop_time) {
            return false;
        }

        echo '<div style="position:fixed;bottom:0px;width:100%;height:30px; background-color: grey;color: white">';
        echo "Script start date and time: " . $this->getDateTime($this->_start_time);
        echo $this->_break;
        echo "Script stop end date and time: " . $this->getDateTime($this->_stop_time);
        echo $this->_break . $this->_break;
        echo "Total execution time: " . $this->getExecutionTime($this->_stop_time);
        echo '</div>';
    }

    // Format time to date and time
    private function getDateTime($time) {
        return date("Y-m-d H:i:s", $time);
    }

    // Get execution time by timestamp
    private function getExecutionTime($time) {
        return $time - $this->_start_time;
    }

}
