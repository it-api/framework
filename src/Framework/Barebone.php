<?php

namespace Barebone\Framework;

use Barebone\Filesystem;
use Barebone\Request;
use Barebone\Config;
use Barebone\SQLLite3;
use GuzzleHttp\Client;

class Barebone {

    /**
     * Local handle on filesystem object
     * @var \Barebone\Filesystem 
     */
    public Filesystem $filesystem;
    
    /**
     * Barebone framework structure
     * @var array Array of the filesystem structure 
     */
    private array $barebone_structure = array(
        'barebone' => array(
            'applications' => array(
                'frontend' => array(),
                'barebone' => array()
            ),
            'config' => array(),
            'models' => array(
                'database' => array(),
                'schemas' => array()
            )
        ),
        'public' => array(
            'assets' => array(
                'barebone' => array(),
                'thirdparty' => array()
            ),
            'css' => array(
                'applications' => array()
            ),
            'images' => array(),
            'js' => array(
                'applications' => array()
            ),
            'layouts' => array()
        ),
        'vendor' => array(
            'Barebone' => array()
        )
    );

    function __construct() {
        $this->filesystem = new Filesystem();
    }
    
    /**
     * Return array with all layouts
     */
    function getLayouts() {
        return $this->filesystem->get_Files_list( FULL_PUBLIC_PATH . 'layouts/' );
    }
    /**
     * Check if a application exists
     */
    function applicationExists( string $application ): bool{
        return is_dir(FULL_APPS_PATH . $application);
    }

    /**
     * Return array with all applications
     */
    function getApplications() {
        return $this->filesystem->get_Files_list( FULL_APPS_PATH );
    }

    /**
     * Return array with all controllers from an application
     * @param string $application name of the application
     */
    function getApplicationControllers( string $application ) {
        return $this->filesystem->get_Files_list( FULL_APPS_PATH . $application . '/controllers/' );
    }

    /**
     * Strip of the Controller.php part
     * @param string $controller name of the controller
     * @return mixed the name of the controller without the eneding Controller.php part
     */
    function getControllerNameFromFilename( string $controller ) {
        return str_replace( 'Controller.php', '', $controller );
    }
	
	function getMethodNameFromMethod( string $method ){
		if(substr($method, -6) === 'Action'){
			return substr($method, 0, -6);
		}elseif(substr($method, -4) === 'AJAX'){
			return substr($method, 0, -4);
		}elseif(substr($method, -4) === 'JSON'){
			return substr($method, 0, -4);
		}elseif(substr($method, -3) === 'XML'){
			return substr($method, 0, -3);
		}elseif(substr($method, -6) === 'JQUERY'){
			return substr($method, 0, -6);
		}else{
			return $method;
		}
	}
	

    /**
     * Strip of the .php part
     * @param string $filename name of the file
     * @return mixed the name of the file without the eneding .php part
     */
    function getModelNameFromFilename( string $filename ) {
        $path_array = explode('/', $filename);
        return str_replace('.php', '', $path_array[count($path_array) - 1]);
    }

    /**
     * Get full path to the controller
     * @param string $application_name name of the application
     * @param string $controller_name name of the controller in the application
     * @return string full path to the controller
     */
    function getControllerFullPath(string $application_name, string $controller_name) {
        return FULL_APPS_PATH . $application_name . '/controllers/' . $controller_name . 'Controller.php';
    }

    /**
     * Get the views for a certain controller
     * @param string $application
     * @param string $controller
     * @return mixed
     */
    function getApplicationControllerViews(string $application, string $controller) {
        return $this->filesystem->get_Files_list(FULL_APPS_PATH . $application . '/views/' . $this->getControllerNameFromFilename($controller));
    }
    
    function getApplicationControllerViewsFromFilename( string $filename ){
        
        $controllername = $this->getControllerNameFromFilename(basename($filename));
        $fa = explode('/', $filename);
        unset($fa[count($fa)-1]);
        $fa[count($fa)-1] = 'views';
        $fa[] = $controllername;

        return implode('/',$fa);
       
    }

    /**
     * Strip of the extension from the view name so it returns just the name
     * @param string $view
     * @return mixed
     */
    function getViewNameFromFilename(string $view) {
        return str_replace('.phtml', '', $view);
    }

    /**
     * Get a list of all application models
     * @param string $application
     * @return mixed
     */
    function getGlobalModels() {
        return $this->filesystem->get_Files_list(FULL_BAREBONE_PATH . 'models/', array('php'));
    }

    /**
     * Get a list of all application models
     * @param string $application
     * @return mixed
     */
    function getApplicationModels(string $application) {
        return $this->filesystem->get_Files_list(FULL_APPS_PATH . $application . '/models/', array('php'));
    }

    /**
     * Return the configuration settings
     */
    function getConfig() {

        $config = Config::getInstance();
        return $config->values;
    }

    /**
     * 
     * @return array Array of files of barebone
     */
    function getBareboneLibrary() {
        return $this->filesystem->listFolderFiles(FULL_VENDOR_PATH . 'it-api/barebone-framework/src');
    }

    /**
     * Get the properties of a class
     * @param string $filename
     * @return array
     */
    function getClassProperties(string $filename): array {
        
        $result = [];
        if((new \Barebone\Filesystem())->getExtension($filename) !== 'php'){
            return $result;
        }
        
        $namespace = \Barebone\Classbuilder::extract_namespace($filename);
        $modelName = $this->getModelNameFromFilename($filename);
        $modelName = ($namespace === NULL ? $modelName : '\\'.$namespace . '\\' . $modelName);
        
        try {
            if(!class_exists($modelName)){
                include_once($filename);
            }
            
            $reflection = new \reflectionClass($modelName);
            
            
            foreach ($reflection->getProperties() as $propertie) {

                $model_method = new \stdClass;
                $model_method->name = $propertie->getName();
                // this produces out before the page is loaded??
                //$model_method->value = $propertie->getValue(new $modelName);
                $model_method->type = $propertie->hasType() ? $propertie->getType()->getName() : '';
                $model_method->phpdoc = $propertie->getDocComment();
                $result[$propertie->getName()] = $model_method;
            }            
            
        } catch (\Exception $e) {
            throw new \Exception();
        } catch (\ReflectionException $re) {
            throw new \Exception();
        }
        sort($result);
        return $result;
    }
    
    /**
     * Get all methods from a model file
     * @param string $model_filename
     * @return array
     */
    function getModelMethods(string $model_filename): array {
        
        $result = [];
        $modelName = $this->getModelNameFromFilename($model_filename);

        try {
            
            include_once($model_filename);
            $reflection = new \reflectionClass($modelName);
            foreach ($reflection->getMethods() as $method) {

                if ($method->class === $modelName) {
                    $model_method = new \stdClass;
                    $model_method->name = $method->getName();
                    $model_method->params = $method->getParameters();
                    $model_method->startline = $method->getStartLine();
                    $model_method->endline = $method->getEndLine();
                    $model_method->returntype = $method->getReturnType();
                    $model_method->phpdoc = $method->getDocComment();
                    $result[$model_method->name] = $model_method;
                }
            }            
            
        } catch (\Exception $e) {
            
        }
        sort($result);
        return $result;
    }

    /**
     * Get the methods from a controller
     * !!! IMPORTANT DO NOT DO THIS IN A LOOP !!!!
     * @param string $application
     * @param string $controller
     * @return array
     */
    function getApplicationControllerMethods(string $application, string $controller) {
		
        $result = [];
        $controller_path = $this->getControllerFullPath($application, $controller);
		if(!file_exists($controller_path)){
			throw new \Exception('File not found');
		}else{
			include_once($controller_path);
			$reflection = new \reflectionClass($controller . 'Controller');
			
			foreach ($reflection->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
				if ($method->class === $reflection->getName()){
					$result[] = $method->getName();
				}				
			}
			
		}
        sort($result);
		
		return $result;
    }

    public static function create_app_framework(string $application_name, array $tables, array $methods=[]) {
        Application::create($application_name);
        foreach ($tables as $table) {
            Controller::create($application_name, strtolower($table), $methods);
        }
    }

    /*
      {
      'barebone':{
      'version':'1.0',
      "applications": {
      "aa": {
      "name": "aa",
      "path": "barebone/applications/aa",
      "uri": "/aa/",
      "css":"public/css/applications/aa.css",
      "js":"public/js/applications/aa.js",
      "controllers": {
      "index": {
      "name": "index",
      "uri": "/aa/index/",
      "filename": "barebone/aa/controllers/indexController.php",
      "views": {
      "index": {
      "name": "index",
      "path": "barebone/aa/views/index/index.phtml"
      }
      },
      "methods": {
      "init": {
      "name": "init",
      "uri": "/aa/index/init/",
      "slug": "init",
      "type": "function",
      "arguments": [],
      "line_start": 17,
      "line_end": 19,
      "phpdoc": ""
      },
      "terminate": {
      "name": "terminate",
      "uri": "/aa/index/terminate/",
      "slug": "terminate",
      "type": "function",
      "arguments": [],
      "line_start": 25,
      "line_end": 27,
      "phpdoc": ""
      },
      "indexAction": {
      "name": "indexAction",
      "uri": "/aa/index/index/",
      "slug": "index",
      "type": "Action",
      "arguments": [
      {
      "name": "args"
      }
      ],
      "line_start": 32,
      "line_end": 34,
      "phpdoc": ""
      }
      }
      }
      },
      "models": [
      "aaModel.php"
      ]
      }
      [.....etc....]
      }
      }
     */

    function createDiagram(bool $add_lib = false) {

        // create the diagram object
        $barebone_diagram = new \stdClass;
        $barebone_diagram->name = 'Barebone Framework';
        $barebone_diagram->version = '3.0';

        if ($add_lib === true) {
            $barebone_diagram->lib['Barebone'] = $this->getBareboneLibrary();
        }
        // create a config array and load the global configuration
        $barebone_diagram->config = $this->getConfig();

        // create a global models array and load the models
        $global_models = $this->getGlobalModels();

        if ($global_models) {
            foreach ($global_models as $global_model) {
                
                $barebone_model = new Model(
                    $this->getModelNameFromFilename($global_model), FULL_APPS_PATH . 'models/' . $global_model, $this->getModelMethods($global_model)
                );
                
                $barebone_diagram->global_models[$this->getModelNameFromFilename($global_model)] = $barebone_model;
            }
        }

        // create a propertie applications
        $barebone_diagram->applications = array();

        // get the applications from the "barebone/applications" folder
        $apps = $this->getApplications();

        // go over each application
        foreach ($apps as $key => $application) {
            // create a barebone application class
            $barebone_application = new Application($application);

            // add the application to the applications array of the diagram object
            $barebone_diagram->applications[$application] = $barebone_application;

            // now get the controllers for this application
            $controllers = $this->getApplicationControllers($application);

            if ($controllers) {
                // go over each of the controllers
                foreach ($controllers as $controller) {
                    // create for each controller a barebone controller object
                    $barebone_controller = new Controller(
                            $this->getControllerNameFromFilename($controller), 'barebone/' . $application . '/controllers/' . $controller
                    );
                    // add the controller to the application controllers array
                    $barebone_application->addController($barebone_controller);

                    // since we use indexController a lot it is not possible to loop over the classes and get the methods
                    // but we can send a post request to the classbuilder with the name of the application and the name of the controller

                    // send post request, this returns a json string like {data: [{name:somename}]}
                    $json = $this->doPost('/barebone/classbuilder/getmethods/', array(
                        'app' => $application,
                        'controller' => $this->getControllerNameFromFilename($controller)
                    ));

                    // convert json into a php object         
                    $data = json_decode($json);

                    // if there are any methods , you never know ;)
                    if ($data->data->methods) {

                        // then go over each method
                        foreach ($data->data->methods as $method) {

                            // create a barebone method object and assign it the values
                            $barebone_method = new Method(
                                    $method->name, $method->params, $method->startline, $method->endline, $method->phpdoc
                            );

                            // add the method to the controller methods array
                            $barebone_application->addMethod($barebone_controller, $barebone_method);
                        }
                    }
                    if ($data->data->properties) {
                        foreach ($data->data->properties as $propertie) {

                            $barebone_controller->properties[$propertie->name] = $propertie->value;
                        }
                    }

                    // get the views
                    $views = $this->getApplicationControllerViews($application, $controller);

                    // if there are any views
                    if ($views) {
                        // go over the views
                        foreach ($views as $view) {

                            // create a barebone view object
                            $barebone_view = new View($this->getViewNameFromFilename($view), 'barebone/' . $application . '/views/' . $this->getControllerNameFromFilename($controller) . '/' . $view);

                            // add the barebone view to the views array of the controller
                            $barebone_controller->views[$this->getViewNameFromFilename($view)] = $barebone_view;
                        }
                    }
                }
            }

            $app_models = $this->getApplicationModels($application);
            if ($app_models) {

                foreach ($app_models as $app_model) {

                    $barebone_model = new Model($this->getModelNameFromFilename($app_model), FULL_APPS_PATH . $application . '/models/' . $app_model);
                    $barebone_application->models[$this->getModelNameFromFilename($app_model)] = $barebone_model;
                }
            }
        }

        print_r($barebone_diagram);
        echo json_encode($barebone_diagram);

        file_put_contents(FULL_APPS_PATH . 'barebone/assets/full-diagram-barebone.json', json_encode($barebone_diagram));
        return $barebone_diagram;
    }
    
    function doPost(string $url, array $data, bool $debug = false){
	    
		$options = array('form_params' => $data,'debug' => $debug);
		$client = new Client();
		$res = $client->request('POST', $url, $options);
		return $res->getBody();
	}

    /**
     * Check if assets directory exists for a certain application
     * @param string $application
     * @return bool
     */
    function assets_directory_exists(string $application) {
        $assets_path = FULL_APPS_PATH . $application . '/assets/';
        return (file_exists($assets_path) && is_dir($assets_path) && is_writable($assets_path));
    }

    /**
     * Create the assets directory for the application
     * apps
     * 	- yourapp
     * 		- assets <<
     * 		- controllers
     * @param string $application the name of the application
     * @return bool
     */
    function create_assets_directory(string $application) {
        $assets_path = FULL_APPS_PATH . $application . '/assets/';
        try {
            mkdir($assets_path);
            chmod($assets_path, 0777);
            return true;
        } catch (\Exception $e) {
            echo $e->getMessage();
            return false;
            ;
        }
    }

    /**
     * Check if assets data directory exists for a certain application
     * @param string $application
     * @return bool
     */
    function assets_data_directory_exists(string $application) {
        $assets_path = FULL_APPS_PATH . $application . '/assets/data/';
        return (file_exists($assets_path) && is_dir($assets_path) && is_writable($assets_path));
    }

    /**
     * Create the assets data directory for the application
     * apps
     * 	- yourapp
     * 		- assets
     * 			- data <<
     * 		- controllers
     * @param string $application the name of the application
     * @return bool
     */
    function create_assets_data_directory(string $application) {
        if (!$this->assets_directory_exists($application)) {
            $this->create_assets_directory($application);
        }
        $assets_path = FULL_APPS_PATH . $application . '/assets/data/';
        try {
            mkdir($assets_path);
            chmod($assets_path, 0777);
            return true;
        } catch (\Exception $e) {
            echo $e->getMessage();
            return false;
            ;
        }
    }

    /**
     * Create a SQLite Database in the assets/data folder of the application
     * @param string $application The name of the application to add the db to
     * @param string $dbname the name of the database. it will have the .db extension
     * @param string $query the create table query
     * @return string|bool
     */
    function create_sqlite_db(string $application, string $dbname, string $query = '') {

        $querys = explode(";\n", $query);

        if (!$this->assets_data_directory_exists($application)) {
            $this->create_assets_data_directory($application);
        }

        $filename = FULL_APPS_PATH . $application . '/assets/data/' . $dbname . '.db';
        $sqlitedb = new SQLLite3($filename);
        chmod($filename, 0777);
        if (!empty($querys)) {
            foreach ($querys as $query){
                $sqlitedb->db->exec($query);
            }
        }

        if ($sqlitedb->db->lastErrorCode() != 0) {
            return $sqlitedb->db->lastErrorMsg();
        } else {
            return true;
        }
    }

    /**
     * Save a file to the repository
     * @param string $filename the name of the file that acts as a template
     * @param string $repository_path
     * @param string $templatename the name for the template
     * @example $this->save_file_as_template('/home/frank/test.txt', '/controllers/', 'index.php')
     */
    function save_file_as_template(string $filename, string $repository_path = '/', string $templatename = '') {
        // get the contents of the file
        $data = file_get_contents($filename);
        // create the full path for the template
        $filename_template = FULL_APPS_PATH . 'repository' . $repository_path . $templatename;
        // save the data to the template
        return file_put_contents($filename_template, $data);
    }

    /**
     * Save a string to the repository
     * @param string $text the text for the template
     * @param string $repository_path
     * @param string $templatename the name for the template
     * @example $this->save_text_as_template('/home/frank/test.txt', '/controllers/', 'index.php')
     */
    function save_text_as_template(string $text, string $repository_path = '/', string $templatename = '') {
        // create the full path for the template
        $filename_template = FULL_APPS_PATH . 'repository' . $repository_path . $templatename;
        // save the data to the template
        return file_put_contents($filename_template, $text);
    }

}
