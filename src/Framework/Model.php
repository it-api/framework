<?php

namespace Barebone\Framework;

/**
 * Description of Model
 * ComposerFW 
 * UTF-8
 *
 * @author Frank
 * @filesource Model.php
 * @since 6-mrt-2017 0:06:50
 * 
 */
class Model {

    /**
     * The name of the model
     * @var string
     */
    public $name;

    /**
     * relative path path to the model from the root directory
     * @var string
     */
    public $filename;

    /**
     * The methods in the controller
     * @var array
     */
    public $methods = array();

    /**
     * Model constructor
     * @param string $name
     * @param string $filename
     * @param array $methods
     * @return $this
     */
    function __construct($name, $filename, $methods = array()) {
        $this->name = $name;
        $this->filename = $filename;
        $this->methods = $methods;
        return $this;
    }

    /**
     * Add a method to the model
     * @param \Barebone\Framework\Method $method
     */
    function addMethod(Method $method) {
        $this->methods[$method->name] = $method;
    }

}
