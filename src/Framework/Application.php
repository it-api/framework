<?php

namespace Barebone\Framework;

/**
 * Description of Application
 * ComposerFW 
 * UTF-8
 *
 * @author Frank
 * @filesource Application.php
 * @since 5-mrt-2017 23:19:28
 * 
 */
class Application {

    /**
     * The name of the application
     * @var string
     */
    public string $name = '';

    /**
     * Relative path to the application from the barebone directory
     * @var string
     */
    public string $path = '';

    /**
     * Uri for this application
     * @var string
     */
    public string $uri = '';

    /**
     * The controllers for this applicaiotn
     * @var array
     */
    public array $controllers = [];
    /**
     * The controllers for this applicaiotn
     * @var array
     */
    public array $models = [];
    /**
     * Application styelsheet /public/css/applications/{appname}.css
     * @var string
     */
    public string $public_css = '';
    /**
     * Application styelsheet {FULL_PUBLIC_PATH}/public/css/applications/{appname}.css
     * @var string
     */
    public string $path_css = '';
    /**
     * Application styelsheet /public/js/applications/{appname}.js
     * @var string
     */
    public string $public_js = '';
    /**
     * Application styelsheet {FULL_PUBLIC_PATH}/public/js/applications/{appname}.js
     * @var string
     */
    public string $path_js = '';

    /**
     * Application constructor
     * @param string $name The name of the application
     * @return $this
     */
    function __construct(string $name) {
        $this->name = $name;
        $this->path = FULL_APPS_PATH . $name;
        $this->uri = '/' . $name . '/';

        $this->path_css = FULL_PUBLIC_PATH . 'css/applications/' . $name . '.css';
        if (file_exists($this->path_css)) {
            $this->public_css = '/public/css/applications/' . $name . '.css';
        } else {
            $this->path_css = null;
        }
        $this->path_js = FULL_PUBLIC_PATH . 'js/applications/' . $name . '.js';
        if (file_exists($this->path_js)) {
            $this->public_js = '/public/js/applications/' . $name . '.js';
        } else {
            $this->path_js = null;
        }
        return $this;
    }

    /**
     * Add a controller to the application
     * @param \Barebone\Framework\Controller $barebone_controller
     */
    function addController(Controller $barebone_controller) {
        $controller = $barebone_controller;
        $controller->uri = $this->uri . $controller->name . '/';
        $this->controllers[$controller->name] = $controller;
    }

    /**
     * Add a method to a controller of the application
     * @param \Barebone\Framework\Controller $controller
     * @param \Barebone\Framework\Method $method
     */
    function addMethod(Controller $controller, Method $method) {
        $method->uri = $this->uri . $controller->name . '/' . $method->slug . '/';
        $controller->methods[$method->name] = $method;
    }

    /**
     * Create a new Application
     * @param type $application_name
     * @return void
     */
    public static function create(string $application_name) {
        if ($application_name === '') {
            return;
        }
        mkdir(FULL_APPS_PATH . '/' . $application_name, 0755);
        mkdir(FULL_APPS_PATH . '/' . $application_name . '/controllers', 0755);
        mkdir(FULL_APPS_PATH . '/' . $application_name . '/config', 0755);
        mkdir(FULL_APPS_PATH . '/' . $application_name . '/models', 0755);
        mkdir(FULL_APPS_PATH . '/' . $application_name . '/views', 0755);
    }
}
