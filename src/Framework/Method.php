<?php

namespace Barebone\Framework;

/**
 * Description of Method
 * ComposerFW 
 * UTF-8
 *
 * @author Frank
 * @filesource Method.php
 * @since 5-mrt-2017 23:31:16
 * 
 */
class Method {

    /**
     * The name of the method
     * @var string
     */
    public string $name;

    /**
     * The uri to reach this method
     * @var string
     */
    public string $uri;

    /**
     * The action without Action, AJAX OR JSON
     * @var string
     */
    public string $slug;

    /**
     * The type of action
     * @var string 'Action', 'AJAX', 'JSON'
     */
    public string $type;

    /**
     * The arguments of this method
     * @var array
     */
    public array $arguments = [];

    /**
     * The line the function starts
     * @var int
     */
    public int $line_start;

    /**
     * The line the function ends
     * @var int
     */
    public int $line_end;

    /**
     * The document comment
     * @var string
     */
    public string $phpdoc;

    /**
     * Method constructor
     * @param string $name The name of the method. You can add Action, AJAX, JSON
     * @param array $arguments Array of arguments
     * @param int $line_start The line this method starts in the file
     * @param int $line_end The line this method ends in the file
     * @param string $phpdoc The PHPDoc of the method
     * @return $this
     */
    function __construct($name, $arguments = array(), $line_start = null, $line_end = null, $phpdoc = null) {
        $this->name = $name;
        $this->arguments = $arguments;
        $this->line_start = $line_start;
        $this->line_end = $line_end;
        $this->phpdoc = $phpdoc;
        if (substr($this->name, strlen($this->name) - 6, strlen($this->name)) === 'Action') {
            $this->type = 'Action';
            $this->slug = substr($this->name, 0, strlen($this->name) - 6);
        } elseif (substr($this->name, strlen($this->name) - 4, strlen($this->name)) === 'AJAX') {
            $this->type = 'AJAX';
            $this->slug = substr($this->name, 0, strlen($this->name) - 4);
        } elseif (substr($this->name, strlen($this->name) - 4, strlen($this->name)) === 'JSON') {
            $this->type = 'JSON';
            $this->slug = substr($this->name, 0, strlen($this->name) - 4);
        } else {
            $this->type = 'function';
            $this->slug = $this->name;
        }
        return $this;
    }
    
    public static function create($application_name, $controller_name, $method_name) {
        
        if ($application_name === ''){
            throw new \Exception('Application name is mandatory.');
        }
        
        if ($controller_name === ''){
            throw new \Exception('Controller name is mandatory.');
        }
        
        if ($method_name === ''){
            throw new \Exception('Method name is mandatory.');
        }
        
        $controller_filename = FULL_APPS_PATH .  $application_name . "/controllers/{$controller_name}Controller.php";
        
        // get the contents from the file
        $class_contents = file_get_contents($controller_filename);

        //var_dump(strrpos($class_contents, '}', -5));

        // read it again until the last closing bracket } of the class
        $class_contents = substr($class_contents, 0, strrpos($class_contents, '}', 0));
        //create a template for the function
    $func_tpl = $class_contents . '	/**
    * The starting point for this action
    * @URI /'.$application_name.'/'.$controller_name.'/'.$method_name.'/
    * @Application '.$application_name.'
    * @Controller '.$controller_name.'
    * @Action '.$method_name.'
    * @param $args array of segments
    */
    function '.$method_name.'(array $args = []){
        
    }
	
}';
    	// write the controller to disk
    	file_put_contents($controller_filename, $func_tpl);
    }

}
