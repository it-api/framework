<?php

/*
  SystemRequirementsCheck.php
  UTF-8
  19-mei-2018 21:33:02
  barebone-php7
  Creator Frank
 */

namespace Barebone\Framework;

/**
 * Description of SystemRequirementsCheck
 *
 * @author Frank
 */
class SystemRequirementsCheck {
	
	const OK = 0;
	const RECOMMEND = 1;
	const REQUIRED = 2;
	
	private $required = ['curl', 'mbstring', 'pdo_mysql', 'sqllite3'];
	private $loaded_extensions = [];
	private $system = [];
	
	public function __construct() {
		
		$this->loaded_extensions = get_loaded_extensions();
		
		$this->system['memory_limit'] = ini_get('memory_limit') === '-1' || (int) ini_get('memory_limit') >= 64 ? self::OK : self::RECOMMEND;
		
		$this->system['pdo'] = $this->getStatusExtension('pdo_mysql');
		$this->system['mysqli'] = $this->getStatusExtension('mysqli');
		$this->system['pdo_sqlite'] = $this->getStatusExtension('pdo_sqlite');		
		$this->system['curl'] = $this->getStatusExtension('curl');		
		$this->system['SimpleXML'] = $this->getStatusExtension('SimpleXML');
		$this->system['fileinfo'] = $this->getStatusExtension('fileinfo');
		$this->system['mbstring'] = $this->getStatusExtension('mbstring');
		$this->system['dom'] = $this->getStatusExtension('dom');
		$this->system['zip'] = $this->getStatusExtension('zip');
		$this->system['bz2'] = $this->getStatusExtension('bz2');
		$this->system['bcmath'] = $this->getStatusExtension('bcmath');
		$this->system['imagick'] = $this->getStatusExtension('imagick');
		$this->system['openssl'] = $this->getStatusExtension('openssl');
		$this->system['sqlite3'] = $this->getStatusExtension('sqlite3');
		
		$this->system['filesystem']['root'] = is_writeable(FULL_ROOT_PATH) ? self::OK : self::REQUIRED;
		$this->system['filesystem']['root/barbone/'] = is_writeable(FULL_BAREBONE_PATH) ? self::OK : self::REQUIRED;
		$this->system['filesystem']['root/public'] = is_writeable(FULL_PUBLIC_PATH) ? self::OK : self::REQUIRED;
	}
	
	public function getSystemCheck(){
		return $this->system;
	}
	
	private function getStatusExtension( $extension ){
		if( !extension_loaded( $extension ) && in_array($extension, $this->required) ){
			return self::REQUIRED;
		}elseif( !extension_loaded( $extension ) && !in_array($extension, $this->required) ){
			return self::RECOMMEND;
		}elseif( extension_loaded( $extension ) ){
			return self::OK;
		}
	}

}
