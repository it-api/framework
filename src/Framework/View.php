<?php

namespace Barebone\Framework;

/**
 * Description of View
 * ComposerFW 
 * UTF-8
 *
 * @author Frank
 * @filesource View.php
 * @since 5-mrt-2017 23:31:22
 * 
 */
class View {

    /**
     * The name of the view
     * @var string
     */
    public $name;

    /**
     * relative path to the view from the barebone directory
     * @var string
     */
    public $path;

    /**
     * 
     * @param string $name
     * @param string $path
     * @return $this
     */
    function __construct($name, $path) {
        $this->name = $name;
        $this->path = $path;
        return $this;
    }

}
