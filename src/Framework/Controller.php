<?php

namespace Barebone\Framework;

/**
 * Description of Controller
 * ComposerFW 
 * UTF-8
 *
 * @author Frank
 * @filesource Controller.php
 * @since 5-mrt-2017 23:26:32
 * 
 */
class Controller {

    /**
     * The name of the controller
     * @var string
     */
    public $name;

    /**
     * The uri to reach this controller
     * @var string
     */
    public $uri;

    /**
     * relative path to the controller from the barebone directory
     * @var string
     */
    public $filename;

    /**
     * The views belonging to this controller
     * @var array
     */
    public $views = array();

    /**
     * The properties in the controller
     * @var array
     */
    public $properties = array();

    /**
     * The methods in the controller
     * @var array
     */
    public $methods = array();

    /**
     * 
     * @param string $name
     * @param string $filename
     * @param array $methods
     * @param array $properties
     * @return $this
     */
    function __construct($name, $filename, $methods = array(), $properties = array()) {
        $this->name = $name;
        $this->filename = $filename;
        $this->methods = $methods;
        $this->properties = $properties;
        return $this;
    }

    /**
     * Add a view to the controller
     * @param \Barebone\Framework\View $view
     */
    function addView(View $view) {
        $this->views[$view->name] = $view;
    }

    /**
     * Add a method to the controller
     * @param \Barebone\Framework\Method $method
     */
    function addMethod(Method $method) {
        $this->methods[$method->name] = $method;
    }

    public static function create($application_name, $controller_name, $methods = []) {

        if ($application_name === ''){
            return;
        }
        
        if ($controller_name === ''){
            return;
        }
        
        $newControllerFilename = FULL_APPS_PATH .  $application_name . "/controllers/{$controller_name}Controller.php";

        $template = file_get_contents(FULL_BAREBONE_PATH . 'repository/controllers/newController.txt');
        $template = str_replace('{author}', '', $template);
        $template = str_replace('{copyright}', '', $template);
        $template = str_replace('{created}', date('d-m-Y H:i:s'), $template);
        $template = str_replace('{filelocation}', $newControllerFilename, $template);
        $template = str_replace('{controller_name}', $controller_name, $template);
        $template = str_replace('{layoutname}', 'default', $template);
        
        mkdir(FULL_APPS_PATH . $application_name . "/views/$controller_name", 0755);
        
        if(count($methods) > 0){
            $funct = '';
            foreach($methods as $method){
                $funct .= "\tfunction $method(array \$args = []){}\n";
                
                $method = substr($method, 0, strlen($method)-6);
                $newActionViewFilename = FULL_APPS_PATH . $application_name . "/views/{$controller_name}/{$method}.phtml";
                $viewActiontemplate = file_get_contents(FULL_BAREBONE_PATH . 'repository/views/index.phtml');
                file_put_contents($newActionViewFilename, $viewActiontemplate);
            }
            
            $template = str_replace('{methods}', $funct, $template);
                
        } else {
                
             $template = str_replace('{methods}', '', $template);
                
        }

        file_put_contents($newControllerFilename, $template);
        
        if(!is_dir(FULL_APPS_PATH . $application_name . "/views/$controller_name")){
            mkdir(FULL_APPS_PATH . $application_name . "/views/$controller_name", 0755);
        }

        $newViewFilename = FULL_APPS_PATH . $application_name . "/views/$controller_name/index.phtml";
        $viewtemplate = file_get_contents(FULL_BAREBONE_PATH . 'repository/views/index.phtml');
        file_put_contents($newViewFilename, $viewtemplate);
    }

}
