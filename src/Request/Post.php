<?php

namespace Barebone\Request;

use Barebone\Component;

/**
 * Barebone $_POST wrapper
 * @author Frank
 *
 */
class Post extends Component{
	
	function __construct(){
		
		parent::__construct( $this->sanitize_array( $_POST ) );
		
	}
	
	function filter_var( string $variable, int $filter = FILTER_UNSAFE_RAW, array $options = [] ){
		
		return filter_input( INPUT_POST, $variable, $filter, $options );
		
	}
	
	function validate(string $variable, int $filter = FILTER_VALIDATE_EMAIL, array $options = [] ){
		
		//FILTER_VALIDATE_EMAIL
		filter_input( INPUT_POST, $variable, $filter, $options );
	}
	
	function filter_input_array( array $args = [] ){
		$myinputs = filter_input_array(INPUT_POST, $args);
		
		return $myinputs;
	}

}
