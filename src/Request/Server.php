<?php

namespace Barebone\Request;

use Barebone\Component;

/**
 * Barebone $_SERVER wrapper
 * @author Frank
 *
 */
class Server extends Component{

	function __construct(){

		parent::__construct( $this->sanitize_array( $_SERVER ) );
	}

}
