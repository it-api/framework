<?php

namespace Barebone\Request;

use Barebone\Component;

/**
 * Barebone $_GET wrapper
 * @author Frank
 *
 */
class Get extends Component{

	function __construct(){

		parent::__construct( $this->sanitize_array( $_GET ) );
	}

	function filter_input_array( $args = array() ){
		return filter_input_array(INPUT_GET, $args);
	}
}
