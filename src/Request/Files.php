<?php

namespace Barebone\Request;

use Barebone\Component;

/**
 * Barebone $_FILES wrapper
 * @author Frank
 *
 */
class Files extends Component{
	
	function __construct() {
	
		parent::__construct( $_FILES );
	}
}

?>
