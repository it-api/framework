<?php

namespace Barebone\Request;

use Barebone\Component;

/**
* Barebone $_SESSION wrapper
* @author Frank
*
*/
class Session extends Component {

    function __construct() {

        if (! $this->is_session_started()) {
            @session_start();
            $_SESSION['csrf_token'] = bin2hex(random_bytes(24));
        }

        parent::__construct($_SESSION);
    }

    function is_session_started() {
        if (php_sapi_name() !== 'cli') {
            if (version_compare(phpversion(), '5.4.0', '>=')) {
                return session_status() === PHP_SESSION_ACTIVE ? TRUE : FALSE;
            } else {
                return session_id() === '' ? FALSE : TRUE;
            }
        }

        return FALSE;
    }
}