<?php

namespace Barebone\Request;

use Barebone\Component;

/**
 * Barebone getallheaders wrapper
 * @author Frank
 *
 */
class Headers extends Component{
	
	function __construct() {
	
		parent::__construct( $this->sanitize_array( getallheaders() ) ); 
	}

}

