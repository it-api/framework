<?php

namespace Barebone\Request;

use Barebone\Component;

/**
 * Barebone $_COOKIE wrapper
 * @author Frank
 *
 */
class Cookie extends Component{

	function __construct(){

		parent::__construct( $this->sanitize_array( $_COOKIE ) );
		
	}
	
	function set(string $name, string $value, int $expires_or_options = null, string $path = '/', string $domain = '', bool $secure = false, bool $httponly = false){
	    if(is_null($expires_or_options)){
	        $expires_or_options = time() - 3600; // expire after session
	    }

		setcookie(
		    name: $name, 
		    value: $value, 
		    expires_or_options: $expires_or_options, 
		    path: $path,
		    domain: $domain,
		    secure: $secure,
		    httponly: $httponly
		);
		
	}
	
	final function get( string $name ){
		
		$cookie = $_COOKIE[$name] ?? null;
		
		return $cookie;
		
	}
}
