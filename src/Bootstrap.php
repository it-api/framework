<?php

/*
  Creator Frank
 */

namespace Barebone;

/**
 * Description of Bootstrap
 *
 * @author Frank
 */
class Bootstrap {
	/**
	 * Bootstrap\Alert
	 * @param string $message
	 * @param string $type
	 * @param bool $dismissable
	 * @return \Barebone\Bootstrap\Alert
	 */
	public static function alert(string $message, string $type = 'info', bool $dismissable = false): Bootstrap\Alert {
		return new Bootstrap\Alert($message, $type, $dismissable);
	}
	/**
	 * Bootstrap\Alerts
	 * @param string $message
	 * @param string $type
	 * @param bool $dismissable
	 * @return \Barebone\Bootstrap\Alerts
	 */
	public static function alerts(string $message, string $type = 'info', $dismissable = false): Bootstrap\Alerts {
		return new Bootstrap\Alerts($message, $type, $dismissable);
	}
	/**
	 * Bootstrap\Breadcrums
	 * @param array $crumbs [['caption'=>'#link']]
	 * @return \Barebone\Bootstrap\Breadcrumbs
	 */
	public static function breadcrumbs(array $crumbs = []): Bootstrap\Breadcrumbs {
		return new Bootstrap\Breadcrumbs($crumbs);
	}
	/**
	 * Bootstrap\Breadcrumb
	 * @param string $caption
	 * @param string $href
	 * @return \Barebone\Bootstrap\Breadcrumb
	 */
	public static function breadcrumb(string $caption, string $href = '#'): Bootstrap\Breadcrumb {
		return new Bootstrap\Breadcrumb($caption, $href);
	}
	/**
	 * Bootstrap\Button
	 * @param string $caption
	 * @param string $contextual
	 * @return \Barebone\Bootstrap\Button
	 */
	public static function button(string $caption, string $contextual = 'default'): Bootstrap\Button {
		return new Bootstrap\Button($caption, $contextual);
	}
	/**
	 * Bootstrap\Buttongroup
	 * @return \Barebone\Bootstrap\ButtonGroup
	 */
	public static function buttongroup(): Bootstrap\ButtonGroup {
		return new Bootstrap\ButtonGroup();
	}
	/**
	 * Bootstrap\Dropdown / UP
	 * @param string $caption
	 * @param string $contextual
	 * @return \Barebone\Bootstrap\Dropdown
	 */
	public static function dropdown(string $caption, string $contextual = 'default'): Bootstrap\Dropdown {
		return new Bootstrap\Dropdown($caption, $contextual);
	}

	/**
	 * Bootstrap\Form
	 * @param array $attributes
	 * @return \Barebone\Bootstrap\Form
	 */
	public static function form(array $attributes = []): Bootstrap\Form {
		return new Bootstrap\Form($attributes);
	}
	/**
	 * Bootstrap\FormGroup
	 * @param int $caption
	 * @param int $input_name
	 * @param array $attributes
	 * @return \Barebone\Bootstrap\FormGroup
	 */
	public static function formgroup(string $caption, string $input_name, array $attributes = []): Bootstrap\FormGroup {
		return new Bootstrap\FormGroup($caption, $input_name, $attributes);
	}
	/**
	 * Bootstrap\GlyphIcon if you want to use: glyphicon 'glyphicon-headphones', you add 'headphones'
	 * @param string $glyphicon 
	 * @return \Barebone\Bootstrap\GlyphIcon
	 */
	public static function glyphicon(string $glyphicon = 'question-sign', int $size = 16): Bootstrap\GlyphIcon {
		return new Bootstrap\GlyphIcon($glyphicon, $size);
	}

	/**
	 * Bootstrap\Grid
	 * @param array $attributes
	 * @return \Barebone\Bootstrap\Grid
	 */
	public static function grid(array $attributes = []): Bootstrap\Grid {
		return new Bootstrap\Grid($attributes);
	}
	/**
	 * Bootstrap\Image
	 * @param int $src
	 * @param array $attributes
	 * @return \Barebone\Bootstrap\Image
	 */
	public static function image(string $src, array $attributes = []): Bootstrap\Image {
		return new Bootstrap\Image($src, $attributes);
	}
	/**
	 * Bootstrap\Link
	 * @param string $caption
	 * @param string $href
	 * @param array $attributes
	 * @return \Barebone\Bootstrap\Link
	 */
	public static function link(string $caption, string $href = '', array $attributes = []): Bootstrap\Link {
		return new Bootstrap\Link($caption, $href, $attributes);
	}
	/**
	 * Bootstrap\Listgroup
	 * @return \Barebone\Bootstrap\Listgroup
	 */
	public static function listgroup(): Bootstrap\Listgroup {
		return new Bootstrap\Listgroup();
	}
	/**
	 * Bootstrap\Panel
	 * @param string $contextual
	 * @return \Barebone\Bootstrap\Panel
	 */
	public static function panel(string $contextual = 'default'): Bootstrap\Panel {
		return new Bootstrap\Panel($contextual);
	}
	/**
	 * Bootstrap\Nav
	 * @return \Barebone\Bootstrap\Nav
	 */
	public function nav(): Bootstrap\Nav {
		return new Bootstrap\Nav();
	}
	/**
	 * Bootstrap\Navbar
	 * @param string $brandname
	 * @param string $type
	 * @return \Barebone\Bootstrap\Navbar
	 */
	public function navbar(string $brandname = '', string $type = 'default'): Bootstrap\Navbar {
		return new Bootstrap\Navbar($brandname, $type);
	}
	/**
	 * Bootstrap\Progress
	 * @param int $value
	 * @param int $valuemin
	 * @param int $valuemax
	 * @param string $contextual
	 * @return \Barebone\Bootstrap\Progress
	 */
	public static function progress(int $value, int $valuemin = 0, int $valuemax = 100, string $contextual = null): Bootstrap\Progress {
		return new Bootstrap\Progress($value, $valuemin, $valuemax, $contextual);
	}
	/**
	 * Bootstrap\Row
	 * @return \Barebone\Bootstrap\Row
	 */
	public static function row(): Bootstrap\Row {
		return new Bootstrap\Row();
	}
	/**
	 * Bootstrap\SplitButton
	 * @param string $caption
	 * @param array $attributes
	 * @return \Barebone\Bootstrap\SplitButton
	 */
	public static function splitbutton(string $caption, array $attributes = []): Bootstrap\SplitButton {
		return new Bootstrap\SplitButton($caption, $attributes);
	}
	/**
	 * Bootstrap\Table
	 * @param array $attributes
	 * @return \Barebone\Bootstrap\Table
	 */
	public static function table(array $attributes = []): Bootstrap\Table {
		return new Bootstrap\Table($attributes);
	}
	/**
	 * Bootstrap\Well
	 * @param string $content
	 * @return \Barebone\Bootstrap\Well
	 */
	public static function well(string $content): Bootstrap\Well {
		return new Bootstrap\Well($content);
	}
}
