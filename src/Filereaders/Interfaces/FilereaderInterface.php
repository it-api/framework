<?php

/*
  FilereaderInterface.php
  UTF-8
  29-apr-2018 20:13:31
  barebone-php7
  Creator Frank
 */

namespace Barebone\Filereaders\Interfaces;

/**
 *
 * @author Frank
 */
interface FilereaderInterface {
	function read();
	function save();
}
