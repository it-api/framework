<?php

namespace Barebone\Filereaders;

/**
 * Resprents a inifile
 * @author Frank
 *
 */
class Ini_file {

    public $filename;
    public $values;

    function load_ini_file() {
        $this->values = parse_ini_file($this->filename, true);
    }

    function getValue($section, $key) {

        if (!isset($this->values[$section])) {
            return false;
        }
        if (!isset($this->values[$section][$key])) {
            return false;
        }
        return $this->values[$section][$key];
    }

    function setValue($section, $key, $value) {
        return $this->values[$section][$key] = $value;
    }

    function save() {
        if(!is_writable($this->filename)){
            throw new \Barebone\Exception(sprintf('File %s is not writable', $this->filename));
        }
        $this->write_ini_file($this->values, $this->filename, true);
    }

    /**
     * @example
     * $sampleData = array(
      'first' => array(
      'first-1' => 1,
      'first-2' => 2,
      'first-3' => 3,
      'first-4' => 4,
      'first-5' => 5,
      ),
      'second' => array(
      'second-1' => 1,
      'second-2' => 2,
      'second-3' => 3,
      'second-4' => 4,
      'second-5' => 5,
      ));
      write_ini_file($sampleData, './data.ini', true);
     * @param unknown_type $assoc_arr
     * @param unknown_type $path
     * @param unknown_type $has_sections
     * @return boolean
     */
    function write_ini_file($assoc_arr, $path, $has_sections = FALSE) {
        $content = "";
        if ($has_sections) {
            foreach ($assoc_arr as $key => $elem) {
                if ($key != '') {
                    $content .= "[" . $key . "]\n";
                    foreach ($elem as $key2 => $elem2) {
                        if (is_array($elem2)) {
                            for ($i = 0; $i < count($elem2); $i ++) {
                                $content .= $key2 . "[] = \"" . $elem2 [$i] . "\"\n";
                            }
                        } else if ($elem2 == ""){
                            $content .= $key2 . " = \n";
                        }else{
                            $content .= $key2 . " = \"" . $elem2 . "\"\n";
                        
                        }
                    }
                }
            }
        } else {
            foreach ($assoc_arr as $key => $elem) {
                if (is_array($elem)) {
                    for ($i = 0; $i < count($elem); $i ++) {
                        $content .= $key . "[] = \"" . $elem [$i] . "\"\n";
                    }
                } else if ($elem == ""){
                    $content .= $key . " = \n";
                }else{
                    $content .= $key . " = \"" . $elem . "\"\n";
                }
            }
        }

        if (!$handle = fopen($path, 'w')) {
            return false;
        }
        if (!fwrite($handle, $content)) {
            return false;
        }
        return fclose($handle);
    }

}
