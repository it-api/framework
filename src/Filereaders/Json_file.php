<?php

namespace Barebone\Filereaders;

class Json_file{
	
	// filename with configuration
	public string $filename = '';
	
	// configuration sections and values
	public array $values = [];
	
	function load_file(){
	    
	    if(file_exists($this->filename)){
	        
		    $this->values = json_decode(file_get_contents( $this->filename ), true);
		    
	    } else {
	        
	        throw new \Barebone\Filesystem\Exception\FileNotFoundException(sprintf('The file: "%s" does noet exist.', $this->filename));
	        
	    }
	}
	
	function getValue(string $section , string $key){
	    
	    // if section doesn't exist then return false
		if (!isset($this->values[$section])) {
            return false;
        }
        // if key doesn't exist in the section then return false
        if (!isset($this->values[$section][$key])) {
            return false;
        }
        // if value is bool then convert it to string booleans 'true' | 'false'
        if(is_bool($this->values[$section][$key])){
            if($this->values[$section][$key] === false){
                return 'false';
            } elseif($this->values[$section][$key] === true){
                return 'true';
            }
        }
        // any other case return the value of the section > key
		return $this->values[$section][$key];
	}
	
	function setValue(string $section , string $key, mixed $value): void{
		$this->values[$section][$key] = $value;
		$this->save();
	}
	
	function save(){
	    
	    if($this->filename === ''){
	        
	        throw new \Barebone\Exception('Filename not set.');
	        
	    } else {
	        
	        file_put_contents($this->filename, json_encode($this->values));
	        
	    }
	}
}
