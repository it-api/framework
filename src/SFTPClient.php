<?php

/*
  SFTPClient.php
  UTF-8
  31-okt-2018 11:17:56
  website-backend
  Creator Frank
 */

namespace Barebone;

/**
 * Description of SFTPClient
 *
 * @author Frank
 */
class SFTPClient extends \phpseclib3\Net\SFTP {
	/**
	 * 
	 * @param string $host
	 * @param int $port
	 * @param int $timeout
	 */
	public function __construct($host, $port = 22, $timeout = 10) {
		parent::__construct($host, $port, $timeout);
	}

}
