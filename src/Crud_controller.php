<?php

namespace Barebone;

class Crud_controller extends Controller{
	
	private $db_conf;
	private string $tablename = '';
	private string $primarykey = '';
	function __construct(){
		
		
		if(!isset( $this->tablename )){
			throw new \Exception('No tablename set. Create a propertie $tablename in your class!');
		}
		
		if(!isset( $this->primarykey )){
			throw new \Exception('No primary set. Create a propertie $primarykey in your class!');
		}
		
		parent::__construct();

	}
	
	function create( $exclude = array() ){
		
		// make sure exclude is an array
		if(!is_array($exclude)) $exclude = array();
		
		// get the columns of this table
		$columns = $this->db->get_columns( $this->tablename, $this->db_conf["database"] );
		
		// create a form
		$attributes = array();
		$attributes['method'] = 'post';
		$form = new Form($attributes);
		
		if($form->is_send()){
			
			if($form->is_valid()){
				
				$this->view->flash('Formulier is opgeslagen.', 'success');
				
				$data = array();
				foreach ($columns as $column){
					if($column->Type == 'tinyint(1)'){
						$data[$column->Field] = isset($_POST[$column->Field]);
					}else{
						$data[$column->Field] = $_POST[$column->Field];
					}
				}
				//print_r($data);
				$this->db->set($this->tablename, $data);
				
			}else{
			
			    $this->view->flash('The security token is invalid.', 'error');	
			}
			
		}else{
		
			// go over each column in the table
			foreach($columns as $column){
					
				// skip creation of the primary key
				if($column->Key != 'PRI' && !in_array($column->Field, $exclude)){		
					
					if($column->Type == 'text' || $column->Type == 'longtext' || $column->Type == 'mediumtext'){
						
						// if column type is text create a textarea
						$form->addTextarea($column->Field, $column->Field);
			
						
					}elseif($column->Type == 'tinyint(1)'){
						
						// create checkbox
						$form->addCheckbox($column->Field, $column->Field, 1);
						
					}else{
					    
						// create an input field
						$form->addInput('text', $column->Field, $column->Field);
					}
					
				}
					
			}
			
			// secure the form
			$form->secure_form();
			
			// add save button
			$form->addButton('submit', 'Save');
		}
	
		$this->view->form = $form;
	}
	
	function read(){
		
		$grid = new JQGrid5Model( $this );
		$grid->table = $this->tablename;

		return $grid;
	}
	
	function edit( $id, $exclude = array() ){

		// make sure exclude is an array
		if(!is_array($exclude)) $exclude = array();
		
		// get the columns of this table
		$columns = $this->db->get_columns( $this->tablename, $this->db_conf["database"] );
		
		// create a form
		$attributes = array();
		$attributes['method'] = 'post';
		$form = new Form($attributes);
		
		if($form->is_send()){
			
			if($form->is_valid()){
				
				$this->view->flash('Formulier is opgeslagen.', 'success');
				
				$data = array();
				foreach ($columns as $column){
					if($column->Type == 'tinyint(1)'){
						$data[$column->Field] = ( isset($_POST[$column->Field]) ? 1 : 0 );
					}else{
						$data[$column->Field] = $_POST[$column->Field];
					}
				}
				print_r($data);
				$this->db->set($this->tablename, $data);
				
			}else{
			
			    $this->view->flash('The security token is invalid.', 'error');	
			}
			
		}else{
				
		    $rows = $this->db->get($this->tablename, array($this->primarykey => $id));
		    $row = $rows[0];
		
			// go over each column in the table
			foreach($columns as $column){
					
				// skip creation of the primary key
				if($column->Key != 'PRI' && !in_array($column->Field, $exclude)){		
					
					if($column->Type == 'text' || $column->Type == 'longtext' || $column->Type == 'mediumtext'){
						
						// if column type is text create a textarea
						$attr['value'] = htmlentities($row->{$column->Field});
						//debug($attr);
						$form->addTextarea($column->Field, $column->Field, $attr);
			
						
					} elseif($column->Type == 'tinyint(1)'){
						
						$row->{$column->Field} == 1 ? $attr['checked'] = 'checked' : '';
						$form->addCheckbox($column->Field, $column->Field, 1);
						
					} elseif($column->Type == 'date'){
						
						//$row->{$column->Field} == 1 ? $attr['checked'] = 'checked' : '';
						$attr['value'] = htmlentities($row->{$column->Field});
						$form->addInput('date', $column->Field, $column->Field, $attr);
						
					} else{
						// if column type is text create a textarea
						$attr['value'] = $row->{$column->Field};
						$form->addInput('text', $column->Field, $column->Field, $attr);
					}
					
				}
				if($column->Key == 'PRI'){
				    $form->addHidden($this->primarykey, $row->{$column->Field} );
				}
					
			}
			
			// secure the form
			$form->secure_form();
			
			// add save button
			$form->addButton('submit', 'Save');
		}
	
		$this->view->form = $form;
	}
	
	function delete($id){
	    if(is_numeric($id) && $id > 0){
		    return $this->db->removerow($this->tablename, $id);
	    } else {
	        return false;
	    }
	}
	
}
