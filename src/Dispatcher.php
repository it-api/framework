<?php

namespace Barebone;

use Barebone\Dispatcher\DispatcherException;

use Barebone\ActionTypes\Action;
use Barebone\ActionTypes\AJAX;
use Barebone\ActionTypes\JSON;
use Barebone\ActionTypes\JQUERY;
use Barebone\ActionTypes\XML;

/**
 * The Dispatcher will dispatch the requested route to the corresponding controller and action
 * @author Frank
 *
 */
class Dispatcher {

    public static $dispatch;
    public static $applications_path;

    public static function getInstance() {
        if (is_null(self::$dispatch)) {
            self::$dispatch = new self();
        }
        return self::$dispatch;
    }

    function __construct() {
        // no constructor
    }

    function __clone() {
        // no clones
    }

    static function dispatch() {

        /**
         * @var $config Config 
         * get the configuration instance
         */
        $config = Config::getInstance();

        /** @var $router Router 
         * get the router instance
         */
        $router = Router::getInstance();
        /*
        // if there is a predefined route in the config/routes.json
        if ($router->defined_routes->routeExists($router->route->getPath())) {
            // redirect to the predefined route
            self::reroute($router->defined_routes->getDispatchRoute($router->route->getPath()));
            //debug($router->defined_routes->getDispatchRoute($router->route->getPath()));
            // stop the dispatcher
            return;
        }
        */
        // set applications folder
        self::$applications_path = FULL_BAREBONE_PATH . $config->getValue('application', 'applications_path');
        // find the application
        $application = self::$applications_path . $router->application . '/';
        if ($config->getValue('application', 'debug') === 'true') {
            if (!is_dir($application)) {
                throw new DispatcherException(sprintf('The application: "%s" does not exist.', $router->application));
            }
        }

        // find the controllers directory of this application
        $controllers = $application . 'controllers/';
        // find the controller
        $filename = realpath($controllers . $router->controller . 'Controller.php');

        if (file_exists($filename)) {

            // include the controller
            require_once($filename);
            // the classname of the controller, we add the Controller part so no need for that
            $classname = (string) $router->controller . 'Controller';
            // the action in the controller in order of priority
            // if action canrun in the controller, it will be executed
            $action = new Action($router->action);
            $ajax   = new AJAX($router->action);
            $json   = new JSON($router->action);
            $jquery = new JQUERY($router->action);
            $xml    = new XML($router->action);
            // the rest of the uri in an array as params
            $params = (array) $router->params;
            // does the Action method exists in the controller
            if ($action->canrun($classname)) {

                $action->run($classname, $params);
            } elseif ($jquery->canrun($classname)) {

                /* AJAX check */
                if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {

                    $jquery->run($classname, $params);
                } else {

                    /* not ajax, do more.... */
                    die('Not ajax call. This endpoint is only for jquery ajax calls.');
                }
            } elseif ($ajax->canrun($classname)) {

                /* AJAX check  */
                if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {
                    $ajax->run($classname, $params);
                } else {

                    /* not ajax, do more.... */
                    die('Not ajax call. This endpoint is only for ajax calls.');
                }
            } elseif ($json->canrun($classname)) {

                $json->run($classname, $params);
            } elseif ($xml->canrun($classname)) {

                $xml->run($classname, $params);
            } elseif ($config->getValue('application', 'debug') === 'true') {

                throw new DispatcherException(sprintf('The %s does not exist is the class: %s in the application %s', $action, $classname, $router->application));
            } elseif ($config->getValue('application', 'production') === 'true') {

                header('HTTP/1.0 404 Not Found', 404);
                self::reroute('frontend/errors/index/404');
            }
        // file does not exist. Depending on configuration show error or reroute to 404
        } elseif ($router->defined_routes->routeExists($router->route->getPath())) {
            // redirect to the predefined route
            self::reroute($router->defined_routes->getDispatchRoute($router->route->getPath()));
            // stop the dispatcher
            return;
        } elseif ($config->getValue('application', 'debug') === 'true') {

            throw new DispatcherException(sprintf('The %sController does not exist in the application folder %s.', $router->controller, $router->application));
        } elseif ($config->getValue('application', 'production') === 'true') {

            header('HTTP/1.0 404 Not Found', 404);
            self::reroute('frontend/errors/index/404/');
            exit();
        }
    }

    static function reroute($route = 'frontend/index/index/') {

        // create an array of the route
        $route = explode('/', $route);
        // get the configuration instance
        $config = Config::getInstance();
        // get the router instance
        $router = Router::getInstance();
        // first item in array is the application
        $router->application = $route[0] ?? $config->getValue('application', 'default_application');
        // second item in array is the controller
        $router->controller = $route[1] ?? $config->getValue('application', 'default_controller');
        // third item in array is the action in the controller
        $router->action = $route[2] ?? $config->getValue('application', 'default_action');
        //the rest is parameters that are passed to the action
        for ($x = 3; $x < count($route); $x++) {
            $router->params[] = $route[$x];
        }
        // find applications folder
        self::$applications_path = FULL_BAREBONE_PATH . $config->getValue('application', 'applications_path');
        // find the application
        $application = self::$applications_path . $router->application . '/';
        // find the controllers directory of this application
        $controllers = $application . 'controllers/';
        // find the controller
        $filename = realpath($controllers . $router->controller . 'Controller.php');
        // include the controller
        require_once($filename);
        // the classname of the controller, we add the Controller part so no need for that
        $classname = $router->controller . 'Controller';
        // the action in the controller
        $action = new Action($router->action);
        // the rest of the uri in an array as params
        $params = $router->params;
        // does the action exists in the controller
        if ($action->canrun($classname)) {

            $action->run($classname, $params);
        } elseif ($config->getValue('application', 'debug') === 'true') {

            throw new \BadMethodCallException(sprintf('The %s does not exist is the class: %s in the application %s', $action, $classname, $router->application));
        }
    }

    function dispatch_cli($uri = 'frontend/index/index/') {

        // create an array of the route
        $route = explode('/', $uri);
        // get the configuration instance
        $config = Config::getInstance();
        // get the router instance
        $router = Router::getInstance();
        // first item in array is the application
        $router->application = $route[0] ?? $config->getValue('application', 'default_application');
        // second item in array is the controller
        $router->controller = $route[1] ?? $config->getValue('application', 'default_controller');
        // third item in array is the action in the controller
        $router->action = $route[2] ?? $config->getValue('application', 'default_action');
        //the rest is parameters that are passed to the action
        for ($x = 3; $x < count($route); $x++) {
            $router->params[] = $route[$x];
        }
        // find applications folder
        self::$applications_path = FULL_BAREBONE_PATH . $config->getValue('application', 'applications_path');
        // find the application
        $application = self::$applications_path . $router->application . '/';
        // find the controllers directory of this application
        $controllers = $application . 'controllers/';
        // find the controller
        $filename = $controllers . $router->controller . 'Controller.php';
        // include the controller
        require_once($filename);
        // the classname of the controller, we add the Controller part so no need for that
        $classname = $router->controller . 'Controller';
        // the action in the controller
        $action = new Action($router->action);
        // the rest of the uri in an array as params
        $params = $router->params;
        // does the action exists in the controller
        if ($action->canrun($classname)) {

            $action->run($classname, $params);
        } elseif ($config->getValue('application', 'debug') === 'true') {

            throw new \BadMethodCallException(sprintf('The %s does not exist is the class: %s in the application %s', $action, $classname, $router->application));
        }
    }
}
