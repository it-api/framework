<?php

namespace Barebone;

/**
 * Description of CSSMin
 * MYFW 
 * UTF-8
 *
 * @author Frank
 * @filesource CSSMin.php
 * @since 8-jan-2017 13:06:51
 * @link https://www.if-not-true-then-false.com/2009/css-compression-with-own-php-class-vs-csstidy/
 * @example 
 *     // Require once CSS min php file
    require_once("CSSMin.php"); // Fix path and Filename if you use some other

    // Create new instance of CSSMin class
    $cssmin = new CSSMin();

    // Add multiple files array (just example)
    $files = array("/path/to/css/your_first.css", "../css/your_second.css");
    $cssmin->addFiles($files);

    // Add single file (just example)
    $cssmin->addFile("your_third.css");

    // Add second single file (just example)
    $cssmin->addFile("your_fourth.css");

    // Set original CSS from all files
    $cssmin->setOriginalCSS();

    // Compress CSS
    $cssmin->compressCSS();

    // Print original and combined css with header
    // $this->printOriginalCSS(true);

    // Print compressed and combined css without header
    $cssmin->printCompressedCSS();

    // Exit
    exit;
 * 
 */
class CSSMin {

    private string $original_css = '';
    private string $compressed_css = '';
    private array $files = [];

    /* Constructor for CSSMin class */

    public function __construct() {
        $this->original_css = "";
        $this->compressed_css = "";
        $this->files = array();
    }

    /* Add file as string (path and filename) */

    public function addFile(string $file = '') {
        if ($file !== "" && substr(strrchr($file, '.'), 1) === "css" && is_file($file)) {
            $this->files[] = $file;
            return true;
        } else {
            return false;
        }
    }

    /* Add multiple files array */

    public function addFiles(array $files = []) {
        if (is_array($files)) {
            $ok = true;
            foreach ($files as $file) {
                $ok = $this->addFile($file);
            }
            return $ok;
        } else {
            return false;
        }
    }

    /* Print original css files concatenated */

    public function printOriginalCSS($header = false) {
        if ($header) {
            header('Content-type: text/css');
        }
        echo $this->original_css;
    }

    /* Print compressed css files concatenated */

    public function printCompressedCSS($header = false) {
        if ($header) {
            header('Content-type: text/css');
        }
        echo $this->compressed_css;
    }

    /* Sets original css loop thru all added files */

    public function setOriginalCSS() {
        foreach ($this->files as $file) {
            $fh = fopen($file, 'r');
            $this->original_css .= fread($fh, filesize($file));
            fclose($fh);
        }
    }

    /* Make simple compression with regexp. */

    public function compressCSS() {
        $patterns = array();
        $replacements = array();

        /* remove multiline comments */
        $patterns[] = '/\/\*.*?\*\//s';
        $replacements[] = '';

        /* remove tabs, spaces, newlines, etc. */
        $patterns[] = '/\r\n|\r|\n|\t|\s\s+/';
        $replacements[] = '';

        /* remove whitespace on both sides of colons : */
        $patterns[] = '/\s?\:\s?/';
        $replacements[] = ':';

        /* remove whitespace on both sides of curly brackets {} */
        $patterns[] = '/\s?\{\s?/';
        $replacements[] = '{';
        $patterns[] = '/\s?\}\s?/';
        $replacements[] = '}';

        /* remove whitespace on both sides of commas , */
        $patterns[] = '/\s?\,\s?/';
        $replacements[] = ',';

        /* compress */
        $this->compressed_css = preg_replace($patterns, $replacements, $this->original_css);
    }
}
