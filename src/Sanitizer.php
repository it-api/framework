<?php

/*
  Creator Frank
 */

namespace Barebone;

/**
 * Description of Sanitizer
 *
 * @author Frank
 */
class Sanitizer {
	
	/**
	 * Sanitize an array of data with an array of sanitizers
	 * @var array $data Data array to be sanitized.
	 * @var string $field The field that needs to get sanitized.
	 * @var array $sanitizers The sanitizers that apply to the field.
	 * @return array
	 */
	private function sanitizeField(array $data, string $field, string $sanitizers){
		if(stristr($sanitizers, '|')){
		    echo 'found pipe |<br>';
			$callbacks = explode('|', $sanitizers);
			inspect($callbacks);
			foreach ($callbacks as $callback){
				if(function_exists($callback)){
				    echo "function {$callback} exists<br>";
					$data[$field] = call_user_func($callback, $data[$field]);
				}
			}
		}elseif(function_exists($sanitizers)){
			$data[$field] = call_user_func($sanitizers, $data[$field]);
		}
		return $data;
	}
	
	/**
	 * Sanitize a value
	 * @var string $rules The sanitizer rules that apply to the data.
	 * @var mixed $data Data array to be sanitized.
	 * @return string
	 */
	public static function sanitizeValue(string $rules, mixed $data){
		if(stristr($rules, '|')){
			$callbacks = explode('|', $rules);
			foreach ($callbacks as $callback){
				if(function_exists($callback)){
					$data = call_user_func($callback, $data);
				}
			}
		}elseif(function_exists($rules)){
			$data = call_user_func($rules, $data);
		}
		return $data;
	}
	
	/**
	 * @var array $rules The sanitizer rules that apply to the data.
	 * @var array $data Data array to be sanitized.
	 * @example
	 * // Construct rules array.
        $rules = [
            'name'      => 'trim|strtolower|ucfirst',
            'email'     => 'trim|strtolower'
        ];
        
        // Data array to be sanitized.
        $data = [
            'name' => ' FRANK ',
            'email' => ' me@DAYLEREES.com '
        ];
        // $result = (new Barebone\Sanitizer)->sanitize($rules, $data);
        // Result
        Array
        (
            [name] => Frank (trim, strtolower, ucfirst)
            [email] => me@daylerees.com (trim, strtolower)
        )
	 */
	public function sanitize(array $rules, array $data): array{

		// loop over the rules
		foreach ($rules as $field => $sanitizers){
		    // sanitize the field and store it in the result array
		    $data = $this->sanitizeField($data, $field, $sanitizers);
		}

		return $data;
	}
}
